#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 19 11:49:59 2018

@author: sosarica
"""

import ROOT
import optparse
#from rootpy.plotting import Hist, HistStack, Legend, Canvas
#import rootpy.plotting.root2matplotlib as rplt

#By me
import lumi_compare as lumi
#import math

ROOT.gROOT.SetBatch(ROOT.kTRUE)

excludedFills=[]
nbins=40
min_val=0.5
max_val=1.5

p = optparse.OptionParser()
usage = "usage: %prog [options] arg1 arg2 ..."
p.add_option("-o", "--outdir", type="string",help="Path to output Dir", dest="outDir", default=".")
p.add_option("-i", "--indir", type="string",help="Path to input Dir", dest="inDir", default=".")

(options, args) = p.parse_args()

detector1label=args[0]
detector2label=args[1]
detector3label=args[2]
    
filename1=detector1label + ".csv"
filename2=detector2label + ".csv"
filename3=detector3label + ".csv"

file_path1=options.inDir +"/"+ filename1
file_path2=options.inDir +"/"+ filename2
file_path3=options.inDir +"/"+ filename3

print ("input file 1: " + file_path1)
print ("input file 2: " + file_path2)
print ("input file 3: " + file_path3)

    
#open files
try:
    file1=open(file_path1)
    file2=open(file_path2)
    file3=open(file_path3)
except:
    print ("input files cant't be opened!!")
    quit()
        
labelRatio13=detector1label+"/"+detector3label
labelRatio23=detector2label+"/"+detector3label
labelRatio12=detector1label+"/"+detector2label

labelratios=[]

labelratios.append(labelRatio13)
labelratios.append(labelRatio23)
labelratios.append(labelRatio12)

    
#info

#Create dicts from csv file
dict1=lumi.myDictFromVdmCSVFile(file1)
dict2=lumi.myDictFromVdmCSVFile(file2)
dict3=lumi.myDictFromVdmCSVFile(file3)

file1.close()
file2.close()

#Select only the common lumi sections for both detectors
overlapKeys13=list(set(dict1).intersection(dict3))
overlapKeys23=list(set(dict2).intersection(dict3))
overlapKeys=list(set(overlapKeys13).intersection(overlapKeys23))


#Sorting to evoid disorder in plots
overlapKeys.sort()


plotVsTime1=ROOT.TGraph()
plotVsTime2=ROOT.TGraph()
plotVsTime3=ROOT.TGraph()
plotsVsTime=[]

ratioHist13=ROOT.TH1F(labelRatio13+"ratioHist",";ratio "+labelRatio13+";",nbins,min_val,max_val)
ratioHist23=ROOT.TH1F(labelRatio23+"ratioHist",";ratio "+labelRatio23+";",nbins,min_val,max_val)
ratioHist12=ROOT.TH1F(labelRatio12+"ratioHist",";ratio "+labelRatio12+";",nbins,min_val,max_val)
ratioHists=[]

##Filling plots with data
iBin=0
iBinx=0
for key in overlapKeys:
    plotVsTime1.SetPoint(iBinx,dict1[key][0],dict1[key][1])
    plotVsTime2.SetPoint(iBinx,dict2[key][0],dict2[key][1])
    plotVsTime3.SetPoint(iBinx,dict3[key][0],dict3[key][1])
    iBinx+=1
    try:
        if dict1[key][4]=='STABLE BEAMS':
            div13=dict1[key][1]/dict3[key][1]
            div23=dict2[key][1]/dict3[key][1]
            div12=dict1[key][1]/dict2[key][1]
            
            ratioHist13.Fill(div13)
            ratioHist23.Fill(div23)
            ratioHist12.Fill(div12)
            
            iBin+=1
    except:
        continue

##Filling multiplots
plotsVsTime.append(plotVsTime1)
plotsVsTime.append(plotVsTime2)
plotsVsTime.append(plotVsTime3)
ratioHists.append(ratioHist13)
ratioHists.append(ratioHist23)
ratioHists.append(ratioHist12)

##Saving plots
solocan=ROOT.TCanvas("solocan","solocan",900,300)
solocan_square=ROOT.TCanvas()

hsVsTime,legVsTime=lumi.plot_compare_multiple_detectors(plotsVsTime,labelratios,"timestamp","Instantaneus luminosity detector ratio")
hsVsTime.GetYaxis().SetRangeUser(0., 4.)
hsRatioHist,legRatioHist=lumi.histo_ratio_multiple_detectors(ratioHists,labelratios)
#hsRatioHist.GetXaxis().SetRangeUser(min_val, max_val)


lumi.saveintoCanvas_and_toFile_withLeg(solocan,hsVsTime,legVsTime,"AL",
                                   options.outDir+ "/" +detector1label+"_"+detector2label+"_" +detector3label+"_plotsVsTime.pdf")
lumi.saveintoCanvas_and_toFile_withLeg(solocan_square,hsRatioHist,legRatioHist,"nostack",
                                   options.outDir+ "/" +detector1label+"_"+detector2label+"_" +detector3label+"_plotsHistos.pdf")


