#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  1 11:17:13 2019

@author: rsosaric
"""

### Configuration File for compare.py
colors=[46, 8, 9, 42, 38, 30, 2, 3, 4, 5, 6, 7, 8,11,28]
markers=[20,21,22,23,47,33,39,29]
#plot_mode="Work in progress"
plot_mode="Preliminary"
plotformat=".pdf"



###### Configurations parameters #########################################################################
xLS=23.31
known_luminometers=["hfoc","hfet","pcc","bcm1f","plt","ram"]

###### Change labeling of detectors ################################
change_label={}
change_label["PXL"]="PCC"
change_label["HFOC"]="HFOC"
change_label["DT"]="DT"
change_label["RAMSES"]="RAM"
change_label["PLTZERO"]="PLT"

  

#################### Stability settings ############################
ymin=0.94
ymax=1.06
#ymin=0.80
#ymax=1.20
#ymin=0.85
#ymax=1.15

### number of ls taking for computing each ratio ##
nls=50
nls_year={}
nls_year[2015,5]=15
nls_year[2015,13]=10
nls_year[2016,13]=50
nls_year[2018,13]=50

## For nls and binning tests
nbins_list=[30,80,200,500]
nls_list=[1,10,15,50,100]


minratio=0.2
maxratio=1.8

### histogram limit values, and binning
hist_xmin =ymin
hist_xmax =ymax
hist_nbins=200
nxbins=150
nybins=60

### Number of bins for ~ equal luminosity binning for ratios method
nLumBins=150#use for Full 2016 (checked)
nLumRatioBins=200#use for Full 2016 (checked)
lumiPjxBins=50

### Fix this to be automatic!!!!!!!!!!!!!!!!!!!!!!!!!
#time_xmin=1461363104 ## only for 2016
#time_xmax=1477577584 ## only for 2016
### For plots with time ##To do: make plots with calendar x-time axis
time_xmin=0
time_xmax=0
default_energy=0
   
### Positions ids for stored data in Dicts (it is suggested to take always this order to mantain compatibility. If needed just add at the end of already taken values)
idTime=2
idRun=1
idFill=0
# Position of the n-(average range), it can be number of Nls, or NRun, etc. Depends of the Dict.
idN=3
idL1=1
idL2=2
idErrinEqLumDIct=3

### Bad fills selction sensitivty
##multiplicity of the sigma interval around mean value for acceptance. Higher means more strict.
ratioSensivty=1.3
##multiplicity of the total luminosity porcent of each pint for acceptance. Higher means less strict.
lumiSensivty=0.05

######***********############*************############************* Settings per Year and energy ######***********############*************############*************
### Year and energy data ranges. It is used to automatically assign the year and energy to the plots and customize values for each year.
rangeFillYearly={}
## Always put 5TeV first!!! (less number of fills)
rangeFillYearly[2015,5]=[4634,4647]

rangeFillYearly[2015,13]=[3829,4720]
rangeFillYearly[2016,13]=[4856,5575]
rangeFillYearly[2017,13]=[5718,6417]
rangeFillYearly[2018,13]=[6570,7407]

energyLabel_list={}
energyLabel_list[0]="nanTeV"
energyLabel_list[5]="5TeV"
energyLabel_list[13]="13TeV"


######***********############*************############************* Bad Fills exclusion per detector ######***********############*************############*************
detectors_excludedFills={}
detectors_excludedFills_for_linearity={}

#detectors_excludedFills["ram"]=[4528,4432,4560]
                                #2016                          #2018
detectors_excludedFills["pcc"]=[4965,4976,4979,4980,5038,5151, 7218] #6616,6617,6618,6620,6621,6624,6640,6645,6646,6650,6659,6901,6904,6929,6946,7008,7124,7127,7137,7139,7144,
                                #2016       #2018
detectors_excludedFills["hfoc"]=[5038,5043, 6919]
                               #2015 #2016                                               #2018
#detectors_excludedFills["dt"]=[4381, 4947,5045,5048,5052,5069,5071,5072,5073,5076,5096,  6929,6939,7042,7087,7091,7124,7256,7315]
                                #2018
#detectors_excludedFills["plt"]=[6773,6774,6778]
#detectors_excludedFills["plt"]=[6700, 6738, 6773, 6774, 6776, 6778, 7005, 7006, 7008, 7013, 7031, 7087, 7097, 7117, 7122, 7137, 7217, 7252, 7266, 7315, 7321]

#2016
#excludedFills=[4965,5038,5043,5045,5048,5052,5056,4979,5151,5331,5433]
#excludedFills=[5017]

#For linearity (dt-pcc)
#excludedFills=[4965,4980,5017,5026,5043,5331,5038,5043,5045,5048,5052,5056,4979,5151,5331,5433]

#For linearity (hfoc-pcc)
#excludedFills=[4935,4930,4942,4965,4947,4980,5026,5331,4965,5038,5043,5045,5048,5052,5056,4979,5151,5331,5433]

#2015(pcc-dt)
#excludedFills=[4528]

#Re-normalization for best/second
norm_values={}
#2016 normalized to pcc: det/pcc=r --->> det = det/r
norm_values[2016,"PXL"]=1.0000
norm_values[2016,"HFOC"]=0.9964
norm_values[2016,"DT"]=0.9944
norm_values[2016,"RAMSES"]=0.9949
norm_values[2016,"PLTZERO"]=0.9957



######***********############*************############************* LINEARITY ######***********############*************############*************
#          year, detector  sbil  min  max
flag_useSBILrange=False
sbil_range={}
sbil_range[2016,"hfoc/pcc"]=[2.0,6.0]
sbil_range[2016]=[1.7,5.2]
sbil_range[2015]=[1.2,2.35]
sbil_min=0.0
sbil_max=0.0

slope_plotting_range={}
slope_plotting_range[2015,5]=[-0.1,0.1]
slope_plotting_range[2015,13]=[-0.04,0.04]
slope_plotting_range[2016,13]=[-0.01,0.01]
slope_plotting_range[2018,13]=[-0.01,0.01]


## options for summary plot
flags_summary_byFillSlopesVsIntLumiAvg=True
flags_summary_lineInSlopeMeanHist_lw_err=True
flags_summary_lineInSlopeMean_errW=True
flags_summary_lineInSlopeMeanHist_err=True
flags_summary_lineInSlopeAll_res=True

#### Extra Plotting settings and fucntions containing customizable options #######
def getColorForDetectorPair(labelRatio):
    labelRatio.lower()
    color=colors[0]
    if (labelRatio=="hfoc/pcc" or labelRatio=="pcc/hfoc"):
        color=colors[8]
    elif (labelRatio=="dt/pcc" or labelRatio=="pcc/dt"):
        color=colors[14]
    elif (labelRatio=="ram/pcc" or labelRatio=="pcc/ram"):
        color=colors[7]
    elif (labelRatio=="plt/pcc" or labelRatio=="pcc/plt"):
        color=colors[4]
    elif (labelRatio=="bcm1f/pcc" or labelRatio=="pcc/bcm1f"):
        color=colors[11]
    elif (labelRatio=="hfet/pcc" or labelRatio=="pcc/hfet"):
        color=colors[10]
        
        
    elif (labelRatio=="dt/hfoc" or labelRatio=="hfoc/dt"):
        color=colors[12]
    elif (labelRatio=="ram/hfoc" or labelRatio=="hfoc/ram"):
        color=colors[14]
    elif (labelRatio=="plt/hfoc" or labelRatio=="hfoc/plt"):
        color=colors[3]
    elif (labelRatio=="bcm1f/hfoc" or labelRatio=="hfoc/bcm1f"):
        color=colors[1]
    elif (labelRatio=="hfet/hfoc" or labelRatio=="hfoc/hfet"):
        color=colors[11]
    elif (labelRatio=="best/second" or labelRatio=="second/best"):
        color=colors[12]
    
        
    return color
    
    
        
        
