#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 29 12:11:53 2018

@author: sosarica
"""

import lumi_compare as lumi
import ROOT
import statistics
import optparse
import settings as setts

ROOT.gROOT.SetBatch(ROOT.kTRUE)
lumi.setRESRStyle()

##Change options here
# year=2015
p = optparse.OptionParser()
(options, args) = p.parse_args()
if len(args) < 1:
    print("Specify at least the year")
    exit()
year = int(args[0])
print("***** YEAR: " + str(year) + " *****")
energy = 13
if year == 2016:
    # detcs=["hfoc-pcc","dt-pcc","ram-pcc","plt-pcc"]
    detcs = ["hfoc-pcc", "ram-pcc", "dt-pcc"]
    # detcs=["hfoc-pcc","ram-pcc"]
    summary_min = -0.006
    summary_max = 0.006
elif year == 2018:
    # detcs=["hfoc-pcc","dt-pcc","ram-pcc","plt-pcc"]
    detcs = ["pcc-hfoc", "dt-hfoc", "ram-hfoc"]
    summary_min = -0.006
    summary_max = 0.006
elif year == 2015:
    detcs = ["dt-pcc", "ram-pcc"]
    summary_min = -0.03
    summary_max = 0.03
else:
    print("No configuration found for year: ", year)
    exit()

middle_PATH = "/goldenJson/Plots/"

flag_AvgsPoints = True
flag_meanErrW = True
flag_mean = False
flag_meanLW = False
flag_combined = False

markerColors = [9, 2, 3, 12]

fillColorSlopeAll = [ROOT.kBlue, ROOT.kRed, ROOT.kGreen, 15]
fillColorHist = [ROOT.kViolet, ROOT.kPink, ROOT.kSpring, 16]
fillStyleSlopeAll = 3004
fillStyleHist = 3005

outputName = []
rootFiles = []
meanSlopes = []
errSlopes = []
savedLumis = []
byFillSlopesVsIntLumiAvgs = []


def getLinearity_infoFromFile(filePath):
    file = open(filePath)
    lines = file.readlines()

    for line in lines:
        try:
            key = line.split(":")[0]
            val = line.split(":")[1]
            if key == "meanSlope lw err hist":
                meanSlope = float(val)
            elif key == "errSlope lw err hist":
                errSlope = float(val)
            elif key == "savedTotLumi":
                savedLumi = float(val)
        except:
            if line != lines[0]:
                print("Problem reading line:" + line)
            pass

    return meanSlope, errSlope, savedLumi


markerColorsId = 0
# legSummary=ROOT.TLegend(.919,.77,.55,.9485)
legSummary = ROOT.TLegend(.89, .74, .52, .9185)
plot_summary = ROOT.TMultiGraph()

for det in detcs:
    temp_outputName = str(year) + middle_PATH + det + "/linearity"
    detlabel1 = det.split("-")[0]
    detlabel2 = det.split("-")[1]
    newlabel = detlabel1 + "/" + detlabel2

    f = ROOT.TFile(temp_outputName + "/" + det + "_byFillSlopesVsIntLumiAvg.root")
    byFillSlopesVsIntLumiAvg = f.Get("byFillSlopesVsIntLumiAvg")
    byFillSlopesVsIntLumiAvg.SetMarkerColor(setts.getColorForDetectorPair(newlabel))
    byFillSlopesVsIntLumiAvg.SetLineColor(setts.getColorForDetectorPair(newlabel))
    byFillSlopesVsIntLumiAvgs.append(byFillSlopesVsIntLumiAvg)

    meanSlope, errSlope, savedLumi = getLinearity_infoFromFile(
        temp_outputName + "/" + det + "_linearityUncertainty.txt")
    meanSlopes.append(meanSlope)
    errSlopes.append(errSlope)
    savedLumis.append(savedLumi)

    markerColorsId += 1

lNpts = 50
savedTotLumi = max(savedLumis)
deltaL = savedTotLumi / lNpts

# print(meanSlopes,errSlopes,meanSlopes_lw,errSlopes_lw,meanSlopes_errw,errSlopes_errw)

det_id = 0
for det in detcs:
    detlabel1 = det.split("-")[0]
    detlabel2 = det.split("-")[1]
    newlabel = detlabel1 + "/" + detlabel2

    lineInSlopeMeanHist = ROOT.TGraphErrors()

    iBin = 0
    for l in range(0, lNpts + 1):
        lumil = l * deltaL
        lineInSlopeMeanHist.SetPoint(iBin, lumil * 23.31 / 1000000000., meanSlopes[det_id])
        lineInSlopeMeanHist.SetPointError(iBin, 0.0, errSlopes[det_id])

        iBin += 1

    lineInSlopeMeanHist.SetFillColor(setts.getColorForDetectorPair(newlabel))
    lineInSlopeMeanHist.SetLineColor(setts.getColorForDetectorPair(newlabel))
    lineInSlopeMeanHist.SetLineStyle(9)
    lineInSlopeMeanHist.SetFillStyle(fillStyleSlopeAll)

    #    lineInSlopeAll_res_list.append(lineInSlopeAll_res)
    #    lineInSlopeMeanHist_err_list.append(lineInSlopeMeanHist_err)
    #
    # for det in

    if flag_AvgsPoints:
        legSummary.AddEntry(byFillSlopesVsIntLumiAvgs[det_id], det.replace("-", "/"), "pe")
        plot_summary.Add(byFillSlopesVsIntLumiAvgs[det_id], "AP")
    legSummary.AddEntry(lineInSlopeMeanHist, "mean #pm err (" + det.replace("-", "/") + ")", "lf")
    plot_summary.Add(lineInSlopeMeanHist, "L 4")

    det_id += 1

# legSummary.SetFillColor(0)
# leg.SetNColumns(2)
legSummary.SetTextFont(42)
legSummary.SetTextSize(.03)
legSummary.SetBorderSize(0)
plot_summary.SetTitle("summary plot all; Integrated Luminosity [fb^{-1}];  slope [(hz/\mub)^{-1}]")

solocan_square = ROOT.TCanvas("sqcan2detcs", "sqcan2detcs", 600, 600)

lumi.saveintoCanvas_and_toFileWIP(solocan_square, plot_summary, "A",
                                  str(year) + "_plot_summary.pdf", "il_sq", year, energy,
                                  ymin=summary_min, ymax=summary_max, drawleg=True, leg=legSummary)
