#!/bin/bash

#--begin 271031 --end 284078

brilcalc lumi -i /afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions16/13TeV/Final/Cert_271036-284044_13TeV_PromptReco_Collisions16_JSON.txt --normtag=dt16v1pre6 -u 'hz/ub' -o CSVperDetector_withNormtags/dt.csv --output-style=csv --byls --tssec
