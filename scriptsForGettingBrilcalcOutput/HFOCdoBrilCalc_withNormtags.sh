#!/bin/bash

#--begin 271031 --end 284078
#cal=hfoc16v4pre1
cal=hfoc16v5

brilcalc lumi -i ../jsons/Cert_271036-284044_13TeV_PromptReco_Collisions16_JSON.txt --normtag=$cal -u 'hz/ub' -o CSVperDetector_withNormtags/$cal.csv --output-style=csv --byls --tssec
