#!/bin/bash

#--begin 271031 --end 284078

for nt in hfoc pcc bcm1f pltzero hfet
	do
    	brilcalc lumi -i ../jsons/json_DCSONLY.txt --normtag=../Normtags/normtag_${nt}.json -u 'hz/ub' -o CSVperDetector_withNormtags/${nt}.csv --output-style=csv --byls --tssec
  	done
