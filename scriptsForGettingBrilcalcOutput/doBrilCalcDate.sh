#!/bin/bash


for nt in hfoc16v6 pccLUM17001pre6 dt16v1pre6
	do
	echo 'Running '$nt		
	brilcalc lumi -b "STABLE BEAMS" --begin 4976 --end 4984 --normtag ${nt} -u 'hz/ub' -o csvFiles/${nt}cernTime.csv --output-style=csv --byls --cerntime
	done
