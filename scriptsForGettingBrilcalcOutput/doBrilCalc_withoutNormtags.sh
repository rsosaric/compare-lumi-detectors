#!/bin/bash

#--begin 271031 --end 284078

for nt in hfoc dt pxl bcm1f pltzero hfet
	do
    	brilcalc lumi -i ../jsons/json_DCSONLY.txt --type ${nt} -u 'hz/ub' -o CSVperDetector_withoutNormtags/${nt}.csv --output-style=csv --byls --tssec
  	done
