#!/bin/bash


for nt in hfoc16v6 pccLUM17001pre6 dt16v1pre6 pltzero16v4 bcm1f16v1
	do
	echo 'Running '$nt		
	brilcalc lumi --begin 4954 --end 4954 --normtag ${nt} -u 'hz/ub' -o csvFiles/vdm${nt}.csv --output-style=csv --byls --tssec
	done
