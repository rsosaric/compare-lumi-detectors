#!/bin/bash
rm -rf CSVperDetector_withNormtags/*
mkdir CSVperDetector_withNormtags/A-B/
mkdir CSVperDetector_withNormtags/C-D/

echo "Running physics A-B"
brilcalc lumi -i ../json/Cert_246908-255031_13TeV_PromptReco_Collisions15_50ns_JSON_v2.txt --normtag=../normtags/normtag_PHYSICS.json -u 'hz/ub' -o CSVperDetector_withNormtags/A-B/physics.csv --output-style=csv --byls --tssec
brilcalc lumi -i ../json/Cert_246908-255031_13TeV_PromptReco_Collisions15_50ns_JSON_v2.txt --normtag=../normtags/normtag_PHYSICScompare.json -u 'hz/ub' -o CSVperDetector_withNormtags/A-B/physics_compare.csv --output-style=csv --byls --tssec


echo "Running physics C-D"
brilcalc lumi -i ../json/Cert_246908-260627_13TeV_PromptReco_Collisions15_25ns_JSON_v2.txt --normtag=../normtags/normtag_PHYSICS.json -u 'hz/ub' -o CSVperDetector_withNormtags/C-D/physics.csv --output-style=csv --byls --tssec
brilcalc lumi -i ../json/Cert_246908-260627_13TeV_PromptReco_Collisions15_25ns_JSON_v2.txt --normtag=../normtags/normtag_PHYSICScompare.json -u 'hz/ub' -o CSVperDetector_withNormtags/C-D/physics_compare.csv --output-style=csv --byls --tssec

for nt in pccLUM15001v2 dtv3 pltzerov2CorV2

	do
	echo "Runing A-B:" $nt
    	brilcalc lumi -i ../json/Cert_246908-255031_13TeV_PromptReco_Collisions15_50ns_JSON_v2.txt --normtag=${nt} -u 'hz/ub' -o CSVperDetector_withNormtags/A-B/${nt}.csv --output-style=csv --byls --tssec
	echo "Runing C-D:" $nt
	brilcalc lumi -i ../json/Cert_246908-260627_13TeV_PromptReco_Collisions15_25ns_JSON_v2.txt --normtag=${nt} -u 'hz/ub' -o CSVperDetector_withNormtags/C-D/${nt}.csv --output-style=csv --byls --tssec
  	done
