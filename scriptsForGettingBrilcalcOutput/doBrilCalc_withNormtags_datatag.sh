#!/bin/bash

#--begin 271031 --end 284078

#for nt in pcc bcm1f pltzero hfet hfoc dt
for nt in pcc hfoc16v1 hfoc16v4pre1 hfoc16v5 hfoc16v6 dt
#for nt in hfoc16v6

	do
	echo "Runing:" $nt
    	brilcalc lumi --datatag online -i ../jsons/Cert_271036-284044_13TeV_PromptReco_Collisions16_JSON.txt --normtag=../Normtags/normtag_${nt}.json -u 'hz/ub' -o CSVperDetector_withNormtags/${nt}.csv --output-style=csv --byls --tssec
  	done
