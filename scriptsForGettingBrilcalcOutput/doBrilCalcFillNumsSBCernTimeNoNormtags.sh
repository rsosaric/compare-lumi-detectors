#!/bin/bash


for nt in hfoc16v6 pccLUM17001pre6
	do
	echo 'Running '$nt		
	brilcalc lumi -b "STABLE BEAMS" --begin 271031 --end 284078 --normtag ${nt} -u 'hz/ub' -o csvFiles/${nt}ct.csv --output-style=csv --byls --cerntime
	done


for nt in dt16v1pre6
	do
	echo 'Running '$nt		
	brilcalc lumi -b "STABLE BEAMS" --begin 273158 --end 284078 --normtag ${nt} -u 'hz/ub' -o csvFiles/${nt}ct.csv --output-style=csv --byls --cerntime
	done
