#!/bin/bash


for nt in hfoc16v6 pccLUM17001pre6 dt16v1pre6
	do
	echo 'Running '$nt		
	brilcalc lumi --begin 4954 --end 4985 --normtag ${nt} -u 'hz/ub' -o csvFiles/${nt}.csv --output-style=csv --byls --tssec
	done

