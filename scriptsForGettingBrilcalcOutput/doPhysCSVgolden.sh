#!/bin/bash

#--begin 271031 --end 284078
echo "Running physics..."
brilcalc lumi -i ../jsons/Cert_271036-284044_13TeV_PromptReco_Collisions16_JSON.txt --normtag=../Normtags/normtag_PHYSICSv2.json -u 'hz/ub' -o CSVperDetector_withNormtags/physics.csv --output-style=csv --byls --tssec
echo "Running physics compare..."
brilcalc lumi -i ../jsons/Cert_271036-284044_13TeV_PromptReco_Collisions16_JSON.txt --normtag=../Normtags/normtag_PHYSICSv2compare.json -u 'hz/ub' -o CSVperDetector_withNormtags/physics_compare.csv --output-style=csv --byls --tssec



 
