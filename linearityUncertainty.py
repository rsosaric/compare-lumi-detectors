#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov  6 10:00:56 2018

@author: sosarica
"""

import ROOT
import optparse
import os

ROOT.gROOT.SetBatch(ROOT.kTRUE)


p = optparse.OptionParser()
usage = "usage: %prog [options] arg1 arg2 ..."
p.add_option("-o", "--outdir", type="string",help="Path to output Dir", dest="outDir", default=".")
p.add_option("-i", "--indir", type="string",help="Path to input Dir", dest="inDir", default=".")

(options, args) = p.parse_args()

print ("******** 2 detector comprison choosed!! ******")

detector1label=args[0]
detector2label=args[1]

filename1=detector1label + ".csv"
filename2=detector2label + ".csv"
file_path1=options.inDir +"/"+ filename1
file_path2=options.inDir +"/"+ filename2
print ("input file 1: " + file_path1)
print ("input file 2: " + file_path2)

#open files
try:
    file1=open(file_path1)
    file2=open(file_path2)
except:
    print ("input files cant't be opened!!")
    quit()
    
## Getting detectors names from file names
print(detector1label)
print(detector2label)

##Create ouput files
options.outDir=options.outDir+"/" + detector1label + "-" + detector2label
try:
    os.mkdir(options.outDir)
except:
    pass


detector1label,detector2label=lumi.detcNameModifier(detector1label,detector2label)
labelRatio=detector1label+"/"+detector2label

#info
print ("******* Detectors in analysis: ",detector1label ," and ", detector2label,"*******") 

#Create dicts from csv file
#dict1=lumi.myDictFromCSVFile(file1,excludedFills)
#dict2=lumi.myDictFromCSVFile(file2,excludedFills)

dict1all,dict1=lumi.myDictFromCSVFileAndExclud(file1,excludedFills)
dict2all,dict2=lumi.myDictFromCSVFileAndExclud(file2,excludedFills)

if iniFill!=-1 and endFill!=-1:
    dict1=lumi.myDictFromCSVFileFillRangAndExcl(file1,excludedFills,iniFill,endFill)
    dict2=lumi.myDictFromCSVFileFillRangAndExcl(file2,excludedFills,iniFill,endFill)

file1.close()
file2.close()

#Select only the common lumi sections for both detectors
overlapKeys=list(set(dict1).intersection(dict2))



#Sorting to evoid disorder in plots
overlapKeys.sort()


if len(excludedFills)!=0:
    overlapKeysAll=list(set(dict1all).intersection(dict2all))
    overlapKeysAll.sort()

year=lumi.identifyYear(overlapKeys)
print("\n -------------------- Year in analysis: ",year," --------------------")
if year!=0 and options.useYearOpts:
    ymin,ymax,minratio,maxratio,nls,hist_xmin,hist_xmax,hist_nbins,nxbins,nybins,nLumBins,nLumRatioBins,lumiPjxBins=lumi.setYearlyOpts(year)
elif year!=0 and options.useYearOpts==False:
    print ("Automatic setting for specifics years not activated. Add -y to your command if you want to use this option \n")
print ("\n")

#info
print ("Lenghts file1,file2,overlapKeys: ",len(dict1),len(dict2),len(overlapKeys))

