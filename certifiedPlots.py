# -*- coding: utf-8 -*-
"""
Created on Wed May 30 12:21:00 2018

@author: Rafael E. Sosa Ricardo, CMS DESY rafael.sosa.ricardo@desy.de
"""


#import sys
import ROOT
import optparse
import lumi_compare as lumi
#from rootpy.plotting import Hist, HistStack, Legend, Canvas
#import rootpy.plotting.root2matplotlib as rplt

#By me

ROOT.gROOT.SetBatch(ROOT.kTRUE)

##Some important definitions
excludedFills=[5038,5043,5045,5048,5052,5056,4979,5151,5331,5433]

p = optparse.OptionParser()
usage = "usage: %prog [options] arg1 arg2"
p.add_option("-o", "--outdir", type="string",help="Path to output Dir", dest="outDir", default=".")

(options, args) = p.parse_args()

n_files=len(args)

if n_files==1:
    file_path=args[0]
    print ("input file: " + file_path)
    try:
        file=open(file_path)
    except:
        print ("input file cant't be opened!!")
        quit()
    dictall,labels=lumi.myDictFromCSVFileReadAll(file)
    
    lumi.physicsFileAnalisys(dictall,labels,excludedFills,options.outDir)
    
    
else:
    print ("Only 1 file entry allowed, you have entered "+str(n_files)+" files")