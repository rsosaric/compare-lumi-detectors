# -*- coding: utf-8 -*-
"""
Created on Wed May 30 12:21:00 2018

@author: Rafael E. Sosa Ricardo, CMS DESY rafael.sosa.ricardo@desy.de
"""
import ROOT
import math
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties
import statistics
import os
import settings as setts
import warnings

#import matplotlib.dates as md
#import numpy as np
#import datetime as dt
#import time
#import time
#from rootpy.plotting import Hist, HistStack, Legend, Canvas
#from rootpy.plotting.style import get_style, set_style
#from rootpy.plotting.utils import draw
#import rootpy.plotting.root2matplotlib as rplt

ROOT.gROOT.SetBatch(ROOT.kTRUE)

## FUNCTIONS ######################################################

def getKey(item):
    return item[1]

#def getOverlapKeys(dict1,dict2):


#Create pyDict from CSV input file
def myDictFromCSVFile(file, excludedFills):
    print ("Reading file"+str(file))
    foundLabels=False
    commentLines=0
#    nLine=0
    labels=[]
    dict={}
    for line in file.readlines():
        line=line.replace(':',',')
        if line[0] == '#': commentLines += 1
        if commentLines==3:
            print ("Stop reading in: ",line)            
            break
        if (foundLabels == False and line[0] == '#'):
            line=line.replace("#","")
            if commentLines==2:
                foundLabels=True
                temp=line.replace("\n","")
                labels=temp.split(",")
            
        elif (foundLabels == True and line[0] != '#'):
            items=line.split(",")
            try:
                if int(items[1]) in excludedFills:
                    continue
                else:
                    #    fill          run          ls               time          recorded        delivered
                    dict[int(items[1]),int(items[0]),int(items[2])]=[int(items[4]),float(items[8]),float(items[7]),items[10].replace("\r\n","")]  
            except:
                pass
    
    print ("This file contains: ",labels)
    return dict

def myDictFromCSVFileFillRangAndExcl(file, excludedFills,iniFill,endFill):
    dict={}
    if iniFill==-1 and endFill==-1:
        dict=myDictFromCSVFile(file, excludedFills)
    else:
        if endFill==-1:
            endFill=10000000
        print ("Reading file"+str(file))
        foundLabels=False
        commentLines=0
    #    nLine=0
        labels=[]
        
        for line in file.readlines():
            line=line.replace(':',',')
            if line[0] == '#': commentLines += 1
            if commentLines==3:
                print ("Stop reading in: ",line)            
                break
            if (foundLabels == False and line[0] == '#'):
                line=line.replace("#","")
                if commentLines==2:
                    foundLabels=True
                    temp=line.replace("\n","")
                    labels=temp.split(",")
                
            elif (foundLabels == True and line[0] != '#'):
                items=line.split(",")
                try:
                    if (int(items[1]) in excludedFills) or int(items[1])<iniFill or int(items[1])>endFill:
                        continue
                    else:
                        #    fill          run          ls               time          recorded        delivered
                        dict[int(items[1]),int(items[0]),int(items[2])]=[int(items[4]),float(items[8]),float(items[7]),items[10].replace("\r\n","")]  
                except:
                    pass
        
        print ("This file contains: ",labels)
    return dict

def myDictFromCSVFileAndExclud(file, excludedFills=[]):
    print ("Reading file"+str(file))
    foundLabels=False
    commentLines=0
#    nLine=0
    labels=[]
    dictexcl={}
    dictall={}

    for line in file.readlines():
        line=line.replace(':',',')
        if line[0] == '#': commentLines += 1
        if commentLines==3:
            print ("Stop reading in: ",line)            
            break
        if (foundLabels == False and line[0] == '#'):
            line=line.replace("#","")
            if commentLines==2:
                foundLabels=True
                temp=line.replace("\n","")
                labels=temp.split(",")
            
        elif (foundLabels == True and line[0] != '#'):
            items=line.split(",")
            try:
                #    fill          run          ls               time          recorded        delivered
                dictall[int(items[1]),int(items[0]),int(items[2])]=[int(items[4]),float(items[8]),float(items[7]),items[10].replace("\r\n","")]  
                if int(items[1]) in excludedFills:
                    continue
                else:
                    #    fill          run          ls               time          recorded        delivered
                    dictexcl[int(items[1]),int(items[0]),int(items[2])]=[int(items[4]),float(items[8]),float(items[7]),items[10].replace("\r\n","")]  
            except:
                pass
    
    print ("This file contains: ",labels)
    return dictall,dictexcl

def myDictFromCSVFileReadAll(file,excludedFills=[]):
    print ("Reading file"+str(file))
    foundLabels=False
    commentLines=0
#    nLine=0
    labels=[]
    dict={}
    for line in file.readlines():
        line=line.replace(':',',')
        if line[0] == '#': commentLines += 1
        if commentLines==3:
            print ("Stop reading in: ",line)            
            break
        if (foundLabels == False and line[0] == '#'):
            line=line.replace("#","")
            if commentLines==2:
                foundLabels=True
                temp=line.replace("\n","")
                labels=temp.split(",")
            
        elif (foundLabels == True and line[0] != '#'):
            items=line.split(",")
            detector_label=items[10].replace("\n","")
            detector_label=detector_label.replace("\r","")
            detector_label=setts.change_label[detector_label]
            try:
                #    fill          run          ls               time          recorded        delivered       detector
                dict[int(items[1]),int(items[0]),int(items[2])]=[int(items[4]),float(items[8]),float(items[7]),detector_label]
            except:
                print("err in this line: " +str(line))
                pass
    
    print ("This file contains: ",labels)
    return dict,labels

def myDictFromCSVFileReadAll_and_norm(file):
    print ("Reading file"+str(file))
    foundLabels=False
    commentLines=0
#    nLine=0
    labels=[]
    dict={}
    year=0
        
    for line in file.readlines():
        line=line.replace(':',',')
        if line[0] == '#': commentLines += 1
        if commentLines==3:
            print ("Stop reading in: ",line)            
            break
        if (foundLabels == False and line[0] == '#'):
            line=line.replace("#","")
            if commentLines==2:
                foundLabels=True
                temp=line.replace("\n","")
                labels=temp.split(",")
            
        elif (foundLabels == True and line[0] != '#'):
            items=line.split(",")
            if year==0:
                iniFill=int(items[1])
                years_energy=list(setts.rangeFillYearly)
                years_energy.sort()
                
                for tag in years_energy:
                    if inRange(iniFill,setts.rangeFillYearly[tag][0],setts.rangeFillYearly[tag][1]):
                        year=tag[0]
                        break
            try:
                detector_label=items[10].replace("\n","")
                detector_label=detector_label.replace("\r","")
                detector_label_org=detector_label
                detector_label=setts.change_label[detector_label]
                #    fill          run          ls               time          recorded                                                   delivered                                                  detector
                dict[int(items[1]),int(items[0]),int(items[2])]=[int(items[4]),float(items[8])/setts.norm_values[year,detector_label_org],float(items[7])/setts.norm_values[year,detector_label_org],detector_label]
            except:
                print("err in this line: " +str(line))
                pass
    
    print ("This file contains: ",labels)
    return dict,labels

def myDictFromCSVFileReadAllNoLabels(file):
    print ("Reading file"+str(file))
    foundLabels=False
    commentLines=0
#    nLine=0
    labels=[]
    dict={}
    for line in file.readlines():
        line=line.replace(':',',')
        if line[0] == '#': commentLines += 1
        if commentLines==3:
            print ("Stop reading in: ",line)            
            break
        if (foundLabels == False and line[0] == '#'):
            line=line.replace("#","")
            if commentLines==2:
                foundLabels=True
                temp=line.replace("\n","")
                labels=temp.split(",")
            
        elif (foundLabels == True and line[0] != '#'):
            items=line.split(",")
            try:
                #    fill          run          ls               time          recorded        delivered       detector
                dict[int(items[1]),int(items[0]),int(items[2])]=[int(items[4]),float(items[8]),float(items[7]),items[10].replace("\r\n","")]
            except:
                pass
    
    print ("This file contains: ",labels)
    return dict

def myDictFromVdmCSVFile(file):
    print ("Reading file"+str(file))
    foundLabels=False
    commentLines=0
#    nLine=0
    labels=[]
    dict={}
    for line in file.readlines():
        line=line.replace(':',',')
        if line[0] == '#': commentLines += 1
        if commentLines==3:
            print ("Stop reading in: ",line)            
            break
        if (foundLabels == False and line[0] == '#'):
            line=line.replace("#","")
            if commentLines==2:
                foundLabels=True
                temp=line.replace("\n","")
                labels=temp.split(",")
            
        elif (foundLabels == True and line[0] != '#'):
            items=line.split(",")
            try:
                dict[int(items[1]),int(items[0]),int(items[2])]=[int(items[4]),float(items[8]),float(items[7]),items[10].replace("\n",""),items[5]]
#                if items[5]=='STABLE BEAMS':
#                    print ("ok")
            except:
                pass
    
    print ("This file contains: ",labels)
    return dict


def identifyYear(keys):
    rangeFillYerly={}
    rangeFillYerly[2015]=[3829,4720]
    rangeFillYerly[2016]=[4856,5575]
    rangeFillYerly[2017]=[5718,6417]
    rangeFillYerly[2018]=[6570,7407]
    
    iniFill=keys[0][0]
    years=list(rangeFillYerly)
    years.sort()
    year=0
    
    for iyear in years:
        if inRange(iniFill,rangeFillYerly[iyear][0],rangeFillYerly[iyear][1]):
            year=iyear
            break
    if year==0:
        print("Initial fill not found in official fill ranges. Automatic yearly fucntions wont work!!")
        iniFill=keys[len(keys)-1][0]
        for iyear in years:
            if inRange(iniFill,rangeFillYerly[iyear][0],rangeFillYerly[iyear][1]):
                year=iyear
                break            
    
    return year

def identifyYear_and_Energy(keys):
    rangeFillYearly=setts.rangeFillYearly
    
    iniFill=keys[0][0]
    years_energy=list(rangeFillYearly)
    years_energy.sort()
    year=0
    energy=0
    
    for tag in years_energy:
        if inRange(iniFill,rangeFillYearly[tag][0],rangeFillYearly[tag][1]):
            year=tag[0]
            energy=tag[1]
            break
    if year==0:
        print("Initial fill not found in official fill ranges. Automatic yearly fucntions wont work!!")
    if energy==0:
        print("Initial fill not found in official fill ranges. Automatic energy fucntions wont work!!")
    
    return year,energy


def inRange(val,minval,maxval):
    inRange=False
    if val >= minval and val<=maxval:
        inRange=True
    return inRange

def setYearlyOpts(year):
    YearlyOpts={}
    #                 ymin,ymax,minratio,maxratio,nls,hist_xmin,hist_xmax,hist_nbins,nxbins,nybins,nLumBins,nLumRatioBins,lumiPjxBins
    YearlyOpts[2015]=[0.94,1.06,    0.15,    1.85, 50,     0.94,     1.06,       200,   150,    60,     100,          200,      50]
    YearlyOpts[2016]=[0.94,1.06,    0.15,    1.85, 50,     0.94,     1.06,       200,   150,    60,     150,          200,      50]
    YearlyOpts[2017]=[0.94,1.06,    0.15,    1.85, 50,     0.94,     1.06,       200,   150,    60,     150,          200,      50]
    YearlyOpts[2018]=[0.94,1.06,    0.15,    1.85, 50,     0.94,     1.06,       200,   150,    60,     150,          200,      50]
    
#    if year==2016:
#        if (d1=="pcc" and d2=="dt") or (d1=="dt" and d2=="pcc"):
#            excludedFills=[4965,5038,5043,5045,5048,5052,5056,4979,5151,5331,5433]            
#    elif year==2015:
#        if (d1=="pcc" and d2=="dt") or (d1=="dt" and d2=="pcc"):
#            excludedFills=[4528]               
#    else:
#        excludedFills=[]
    
    
    ymin=YearlyOpts[year][0]
    ymax=YearlyOpts[year][1]
    minratio=YearlyOpts[year][2]
    maxratio=YearlyOpts[year][3]
    nls=YearlyOpts[year][4]
    hist_xmin =YearlyOpts[year][5]
    hist_xmax =YearlyOpts[year][6]
    hist_nbins=YearlyOpts[year][7]
    nxbins=YearlyOpts[year][8]
    nybins=YearlyOpts[year][9]
    nLumBins=YearlyOpts[year][10]
    nLumRatioBins=YearlyOpts[year][11]
    lumiPjxBins=YearlyOpts[year][12]
    
    return ymin,ymax,minratio,maxratio,nls,hist_xmin,hist_xmax,hist_nbins,nxbins,nybins,nLumBins,nLumRatioBins,lumiPjxBins

def setYearlyExclusionFills(year,):
    YearlyOpts={}
    #                 ymin,ymax,minratio,maxratio,nls,hist_xmin,hist_xmax,hist_nbins,nxbins,nybins,nLumBins,nLumRatioBins,lumiPjxBins
    YearlyOpts[2015]=[0.94,1.06,    0.15,    1.85, 50,     0.94,     1.06,       200,   150,    60,     100,          200,      50]
    YearlyOpts[2016]=[0.94,1.06,    0.15,    1.85, 50,     0.94,     1.06,       200,   150,    60,     150,          200,      50]
    YearlyOpts[2017]=[0.94,1.06,    0.15,    1.85, 50,     0.94,     1.06,       200,   150,    60,     150,          200,      50]
    YearlyOpts[2018]=[0.94,1.06,    0.15,    1.85, 50,     0.94,     1.06,       200,   150,    60,     150,          200,      50]
    
    ymin=YearlyOpts[year][0]
    ymax=YearlyOpts[year][1]
    minratio=YearlyOpts[year][2]
    maxratio=YearlyOpts[year][3]
    nls=YearlyOpts[year][4]
    hist_xmin =YearlyOpts[year][5]
    hist_xmax =YearlyOpts[year][6]
    hist_nbins=YearlyOpts[year][7]
    nxbins=YearlyOpts[year][8]
    nybins=YearlyOpts[year][9]
    nLumBins=YearlyOpts[year][10]
    nLumRatioBins=YearlyOpts[year][11]
    lumiPjxBins=YearlyOpts[year][12]
    
    return ymin,ymax,minratio,maxratio,nls,hist_xmin,hist_xmax,hist_nbins,nxbins,nybins,nLumBins,nLumRatioBins,lumiPjxBins

def headOnSelctionFromCSVvdmFile(file,deltaD):
    print ("Reading file"+str(file))
    nLines=0
    timelist=[]
    lslist=[]
    separationlist=[]
    for line in file.readlines():
        if nLines!=0:
            items=line.split(",")
            
            try:
                separation=float(items[13])
                separationlist.append((int(items[4]),separation))
                ls=int(items[2])
                if abs(separation)<=deltaD:
                    timelist.append(int(items[4]))
                    if ls not in lslist:
                        lslist.append(ls)
            except:
                pass
        nLines+=1
    return timelist,separationlist,lslist

def readNBXperFill(nBXfileName):
    NBXPerFill={}
    try:
        nbxfile=open(nBXfileName)
    except:
        print (nBXfileName," ERR: NBXPerFill file Not found!!!!")
        if nBXfileName=="":
            print("TIP: Use input option -n to specifie the file.")
        quit()
    for line in nbxfile.readlines():
        items=line.split(",")
        try:
            fill=int(items[0])
            NBX=int(items[1])
            NBXPerFill[fill]=NBX
        except:
            print ("Problem with line",line)

    nbxfile.close()
    return NBXPerFill

def excludeFills(keys,fillList):
    for key in keys:
        for fill in fillList:
            if fill in key:
                keys.remove(key)
    return keys
    

## gets the run average lumis for a detector, output contains for each run: average lumi, sigma, avg time of the run 
def averageLumi_x_RunErrs(dictx,overlapKeys):
    avgs={}
    output={}
    #Min value for number of events requiered to consider a Fill
    min_lumis=10
    min_lumi=0.001
    avg=0.0
    n_avg=0
    current_run=overlapKeys[0][1]
    run_num=0
    n=0
    runs=[]
    time0=dictx[overlapKeys[0]][0]
    timeAvg=0
    labelFillinRun={}
    
    
    #Calc Avg
    for key in overlapKeys:
        n+=1
        
        if (key[1] != (current_run) or n == len(overlapKeys)):
            timeAvg=(dictx[key][0]+time0)/2
            runs.append(current_run)
            #Calc avg
            avg=avg/n_avg
            
            avgs[int(current_run)]=[avg,n_avg,timeAvg]
            avg=0.0
            n_avg=0
            run_num+=1
            time0=dictx[key][0]
            
        avg+=dictx[key][1]         
        n_avg+=1
        current_run=key[1]
    #Calc sigma
    n=0
    sigma=0.0
    current_run=overlapKeys[0][1]   
    for key in overlapKeys:
        n+=1
        if (key[1] != (current_run) or n == len(overlapKeys)):
            #print "Calcular :",current_run
            
            if (avgs[current_run][1] < min_lumis or avgs[current_run][0] < min_lumi):
                print ("Run ",current_run, "not included!!"," <---: lumi=",avgs[current_run][0]," in ",avgs[current_run][1]," ls")
                runs.remove(current_run)
                sigma=0.0
            else:
                sigma=math.sqrt(sigma/(avgs[current_run][1]-1))
                output[int(current_run)]=[avgs[current_run][0],sigma/math.sqrt(avgs[current_run][1]),avgs[current_run][2]]
                labelFillinRun[int(current_run)]=int(key[0])
                sigma=0.0
         
        sigma += (dictx[key][1]-avgs[key[1]][0])**2
        current_run=key[1]        
    
    return output,runs,labelFillinRun

## compute ratios taking as input the avgs(output) from averageLumi_x_... functions
def ratios_from_avgs(avg1,avg2,keys):
    ratios=[]
    errs_tot=[]
    time=[]
    #temp=avg1[keys[0]][0]/avg2[keys[0]][0]
    tempRun=1.0
    fileJumps = open("BigJumps.txt","w")
    for key in keys:
        ratio=avg1[key][0]/avg2[key][0]
        
        if abs(ratio-tempRun)>0.04:
            print ("BIG JUMP")
            fileJumps.write(str(abs(ratio-tempRun))+" in "+ str(key)+"\n")
        
        tempRun=ratio
        ratios.append(ratio)
        err=ratio*math.sqrt((avg1[key][1]/avg1[key][0])**2+(avg2[key][1]/avg2[key][0])**2)
        errs_tot.append(err)
        time.append(avg1[key][2])
        
    fileJumps.close()
    
    return ratios,errs_tot,time


## compute ratios taking as input dicts, not take avgs, just ratios vs time for 2 detectors
def calc_ratio(dict1,dict2,overlapKeys,minratio,maxratio):
    
    ratios=[]
    time=[]
    
    
    for key in overlapKeys:
        errfill=0
        if dict2[key][1]<=0:
            if key[0]!=errfill:
                errfill=key[0]        
                #print "negative or null value in fill ", errfill
            continue
        
        try:
            div=dict1[key][1]/dict2[key][1]        
            if div<maxratio and div>minratio:
                ratios.append(div)
                time.append(dict1[key][0])
                
                
            else:
                if key[0]!=errfill:
                    errinfill=key[0]
                    print ("Fill ", errinfill, " contain outrange values!!!!")
            
        except:
           pass
    return ratios,time

def calc_ratio3detect_noavg(dict1,dict2,dict3,overlapKeys,minratio,maxratio):
    
    ratios13=[]
    ratios23=[]
    time=[]
    runs=[]
    fillinRun=[]
    
    
    for key in overlapKeys:
        errfill=0
        if dict3[key][1]<=0:
            if key[0]!=errfill:
                errfill=key[0]        
                print ("negative or null value in fill ", errfill)
            continue
        
        try:
            div13=dict1[key][1]/dict3[key][1]
            div23=dict2[key][1]/dict3[key][1]
            
            if div13<maxratio and div13>minratio and div23<maxratio and div23>minratio:
                ratios13.append(div13)
                ratios23.append(div23)
                
                if dict1[key][0]==dict2[key][0]:
                    time.append(dict1[key][0])
                else:
                    print ("Err in time match")
                runs.append(key[1])
                fillinRun.append(key[0])
                
            else:
                if key[0]!=errfill:
                    errinfill=key[0]
                    #print "Fill ", errinfill, " contain outrange values!!!!"
            
        except:
           pass
    return ratios13,ratios23,runs,time,fillinRun

def calc_ratio3detect(dict1,dict2,dict3,overlapKeys,minratio,maxratio,nls):
    
    ratios13=[]
    ratios23=[]
    ratios12=[]
    time=[]
    runs=[]
    labelFillinRun={}
    current_run=overlapKeys[0][1]    
    n=0
    n_avg=0
    avg1=0.0
    avg2=0.0
    avg3=0.0
    lsavg1=0.0
    lsavg2=0.0
    lsavg3=0.0
    errinfill=0
    time.append(dict1[overlapKeys[0]][0])
    
    ratios13nLS=[]
    ratios23nLS=[]
    ratios12nLS=[]
    labelFillinNls={}
    inls=0
    nl=0
    
    
    for key in overlapKeys:
        n+=1
        nl+=1
        
        if (key[1] != (current_run) or n == len(overlapKeys)):
            
            if (n_avg!=0):
                #Calc avg
                avg1=avg1/n_avg                            
                avg2=avg2/n_avg
                avg3=avg3/n_avg
                ratios13.append(avg1)
                ratios23.append(avg2)
                ratios12.append(avg3)
                runs.append(current_run)
                labelFillinRun[current_run]=int(key[0])
                time.append(dict1[key][0])
                        
            avg1=0.0
            avg2=0.0
            avg3=0.0
            
            n_avg=0

        if (nl == nls-1 or n == len(overlapKeys)):
            
            lsavg1=lsavg1/(nl+1)                            
            lsavg2=lsavg2/(nl+1)
            lsavg3=lsavg3/(nl+1)
            
            ratios13nLS.append(lsavg1)
            ratios23nLS.append(lsavg2)
            ratios12nLS.append(lsavg3)
            
            labelFillinNls[inls]=int(key[0])            
                        
            lsavg1=0.0
            lsavg2=0.0
            lsavg3=0.0
            
            inls+=1
            nl=0
            
        try:
            div13=dict1[key][1]/dict3[key][1]
            div23=dict2[key][1]/dict3[key][1]
            div12=dict1[key][1]/dict2[key][1]
            
            if div13<maxratio and div13>minratio and div23<maxratio and div23>minratio:
                avg1+=div13
                avg2+=div23
                avg3+=div12
                
                lsavg1+=div13
                lsavg2+=div23
                lsavg2+=div12                                
                n_avg+=1
            else:
                if key[0]!=errinfill:
                    errinfill=key[0]
                    print ("Fill ", errinfill, " contain outrange values!!!!")
                
        except:
            pass
        
        current_run=key[1]
    
    
    return ratios13,ratios23,ratios12,runs,time,labelFillinRun,ratios13nLS,ratios23nLS,ratios12nLS,labelFillinNls
    
    
def calc_ratioDict2detect(dict1,dict2,overlapKeys,minratio,maxratio):
    
    ## Contains: [0]run,[1]ratio,[2]det1Lumi,[3]det2Lumi,[4]det1+detc2Lumi
    ratiosDict={}
    
      
    n=0
    errinfill=0
    nvls=0
    errsinfill=0
    fillsWithErrs=[]
   
#    tempratio=dict1[overlapKeys[0]][1]/dict2[overlapKeys[0]][1]
    
    for key in overlapKeys:
        n+=1
        
        
        try:
            div=dict1[key][1]/dict2[key][1]
            if div<maxratio and div>minratio:
                ratiosDict[key[0],key[1],dict1[key][0],nvls]=[div,dict1[key][1],dict2[key][1],dict1[key][1]+dict2[key][1]]
                nvls+=1
                
            else:
                if key[0]!=errinfill:
                    errinfill=key[0]
                    errsinfill+=1
                    fillsWithErrs.append(errinfill)
        except:
            pass
        
    print (errsinfill, "Fills containing outrange values!!!!",fillsWithErrs)
    
    
    return ratiosDict

def calc_ratioDict2detectCorrected(f1,mean,dict1,dict2,nBXPerFill,overlapKeys,minratio,maxratio):
    
    ## Contains: [0]run,[1]ratio,[2]det1Lumi,[3]det2Lumi,[4]det1+detc2Lumi
    ratiosDict={}     
    n=0
    errinfill=0
    nvls=0
    errsinfill=0
    fillsWithErrs=[]
    Lumi1=0.0
    Lumi2=0.0
    slope=f1.GetParameter(1)
    
    
    for key in overlapKeys:
        n+=1      
        
        try:
            div=dict1[key][0]/dict2[key][0]            
            if div<maxratio and div>minratio:
                #print (f1.Eval(dict2[key][0]))
                val2corrct=(dict2[key][1])*f1.Eval(dict2[key][1]/nBXPerFill[key[0]])/mean
                #val2corrct=dict2[key][1]*(1+slope*(dict2[key][1]/nBXPerFill[key[0]]))
                div=dict1[key][1]/val2corrct
                ratiosDict[key[0],key[1],dict1[key][0],nvls]=[div,dict1[key][1],val2corrct,val2corrct+dict1[key][1]]
                nvls+=1
                Lumi2+= val2corrct
                Lumi1+= dict1[key][1]
            else:
                if key[0]!=errinfill:
                    errinfill=key[0]
                    errsinfill+=1
                    fillsWithErrs.append(errinfill)
        except:
            #print("ERRRRR!!!!")
            pass
        
    print (errsinfill, "Fills containing outrange values!!!!",fillsWithErrs)    
    return ratiosDict,Lumi1,Lumi2

def calc_ratio2detectNlsDictCorrected(f1,mean,dict1,dict2,nBXPerFill,overlapKeys,nls):
        
    nlsDict={}
    totLumiAll=0.0
    totLumi1=0.0
    totLumi2=0.0
    lsTotL1=0.0
    lsTotL2=0.0
    
    n=0
    n_nullNeg=0
    ntot=0
    ls=0
    
    for key in overlapKeys:
        ntot+=1
        try:            
            div=dict1[key][1]/dict2[key][1]
        except:
            continue
        
        if div>0:
            val2corrct=(dict2[key][1])*f1.Eval(dict2[key][1]/nBXPerFill[key[0]])/mean        
            div=dict1[key][1]/val2corrct
            n+=1
            totLumiAll+=(dict1[key][1]+val2corrct)
            lsTotL1+=dict1[key][1]
            lsTotL2+=val2corrct
        else:
            n_nullNeg+=1
            
        if n==nls or ntot==len(overlapKeys):
            nlsDict[key[0],key[1],dict1[key][0],ls]=[lsTotL1/lsTotL2,lsTotL1,lsTotL2]
            totLumi1+=lsTotL1
            totLumi2+=lsTotL2
                
            lsTotL1=0.0
            lsTotL2=0.0
                
            n=0
            ls+=1
       
    
    if n_nullNeg!=0:
        print (n_nullNeg," null values!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
    print ("TotLumi1/TotLumi2 = ", totLumi1/totLumi2)
    
    return nlsDict,totLumi1,totLumi2



def equalLumiAnalysis(ratioDict,nbins,totLumi):
    lumEqDict={}
    keys=list(ratioDict)
    keys.sort()
    binLumi=totLumi/nbins
    
    n=0
    tempLumi=0.0
    tempLumi1=0.0
    tempLumi2=0.0
    
    tempRatio=0.0
    tempnls=0
    for key in keys:
        n+=1
        tempLumi+=ratioDict[key][3]
        tempLumi1+=ratioDict[key][1]
        tempLumi2+=ratioDict[key][2]
        tempRatio+=ratioDict[key][0]
        tempnls+=1
        
        if tempLumi>=binLumi or n==len(keys):
            tempRatio=tempRatio/tempnls
            lumEqDict[key]=[tempLumi1/tempLumi2,tempRatio,tempnls]
            
            tempLumi=0
            tempLumi1=0
            tempLumi2=0
            tempRatio=0
            tempnls=0
    
    keysEqLumi=list(lumEqDict)
    keysEqLumi.sort()   
    n=0
    
    for keyEqLumi in keysEqLumi:
        avg=lumEqDict[keyEqLumi][1]
        Nls=lumEqDict[keyEqLumi][2]
        ratioLum=lumEqDict[keyEqLumi][0]
        sigma=0.0
        for i in range(n,n+Nls-1):
            n+=1
            sigma+=(ratioDict[keys[i]][0]-avg)**2
        
        sigma=math.sqrt(sigma/(Nls-1))
        lumEqDict[keyEqLumi]=[ratioLum,avg,Nls,sigma]            
        
    return lumEqDict

def calc_ratioDict3detect(dict1,dict2,dict3,overlapKeys,minratio,maxratio):
    
    ## Contains: [0]run,[1]ratio,[2]det1Lumi,[3]det2Lumi,[4]det1+detc2Lumi
    ratiosDict13={}
    ratiosDict23={}
    ratiosDict12={}
       
    n=0
    errinfill=0
    nvls=0
    errsinfill=0
    fillsWithErrs=[]
   
#    tempratio=dict1[overlapKeys[0]][1]/dict2[overlapKeys[0]][1]
    
    for key in overlapKeys:
        n+=1
        
        
        try:
            div13=dict1[key][1]/dict3[key][1]
            div23=dict2[key][1]/dict3[key][1]
            div12=dict1[key][1]/dict2[key][1]
            divmax=max(div13,div23,div12)
            divmin=min(div13,div23,div12)
            
            if divmax<maxratio and divmin>minratio:
                ratiosDict13[key[0],key[1],nvls,dict1[key][0]]=[div13,dict1[key][1],dict3[key][1],dict1[key][1]+dict3[key][1]]
                ratiosDict23[key[0],key[1],nvls,dict1[key][0]]=[div23,dict2[key][1],dict3[key][1],dict2[key][1]+dict3[key][1]]
                ratiosDict12[key[0],key[1],nvls,dict1[key][0]]=[div12,dict1[key][1],dict2[key][1],dict1[key][1]+dict2[key][1]]
                nvls+=1
                
            else:
                if key[0]!=errinfill:
                    errinfill=key[0]
                    errsinfill+=1
                    fillsWithErrs.append(errinfill)
        except:
            pass
        
    print (errsinfill, "Fills containing outrange values!!!!---->>",fillsWithErrs)
    
    
    return ratiosDict13,ratiosDict23,ratiosDict12

def calc_ratio3detectNlsDict(dict1,dict2,dict3,overlapKeys,nls):
        
    nlsDict13={}
    nlsDict23={}
    nlsDict12={}
    
    totLumi1=0.0
    totLumi2=0.0
    totLumi3=0.0
    lsTotL1=0.0
    lsTotL2=0.0
    lsTotL3=0.0
    
    n=0
    n_nullNeg=0
    ntot=0
    ls=0
    
    for key in overlapKeys:
        ntot+=1
        try:            
            div13=dict1[key][1]/dict3[key][1]
            div23=dict2[key][1]/dict3[key][1]
            div12=dict1[key][1]/dict2[key][1]
            divmin=min(div13,div23,div12)
        except:
            continue
        
        if divmin>0:
            n+=1
            lsTotL1+=dict1[key][1]
            lsTotL2+=dict2[key][1]
            lsTotL3+=dict3[key][1]
        else:
            n_nullNeg+=1
            
        if n==nls or ntot==len(overlapKeys):
            nlsDict13[key[0],key[1],dict1[key][0],ls]=[lsTotL1/lsTotL3,lsTotL1,lsTotL3,totLumi3]
            nlsDict23[key[0],key[1],dict1[key][0],ls]=[lsTotL2/lsTotL3,lsTotL2,lsTotL3,totLumi3]
            nlsDict12[key[0],key[1],dict1[key][0],ls]=[lsTotL1/lsTotL2,lsTotL1,lsTotL2,totLumi3]
            totLumi1+=lsTotL1
            totLumi2+=lsTotL2
            totLumi3+=lsTotL3
                
            lsTotL1=0.0
            lsTotL2=0.0
            lsTotL3=0.0
                
            n=0
            ls+=1
       
    
    if n_nullNeg!=0:
        print (n_nullNeg," null or negative values!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
    print ("TotLumi1/TotLumi2 = ", totLumi1/totLumi2)
    
    return nlsDict13,nlsDict23,nlsDict12,totLumi1,totLumi2,totLumi3

def calc_ratio2detectNlsDictWithAllData(dict1,dict2,dict1all,dict2all,biggerKeys,biggerDict,nls):
        
    nlsDict12={}
    
    nlsDict12all={}
    
    totLumi1=0.0
    totLumi2=0.0
    
    totLumi1all=0.0
    totLumi2all=0.0
    totLumi3all=0.0
    
    lsTotL1=0.0
    lsTotL2=0.0
    lsTotL1all=0.0
    lsTotL2all=0.0
    lsTotL3all=0.0
    
    n=0
    n_nullNeg=0
    ntot=0
    lsall=0
    selNls=0
    selNlsAll=0
    
    for key in biggerKeys:
        ntot+=1
        div12=0.0
        div12all=0.0
        n+=1
        
        try:            
            div12=dict1[key][1]/dict2[key][1]
        except:
            pass
        try: 
            div12all=dict1all[key][1]/dict2all[key][1]
        except:
            pass
        
        
        
        if div12>0:
            lsTotL1+=dict1[key][1]
            lsTotL2+=dict2[key][1]
            selNls+=1
            
        else:
            n_nullNeg+=1
        if div12all>0:
            lsTotL1all+=dict1all[key][1]
            lsTotL2all+=dict2all[key][1]
            lsTotL3all+=biggerDict[key][1]
            selNlsAll+=1
            
        if n==nls or ntot==len(biggerKeys):
            lsall+=1
            n=0
            try:
                if selNlsAll>0.9*nls:
                    nlsDict12all[key[0],key[1],biggerDict[key][0],lsall]=[lsTotL1all/lsTotL2all,lsTotL1all,lsTotL2all,totLumi3all]
            except:
                pass
            try:
                if selNls>0.9*nls:
                    nlsDict12[key[0],key[1],biggerDict[key][0],lsall]=[lsTotL1/lsTotL2,lsTotL1,lsTotL2,totLumi3all]
            except:
                pass
            
            totLumi1all+=lsTotL1all
            totLumi2all+=lsTotL2all
            totLumi3all+=lsTotL3all
            lsTotL1all=0.0
            lsTotL2all=0.0
            lsTotL3all=0.0
            
            totLumi1+=lsTotL1
            totLumi2+=lsTotL2
            lsTotL1=0.0
            lsTotL2=0.0
       
    
    if n_nullNeg!=0:
        print (n_nullNeg," null or negative values!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
    print ("TotLumi1/TotLumi2 = ", totLumi1/totLumi2)
    
    return nlsDict12,nlsDict12all,totLumi1,totLumi2,totLumi1all,totLumi2all,totLumi3all

def calc_ratio2detectDictWithAllData(dict1,dict2,dict1all,dict2all,biggerKeys,biggerDict):
    ratioDict12={}
    
    ratioDict12all={}
    
    totLumi1=0.0
    totLumi2=0.0
    
    totLumi1all=0.0
    totLumi2all=0.0
    totLumi3all=0.0
    
    n=0
    n_nullNeg=0
    ntot=0
    lsall=0
    selNls=0
    selNlsAll=0
    
    for key in biggerKeys:
        ntot+=1
        n+=1
        
        
        
        lsTotL3all=biggerDict[key][1]
        try:
            lsTotL1=dict1[key][1]
            lsTotL2=dict2[key][1]
            div12=dict1[key][1]/dict2[key][1]
            selNls+=1
            ratioDict12[key[0],key[1],biggerDict[key][0],lsall]=[div12,lsTotL1,lsTotL2,totLumi3all]
            totLumi1+=lsTotL1
            totLumi2+=lsTotL2
        except:
            pass
        try:
            lsTotL1all=dict1all[key][1]
            lsTotL2all=dict2all[key][1]
            div12all=lsTotL1all/lsTotL2all
            selNlsAll+=1
            ratioDict12all[key[0],key[1],biggerDict[key][0],lsall]=[div12all,lsTotL1all,lsTotL2all,totLumi3all]
            totLumi1all+=lsTotL1all
            totLumi2all+=lsTotL2all
        except:
            pass
            
        
        totLumi3all+=lsTotL3all
        lsall+=1
    
    if n_nullNeg!=0:
        print (n_nullNeg," null or negative values!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
    print ("TotLumi1/TotLumi2 = ", totLumi1/totLumi2)
    
    return ratioDict12,ratioDict12all,totLumi1,totLumi2,totLumi1all,totLumi2all,totLumi3all
    

def calc_ratio2detect(dict1,dict2,overlapKeys,minratio,maxratio):
    
    ## Contains: [0]run,[1]ratio,[2]det1Lumi,[3]det2Lumi,[4]det1+detc2Lumi
    ratios_runs=[]
    ratios_fills=[]
    ratios_time=[]
    trackFillsRunsTime=[]
      
    n=0
    n_avg=0
    avg=0.0
    errinfill=0
   
#    tempratio=dict1[overlapKeys[0]][1]/dict2[overlapKeys[0]][1]
    
    for key in overlapKeys:
        n+=1
        
        
        try:
            div=dict1[key][1]/dict2[key][1]
            if div<maxratio and div>minratio:
#                if abs(tempratio-div)>0.06:
#                    print "BIG JUMP!!!!!!!!!! ",abs(tempratio-div) ,"in ",key
                avg+=div
                
                n_avg+=1
                ratios_runs.append((key[1],div,dict1[key][1],dict2[key][1],dict1[key][1]+dict2[key][1]))
                ratios_fills.append((key[0],div,dict1[key][1],dict2[key][1],dict1[key][1]+dict2[key][1]))
                ratios_time.append((dict1[key][0],div))
                trackFillsRunsTime.append((key[0],key[1],dict1[key][0]))
                
            else:
                if key[0]!=errinfill:
                    errinfill=key[0]
                    print ("Fill ", errinfill, " contain outrange values!!!!")
                
        except:
            pass
        
        
    
    
    return ratios_runs,ratios_fills,ratios_time,trackFillsRunsTime

def calc_ratio2detectV2(dict1,dict2,overlapKeys,minratio,maxratio):
    
    ## Contains: [0]ratio,[1]det1Lumi,[2]det2Lumi,[3]det1+detc2Lumi
    dictRatios={}
    trackFillsRunsTime=[]
      
    n=0
    n_avg=0
    avg=0.0
    errinfill=0
    
    
   
#    tempratio=dict1[overlapKeys[0]][1]/dict2[overlapKeys[0]][1]
    
    for key in overlapKeys:
        n+=1
        
        
        try:
            div=dict1[key][1]/dict2[key][1]
            if div<maxratio and div>minratio:
#                if abs(tempratio-div)>0.06:
#                    print "BIG JUMP!!!!!!!!!! ",abs(tempratio-div) ,"in ",key
                avg+=div
                n_avg+=1
                dict[key]=[div,dict1[key][1],dict2[key][1],dict1[key][1]+dict2[key][1]]
                tempratio=div
            else:
                if key[0]!=errinfill:
                    errinfill=key[0]
                    #print "Fill ", errinfill, " contain outrange values!!!!"
                
        except:
            pass  
    
    return dictRatios


def calc_ratio2detectNls(dict1,dict2,overlapKeys,nls):
        
    ratiosnLS=[]
    ratiosnLS_time=[]
    labelFillinNls=[]
    L1=[]
    L2=[]
    totLumiAll=0.0
    totLumi1=0.0
    totLumi2=0.0
    lsTotL1=0.0
    lsTotL2=0.0
    
    n=0
    n_nullNeg=0
    ntot=0
    avg=0.0
    timeNls=dict1[overlapKeys[0]][0]
    
    for key in overlapKeys:
        ntot+=1
        try:            
            div=dict1[key][1]/dict2[key][1]
        except:
            continue
        
        if div>0:
            n+=1
            totLumiAll+=(dict1[key][1]+dict2[key][1])
            lsTotL1+=dict1[key][1]
            lsTotL2+=dict2[key][1]
            if n==1:
                timeNls=dict1[key][0]
        else:
            n_nullNeg+=1
            
        if n==nls or ntot==len(overlapKeys):
            ratiosnLS.append(lsTotL1/lsTotL2)
            if dict1[key][0]!=dict2[key][0]:
                print ("time missmatch")
            ratiosnLS_time.append((timeNls,lsTotL1/lsTotL2))
            L1.append(lsTotL1)
            L2.append(lsTotL2)
                
            labelFillinNls.append((key[0],key[1],dict1[key][0]))
                
            totLumi1+=lsTotL1
                
            totLumi2+=lsTotL2
                
            lsTotL1=0.0
            lsTotL2=0.0
                
            n=0
       
    
    if n_nullNeg!=0:
        print (n_nullNeg," null values!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
    print ("TotLumi1/TotLumi2 = ", totLumi1/totLumi2)
    
    return ratiosnLS,ratiosnLS_time,labelFillinNls,L1,L2,totLumi1,totLumi2

def calc_ratio2detectNlsDict(dict1,dict2,overlapKeys,nls):
        
    nlsDict={}
    totLumiAll=0.0
    totLumi1=0.0
    totLumi2=0.0
    lsTotL1=0.0
    lsTotL2=0.0
    
    n=0
    n_nullNeg=0
    ntot=0
    ls=0
    
    for key in overlapKeys:
        ntot+=1
        try:            
            div=dict1[key][1]/dict2[key][1]
        except:
            continue
        
        if div>0:
            n+=1
            totLumiAll+=(dict1[key][1]+dict2[key][1])
            lsTotL1+=dict1[key][1]
            lsTotL2+=dict2[key][1]
        else:
            n_nullNeg+=1
            
        if n==nls or ntot==len(overlapKeys):
            nlsDict[key[0],key[1],dict1[key][0],ls]=[lsTotL1/lsTotL2,lsTotL1,lsTotL2]
            totLumi1+=lsTotL1
            totLumi2+=lsTotL2
                
            lsTotL1=0.0
            lsTotL2=0.0
                
            n=0
            ls+=1
       
    
    if n_nullNeg!=0:
        print (n_nullNeg," null values!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
    print ("TotLumi1/TotLumi2 = ", totLumi1/totLumi2)
    
    return nlsDict,totLumi1,totLumi2

def calc_ratio2detectByRunDict(dict1,dict2,overlapKeys):
        
    runDict={}
    totLumiAll=0.0
    lsTotL1=0.0
    lsTotL2=0.0
    
    n=0
    nullRun=0
    n_nullNeg=0
    ntot=0
    run=overlapKeys[0][1]
    fill=overlapKeys[0][0]
    nruns=0
    
    for key in overlapKeys:
        ntot+=1
        
        try:            
            div=dict1[key][1]/dict2[key][1]
        except:
            continue
        
        if div>0:
            n+=1
            totLumiAll+=(dict1[key][1]+dict2[key][1])
            lsTotL1+=dict1[key][1]
            lsTotL2+=dict2[key][1]
        else:
            n_nullNeg+=1
            
        if run!=key[1] or ntot==len(overlapKeys):
            if lsTotL2==0 or lsTotL1==0:
                nullRun+=1
                continue
            runDict[fill,run,dict1[key][0],nruns]=[lsTotL1/lsTotL2,lsTotL1,lsTotL2]
            fill=key[0]    
            lsTotL1=0.0
            lsTotL2=0.0
                
            n=0
            run=key[1]
            nruns+=1
            
    if nullRun!=0:
        print (nullRun,"nullRuns found!!")
    return runDict

def calc_ratio2detectByFillDict(dict1,dict2,overlapKeys):
        
    fillDict={}
    totLumiAll=0.0
    lsTotL1=0.0
    lsTotL2=0.0
    
    n=0
    nullFill=0
    n_nullNeg=0
    ntot=0
    run=overlapKeys[0][1]
    fill=overlapKeys[0][0]
    nfills=0
    
    for key in overlapKeys:
        ntot+=1
        
        try:            
            div=dict1[key][1]/dict2[key][1]
        except:
            continue
        
        if div>0:
            n+=1
            totLumiAll+=(dict1[key][1]+dict2[key][1])
            lsTotL1+=dict1[key][1]
            lsTotL2+=dict2[key][1]
        else:
            n_nullNeg+=1
            
        if fill!=key[0] or ntot==len(overlapKeys):
            if lsTotL2==0 or lsTotL1==0:
                nullFill+=1
                continue
            fillDict[fill,run,dict1[key][0],nfills]=[lsTotL1/lsTotL2,lsTotL1,lsTotL2]
            fill=key[0]
            run=key[1]
            lsTotL1=0.0
            lsTotL2=0.0
                
            n=0
            fill=key[0]
            nfills+=1
            
    if nullFill!=0:
        print (nullFill,"nullRuns found!!")
    return fillDict

def calc_ratio3detectByRunDict(dict1,dict2,dict3,overlapKeys):
        
    runDict13={}
    runDict23={}
    runDict12={}
    
    runTotL1=0.0
    runTotL2=0.0
    runTotL3=0.0
    
    n=0
    nullRun=0
    n_nullNeg=0
    ntot=0
    run=overlapKeys[0][1]
    nruns=0
    
    for key in overlapKeys:
        ntot+=1
        
        try:            
            div13=dict1[key][1]/dict3[key][1]
            div23=dict2[key][1]/dict3[key][1]
            div12=dict1[key][1]/dict2[key][1]
            divmin=min(div13,div23,div12)
        except:
            continue
        
        if divmin>0:
            n+=1
            runTotL1+=dict1[key][1]
            runTotL2+=dict2[key][1]
            runTotL3+=dict3[key][1]
        else:
            n_nullNeg+=1
            
        if run!=key[1] or ntot==len(overlapKeys):
            if runTotL2==0 or runTotL1==0 or runTotL3==0:
                nullRun+=1
                continue
            runDict13[key[0],key[1],dict1[key][0],nruns]=[runTotL1/runTotL3,runTotL1,runTotL3]
            runDict23[key[0],key[1],dict1[key][0],nruns]=[runTotL2/runTotL3,runTotL2,runTotL3]
            runDict12[key[0],key[1],dict1[key][0],nruns]=[runTotL1/runTotL2,runTotL1,runTotL2]
                
            runTotL1=0.0
            runTotL2=0.0
            runTotL3=0.0
                
            n=0
            run=key[1]
            nruns+=1
            
    if nullRun!=0:
        print (nullRun,"nullRuns found!!")
    return runDict13,runDict23,runDict12

    
def physicsFileAnalisys(dict,labels,excludedFills,outputDIR):
    
    sortedKeys=list(dict)
    sortedKeys.sort()
    lumi_unit=labels[6].replace("delivered(","")
    lumi_unit=lumi_unit.replace(")","")
    lumi_IntUnit=lumi_unit.replace("hz/","1/")
    
    totNls_inRangeFills=0
    totNls=len(sortedKeys)
    
    ## For a range in Fill or Runs:
    #rangeFill_ini=4958
    #rangeFill_end=4965
    ##All fills
    rangeFill_ini=sortedKeys[0][0]
    rangeFill_end=sortedKeys[totNls-1][0]
    
    integratedTotalLumi=0.0
    excludedIntegratedTotalLumi=0.0
    
    xLS=23.31
    nbins=50 
    
    maxLumi=0.0
    del_maxLumi=0.0
    minLumi=1000000.0
    del_minLumi=1000000.0
    negV=0
    del_negV=0
    negVlumi=0.0
    del_negVlumi=0.0
    detectors=[]
    detectorsInfo={}
    detectorsInfoPorcent={}
    
    
    for key in sortedKeys:
        if key[0]>=rangeFill_ini and key[0]<=rangeFill_end:
            totNls_inRangeFills+=1
        ##Look for the max
        if dict[key][1]>maxLumi:
            maxLumi=dict[key][1]
        if dict[key][2]>del_maxLumi:
            del_maxLumi=dict[key][2]
                  
        ##Look for the min
        if dict[key][1]<minLumi:
            minLumi=dict[key][1]
        if dict[key][2]<del_minLumi:
            del_minLumi=dict[key][1]
        
        ##Check for negative values
        if dict[key][1]<0:
            print ("Negative value!!! in ",key," Detector: ",dict[key][3])
            negV+=1
            negVlumi+=dict[key][1]
        if dict[key][2]<0:
            print ("Negative delivered value!!! in ",key," Detector: ",dict[key][3])
            del_negV+=1
            del_negVlumi+=dict[key][2]
        
        ##Collect info about the detectors: nls and Lumi
        if dict[key][3] in detectors:
            nls=detectorsInfo[dict[key][3]][0]
            Lumi=detectorsInfo[dict[key][3]][1]
            nls+=1
            Lumi+=dict[key][1]
            detectorsInfo[dict[key][3]]=[nls,Lumi]
            
        else:
            detectors.append(dict[key][3])
            #                           Nls,%totLumi
            detectorsInfo[dict[key][3]]=[0,0.0]
        
        if key[0] in excludedFills:
            excludedIntegratedTotalLumi+=dict[key][1]
                    
        integratedTotalLumi+=dict[key][1]
    
    
    
    
    ## by Dtectors
    plots_byDetc_VsTime=[]
    pointsDetc=[]
    for detc in detectors:
        detectorsInfoPorcent[detc]=[float(detectorsInfo[detc][0])*100/totNls,detectorsInfo[detc][1]*100/integratedTotalLumi]
        plots_byDetc_VsTime.append(ROOT.TGraph())
        pointsDetc.append(0)
        
    integratedTotalLumi=integratedTotalLumi*xLS
    excludedIntegratedTotalLumi=excludedIntegratedTotalLumi*xLS
    
    iniRun=sortedKeys[0][1]
    lastRun=sortedKeys[totNls-1][1]
    iniFill=sortedKeys[0][0]
    lastFill=sortedKeys[totNls-1][0]
    iniTime=dict[sortedKeys[0]][0]
    lastTime=dict[sortedKeys[totNls-1]][0]
       
     ### PLOTS ###
    plot_hist=ROOT.TH1F("hist","Recorded Inst. Luminosity Histogram;Recorded Inst. Lumi."+"("+lumi_unit+");"+"Number of LS",nbins,minLumi,maxLumi)
    plot_del_hist=ROOT.TH1F("hist_del","Delivered Inst. Luminosity Histogram;Delivered Inst. Lumi."+"("+lumi_unit+");"+"Number of LS",nbins,minLumi,maxLumi)
    
    ## 2D hist Inst. lumi per Runs
    plot_hist2DbyRun=ROOT.TH2F("h2","",100,iniRun,lastRun,50,minLumi,maxLumi)    
    plot_VsTime=ROOT.TGraph()       
    plot_IntVsTime=ROOT.TGraph()       
    plot_IntVsRun=ROOT.TGraph()        
    plot_IntVsRunNoEmpty=ROOT.TGraph()    
    plot_IntVsNls=ROOT.TGraph()
    #    pointRange=0
#    plot_IntVsNlsInRange=ROOT.TGraph()
#    plot_IntVsNlsInRange.SetTitle("Recorded Integrated Luminosity vs nLs in: "+str(range_ini)+"->"+str(range_end))
#    plot_IntVsNlsInRange.GetXaxis().SetTitle("nLs")
#    plot_IntVsNlsInRange.GetYaxis().SetTitle("Reacorded Integrated Lumi."+lumi_IntUnit)
        
    plot_VsTimeX0=ROOT.TGraph()    
    plot_VsNls=ROOT.TGraph()
    
    #delivered    
    plot_del_VsTime=ROOT.TGraph()        
    plot_del_IntVsTime=ROOT.TGraph()        
    plot_del_IntVsRun=ROOT.TGraph()        
    plot_del_IntVsRunNoEmpty=ROOT.TGraph()    
    plot_del_IntVsNls=ROOT.TGraph()
    plot_del_VsTimeX0=ROOT.TGraph()    
    plot_del_VsNls=ROOT.TGraph()
    

    

    
    
    
    ## Filling the plots
    point=0
    pointRun=0
    intLumi=0
    intLumiRun=0
    del_intLumi=0
    del_intLumiRun=0
    current_run=sortedKeys[0][1]
    for key in sortedKeys:
        if key[0]>=rangeFill_ini and key[0]<=rangeFill_end:
            
            intLumi+=dict[key][1]*xLS
            del_intLumi+=dict[key][2]*xLS
        
            if key[1]!=current_run or point==totNls_inRangeFills-1:
                current_run=key[1]
                
                plot_IntVsRun.SetPoint(pointRun,current_run,intLumi)
                plot_IntVsRunNoEmpty.SetPoint(pointRun,pointRun,intLumi)
                
                plot_del_IntVsRun.SetPoint(pointRun,current_run,del_intLumi)
                plot_del_IntVsRunNoEmpty.SetPoint(pointRun,pointRun,del_intLumi)
                
                pointRun+=1
            
            det_index=detectors.index(dict[key][3])
            plots_byDetc_VsTime[det_index].SetPoint(pointsDetc[det_index],dict[key][0],dict[key][1])
            pointsDetc[det_index]+=1
            
            plot_hist.Fill(dict[key][1])           
            plot_hist2DbyRun.Fill(key[1],dict[key][1])           
            plot_VsTime.SetPoint(point,dict[key][0],dict[key][1])           
            plot_VsTimeX0.SetPoint(point,dict[key][0]-iniTime,dict[key][1])        
            plot_VsNls.SetPoint(point,point,dict[key][1])           
            plot_IntVsTime.SetPoint(point,dict[key][0]-iniTime,intLumi)           
            plot_IntVsNls.SetPoint(point,point,intLumi)            
            
            #delivered
            plot_del_hist.Fill(dict[key][2])
            plot_del_VsTime.SetPoint(point,dict[key][0],dict[key][2])
            plot_del_VsTimeX0.SetPoint(point,dict[key][0]-iniTime,dict[key][2])
            plot_del_VsNls.SetPoint(point,point,dict[key][2])
            plot_del_IntVsTime.SetPoint(point,dict[key][0]-iniTime,del_intLumi)
            plot_del_IntVsNls.SetPoint(point,point,del_intLumi)
            
            point+=1
    
    plot_mult_VsTime,leg_mult_VsTime=plot_multiple_Tgraphs(plots_byDetc_VsTime,
                                                           "Inst. Lumi. Vs Time for the different detectors",
                                                           detectors,"Time","Reacorded Inst. Lumi."+lumi_unit,"AP")   
    
    plot_VsTime.SetTitle("Recorded Inst. Luminosity vs Time")
    plot_VsTime.GetXaxis().SetTitle("Time")
    plot_VsTime.GetYaxis().SetTitle("Recorded Inst. Lumi."+lumi_unit)
    
    plot_hist2DbyRun.SetTitle("Recorded Inst. Luminosity by Runs")
    plot_hist2DbyRun.GetXaxis().SetTitle("Runs")
    plot_hist2DbyRun.GetYaxis().SetTitle("Recorded Inst. Lumi."+lumi_unit)
    
    plot_IntVsTime.SetTitle("Recorded Integrated Luminosity vs Time")
    plot_IntVsTime.GetXaxis().SetTitle("Time")
    plot_IntVsTime.GetYaxis().SetTitle("Recorded Integrated Lumi."+lumi_IntUnit)
    
    plot_IntVsRun.SetTitle("Recorded Integrated Luminosity vs Run")
    plot_IntVsRun.GetXaxis().SetTitle("Run")
    plot_IntVsRun.GetYaxis().SetTitle("Recorded Integrated Lumi."+lumi_IntUnit)
    
    plot_IntVsRunNoEmpty.SetTitle("Recorded Integrated Luminosity vs nRun")
    plot_IntVsRunNoEmpty.GetXaxis().SetTitle("nRun")
    plot_IntVsRunNoEmpty.GetYaxis().SetTitle("Recorded Integrated Lumi."+lumi_IntUnit)
    
    plot_VsNls.SetTitle("Recorded Inst. Luminosity vs nLs")
    plot_VsNls.GetXaxis().SetTitle("nLs")
    plot_VsNls.GetYaxis().SetTitle("Recorded Inst. Lumi."+lumi_unit)
    
    plot_IntVsNls.SetTitle("Recorded Integrated Luminosity vs nLs")
    plot_IntVsNls.GetXaxis().SetTitle("nLs")
    plot_IntVsNls.GetYaxis().SetTitle("Recorded Integrated Lumi."+lumi_IntUnit)
    
    ##delivered
    plot_del_VsTime.SetTitle("Delivered Inst. Luminosity vs Time")
    plot_del_VsTime.GetXaxis().SetTitle("Time")
    plot_del_VsTime.GetYaxis().SetTitle("Delivered Inst. Lumi."+lumi_unit)
    plot_del_VsTime.GetYaxis().SetRangeUser(del_minLumi,16000.0)
    
    plot_del_IntVsTime.SetTitle("Delivered Integrated Luminosity vs Time")
    plot_del_IntVsTime.GetXaxis().SetTitle("Time")
    plot_del_IntVsTime.GetYaxis().SetTitle("Delivered Integrated Lumi."+lumi_IntUnit)
    
    plot_del_IntVsRun.SetTitle("Delivered Integrated Luminosity vs Run")
    plot_del_IntVsRun.GetXaxis().SetTitle("Run")
    plot_del_IntVsRun.GetYaxis().SetTitle("Delivered Integrated Lumi."+lumi_IntUnit)
    
    plot_del_IntVsRunNoEmpty.SetTitle("Delivered Integrated Luminosity vs nRun")
    plot_del_IntVsRunNoEmpty.GetXaxis().SetTitle("nRun")
    plot_del_IntVsRunNoEmpty.GetYaxis().SetTitle("Delivered Integrated Lumi."+lumi_IntUnit)
    
    plot_del_VsNls.SetTitle("Delivered Inst. Luminosity vs nLs")
    plot_del_VsNls.GetXaxis().SetTitle("nLs")
    plot_del_VsNls.GetYaxis().SetTitle("Delivered Inst. Lumi."+lumi_unit)
    
    plot_del_IntVsNls.SetTitle("Delivered Integrated Luminosity vs nLs")
    plot_del_IntVsNls.GetXaxis().SetTitle("nLs")
    plot_del_IntVsNls.GetYaxis().SetTitle("Delivered Integrated Lumi."+lumi_IntUnit)
    
    ### Saving Plots to .pdf
    sqcan=ROOT.TCanvas()
    can=ROOT.TCanvas("solocan","solocan",900,300)
    saveintoCanvas_and_toFile(can,plot_hist,"hist",
                                   outputDIR+ "/" + "physics_Histo.pdf")
    saveintoCanvas_and_toFile(can,plot_hist2DbyRun,"COLZ",
                                   outputDIR+ "/" + "physics_hist2DbyRun.pdf")
    saveintoCanvas_and_toFile(can,plot_VsTime,"AL",
                                   outputDIR+ "/" + "physics_vsTime.pdf")
    saveintoCanvas_and_toFile(can,plot_VsTimeX0,"AL",
                                   outputDIR+ "/" + "physics_vsTimeX0.pdf")
    saveintoCanvas_and_toFile(can,plot_IntVsTime,"ALP",
                                   outputDIR+ "/" + "physics_IntvsTime.pdf")
    saveintoCanvas_and_toFile(can,plot_IntVsRun,"ALP",
                                   outputDIR+ "/" + "physics_IntvsRun.pdf")
    saveintoCanvas_and_toFile(can,plot_IntVsRunNoEmpty,"ALP",
                                   outputDIR+ "/" + "physics_IntvsnRun.pdf")
    saveintoCanvas_and_toFile(can,plot_IntVsNls,"ALP",
                                   outputDIR+ "/" + "physics_IntvsNls.pdf")
    #plot_VsNls.GetYaxis().SetRangeUser(-2.5,2.5)
    saveintoCanvas_and_toFile(can,plot_VsNls,"ALP",
                                   outputDIR+ "/" + "physics_vsNls.pdf")
    saveintoCanvas_and_toFile_withLeg(can,plot_mult_VsTime,leg_mult_VsTime,"a",
                                   outputDIR+ "/" + "physics_mult_VsTime.pdf")
    
    #delivered
    saveintoCanvas_and_toFile(can,plot_del_hist,"hist",
                                   outputDIR+ "/" + "physics_del_Histo.pdf")
    saveintoCanvas_and_toFile(can,plot_del_VsTime,"AL",
                                   outputDIR+ "/" + "physics_del_vsTime.pdf")
    saveintoCanvas_and_toFile(can,plot_del_VsTimeX0,"AL",
                                   outputDIR+ "/" + "physics_del_vsTimeX0.pdf")
    saveintoCanvas_and_toFile(can,plot_del_IntVsTime,"ALP",
                                   outputDIR+ "/" + "physics_del_IntvsTime.pdf")
    saveintoCanvas_and_toFile(can,plot_del_IntVsRun,"ALP",
                                   outputDIR+ "/" + "physics_del_IntvsRun.pdf")
    saveintoCanvas_and_toFile(can,plot_del_IntVsRunNoEmpty,"ALP",
                                   outputDIR+ "/" + "physics_del_IntvsnRun.pdf")
    saveintoCanvas_and_toFile(can,plot_del_IntVsNls,"ALP",
                                   outputDIR+ "/" + "physics_del_IntvsNls.pdf")
 
    
    
    ## Final Info in console
    print ("\n")
    print ("(Fill:Run:Time) range:  (",iniFill,":",iniRun,":",iniTime,") -> (",lastFill,":",lastRun,":",lastTime,")")
    print ("min lumi value: ",minLumi)
    print ("max lumi value: ",maxLumi)
    print ("min delivered lumi value: ",del_minLumi)
    print ("max delivered lumi value: ",del_maxLumi)
    
    print ("total integrated lumi: ",integratedTotalLumi,lumi_IntUnit)
    print ("Number and Total Lumi of negatives values: ",negV,negVlumi*xLS,lumi_IntUnit)
    print ("Number and Total Lumi of negatives values (delivered): ",del_negV,del_negVlumi*xLS,lumi_IntUnit)
    print ("Porcent of negatives values (Nls,Lumi): ",float(negV)*100/totNls," , ",abs(negVlumi)*100*xLS/integratedTotalLumi)
    print ("\n")
    print ("Detectors Info:")
    for detc in detectors:
        print (detc," >> ",detectorsInfoPorcent[detc][0],"% of total Nls, ",detectorsInfoPorcent[detc][1],"% of total Lumi. \n")
    
    print ("\n")
    if len(excludedFills)!=0:
        print ("Fill exclusion analysis for fills: ", excludedFills)
        print ("Total excluded Integrated Lumi.: ", excludedIntegratedTotalLumi," ",lumi_IntUnit," (",excludedIntegratedTotalLumi*100/integratedTotalLumi,"% of Tot. Lumi.)")
        print ("Total Lumi after exclusion: ",integratedTotalLumi-excludedIntegratedTotalLumi," ",lumi_IntUnit)
    
    print (totNls,totNls_inRangeFills)

def physicsFileAnalisysCompare(dict1,dict2,labels,outputDIR,year=0,energy=0):
    print("******************* Running best/second best anslysis *******************")
    nbins=200
    min_val=0.96
    max_val=1.04
    nls=50
    sortedKeys=list(set(dict1).intersection(dict2))
    sortedKeys.sort()
    lumi_unit=labels[6].replace("delivered(","")
    lumi_unit=lumi_unit.replace(")","")
    lumi_IntUnit=lumi_unit.replace("hz/","1/")
    colors=[46, 8, 9, 42, 38, 30, 2, 3, 4, 5, 6, 7, 8]
    #convertion to fb-1
    lumiConvert=1000000000.0
    integrationUnitLabel="fb-1"
    
    detectorsPhys=[]
    detectorsComp=[]
    detectorsPairs=[]
    equalValuesFound=0
    ## for computing usage percent: dict["label"]= [nls/totLS,lum/totLum]
    detectorsPhysUsg={}
    detectorsCompUsg={}
    detectorsPairsUsg={}
    
    totPhysLumi=0.0
    totCompLumi=0.0
    
    totPhysNls=0.0
    totCompNls=0.0
    
    for key in sortedKeys:
        lumi1=dict1[key][1]
        lumi2=dict2[key][1]    
        
        if (dict1[key][3]==dict2[key][3]):
            equalValuesFound+=1
            print("***** Warining: "+dict1[key][3]+"/"+dict2[key][3]+" in ",key)
            continue
        if dict1[key][3] not in detectorsPhys:
            detectorsPhys.append(dict1[key][3])
            detectorsPhysUsg[dict1[key][3]]=[0.0,0.0]
            
        if dict2[key][3] not in detectorsComp:
            detectorsComp.append(dict2[key][3])
            detectorsCompUsg[dict2[key][3]]=[0.0,0.0]
        
        if (dict1[key][3]+"/"+dict2[key][3]) not in detectorsPairs:
            detectorsPairs.append(dict1[key][3]+"/"+dict2[key][3])
            detectorsPairsUsg[dict1[key][3]+"/"+dict2[key][3]]=[0.0,0.0]
        
        detPhysLs=detectorsPhysUsg[dict1[key][3]][0]
        detPhysLumi=detectorsPhysUsg[dict1[key][3]][1]
        
        detCompLs=detectorsCompUsg[dict2[key][3]][0]
        detCompLumi=detectorsCompUsg[dict2[key][3]][1]
        
        detPairsLs=detectorsPairsUsg[dict1[key][3]+"/"+dict2[key][3]][0]
        detPairsLumi=detectorsPairsUsg[dict1[key][3]+"/"+dict2[key][3]][1]
        
        detPhysLs+=1
        detCompLs+=1
        
        detPhysLumi+=lumi1
        detCompLumi+=lumi2
        
        detPairsLs+=1
        detPairsLumi+=lumi1
        
        detectorsPhysUsg[dict1[key][3]]=[detPhysLs,detPhysLumi]
        detectorsCompUsg[dict2[key][3]]=[detCompLs,detCompLumi]
        detectorsPairsUsg[dict1[key][3]+"/"+dict2[key][3]] = [detPairsLs,detPairsLumi] 
        
        totPhysLumi+=lumi1
        totCompLumi+=lumi2
        totPhysNls+=1
        totCompNls+=1
    
    
    print("best detectors, #ls/totLS (%), #lumi/totLumi (%)")
    for det in detectorsPhys:
        detLs=detectorsPhysUsg[det][0]
        detLumi=detectorsPhysUsg[det][1]
        detectorsPhysUsg[det]=[detLs/totPhysNls,detLumi/totPhysLumi]
        print(det,detLs*100/totPhysNls,detLumi*100/totPhysLumi)
    print("second best detectors (comparison), #ls/totLS (%), #lumi/totLumi (%)")
    for det in detectorsComp:
        detLs=detectorsCompUsg[det][0]
        detLumi=detectorsCompUsg[det][1]
        detectorsCompUsg[det]=[detLs/totCompNls,detLumi/totCompLumi]
        print(det,detLs*100/totCompNls,detLumi*100/totCompLumi)
    print("detector pairs, #ls/totLS (%), #lumi/totLumi (%)")
    for detpair in detectorsPairs:
        detLs=detectorsPairsUsg[detpair][0]
        detLumi=detectorsPairsUsg[detpair][1]
        detectorsPairsUsg[detpair]=[detLs/totPhysNls,detLumi/totPhysLumi]
        print(detpair,detLs*100/totCompNls,detLumi*100/totCompLumi)
    print("Total luimositiy in Physics: "+str(totPhysLumi*setts.xLS/lumiConvert)+" ["+integrationUnitLabel+" ]")
    print("Total luimositiy in Compare: "+str(totCompLumi*setts.xLS/lumiConvert)+" ["+integrationUnitLabel+" ]")
    print("Total luimositiy difference in Compare and Physics (%): "+str(abs(totCompLumi-totPhysLumi)*100/totPhysLumi)+" % ")
    
    plots_RatiosbyDetc_VsTime=[]
    plot_RatiosbyNls_VsTime=ROOT.TGraph()
    plot_RatiosbyNls_VsNls=ROOT.TGraph()
    plot_RatiosbyRun_VsRun=ROOT.TGraph()
    plots_RatiosbyDetc_noempty=[]
    plots_RatiosbyDetc_VsRun=[]
    plots_RatiosbyDetc_VsIntegratedLumi=[]
    plots_HistobyDetc=[]
    
    plots_Ratios_histo=ROOT.TH1F("histPhys","; ratio; Lumi sections",nbins,min_val,max_val)
    plots_Ratios_histo_w=ROOT.TH1F("histPhysw",";best/second luminometer ratio; Luminosity [fb^{-1}]",nbins,min_val,max_val)
    pointsDetc=[]
   
    for detc in detectorsPairs:
        plots_RatiosbyDetc_VsTime.append(ROOT.TGraph())
        plots_RatiosbyDetc_noempty.append(ROOT.TGraph())
        plots_RatiosbyDetc_VsRun.append(ROOT.TGraph())
        plots_RatiosbyDetc_VsIntegratedLumi.append(ROOT.TGraph())
        plots_HistobyDetc.append(ROOT.TH1F("histPhys"+detc,"; ratio; Lumi sections",nbins,min_val,max_val))
        
        pointsDetc.append(0)
    
    errsInDiv=0
    errsInDivSign=0
    ls=0
    iBinls=0
    lumiNls1=0.0
    lumiNls2=0.0
    lumi1=0.0
    
    for key in sortedKeys:
        lumiNls1+=dict1[key][1]
        lumiNls2+=dict2[key][1]
        ls+=1
        if ls==nls:
            divls=lumiNls1/lumiNls2
            plot_RatiosbyNls_VsTime.SetPoint(iBinls,dict1[key][0],divls)
            plot_RatiosbyNls_VsNls.SetPoint(iBinls,iBinls,divls)               
            
            lumiNls1=0
            lumiNls2=0
            ls=0
            iBinls+=1
                
        try:
            div=dict1[key][1]/dict2[key][1]
            if div<0:
                errsInDivSign+=1
                continue
        except:
            errsInDiv+=1
            continue
        lumi1+=dict1[key][1]
        plots_Ratios_histo.Fill(div)
        plots_Ratios_histo_w.Fill(div,dict1[key][1]*23.31/1000000000)
        
        if (dict1[key][3]!=dict2[key][3]):
            det_index=detectorsPairs.index(dict1[key][3]+"/"+dict2[key][3])
            plots_HistobyDetc[det_index].Fill(div,dict1[key][1]*23.31/1000000000)
            plots_RatiosbyDetc_VsTime[det_index].SetPoint(pointsDetc[det_index],dict1[key][0],div)
            plots_RatiosbyDetc_VsRun[det_index].SetPoint(pointsDetc[det_index],key[1],div)
            plots_RatiosbyDetc_noempty[det_index].SetPoint(pointsDetc[det_index],pointsDetc[det_index],div)
            plots_RatiosbyDetc_VsIntegratedLumi[det_index].SetPoint(pointsDetc[det_index],lumi1*23.31/1000000000.,div)        
            pointsDetc[det_index]+=1
    
    plots_Ratios_histo_w.SetFillColor(38)
    
    
    
    
#    plot_multRatios_VsTime,leg_multRatios_VsTime=plot_compare_multiple_detectorsOptColorList(plots_RatiosbyDetc_VsTime,detectorsPairs,
#                                                                                               "Inst. Luminosity ratios showing also the reference and comparison detector vs Timestamp",
#                                                                                               "Time","ratio","AP",colors)
#    plot_multRatios_noempty,leg_multRatios_noempty=plot_compare_multiple_detectorsOptColorList(plots_RatiosbyDetc_noempty,detectorsPairs,
#                                                                                               "Inst. Luminosity ratios showing also the reference and comparison detector vs number of valid LS",
#                                                                                               "nLS","ratio","AP",colors)
#    plot_multRatios_VsRun,leg_multRatios_VsRun=plot_compare_multiple_detectorsOptColorList(plots_RatiosbyDetc_VsRun,detectorsPairs,
#                                                                                               "Inst. Luminosity ratios showing also the reference and comparison detector vs number of valid LS",
#                                                                                               "Run","ratio","AP",colors)
#    plot_multRatios_VsLumi,leg_multRatios_VsLumi=plot_compare_multiple_detectorsOptColorList(plots_RatiosbyDetc_VsIntegratedLumi,detectorsPairs,
#                                                                                               "Inst. Luminosity ratios showing also the reference and comparison detector vs Integrated Luminosity",
#                                                                                               "Integrated Luminosity [fb^{-1}]","ratio","AP",colors)
    plot_multRatios_VsTime,leg_multRatios_VsTime=plot_compare_multiple_detectorsOptColorList(plots_RatiosbyDetc_VsTime,detectorsPairs,
                                                                                               "",
                                                                                               "Timestamp","ratio","AP",colors)
    #plot_multRatios_noempty,leg_multRatios_noempty=plot_compare_multiple_detectorsOptColorList(plots_RatiosbyDetc_noempty,detectorsPairs,
#                                                                                               "",
#                                                                                               "nLS","ratio","AP",colors)
    plot_multRatios_VsRun,leg_multRatios_VsRun=plot_compare_multiple_detectorsOptColorList(plots_RatiosbyDetc_VsRun,detectorsPairs,
                                                                                               "",
                                                                                               "Run","ratio","AP",colors)
    plot_multRatios_VsLumi,leg_multRatios_VsLumi=plot_compare_multiple_detectorsOptColorList(plots_RatiosbyDetc_VsIntegratedLumi,detectorsPairs,
                                                                                               "",
                                                                                               "Integrated Luminosity [fb^{-1}]","ratio","AP",colors)
    
    plot_multRatios_VsTime.GetYaxis().SetRangeUser(min_val, max_val)
#    plot_multRatios_noempty.GetYaxis().SetRangeUser(min_val, max_val)
    plot_multRatios_VsRun.GetYaxis().SetRangeUser(min_val, max_val)
    plot_multRatios_VsLumi.GetYaxis().SetRangeUser(min_val, max_val)
    
    
    can=ROOT.TCanvas("solocan","solocan",900,300)
    sqrcan=ROOT.TCanvas("sqcan","sqcan",600,600)
    saveintoCanvas_and_toFile_withLeg(can,plot_multRatios_VsTime,leg_multRatios_VsTime,"a",outputDIR+ "/" + "physics_multRatios_VsTime.pdf")
    #saveintoCanvas_and_toFile_withLeg(can,plot_multRatios_noempty,leg_multRatios_noempty,"a",outputDIR+ "/" + "physics_multRatios_noempty.pdf")
    saveintoCanvas_and_toFile_withLeg(can,plot_multRatios_VsRun,leg_multRatios_VsRun,"a",outputDIR+ "/" + "physics_multRatios_vsRun.pdf")
    saveintoCanvas_and_toFileWIP_withLeg(can,plot_multRatios_VsLumi,leg_multRatios_VsLumi,"a",outputDIR+ "/" + "physics_multRatios_vsLumi.pdf","il_nsq",yearLabel=year,energy=energy)
    ROOT.gStyle.SetOptStat(0)
    #ROOT.gStyle.SetOptStat(1110)
    saveintoCanvas_and_toFile(sqrcan,plots_Ratios_histo,"HIST",outputDIR + "/" +"_plot_Ratios_histo.pdf") 
    saveintoCanvas_and_toFileWIP(sqrcan,plots_Ratios_histo_w,"HIST",outputDIR + "/" +"_plot_Ratios_histo_w.pdf","il_sq")
    saveintoCanvas_and_toFile(sqrcan,plot_RatiosbyNls_VsNls,"AL",outputDIR + "/" +"_plot_RatiosbyNls_VsNls.pdf")
    saveintoCanvas_and_toFile(sqrcan,plot_RatiosbyNls_VsTime,"AL",outputDIR + "/" +"_plot_RatiosbyNls_VsTime.pdf")       
    
    ###ALL hist plots
    plotAllHist= ROOT.THStack("hsall_exclude","")
    plotAllHist.SetTitle(";Detector ratios;Integrated Luminosity [fb^{-1}]")
    
    legAllHist = ROOT.TLegend (.87,.6,.6,.88)
    legAllHist.SetFillColor(0)
    legAllHist.SetTextFont(50)
    legAllHist.SetTextSize(.04)
    legAllHist.SetBorderSize(0)
    
    
    plots_Ratios_histo_w.SetFillColor(setts.getColorForDetectorPair("best/second"))
    plots_Ratios_histo_w.SetLineColor(setts.getColorForDetectorPair("best/second"))
    
    legAllHist.AddEntry(plots_Ratios_histo_w,"best/second","f")
    plotAllHist.Add(plots_Ratios_histo_w,"HIST")
    
    
    i=1
    for pdetc in detectorsPairs:
        #print (pdetc)
        det_index=detectorsPairs.index(pdetc)
        #print(det_index)
        color=setts.getColorForDetectorPair(pdetc.lower())
        plots_HistobyDetc[det_index].SetFillColor(color)
        plots_HistobyDetc[det_index].SetLineColor(color)
        
        legAllHist.AddEntry(plots_HistobyDetc[det_index],pdetc,"f")
        plotAllHist.Add(plots_HistobyDetc[det_index],"HIST C")        
        i+=1

    saveintoCanvas_and_toFileWIP_withLeg(sqrcan,plotAllHist,legAllHist,"nostack",
                                   outputDIR+ "/" +"_plotAllHist.pdf","il_sq")   
    

def defineWx1detc(L,totLumi):
    w=[]
    
    for l in L:
        w.append(l/totLumi)    
    return w


def defineWxAlldetc(L1,L2,totLumi1,totLumi2):
    w=[]
    totLumi12=totLumi1+totLumi2
    for i in range(0,(len(L1)-1)):
        w.append((L1[i]+L2[i])/totLumi12)    
    
    return w

def get_uncertainty(keys,dict1,dict2,dictp):
    tOfficialLumi=0.0
    tLumi1=0.0
    tLumi2=0.0
    dumpedLS=0
    validKeys=[]
    slRanges=[]
    
    for key in keys:
        try:
            if dictp[key][1]>=0 and dict1[key][1]>=0 and dict2[key][1]>=0:
               #Very important: valid keys for futures loops
               validKeys.append(key)
               tOfficialLumi+=dictp[key][1]
               tLumi1+=dict1[key][1]
               tLumi2+=dict2[key][1]
            else:
                dumpedLS+=1
                
        except:
            pass
    print ("Dicts Sizes and ratio: ", len(keys),len(validKeys),float(len(validKeys))/float(len(keys)))
    print ("TotLumiPHY,totLumi1,totLumi2: ",tOfficialLumi,tLumi1,tLumi2)
    print ("totLumi1/TotLumiPHY,totLumi2/TotLumiPHY,totLumi1/totLumi2: ",tLumi1/tOfficialLumi,tLumi2/tOfficialLumi,tLumi1/tLumi2)
    print ("%-->(totLumi1-TotLumiPHY)/TotLumiPHY,(totLumi2-TotLumiPHY)/TotLumiPHY,(totLumi1-totLumi2)/TotLumiPHY: ",abs(tLumi1-tOfficialLumi)*100/tOfficialLumi,abs(tLumi2-tOfficialLumi)*100/tOfficialLumi,abs(tLumi1-tLumi2)*100/tOfficialLumi)
    Nls=len(validKeys)
    diffAll=[]
    for key in validKeys:
        diff
                


########################################
##############    PLOTS    #############
########################################


## Two detectors Ratio Histo, returns TH1F  
def histo_ratio_2detectors(overlapKeys,dict1,dict2,labelRatio,nbins,min_val,max_val):   
    
    ratio=ROOT.TH1F(labelRatio,";ratio "+labelRatio+";",nbins,min_val,max_val)
    #ratio=ROOT.TH1F("ratio",";ratio "+labelRatio+";",200,0.002,0.004)
    #ratio.SetFillStyle(3001)
    #atio.SetFillColor(3)
    
    ratio.linewidth = 1

    iBin=0
   
    for key in overlapKeys:
        try:
            num=dict1[key][1]
            den=dict2[key][1]
            ratio.Fill(num/den)
        

            iBin+=1 
        
        except:
            pass
    
    return ratio

def histo_ratio_2detectorsNls_w(ratios,w,nls,labelRatio,nbins,min_val,max_val):   
    
    plot=ROOT.TH1F(labelRatio+" w_histo x"+str(nls)+"LS",";ratio "+labelRatio+";",nbins,min_val,max_val)
    #ratio=ROOT.TH1F("ratio",";ratio "+labelRatio+";",200,0.002,0.004)
    #ratio.SetFillStyle(3001)
    #atio.SetFillColor(3)
    
    plot.linewidth = 1

    for i in range(0,len(ratios)-1):
        plot.Fill(ratios[i],w[i])
        
    return plot

def histo_ratio_2detectorsNls(ratioNls,nls,labelRatio,nbins,min_val,max_val):   
    
    ratio=ROOT.TH1F(labelRatio+" histo x"+str(nls)+"LS",";ratio "+labelRatio+";",nbins,min_val,max_val)
    #ratio=ROOT.TH1F("ratio",";ratio "+labelRatio+";",200,0.002,0.004)
    #ratio.SetFillStyle(3001)
    #atio.SetFillColor(3)
    
    ratio.linewidth = 1

    for r in ratioNls:
        ratio.Fill(r)
        
    return ratio
    

# Ratio Plot vs time    
def ratioVsTime(ratios,time,labelRatio,ymin,ymax):
    SingleFillVsTime=ROOT.TGraph()
    #SingleFillVsTime_ls=ROOT.TGraph()
    SingleFillVsTime.SetTitle(labelRatio+"; Time(s); Recorded Lumi Ratio")
    SingleFillVsTime.SetMarkerStyle(20)
    SingleFillVsTime.SetMarkerSize(0.2)
    SingleFillVsTime.SetMarkerColor(2)
    SingleFillVsTime.SetLineColor(2)
    
    #SingleFillVsTime.GetYaxis().SetRangeUser(0.7, 1.3)
    
    iBin=0
    time0=time[0]
    
    for iBin in range(0,len(time)):
        try:
            SingleFillVsTime.SetPoint(iBin,time[iBin]-time0,ratios[iBin])
            
        except:
           pass

    SingleFillVsTime.GetYaxis().SetRangeUser(ymin, ymax)
    
    return SingleFillVsTime

#use an array (time,ratio)
def ratioVsTimeV2(ratios_time,labelRatio,ymin,ymax):
    SingleFillVsTime=ROOT.TGraph()
    #SingleFillVsTime_ls=ROOT.TGraph()
    SingleFillVsTime.SetTitle(labelRatio+"; Time(s); Recorded Lumi Ratio")
    SingleFillVsTime.SetMarkerStyle(20)
    SingleFillVsTime.SetMarkerSize(0.2)
    SingleFillVsTime.SetMarkerColor(2)
    SingleFillVsTime.SetLineColor(2)
    
    #SingleFillVsTime.GetYaxis().SetRangeUser(0.7, 1.3)
    
    iBin=0
    
    for iBin in range(0,len(ratios_time)):
        try:
            SingleFillVsTime.SetPoint(iBin,ratios_time[iBin][0],ratios_time[iBin][1])
            
        except:
           pass

    SingleFillVsTime.GetYaxis().SetRangeUser(ymin, ymax)
    
    return SingleFillVsTime


## Ratio Plot, can be use for avg_x_fill or run, output TGraph
def averageRatio_Root(ratios,x_values,xlabel,ylabel,title,ymin,ymax):
    f=ROOT.TGraph()
       
    pts=[]
    
    for i in range(0,len(x_values)):
        pts.append((x_values[i],ratios[i])) 
    
    pts.sort()
    
    x0=pts[0][0]
    
    iBin=0
        
    for iBin in range(0,len(x_values)):
        try:
            #f.SetPoint(iBin,pts[iBin][0]-x0,pts[iBin][1])
            f.SetPoint(iBin,pts[iBin][0],pts[iBin][1])
            
        except:
           pass
       
    
    f.SetMarkerColor(3)
    f.SetMarkerSize(0.5)
    f.SetMarkerStyle(20)
    f.SetLineColor(3)
    #f.SetLineStyle(2)
    #f.SetLineWidth(1)    
    f.SetFillColor(3)
    f.SetTitle(ylabel+" "+title)
    f.GetXaxis().SetTitle(xlabel)
    f.GetYaxis().SetTitle(ylabel)
    f.GetYaxis().SetRangeUser(ymin, ymax)
    
    return f
    

## For comparing an array of Histos using THStack class 
def histo_ratio_multiple_detectors(histos,labelratios):    
    hs = ROOT.THStack("hs","Inst. Lumi Ratios Histograms")
    leg = ROOT.TLegend (.1,.7,.3,.9)
    leg.SetFillColor(0)
    
    cl=1    
    for i in range(0,len(histos)):
        
        #histos[i].SetMarkerColor(2+i)
        #histos[i].SetFillColor(2+i)
        histos[i].SetMarkerStyle(21)
        histos[i].SetMarkerSize(0.2)
        
        if cl==10:
            cl=40
        
        if cl>49:
            cl=11
        
        #histos[i].SetMarkerColor(cl)        
        histos[i].SetLineColor(cl)
        
        #histos[i].SetFillColor(2+i)
        histos[i].SetFillStyle(0)
        #histos[i].SetFillColorAlpha(cl, 0.10)
        leg.AddEntry(histos[i],labelratios[i])
        hs.Add(histos[i])
        cl+=1
    
    return hs,leg

def histo_fromDict(dict,posVal,labelRatio,nbins,min_val,max_val):
    keys=list(dict)
    hist=ROOT.TH1F(labelRatio+"fromDictHistLS"+str(len(dict)),";"+labelRatio+" ratio;",nbins,min_val,max_val)
    for key in keys:
        hist.Fill(dict[key][posVal])
    
    return hist

def histo_fromDictLumiBin(dict,posVal,labelRatio,nbins,min_val,max_val,lumiXbin):
    keys=list(dict)
    hist=ROOT.TH1F(labelRatio+"fromDictHistLS"+str(len(dict)),";"+labelRatio+" ratio;",nbins,min_val,max_val)
    for key in keys:
        hist.Fill(dict[key][posVal],lumiXbin)
    
    return hist




def histoW_fromDict(dict,posVal,posW,labelRatio,nbins,min_val,max_val):
    keys=list(dict)
    hist=ROOT.TH1F(labelRatio+"fromDictWhist"+str(len(dict)),";ratio "+labelRatio+";",nbins,min_val,max_val)
    for key in keys:
        hist.Fill(dict[key][posVal],dict[key][posW]*23.31/1000000000.)
    
    return hist

def histoW_fromDictColor(dict,posVal,posW,labelRatio,nbins,min_val,max_val):
    keys=list(dict)
    hist=ROOT.TH1F(labelRatio+"fromDictWhistColor"+str(len(dict)),";ratio "+labelRatio+";",nbins,min_val,max_val)
    for key in keys:
        hist.Fill(dict[key][posVal],dict[key][posW]*23.31/1000000000.)
    color=setts.getColorForDetectorPair(labelRatio)
    
    hist.SetLineColor(color)
    
    return hist

     
    
def plot_compare_multiple_detectors(plots,labelratios,xlabel,ylabel):    
    hs = ROOT.TMultiGraph()
    hs.SetTitle("Inst. Lumi Ratios;"+xlabel+";"+ylabel)
    leg = ROOT.TLegend (.62,.78,.39,.88)
    leg.SetFillColor(0)
    leg.SetNColumns(len(labelratios))
    leg.SetTextFont(42)
    leg.SetTextSize(.03)
    leg.SetBorderSize(0)
    cl=1
    for i in range(0,len(plots)):
        #plots[i].SetMarkerColor(2+i)
        #plots[i].SetFillColor(2+i)
        plots[i].SetMarkerStyle(21)
        plots[i].SetMarkerSize(0.05)
        #plots[i].SetTitle(labelratios[i] + "Recorded Lumi Ratio")
        
        if cl==10:
            cl=41
        
        if cl>49:
            cl=11
            
           
        plots[i].SetMarkerColor(cl)        
        plots[i].SetLineColor(cl)
        #plots[i].SetFillColor(2+i)
        #plots[i].SetFillStyle(3005)
        leg.AddEntry(plots[i],labelratios[i].upper(),"l")       
        hs.Add(plots[i],"PL")
        cl+=1
        
        
    
    return hs,leg

def plot_compare_multiple_detectorsColorList(plots,labelratios,title,xlabel,ylabel,plotOpt="PL",legOpt="l",markerS=0.05):    
    hs = ROOT.TMultiGraph()
    hs.SetTitle(title+";"+xlabel+";"+ylabel)
    x1=0.5+len(plots)*0.11/2.
    x2=0.5-len(plots)*0.11/2.
    leg = ROOT.TLegend (x1,.81,x2,.91)
    leg.SetFillColor(0)
    leg.SetNColumns(len(labelratios))
    leg.SetTextFont(42)
    leg.SetTextSize(.06)
    leg.SetBorderSize(0)
    if plotOpt=="AP":
        markerS=0.3
    
    cl=0
    for i in range(0,len(plots)):
        #plots[i].SetMarkerColor(2+i)
        #plots[i].SetFillColor(2+i)
        plots[i].SetMarkerStyle(21)
        plots[i].SetMarkerSize(markerS)
        #plots[i].SetTitle(labelratios[i] + "Recorded Lumi Ratio")
        plots[i].SetMarkerColor(setts.getColorForDetectorPair(labelratios[i]))        
        plots[i].SetLineColor(setts.getColorForDetectorPair(labelratios[i]))
        #plots[i].SetFillColor(2+i)
        #plots[i].SetFillStyle(3005)
        leg.AddEntry(plots[i],labelratios[i].upper(),legOpt)       
        hs.Add(plots[i],plotOpt)
        cl+=1
    
    return hs,leg

def plot_compareInclExcl_multiple_detectorsColorList(plotsWithExcl,plotsExcl,labelratios,title,xlabel,ylabel,plotOpt="AP",plotStyle=0):    
    hs = ROOT.TMultiGraph("test","test")
    hs.SetTitle(title+";"+xlabel+";"+ylabel)
    x1=0.5+len(plotsWithExcl)*0.2/2.
    x2=0.5-len(plotsWithExcl)*0.2/2.
    leg = ROOT.TLegend (x1,.81,x2,.91)
    leg.SetFillColor(0)
    leg.SetNColumns(len(labelratios))
    leg.SetTextFont(42)
    leg.SetTextSize(.06)
    leg.SetBorderSize(0)
    markerS=0.4
    
    altMarkers=[24,25,26,32]
    altMarkersWithExcl=[20,21,22]
    
    cl=0
    
    ## Option for excluded points in red, diferent simbol and color for each detector
    if plotStyle==0:
        for i in range(0,len(plotsWithExcl)):
            plotsWithExcl[i].SetMarkerStyle(altMarkers[cl])
            plotsWithExcl[i].SetMarkerSize(markerS)
            #plotsWithExcl[i].SetMarkerColor(setts.getColorForDetectorPair(labelratios[i]))        
            #plotsWithExcl[i].SetLineColor(setts.getColorForDetectorPair(labelratios[i]))
            plotsWithExcl[i].SetMarkerColorAlpha(setts.getColorForDetectorPair(labelratios[i]),0.5)        
            
            plotsExcl[i].SetMarkerStyle(altMarkers[cl])
            plotsExcl[i].SetMarkerSize(0.8)
            #plotsExcl[i].SetMarkerColorAlpha(setts.getColorForDetectorPair(labelratios[i]),1)  
            plotsExcl[i].SetMarkerColorAlpha(2,1)  
            #plotsExcl[i].SetLineColor(setts.getColorForDetectorPair(labelratios[i]))
            leg.AddEntry(plotsWithExcl[i],labelratios[i].upper(),"p")
            hs.Add(plotsWithExcl[i],plotOpt)
            
            cl+=1
    
    ## Option for excluded points in a softer tone, diferent simbol and color for each detector
    elif plotStyle==1:
        for i in range(0,len(plotsWithExcl)):
            plotsWithExcl[i].SetMarkerStyle(altMarkersWithExcl[cl])
            plotsWithExcl[i].SetMarkerSize(markerS)
            #plotsWithExcl[i].SetMarkerColor(setts.getColorForDetectorPair(labelratios[i]))        
            #plotsWithExcl[i].SetLineColor(setts.getColorForDetectorPair(labelratios[i]))
            plotsWithExcl[i].SetMarkerColorAlpha(setts.getColorForDetectorPair(labelratios[i]),1)        
            
            #plotsExcl[i].SetMarkerStyle(5)
            plotsExcl[i].SetMarkerStyle(altMarkers[cl])
            
            plotsExcl[i].SetMarkerSize(0.05)
            #plotsExcl[i].SetMarkerColorAlpha(setts.getColorForDetectorPair(labelratios[i]),1)  
            plotsExcl[i].SetMarkerColorAlpha(setts.getColorForDetectorPair(labelratios[i]),0.8)  
            #plotsExcl[i].SetLineColor(setts.getColorForDetectorPair(labelratios[i]))
            leg.AddEntry(plotsWithExcl[i],labelratios[i].upper(),"p")
            hs.Add(plotsWithExcl[i],plotOpt)
            
            cl+=1
    
    ## Option for excluded points in a softer tone and cross simbol, different simbol and color for each detector
    elif plotStyle==2:
        for i in range(0,len(plotsWithExcl)):
            plotsWithExcl[i].SetMarkerStyle(altMarkers[cl])
            plotsWithExcl[i].SetMarkerSize(markerS)
            #plotsWithExcl[i].SetMarkerColor(setts.getColorForDetectorPair(labelratios[i]))        
            #plotsWithExcl[i].SetLineColor(setts.getColorForDetectorPair(labelratios[i]))
            plotsWithExcl[i].SetMarkerColorAlpha(setts.getColorForDetectorPair(labelratios[i]),1)        
            
            plotsExcl[i].SetMarkerStyle(5)
            #plotsExcl[i].SetMarkerStyle(altMarkers[cl])
            
            plotsExcl[i].SetMarkerSize(markerS+0.3)
            #plotsExcl[i].SetMarkerColorAlpha(setts.getColorForDetectorPair(labelratios[i]),1)  
            plotsExcl[i].SetMarkerColorAlpha(setts.getColorForDetectorPair(labelratios[i]),1)  
            #plotsExcl[i].SetLineColor(setts.getColorForDetectorPair(labelratios[i]))
            leg.AddEntry(plotsWithExcl[i],labelratios[i].upper(),"p")
            hs.Add(plotsWithExcl[i],plotOpt)
            
            cl+=1
            
    elif plotStyle==3:
        for i in range(0,len(plotsWithExcl)):
            plotsWithExcl[i].SetMarkerStyle(altMarkers[cl])
            plotsWithExcl[i].SetMarkerSize(markerS)
            #plotsWithExcl[i].SetMarkerColor(setts.getColorForDetectorPair(labelratios[i]))        
            #plotsWithExcl[i].SetLineColor(setts.getColorForDetectorPair(labelratios[i]))
            plotsWithExcl[i].SetMarkerColorAlpha(setts.getColorForDetectorPair(labelratios[i]),1)        
            
            plotsExcl[i].SetMarkerStyle(altMarkers[cl])
            
            plotsExcl[i].SetMarkerSize(markerS+0.2)
            #plotsExcl[i].SetMarkerColorAlpha(setts.getColorForDetectorPair(labelratios[i]),1)  
            plotsExcl[i].SetMarkerColorAlpha(setts.getColorForDetectorPair(labelratios[i]),0.08)  
            #plotsExcl[i].SetLineColor(setts.getColorForDetectorPair(labelratios[i]))
            leg.AddEntry(plotsWithExcl[i],labelratios[i].upper(),"p")
            hs.Add(plotsWithExcl[i],plotOpt)
            
            cl+=1
    
    for i in range(0,len(plotsWithExcl)):
        hs.Add(plotsExcl[i],plotOpt)
#        leg.AddEntry(plotsExcl[i],labelratios[i].upper()+"(Excl)","p")    
    
    return hs,leg


def plot_compare_multiple_detectorsOpts(plots,title,labelratios,xlabel,ylabel,optDraw):    
    hs = ROOT.TMultiGraph()
    hs.SetTitle(title+";"+xlabel+";"+ylabel)
    leg = ROOT.TLegend (.62,.78,.39,.88)
    leg.SetFillColor(0)
    leg.SetNColumns(len(labelratios))
    leg.SetTextFont(42)
    leg.SetTextSize(.03)
    leg.SetBorderSize(0)
    cl=1
    for i in range(0,len(plots)):
        #plots[i].SetMarkerColor(2+i)
        #plots[i].SetFillColor(2+i)
        plots[i].SetMarkerStyle(21)
        plots[i].SetMarkerSize(0.5)
        #plots[i].SetTitle(labelratios[i] + "Recorded Lumi Ratio")
        
        if cl==10:
            cl=41
        
        if cl>49:
            cl=11
            
           
        plots[i].SetMarkerColor(cl)        
        plots[i].SetLineColor(cl)
        #plots[i].SetFillColor(2+i)
        #plots[i].SetFillStyle(3005)
        leg.AddEntry(plots[i],labelratios[i].upper(),"l")       
        hs.Add(plots[i],optDraw)
        cl+=1
        
        
    
    return hs,leg

def plot_compare_multiple_detectorsOptColorList(plots,labelratios,title,xlabel,ylabel,optDraw,colors):    
    hs = ROOT.TMultiGraph()
    hs.SetTitle(title+";"+xlabel+";"+ylabel)
    x1=0.5+len(plots)*0.12/2.
    x2=0.5-len(plots)*0.12/2.
    leg = ROOT.TLegend (x1,.78,x2,.88)
    leg.SetFillColor(0)
    leg.SetNColumns(len(labelratios))
    leg.SetTextFont(42)
    leg.SetTextSize(.06)
    leg.SetBorderSize(0)
    cl=0
    for i in range(0,len(plots)):
        #plots[i].SetMarkerColor(2+i)
        #plots[i].SetFillColor(2+i)
        plots[i].SetMarkerStyle(20)
        plots[i].SetMarkerSize(0.3)
        #plots[i].SetTitle(labelratios[i] + "Recorded Lumi Ratio")
        color=setts.getColorForDetectorPair(labelratios[i].lower())         
        plots[i].SetMarkerColor(color)        
        plots[i].SetLineColor(color)
        #plots[i].SetFillColor(2+i)
        #plots[i].SetFillStyle(3005)
        leg.AddEntry(plots[i],labelratios[i].upper(),"l")       
        hs.Add(plots[i],optDraw)
        cl+=1
        
        
    
    return hs,leg

def plot_compare_All_with_exclude(plot1,plotall,labelRatio,title,xlabel,ylabel,colors):    
    hs = ROOT.THStack("hsall_exclude","Inst. Lumi Ratios Histograms")
    hs.SetTitle(title+";"+xlabel+";"+ylabel)
    leg = ROOT.TLegend (.69,.74,.56,.92)
    leg.SetFillColor(0)
    #leg.SetNColumns(2)
    leg.SetTextFont(42)
    leg.SetTextSize(.05)
    leg.SetBorderSize(0)
    color=setts.getColorForDetectorPair(labelRatio)
    
    
    
    plot1.SetLineColor(color)
    rms1=plot1.GetRMS()
    #plot1.SetLineStyle(2)
    plot1.SetFillColor(color)       
    rmsall=plotall.GetRMS()
    #plots[i].SetFillStyle(3005)
    leg.AddEntry(plot1,"good ("+str(float("{0:.4f}".format(rms1)))+")","f")       
    hs.Add(plot1,"HIST")

    plotall.SetLineColor(12)
    plotall.SetFillColor(color)
    plotall.SetFillStyle(3005) 
    #plots[i].SetFillStyle(3005)
    leg.AddEntry(plotall,"all ("+str(float("{0:.4f}".format(rmsall)))+")","f")       
    
    leg.SetHeader("Data (\sigma)","L")
    
    hs.Add(plotall,"HIST")
        
        
    
    return hs,leg

def plot_combine_hists(plots,labels,title,xlabel,ylabel,colors=[46, 8, 9, 42, 38, 30, 2, 3, 4, 5, 6, 7, 8]):    
    hs = ROOT.THStack("hsall_exclude","Inst. Lumi Ratios Histograms")
    hs.SetTitle(title+";"+xlabel+";"+ylabel)
    leg = ROOT.TLegend (.82,.76,.69,.94)
    leg.SetFillColor(0)
    #leg.SetNColumns(2)
    leg.SetTextFont(42)
    leg.SetTextSize(.05)
    leg.SetBorderSize(0)
    
    iplot=0
    for plot in plots:
        plot.SetLineColor(colors[iplot])
        plot.SetFillColor(colors[iplot])
        
        leg.AddEntry(plot,labels[iplot],"f")       
        hs.Add(plot,"HIST")
        iplot+=1
        
        
    
    return hs,leg


def plot_multiple_Tgraphs(plots,title,labels,xlabel,ylabel,stylePlot):    
    hs = ROOT.TMultiGraph()
    hs.SetTitle(title+";"+xlabel+";"+ylabel)
    leg = ROOT.TLegend (.62,.78,.39,.88)
    leg.SetFillColor(0)
    leg.SetNColumns(len(plots))
    leg.SetTextFont(42)
    leg.SetTextSize(.03)
    leg.SetBorderSize(0)
    cl=2
    for i in range(0,len(plots)):
        #plots[i].SetMarkerColor(2+i)
        #plots[i].SetFillColor(2+i)
        plots[i].SetMarkerStyle(21)
        plots[i].SetMarkerSize(0.1)
        if labels[i]=="PXL":
            plots[i].SetMarkerSize(0.4)
        elif labels[i]=="DT" or labels[i]=="HFOC":
            plots[i].SetMarkerSize(0.3)
        #plots[i].SetTitle(labelratios[i] + "Recorded Lumi Ratio")
        
        if cl==10:
            cl=41
        
        if cl>49:
            cl=11
            
           
        plots[i].SetMarkerColor(cl)        
        plots[i].SetLineColor(cl)
        #plots[i].SetFillColor(2+i)
        #plots[i].SetFillStyle(3005)
        leg.AddEntry(plots[i],labels[i].upper(),"pl")       
        hs.Add(plots[i],stylePlot)
        cl+=1
        
        
    
    return hs,leg

def plot_multiple_TgraphsErrs(plots,title,labels,xlabel,ylabel,stylePlot):    
    hs = ROOT.TMultiGraph()
    hs.SetTitle(title+";"+xlabel+";"+ylabel)
    leg = ROOT.TLegend (.90,.84,.77,.94)
    
    leg.SetTextFont(42)
    leg.SetTextSize(.03)
    leg.SetBorderSize(0)
    
    cl=0
    for plot in plots:
        plot.SetMarkerStyle(setts.markers[cl])
        plot.SetMarkerSize(0.6)
           
        plot.SetMarkerColor(setts.colors[cl])        
        plot.SetLineColor(setts.colors[cl])
        #plots[i].SetFillColor(2+i)
        #plots[i].SetFillStyle(3005)
        leg.AddEntry(plot,labels[cl],"pl")       
        hs.Add(plot,stylePlot)
        cl+=1
        
        
    
    return hs,leg


## For comparing an array of Histos using THStack class 
def averageRatio_Hist2D(ratios,x_values,xlabel,ylabel,title,ymin,ymax):
    
    
    
    pts=[]    
    for i in range(0,len(x_values)):
        pts.append((x_values[i],ratios[i])) 
    
    pts.sort()
    
    x0=pts[0][0]
    maxval=pts[len(x_values)-1][0]
    
    f=ROOT.TH2F("h2","",100,x0,maxval,50,ymin,ymax)
    
    
    
    iBin=0
        
    for iBin in range(0,len(x_values)):
        try:
            #f.Fill(pts[iBin][0]-x0,pts[iBin][1])
            f.Fill(pts[iBin][0],pts[iBin][1])
            
        except:
           pass
       
    #f.GetYaxis().SetRangeUser(ymin, ymax)
#    f.SetMarkerColor(4)
#    f.SetMarkerSize(0.5)
#    f.SetMarkerStyle(20)
#    f.SetLineColor(4)
#    f.SetFillColor(4)
    f.SetTitle(ylabel+" "+title)
    f.GetXaxis().SetTitle(xlabel)
    f.GetYaxis().SetTitle(ylabel)
    
    return f

def plot_ratio_noempty(ratios,xlabel,ylabel,title,ymin,ymax):
    f=ROOT.TGraph()
    
    
    
#    pts=[]    
#    for i in range(0,len(x_values)):
#        pts.append((x_values[i],ratios[i])) 
#    
#    pts.sort()
        
    iBin=0
        
    for iBin in range(0,len(ratios)):
        try:
            
            f.SetPoint(iBin,iBin,ratios[iBin])
            
        except:
           pass
       
    color=4
    xmax=len(ratios)-1
    f.SetMarkerColor(color)
    f.SetMarkerSize(0.1)
    f.SetMarkerStyle(20)
    f.SetLineColor(color)
    f.SetFillColor(color)
    f.SetTitle(ylabel+" "+title)
    f.GetXaxis().SetTitle(xlabel)
    f.GetYaxis().SetTitle(ylabel)
    f.GetYaxis().SetRangeUser(ymin, ymax)
    f.GetXaxis().SetRangeUser(0, xmax)
    
    return f    

def plot_ratio_noempty_fromDict(dict,xlabel,ylabel,title,ymin,ymax):
    f=ROOT.TGraph()
    iBin=0
    keys=list(dict)
    keys.sort()

    for key in keys:
        f.SetPoint(iBin,iBin,dict[key][0])
        iBin+=1
    
    color=4
    xmax=len(keys)-1
    f.SetMarkerColor(color)
    f.SetMarkerSize(0.1)
    f.SetMarkerStyle(20)
    f.SetLineColor(color)
    f.SetFillColor(color)
    f.SetTitle(ylabel+" "+title)
    f.GetXaxis().SetTitle(xlabel)
    f.GetYaxis().SetTitle(ylabel)
    f.GetYaxis().SetRangeUser(ymin, ymax)
    f.GetXaxis().SetRangeUser(0, xmax)
    
    return f    
    



def plot_ratio_noempty_nls(ratios,nls,xlabel,ylabel,title,ymin,ymax):
    f=ROOT.TGraph()   
    
    for iBin in range(0,len(ratios)):
        try:
            f.SetPoint(iBin,iBin,ratios[iBin])
            
        except:
           pass
       
    color=4
    xmax=len(ratios)-1
    f.SetMarkerColor(color)
    f.SetMarkerSize(0.1)
    f.SetMarkerStyle(20)
    f.SetLineColor(color)
    f.SetFillColor(color)
    f.SetTitle(ylabel+" "+title)
    f.GetXaxis().SetTitle(xlabel)
    f.GetYaxis().SetTitle(ylabel)
    f.GetYaxis().SetRangeUser(ymin, ymax)
    f.GetXaxis().SetRangeUser(0, xmax)
    
    return f    

def plot_vsIntegratedLumi_fromDict(dict,xlabel,ylabel,title,ymin,ymax):
    f=ROOT.TGraph()
    iBin=0
    keys=list(dict)
    keys.sort()
    lumi=0
    for key in keys:
        lumi+=dict[key][1]*23.31/1000000000.
        f.SetPoint(iBin,lumi,dict[key][0])
        iBin+=1
        
    
    color=4
    xmax=lumi
    f.SetMarkerColor(color)
    f.SetMarkerSize(0.1)
    f.SetMarkerStyle(20)
    f.SetLineColor(color)
    f.SetFillColor(color)
    f.SetTitle(title)
    f.GetXaxis().SetTitle(xlabel)
    f.GetYaxis().SetTitle(ylabel)
    f.GetYaxis().SetRangeUser(ymin, ymax)
    f.GetXaxis().SetRangeUser(0, xmax)
    
    return f    

def plot_vsIntegratedLumi_fromDictLdict(dict,ldict,xlabel,ylabel,title,ymin,ymax):
    f=ROOT.TGraph()
    iBin=0
    keys=list(dict)
    keys.sort()
    for key in keys:
        try:
            f.SetPoint(iBin,ldict[key][3]*23.31/1000000000.,dict[key][0])
            iBin+=1
        except:
            continue
    
    color=4
    xmax=len(keys)-1
    f.SetMarkerColor(color)
    f.SetMarkerSize(0.1)
    f.SetMarkerStyle(20)
    f.SetLineColor(color)
    f.SetFillColor(color)
    f.SetTitle(title)
    f.GetXaxis().SetTitle(xlabel)
    f.GetYaxis().SetTitle(ylabel)
    f.GetYaxis().SetRangeUser(ymin, ymax)
    f.GetXaxis().SetRangeUser(0, xmax)
    
    return f

def plotExcl_vsIntegratedLumi_fromDictLdict(dict1,dict1all,alldict,xlabel,ylabel,title,ymin,ymax):
    f=ROOT.TGraph()
    iBin=0
    keysExcl=[]
    keysall=list(alldict)
    keys1=list(dict1)
    keys1all=list(dict1all)
    keysall.sort()
    keys1.sort()
    
    percent=0.0
    count=0
    for k1 in keys1all:
        if k1 not in keys1:
            keysExcl.append(k1)
            f.SetPoint(iBin,alldict[k1][3]*23.31/1000000000.,dict1all[k1][0])
            iBin+=1
#    noExclId=0
#    for k1 in keys1all:
#        iniID=noExclId
#        for id_dict in range(iniID,len(keys1)):
#            if k1 != keys1[id_dict]:
#                keysExcl.append(k1)
#                f.SetPoint(iBin,alldict[k1][3]*23.31/1000000000.,dict1all[k1][0])
#                iBin+=1
#                break
#            
#            noExclId+=1
            

    color=4
    xmax=len(keys1all)-1
    f.SetMarkerColor(color)
    f.SetMarkerSize(0.1)
    f.SetMarkerStyle(20)
    f.SetLineColor(color)
    f.SetFillColor(color)
    f.SetTitle(title)
    f.GetXaxis().SetTitle(xlabel)
    f.GetYaxis().SetTitle(ylabel)
    f.GetYaxis().SetRangeUser(ymin, ymax)
    f.GetXaxis().SetRangeUser(0, xmax)
    
    return f

#def infoProccessPercent(counter,limit,showInfoInValues):
#    temp_showInfoInValues=showInfoInValues
#    percent=
#    if ()        

def plot_vsIntegratedLumi_fromDictLdictExcl(dict,xlabel,ylabel,title,ymin,ymax,labelRatio):
    f=ROOT.TGraph()
    iBin=0
    detc1=labelRatio.split("/")[0]
    detc2=labelRatio.split("/")[1]
    
    keys=list(dict)
    keys.sort()
    for key in keys:
        try:
            if (key[0] in setts.detectors_excludedFills[detc1]) or ((key[0] in setts.detectors_excludedFills[detc2])):
                #print("Fill ",key[0],"excluded")
                continue
        except:
            pass
        #lumi+=dict[key][3]*23.31/1000000000.
        f.SetPoint(iBin,dict[key][3]*23.31/1000000000.,dict[key][0])
        iBin+=1
        
    
    color=4
    xmax=len(keys)-1
    f.SetMarkerColor(color)
    f.SetMarkerSize(0.1)
    f.SetMarkerStyle(20)
    f.SetLineColor(color)
    f.SetFillColor(color)
    f.SetTitle(title)
    f.GetXaxis().SetTitle(xlabel)
    f.GetYaxis().SetTitle(ylabel)
    f.GetYaxis().SetRangeUser(ymin, ymax)
    f.GetXaxis().SetRangeUser(0, xmax)
    
    return f   
    

def plot_ratio_vsVariable(ratios_var,xlabel,ylabel,title,ymin,ymax,xmin,xmax):
    f=ROOT.TGraph()
    
    
    
#    pts=[]    
#    for i in range(0,len(x_values)):
#        pts.append((x_values[i],ratios[i])) 
#    
#    pts.sort()
        
    iBin=0
        
    for iBin in range(0,len(ratios_var)):
        try:
            f.SetPoint(iBin,ratios_var[iBin][0],ratios_var[iBin][1])
            
        except:
           pass
       
    color=4
    f.SetMarkerColor(color)
    f.SetMarkerSize(0.3)
    f.SetLineWidth(1)
    f.SetMarkerStyle(2)
    f.SetLineColor(33)
    f.SetFillColor(color)
    f.SetTitle(ylabel+" "+title)
    f.GetXaxis().SetTitle(xlabel)
    f.GetYaxis().SetTitle(ylabel)
    f.GetYaxis().SetRangeUser(ymin, ymax)
    f.GetXaxis().SetRangeUser(xmin, xmax)
    #f.GetXaxis().SetTimeDisplay(1)
    return f    

def plot_ratioInDict_vs_VariablePos(Dict,pos_x_inKeys,xlabel,ylabel,title,ymin,ymax,xmin,xmax):
    f=ROOT.TGraph()
    
    keys=list(Dict)
    keys.sort()
    
    iBin=0
    
    for key in keys:
        f.SetPoint(iBin,key[pos_x_inKeys],Dict[key][0])
        iBin+=1
    
    color=4
    f.SetMarkerColor(color)
    f.SetMarkerSize(0.3)
    f.SetLineWidth(1)
    f.SetMarkerStyle(2)
    f.SetLineColor(33)
    f.SetFillColor(color)
    f.SetTitle(ylabel+" "+title)
    f.GetXaxis().SetTitle(xlabel)
    f.GetYaxis().SetTitle(ylabel)
    f.GetYaxis().SetRangeUser(ymin, ymax)
    
    if xmin!=0 or xmax!=0:
        f.GetXaxis().SetRangeUser(xmin, xmax)
        
    return f    

def plot_ratioInDict_vs_VariablePosExcl(Dict,pos_x_inKeys,xlabel,labelRatio,ylabel,title,ymin,ymax,xmin,xmax):
    f=ROOT.TGraph()
    
    keys=list(Dict)
    keys.sort()
    
    iBin=0
    detc1=labelRatio.split("/")[0]
    detc2=labelRatio.split("/")[1]
    print (detc1,detc2)
    for key in keys:
        f.SetPoint(iBin,key[pos_x_inKeys],Dict[key][0])
        iBin+=1
    
    color=4
    f.SetMarkerColor(color)
    f.SetMarkerSize(0.3)
    f.SetLineWidth(1)
    f.SetMarkerStyle(2)
    f.SetLineColor(33)
    f.SetFillColor(color)
    f.SetTitle(ylabel+" "+title)
    f.GetXaxis().SetTitle(xlabel)
    f.GetYaxis().SetTitle(ylabel)
    f.GetYaxis().SetRangeUser(ymin, ymax)
    
    if xmin!=0 or xmax!=0:
        f.GetXaxis().SetRangeUser(xmin, xmax)
        
    return f    

def plot_ratioInDict_vs_VariablePosDict(Dict,pos_x_inDict,xlabel,ylabel,title,ymin,ymax,xmin,xmax):
    f=ROOT.TGraph()
    
    keys=list(Dict)
    keys.sort()
    
    iBin=0
    
    for key in keys:
        f.SetPoint(iBin,Dict[key][pos_x_inDict],Dict[key][0])
        iBin+=1
    
    color=4
    f.SetMarkerColor(color)
    f.SetMarkerSize(0.3)
    f.SetLineWidth(1)
    f.SetMarkerStyle(2)
    f.SetLineColor(33)
    f.SetFillColor(color)
    f.SetTitle(ylabel+" "+title)
    f.GetXaxis().SetTitle(xlabel)
    f.GetYaxis().SetTitle(ylabel)
    f.GetYaxis().SetRangeUser(ymin, ymax)
    
    if xmin!=0 or xmax!=0:
        f.GetXaxis().SetRangeUser(xmin, xmax)
        
    return f    

def plot_sbill_all(Dict,nBXPerFill,pos_x_inDict,xlabel,ylabel,title,ymin,ymax):
    f=ROOT.TGraph()
    
    keys=list(Dict)
    keys.sort()
    
    iBin=0
    
    for key in keys:
        try:
            f.SetPoint(iBin,Dict[key][pos_x_inDict]/(nBXPerFill[key[0]]),Dict[key][0])
            iBin+=1
        except:
            pass
            #print ("Fill "+str(key[0])+" in NBX file but not in detectors data")
    
    color=4
    f.SetMarkerColor(color)
    f.SetMarkerSize(0.3)
    f.SetLineWidth(1)
    f.SetMarkerStyle(2)
    f.SetLineColor(33)
    f.SetFillColor(color)
    f.SetTitle(ylabel+" "+title)
    f.GetXaxis().SetTitle(xlabel)
    f.GetYaxis().SetTitle(ylabel)
    f.GetYaxis().SetRangeUser(ymin, ymax)
        
    return f

def plot_sbill_nls(Dict,nBXPerFill,pos_x_inDict,nls,xlabel,ylabel,title,ymin,ymax):
    f=ROOT.TGraph()
    
    keys=list(Dict)
    keys.sort()
    
    iBin=0
    
    for key in keys:
        try:
            f.SetPoint(iBin,Dict[key][pos_x_inDict]/(nBXPerFill[key[0]]*nls),Dict[key][0])
            iBin+=1
        except:
            pass
    
    color=4
    f.SetMarkerColor(color)
    f.SetMarkerSize(0.3)
    f.SetLineWidth(1)
    f.SetMarkerStyle(2)
    f.SetLineColor(33)
    f.SetFillColor(color)
    f.SetTitle(ylabel+" "+title)
    f.GetXaxis().SetTitle(xlabel)
    f.GetYaxis().SetTitle(ylabel)
    f.GetYaxis().SetRangeUser(ymin, ymax)
        
    return f    
 
def plotBinnedTgraphFromDict_sbil(rDict,nBXPerFill,sbilBins,minval,maxval):
    keys=list(rDict)
    keys.sort()
    
    delta=(maxval-minval)/sbilBins
    if delta<=0:
        print ("Error in plotBinnedTgraphFromDict_sbil funtion: negative or null delta value")
        quit()
    
    ##Contruct binning ranges
    val=minval
    sbilList=[]
    ratioList=[]
    
    plot=ROOT.TGraphErrors()
    
    for i in range(0,sbilBins-1):
        del sbilList[:]
        del ratioList[:]
        val+=i*delta
        for key in keys:
            sbil=rDict[key][2]/nBXPerFill[key[0]]
            if sbil>=val and sbil<(val+delta):
                sbilList.append(sbil)
                ratioList.append(rDict[key][0])
        if len(sbilList)>1:
            plot.SetPoint(i,val+delta/2,statistics.mean(ratioList))
            plot.SetPointError(i,statistics.stdev(sbilList)/math.sqrt(len(sbilList)),statistics.stdev(ratioList)/math.sqrt(len(ratioList)))
        
    return plot

def plotBinnedNptsTgraphFromDict_sbil(rDict,nBXPerFill,npts):
    keys=list(rDict)
    values_list=[]
    ##create sbilVsRatio list
    for key in keys:
        sbil=rDict[key][2]/nBXPerFill[key[0]]
        values_list.append((sbil,rDict[key][0]))
    values_list.sort()
    
    icont=0
    sbilList=[]
    ratioList=[]
    iBin=0
    n=0
    plot=ROOT.TGraphErrors()
    for sbil,ratio in values_list:
        icont+=1
        n+=1
        sbilList.append(sbil)
        ratioList.append(ratio)
        if icont==npts or n==len(values_list):
            plot.SetPoint(iBin,statistics.mean(sbilList),statistics.mean(ratioList))
            plot.SetPointError(iBin,statistics.stdev(sbilList)/math.sqrt(len(sbilList)),statistics.stdev(ratioList)/math.sqrt(len(ratioList)))
            #plot.SetPointError(iBin,statistics.stdev(sbilList),statistics.stdev(ratioList))
            icont=0
            iBin+=1
            del sbilList[:]
            del ratioList[:]
    
    return plot

        

def perFillLineatiyAnalysis(ratiosDict,nlsDict,nBXPerFill,ymin,ymax,labelRatio,outDir_corr,fit_min=0.0,fit_max=0.0,year=0,sbil_min_exclud=0.0,sbil_max_exclud=0.0,energy=0):
    print ("\n *********************** Starting per Fill Linearity Analysis ***********************\n")
    xlabel="SBIL"
    ylabel="ratio"
    labelRatioMod=labelRatio.replace('/','-')
    colors=setts.colors
    sbilBins=500
    relErr_max=0.05
    detectors_lin=labelRatioMod.split('-')
    slope_minval=setts.slope_plotting_range[year,energy][0]
    slope_maxval=setts.slope_plotting_range[year,energy][1]
    
    ROOT.gStyle.SetOptStat(0)
    slopeBins=100
    bins_fit=150
    nls=30
    big_nls=500
    pos_x_inDict=2
    if year==2018:
        ymin_closer=0.98
        ymax_closer=1.02
        N_lumiBins=6
        sbilNpts=1500
        slope_maxErr = 0.2
        ndof_lim=50
        
    elif year==2016:
        ymin_closer=0.98
        ymax_closer=1.02
        N_lumiBins=6
#        if "plt" in detectors_lin:
#            N_lumiBins=8
#        elif "ram" in detectors_lin:
#            N_lumiBins=8
#        else:
#            N_lumiBins=8
        sbilNpts=1500
        slope_maxErr = 0.0015
        ndof_lim=15
    elif year==2015 and energy==5:
        slope_maxErr = 0.01
        sbil_min=1.0
        sbil_max=3.8
        N_lumiBins=1
        sbilNpts=200
        ndof_lim=5
        nls=20
    elif year==2015:
        slope_maxErr = 0.005
        sbil_min=1.0
        sbil_max=3.8
        N_lumiBins=3
        sbilNpts=200
        ndof_lim=5
    else:
        ymin_closer=ymin
        ymax_closer=ymax
        N_lumiBins=4
        sbilNpts=200
        slope_maxErr = 0.01
        ndof_lim=20
        
    #model_f=ROOT.TF1("model_scale","[0]*(1+[1])*x")
    
    
    solocan_square=ROOT.TCanvas("sqcan","sqcan",600,600)
    color=4
    
#    keys=list(ratiosDict)
#    keys.sort()
#    if sbil_min_exclud!=0.0 or sbil_max_exclud!=0.0:
#        for key in keys:
#            sbil_for_range=ratiosDict[key][pos_x_inDict]/(nBXPerFill[key[0]])
#            if sbil_for_range<sbil_min_exclud or sbil_for_range>sbil_max_exclud:
#                del ratiosDict[key]
    keys=list(ratiosDict)
    keys.sort()
    
    iBin=0
    cont=0
    iBinNls=0
    iBinFills=0
    iBinAllFills=0
    lumi1=0.0
    lumi2=0.0
    lumiFill=0.0
    
    
    byFillFile=outDir_corr+"/perFill"
    try:
        os.mkdir(byFillFile)
    except:
        pass
    
    fill=keys[0][0]
    
    pointInfill=[]
    fillLinearityInfo={}
    nlsdivs=[]
    nlssbil=[]
    nlssbilAll=[]
    slopeList=[]
    
    plot=ROOT.TGraph()
    hist_fill=ROOT.TH1F("fill_histo"," fill_histo ; ratios; count",slopeBins,ymin,ymax)
    plotChi2Fill=ROOT.TGraph()
    #nlsData=ROOT.TH1F("ratioNarrowRun",";ratio "+label+";",300,0.8=,1.2)
    plotnls=ROOT.TGraphErrors()
    byFillSlopes=ROOT.TGraphErrors()
    byFillAllSlopes=ROOT.TGraphErrors()
    byFillp0=ROOT.TGraphErrors()
    byFillSlopesVsIntLumi=ROOT.TGraphErrors()
    byFillAllSlopesVsIntLumi=ROOT.TGraphErrors()
    #ratioNarrowRun=ROOT.TH1F("ratioNarrowRun",";ratio "+label+";",300,ymin,ymax)
    hist_slopes_w=ROOT.TH1F("slope_histo_w"," slope_histo_w ;"+labelRatio.upper()+" slope [(hz/ub)^{-1}];Integrated Luminosity [fb^{-1}]",slopeBins,slope_minval,slope_maxval)
    hist_slopes_w.SetLineColor(colors[0])
    hist_slopes_w.SetFillColor(colors[0])
    hist_slopes_errw=ROOT.TH1F("slope_histo_errw"," slope_histo_errw ;"+labelRatio.upper()+" slope [(hz/ub)^{-1}];1/(Slope err)^2 [hz/ub]",slopeBins,slope_minval,slope_maxval)
    hist_slopes_errw.SetLineColor(colors[0])
    hist_slopes_errw.SetFillColor(colors[0])        
    hist_slopes=ROOT.TH1F("slope_histo"," slope_histo ;"+labelRatio.upper()+" slope [(hz/ub)^{-1}];N of events",slopeBins,slope_minval,slope_maxval)
    hist_slopes.SetLineColor(colors[0])
    hist_slopes.SetFillColor(colors[0])  
    hist_slopes_errw_lw=ROOT.TH1F("slope_histo_errw_lw"," slope_histo_errw_lw ;"+labelRatio.upper()+" slope [(hz/ub)^{-1}]; weigth (err,lumi)",slopeBins,slope_minval,slope_maxval)
    hist_slopes_errw_lw.SetLineColor(colors[0])
    hist_slopes_errw_lw.SetFillColor(colors[0])
    
    
    hist_Allslopes_w=ROOT.TH1F("Allslope_histo_w"," slope_histo_w ;"+labelRatio.upper()+" slope [(hz/ub)^{-1}];Integrated Luminosity [fb^{-1}]",slopeBins,slope_minval,slope_maxval)
    hist_Allslopes_w.SetLineColor(colors[0])
    hist_Allslopes_w.SetFillColor(colors[0])
    hist_Allslopes_errw=ROOT.TH1F("Allslope_histo_errw"," slope_histo_errw ;"+labelRatio.upper()+" slope [(hz/ub)^{-1}];1/(Slope err)^2 [hz/ub]",slopeBins,slope_minval,slope_maxval)
    hist_Allslopes_errw.SetLineColor(colors[0])
    hist_Allslopes_errw.SetFillColor(colors[0])        
    hist_Allslopes=ROOT.TH1F("Allslope_histo"," slope_histo ;"+labelRatio.upper()+" slope [(hz/ub)^{-1}];N of events",slopeBins,slope_minval,slope_maxval)
    hist_Allslopes.SetLineColor(colors[0])
    hist_Allslopes.SetFillColor(colors[0])  
    
    hist_chi2=ROOT.TH1F("slope_chi2"," slope_chi2 ;"+labelRatio.upper()+" Chi2;",40,0.0,100)
    #hist_chi2_dof=ROOT.TH1F("slope_chi2_dof"," slope_chi2_dof ;"+labelRatio.upper()+" Chi2/dof;",40,0.0,50)
    hist_chi2w=ROOT.TH1F("slope_chi2"," slope_chi2 ;"+labelRatio.upper()+" Chi2;Integrated Luminosity [fb^{-1}]",40,0.0,100)
    hist_slopeErrs=ROOT.TH1F("slope_errs"," slope_errs ;"+labelRatio.upper()+" errs;",40,slope_minval,slope_maxval)
    hist_slopeRelErrs=ROOT.TH1F("slope_rel_errs"," slope_rel_errs ;"+labelRatio.upper()+" rel. errs;",40,0,2)
        

    inls=0
    ifill=0
    totLumi=0.0
    totSelLumi=0.0
    lumiCount=0.0
    for key in keys:
        sbil=ratiosDict[key][pos_x_inDict]/(nBXPerFill[key[0]])
        nlssbilAll.append(sbil)
    
    hist_sbil=ROOT.TH1F("sbils"," sbils ;"+ labelRatio.split("/")[1] +" SBIL [hz/ub];N",40,min(nlssbilAll),max(nlssbilAll))
    for sbil in nlssbilAll:
        hist_sbil.Fill(sbil)
    axis=hist_sbil.GetXaxis()
    axis.SetNdivisions(504)
    saveintoCanvas_and_toFileWIP(solocan_square,hist_sbil,"HIST",
                                   outDir_corr+"/"+labelRatioMod+"hist_sbil"+setts.plotformat,"il_sq",year,energy,
                                   extraText1='#splitline{Mean = '+str(float("{0:.4f}".format(hist_sbil.GetMean())))+'}{#splitline{#sigma = '+str(float("{0:.4f}".format(hist_sbil.GetStdDev())))+'}{}}',
                                   pos_extraText1=[0.88,0.92],s_extraText1=0.035)
    
    for key in keys:
        inls+=1
        cont+=1
        iBin+=1
        totLumi+=ratiosDict[key][2]
        lumiCount+=ratiosDict[key][2]
        
        sbil=ratiosDict[key][pos_x_inDict]/(nBXPerFill[key[0]])
        plot.SetPoint(iBin,sbil,ratiosDict[key][0])
        lumi1+=ratiosDict[key][1]
        lumi2+=ratiosDict[key][2]
        lumiFill+=ratiosDict[key][2]
        hist_fill.Fill(ratiosDict[key][0])
        
        nlsdivs.append(ratiosDict[key][0])
        nlssbil.append(sbil)
        
        
        if inls==nls or cont>=len(keys) or key[0]!=fill:
            try:                
                div=lumi1/lumi2                
                if div>0.8 and div<1.2:
                    #err=abs(lumi2-lumi1)/lumi2
                    #errdiv=statistics.stdev(nlsdivs)
                    errdiv=statistics.stdev(nlsdivs)/math.sqrt(len(nlsdivs))
                    errsbil=statistics.stdev(nlssbil)/math.sqrt(len(nlssbil))
                    plotnls.SetPoint(iBinNls,lumi2/(nBXPerFill[key[0]]*inls),div)
                    #plotnls.SetPointError(iBinNls,errsbil,errdiv)
                    plotnls.SetPointError(iBinNls,0.0,errdiv)
                    
                    iBinNls+=1                            
            except:
                pass
            
            lumi1=0.0
            lumi2=0.0
            inls=0
            nlsdivs=[]
            nlssbil=[]
        
        if key[0]!=fill or cont>=len(keys):
                        
            plot.SetMarkerColor(color)
            plot.SetMarkerSize(0.5)
            plot.SetLineWidth(1)
            plot.SetMarkerStyle(2)
            plot.SetLineColor(33)
            plot.SetTitle(ylabel+str(fill))
            plot.GetXaxis().SetTitle(xlabel)
            plot.GetYaxis().SetTitle(ylabel)
            plot.GetYaxis().SetRangeUser(ymin, ymax)
            
            plotnls.SetMarkerColor(color)
            plotnls.SetMarkerSize(0.5)
            plotnls.SetLineWidth(1)
            #plotnls.SetMarkerStyle(2)
            plotnls.SetLineColor(33)
            plotnls.SetTitle(ylabel+str(fill))
            plotnls.GetXaxis().SetTitle(xlabel)
            plotnls.GetYaxis().SetTitle(ylabel)
            plotnls.GetYaxis().SetRangeUser(ymin, ymax)
            
            plotnls.Fit("pol1")
            
            try:
                fnls=plotnls.GetFunction("pol1")
                chi2=fnls.GetChisquare()
                ndf=fnls.GetNDF()
                prob=fnls.GetProb()
                slope=fnls.GetParameter(1)
                slopeErr=fnls.GetParError(1)
                p0=fnls.GetParameter(0)
                p0Err=fnls.GetParError(0)
                plotChi2Fill.SetPoint(ifill,fill,chi2/ndf)
                hist_chi2.Fill(chi2/ndf)
                hist_chi2w.Fill(chi2/ndf,lumiFill)
                hist_slopeErrs.Fill(abs(slopeErr))
                hist_slopeRelErrs.Fill(abs(slopeErr/slope))
                ifill+=1
        
            except:
                print("fitting failed")
            
            passed_test=False
            
            if year==2016:
                if iBinNls>5 and abs(slopeErr)<abs(slope_maxErr):
                #if iBinNls>5:
                    passed_test=True
            if year==2015:
                #if iBinNls>5 and abs(slopeErr)<abs(slope_maxErr):
                if iBinNls>5:
                    passed_test=True
            else:
                if iBinNls>5:
                #if iBinNls>5 and chi2/ndf < ndof_lim:
                    passed_test=True    
            
            if passed_test:
                totSelLumi+=ratiosDict[key][2]
                passed_test=True
                byFillSlopes.SetPoint(iBinFills,fill,slope)
                byFillSlopes.SetPointError(iBinFills,0.0,slopeErr)
                byFillp0.SetPoint(iBinFills,fill,p0)
                byFillp0.SetPointError(iBinFills,0.0,p0Err)
                
                byFillSlopesVsIntLumi.SetPoint(iBinFills,totLumi*23.31/1000000000.,slope)
                byFillSlopesVsIntLumi.SetPointError(iBinFills,0.0,slopeErr)
                hist_slopes_w.Fill(slope,lumiFill*23.31/1000000000.)
                hist_slopes_errw.Fill(slope,1/(slopeErr**2))
                hist_slopes.Fill(slope)
                fillLinearityInfo[fill]=[lumiFill,slope,slopeErr,iBin,p0,p0Err,hist_fill.GetMean()]
                slopeList.append((slope,slopeErr,lumiFill,chi2,ndf))
                iBinFills+=1                
                pointInfill.append((fill,iBin))
            
            hist_Allslopes_w.Fill(slope,lumiFill*23.31/1000000000.)
            hist_Allslopes_errw.Fill(slope,1/(slopeErr**2))
            hist_Allslopes.Fill(slope)
            byFillAllSlopesVsIntLumi.SetPoint(iBinAllFills,totLumi*23.31/1000000000.,slope)
            byFillAllSlopesVsIntLumi.SetPointError(iBinAllFills,0.0,slopeErr)
            if passed_test:
                label_plot_fit = "GOOD"
            else:
                label_plot_fit = "BAD"
            saveintoCanvas_and_toFileWIP(solocan_square,plot,"AP",
                                   byFillFile+"/" +str(fill)+"fill.png","il_sq",year,energy,extraText2=label_plot_fit,pos_extraText2=[0.3,0.2])
            saveintoCanvas_and_toFileWIP(solocan_square,plotnls,"AP",
                                   byFillFile+"/" +str(fill)+"fillNls.png","il_sq",year,energy,extraText2=label_plot_fit,pos_extraText2=[0.3,0.2])
#            except:
#                print ("fitting not possible")
#                pass
            byFillAllSlopes.SetPoint(iBinAllFills,fill,slope)
            byFillAllSlopes.SetPointError(iBinAllFills,0.0,slopeErr)
            
            iBinAllFills+=1
            fill = key[0]
            iBin=0
            iBinNls=0
            lumiFill=0.0
            plot=ROOT.TGraph()
            hist_fill=ROOT.TH1F("fill_histo"," fill_histo ; ratios; count",slopeBins,ymin,ymax)
            plotnls=ROOT.TGraphErrors()
    savedTotLumi=totLumi
    
    ##Fill normalized weighted histogram
    totErrInv=0.0
    lumiSel2=0.0
    test=0.0
    for slope,slopeErr,lumiFill,chi2,ndf in slopeList:
        totErrInv+=(1/(slopeErr*slopeErr))
        lumiSel2+=lumiFill*lumiFill
    for slope,slopeErr,lumiFill,chi2,ndf in slopeList:
        test+=((1/(slopeErr*slopeErr))/totErrInv+(lumiFill*lumiFill)/lumiSel2)/2
    print ("test: ",test)
    for slope,slopeErr,lumiFill,chi2,ndf in slopeList:
        hist_slopes_errw_lw.Fill(slope,((1/(slopeErr*slopeErr))/totErrInv+(lumiFill*lumiFill)/lumiSel2)/2)
    
    
    
    setPlotProperties(byFillSlopes,"Slopes by Fill","Fill",labelRatio.upper()+" slope [(hz/ub)^{-1}]",slope_minval,slope_maxval)
    setPlotProperties(byFillSlopesVsIntLumi,"Slopes by Fill","Integrated Luminosity [fb^{-1}]",labelRatio.upper()+" slope [(hz/ub)^{-1}]",slope_minval,slope_maxval)
    setPlotProperties(byFillAllSlopesVsIntLumi,"Slopes by Fill","Integrated Luminosity [fb^{-1}]",labelRatio.upper()+" slope [(hz/ub)^{-1}]",slope_minval,slope_maxval)
    setPlotProperties(byFillp0,"Intercepts by Fill","Fill",labelRatio.upper()+" intercept",ymin,ymax)
    
    plot_allSlopesSelc,leg_allVsSel = plot_multiple_TgraphsErrs([byFillAllSlopes,byFillSlopes],"Slopes selection",["all","selected"],"fills","Slopes","")
    setPlotProperties(plot_allSlopesSelc,"Slopes selection","Fill","slope",slope_minval,slope_maxval)
    saveintoCanvas_and_toFileWIP(solocan_square,plot_allSlopesSelc,"AP",
                                   outDir_corr+"/"+labelRatioMod+"_allVsSel.pdf","il_sq",year,energy,drawleg=True,leg=leg_allVsSel)
    
    saveintoCanvas_and_toFileWIP(solocan_square,byFillSlopes,"AP",
                                   outDir_corr+"/"+labelRatioMod+"_byFillSlopes.pdf","il_sq",year,energy)
    saveintoCanvas_and_toFileWIP(solocan_square,byFillAllSlopes,"AP",
                                   outDir_corr+"/"+labelRatioMod+"_byFillAllSlopes.pdf","il_sq",year,energy)
    saveintoCanvas_and_toFileWIP(solocan_square,byFillp0,"AP",
                                   outDir_corr+"/"+labelRatioMod+"_byFillp0.pdf","il_sq",year,energy)
    saveintoCanvas_and_toFileWIP(solocan_square,plotChi2Fill,"AP",
                                   outDir_corr+"/"+labelRatioMod+"_plotChi2Fill.pdf","il_sq",year,energy)
    saveintoCanvas_and_toFileWIP(solocan_square,byFillSlopesVsIntLumi,"AP",
                                   outDir_corr+"/"+labelRatioMod+"_byFillSlopesVsIntLumi.pdf","il_sq",year,energy)
    saveintoCanvas_and_toFileWIP(solocan_square,byFillAllSlopesVsIntLumi,"AP",
                                   outDir_corr+"/"+labelRatioMod+"_byFillAllSlopesVsIntLumi.pdf","il_sq",year,energy)
    saveintoCanvas_and_toFileWIP(solocan_square,byFillSlopes,"AP",
                                   outDir_corr+"/"+labelRatioMod+"_byFillSlopes.png","il_sq",year,energy)
    
    axis=hist_slopes_w.GetXaxis()
    axis.SetNdivisions(504)
    meanSlope_lw=hist_slopes_w.GetMean()
    stdvSlope_lw=hist_slopes_w.GetStdDev()
    #errSlope_lw=stdvSlope_lw/math.sqrt(hist_slopes_w.GetEntries())
    errSlope_lw=hist_slopes_w.GetMeanError()
    
    axis=hist_slopes.GetXaxis()
    axis.SetNdivisions(504)
    meanSlope=hist_slopes.GetMean()
    stdvSlope=hist_slopes.GetStdDev()
    errSlope=stdvSlope/math.sqrt(hist_slopes.GetEntries())
    
    axis=hist_slopes_errw_lw.GetXaxis()
    axis.SetNdivisions(504)
    meanSlope_errw_lw=hist_slopes_errw_lw.GetMean()
    stdvSlope_errw_lw=hist_slopes_errw_lw.GetStdDev()
    errSlope_errw_lw=stdvSlope_errw_lw/math.sqrt(hist_slopes_errw_lw.GetEntries())
    
    axis=hist_slopes_errw.GetXaxis()
    axis.SetNdivisions(504)
    meanSlope_errw=hist_slopes_errw.GetMean()
    stdvSlope_errw=hist_slopes_errw.GetStdDev()
    errSlope_errw=stdvSlope_errw/math.sqrt(hist_slopes_errw.GetEntries())
    
    axis=hist_Allslopes_w.GetXaxis()
    axis.SetNdivisions(504)
    meanAllSlope_lw=hist_Allslopes_w.GetMean()
    stdvAllSlope_lw=hist_Allslopes_w.GetStdDev()
    errAllSlope_lw=stdvAllSlope_lw/math.sqrt(hist_Allslopes_w.GetEntries())
    
    axis=hist_Allslopes.GetXaxis()
    axis.SetNdivisions(504)
    meanAllSlope=hist_Allslopes.GetMean()
    stdvAllSlope=hist_Allslopes.GetStdDev()
    errAllSlope=stdvAllSlope/math.sqrt(hist_Allslopes.GetEntries())
    
    axis=hist_Allslopes_errw.GetXaxis()
    axis.SetNdivisions(504)
    meanAllSlope_errw=hist_Allslopes_errw.GetMean()
    stdvAllSlope_errw=hist_Allslopes_errw.GetStdDev()
    errAllSlope_errw=stdvAllSlope_errw/math.sqrt(hist_Allslopes_errw.GetEntries())
    
    axis=hist_slopeErrs.GetXaxis()
    axis.SetNdivisions(504)
    meanErrsHist=hist_slopeErrs.GetMean()
    stdvErrsHist=hist_slopeErrs.GetStdDev()
    errErrsHist=stdvErrsHist/math.sqrt(hist_slopeErrs.GetEntries())
    
    axis=hist_slopeRelErrs.GetXaxis()
    axis.SetNdivisions(504)
    meanRelErrsHist=hist_slopeRelErrs.GetMean()
    stdvRelErrsHist=hist_slopeRelErrs.GetStdDev()
    errRelErrsHist=stdvRelErrsHist/math.sqrt(hist_slopeRelErrs.GetEntries())

    saveintoCanvas_and_toFileWIP(solocan_square,hist_slopes_w,"HIST",
                                   outDir_corr+"/"+labelRatioMod+"_byFillSlopesHistLw.pdf","il_sq",year,energy,
                                   extraText1='#splitline{Mean = '+str(float("{0:.4f}".format(meanSlope_lw)))+'}{#splitline{#sigma = '+str(float("{0:.4f}".format(stdvSlope_lw)))+'}{error = '+str(float("{0:.4f}".format(errSlope_lw)))+'}}',
                                   pos_extraText1=[0.88,0.92],s_extraText1=0.035)
    saveintoCanvas_and_toFileWIP(solocan_square,hist_slopes_errw,"HIST",
                                   outDir_corr+"/"+labelRatioMod+"_byFillSlopesHisterrw.pdf","il_sq",year,energy,
                                   extraText1='#splitline{Mean = '+str(float("{0:.4f}".format(meanSlope_errw)))+'}{#splitline{#sigma = '+str(float("{0:.4f}".format(stdvSlope_errw)))+'}{error = '+str(float("{0:.4f}".format(errSlope_errw)))+'}}',
                                   pos_extraText1=[0.88,0.92],s_extraText1=0.035)
    saveintoCanvas_and_toFileWIP(solocan_square,hist_slopes,"HIST",
                                   outDir_corr+"/"+labelRatioMod+"_byFillSlopesHist.pdf","il_sq",year,energy,
                                   extraText1='#splitline{Mean = '+str(float("{0:.4f}".format(meanSlope)))+'}{#splitline{#sigma = '+str(float("{0:.4f}".format(stdvSlope)))+'}{error = '+str(float("{0:.4f}".format(errSlope)))+'}}',
                                   pos_extraText1=[0.88,0.92],s_extraText1=0.035)
    
    saveintoCanvas_and_toFileWIP(solocan_square,hist_Allslopes_w,"HIST",
                                   outDir_corr+"/"+labelRatioMod+"_byFillAllSlopesHistLw.pdf","il_sq",year,energy,
                                   extraText1='#splitline{Mean = '+str(float("{0:.4f}".format(meanAllSlope_lw)))+'}{#splitline{#sigma = '+str(float("{0:.4f}".format(stdvAllSlope_lw)))+'}{error = '+str(float("{0:.4f}".format(errAllSlope_lw)))+'}}',
                                   pos_extraText1=[0.88,0.92],s_extraText1=0.035)
    saveintoCanvas_and_toFileWIP(solocan_square,hist_Allslopes_errw,"HIST",
                                   outDir_corr+"/"+labelRatioMod+"_byFillAllSlopesHisterrw.pdf","il_sq",year,energy,
                                   extraText1='#splitline{Mean = '+str(float("{0:.4f}".format(meanAllSlope_errw)))+'}{#splitline{#sigma = '+str(float("{0:.4f}".format(stdvAllSlope_errw)))+'}{error = '+str(float("{0:.4f}".format(errAllSlope_errw)))+'}}',
                                   pos_extraText1=[0.88,0.92],s_extraText1=0.035)
    saveintoCanvas_and_toFileWIP(solocan_square,hist_Allslopes,"HIST",
                                   outDir_corr+"/"+labelRatioMod+"_byFillAllSlopesHist.pdf","il_sq",year,energy,
                                   extraText1='#splitline{Mean = '+str(float("{0:.4f}".format(meanAllSlope)))+'}{#splitline{#sigma = '+str(float("{0:.4f}".format(stdvAllSlope)))+'}{error = '+str(float("{0:.4f}".format(errAllSlope)))+'}}',
                                   pos_extraText1=[0.88,0.92],s_extraText1=0.035)
    saveintoCanvas_and_toFileWIP(solocan_square,hist_slopes_errw_lw,"HIST",
                                   outDir_corr+"/"+labelRatioMod+"_byFillSlopesHist_errw_lw.pdf","il_sq",year,energy,
                                   extraText1='#splitline{Mean = '+str(float("{0:.4f}".format(meanSlope_errw_lw)))+'}{#splitline{#sigma = '+str(float("{0:.4f}".format(stdvSlope_errw_lw)))+'}{error = '+str(float("{0:.4f}".format(errSlope_errw_lw)))+'}}',
                                   pos_extraText1=[0.88,0.92],s_extraText1=0.035)
    
    plots_compare_selection=[hist_slopes,hist_Allslopes]
    plots_compare_selection_labels=["selection","All"]
    hist_compare_selection,leg_compare_selection = plot_combine_hists(plots_compare_selection,plots_compare_selection_labels,"Selection checking","Slopes","N",colors=setts.colors)
    saveintoCanvas_and_toFileWIP(solocan_square,hist_compare_selection,"HIST",
                                   outDir_corr+"/"+labelRatioMod+"_byFillSlopesSelectionCompareHist.pdf","il_sq",year,energy,
                                   drawleg=True,leg=leg_compare_selection)
    plots_compare_selection_errw=[hist_slopes_errw,hist_Allslopes_errw]
    plots_compare_selection_labels_errw=["selection errW","All"]
    hist_compare_selection_errw,leg_compare_selection_errw = plot_combine_hists(plots_compare_selection_errw,plots_compare_selection_labels_errw,"Selection checking","Slopes","(1/err)^2",colors=setts.colors)
    saveintoCanvas_and_toFileWIP(solocan_square,hist_compare_selection_errw,"HIST",
                                   outDir_corr+"/"+labelRatioMod+"_byFillSlopesSelectionCompareHist_errw.pdf","il_sq",year,energy,
                                   drawleg=True,leg=leg_compare_selection)
    plots_compare_selection_lw=[hist_slopes_w,hist_Allslopes_w]
    plots_compare_selection_labels_lw=["selection LW","All"]
    hist_compare_selection_lw,leg_compare_selection_lw = plot_combine_hists(plots_compare_selection_lw,plots_compare_selection_labels_lw,"Selection checking","Slopes","Luminosity",colors=setts.colors)
    saveintoCanvas_and_toFileWIP(solocan_square,hist_compare_selection_lw,"HIST",
                                   outDir_corr+"/"+labelRatioMod+"_byFillSlopesSelectionCompareHist_lw.pdf","il_sq",year,energy,
                                   drawleg=True,leg=leg_compare_selection)
    
    
    
    saveintoCanvas_and_toFileWIP(solocan_square,hist_slopeErrs,"HIST",
                                   outDir_corr+"/"+labelRatioMod+"_byFillSlopesErrs.pdf","il_sq",year,energy,
                                   extraText1='#splitline{Mean = '+str(float("{0:.4f}".format(meanErrsHist)))+'}{#splitline{#sigma = '+str(float("{0:.4f}".format(stdvErrsHist)))+'}{error = '+str(float("{0:.4f}".format(errErrsHist)))+'}}',
                                   pos_extraText1=[0.88,0.92],s_extraText1=0.035)
    saveintoCanvas_and_toFileWIP(solocan_square,hist_slopeRelErrs,"HIST",
                                   outDir_corr+"/"+labelRatioMod+"_byFillSlopesRelErrs.pdf","il_sq",year,energy,
                                   extraText1='#splitline{Mean = '+str(float("{0:.4f}".format(meanRelErrsHist)))+'}{#splitline{#sigma = '+str(float("{0:.4f}".format(stdvRelErrsHist)))+'}{error = '+str(float("{0:.4f}".format(errRelErrsHist)))+'}}',
                                   pos_extraText1=[0.88,0.92],s_extraText1=0.035)
    saveintoCanvas_and_toFileWIP(solocan_square,hist_chi2,"HIST",
                                   outDir_corr+"/"+labelRatioMod+"_hist_chi2.png","il_sq",year,energy)
    saveintoCanvas_and_toFileWIP(solocan_square,hist_chi2w,"HIST",
                                   outDir_corr+"/"+labelRatioMod+"_hist_chi2w.png","il_sq",year,energy)
    
    
    if len(slopeList)<5:
        warnings.warn("only "+str(len(slopeList))+" slopes passed chi2 and errs cuts!!!")
       
    linFills=list(fillLinearityInfo)
    linFills.sort()
    print ("===============> ",linFills)
    
    fill=linFills[0]
    plotbyFill=ROOT.TGraph()
    byFillSlopesVsIntLumiAvg=ROOT.TGraphErrors()
    byFillSlopesVsIntLumiAvgNoW=ROOT.TGraphErrors()
    iBin=0
    count=0
    lumiCount=0.0
    iBinFills=0
    slopeListForMean=[]
    slopeListForMeanAll=[]
    lumiBin=savedTotLumi/N_lumiBins
    totLumi=0.0
    ratiosDict_res={}
    xlumi_value=0.0
    totSlopes=len(linFills)

    
    for key in keys:
        count+=1
        lumiCount+=ratiosDict[key][2]
        totLumi+=ratiosDict[key][2]
        
        if key[0] in linFills:
            sbil=ratiosDict[key][pos_x_inDict]/(nBXPerFill[key[0]])
            lumi1=ratiosDict[key][1]
            #lumi2rescaled = ratiosDict[key][1]/(1+ratiosDict[key][0]-fillLinearityInfo[key[0]][4])
            # Lumi rescaled = old_lumi/(1+ratio_old-meanOldRatios)
            lumi2rescaled = ratiosDict[key][1]/(1+ratiosDict[key][0]-fillLinearityInfo[key[0]][6])
            #lumi2rescaled = ratiosDict[key][2]
            #lumi2rescaled = ratiosDict[key][1]/fillLinearityInfo[key[0]][6]
            ratiosDict_res[key]=[lumi1/lumi2rescaled,lumi1,lumi2rescaled,lumi1+lumi2rescaled]
            plotbyFill.SetPoint(iBin,sbil,lumi1/lumi2rescaled)        
            iBin+=1
            
            if (key[0]!=fill or count>=len(keys)):
                slopeListForMean.append((fillLinearityInfo[fill][1],fillLinearityInfo[fill][2],fillLinearityInfo[fill][0]))
                slopeListForMeanAll.append((fillLinearityInfo[fill][1],fillLinearityInfo[fill][2]))
                if lumiCount>=lumiBin or count>=len(keys):
                    xlumi_value=(totLumi-lumiCount/2)*23.31/1000000000.
                    meanLumiBin,errLumiBin = getMean_wErrsLumi(slopeListForMean,minlim=slope_minval,maxlim=slope_maxval)
                    meanLumiBinNoW,errLumiBinNoW = getMean_Errs(slopeListForMean)
                    #meanLumiBin,errLumiBin = getMean_wErrs(slopeListForMean)
                    byFillSlopesVsIntLumiAvg.SetPoint(iBinFills,xlumi_value,meanLumiBin)
                    byFillSlopesVsIntLumiAvg.SetPointError(iBinFills,0.0,errLumiBin)
                    byFillSlopesVsIntLumiAvgNoW.SetPoint(iBinFills,xlumi_value,meanLumiBinNoW)
                    byFillSlopesVsIntLumiAvgNoW.SetPointError(iBinFills,0.0,errLumiBinNoW)
                    lumiCount=0.0
                    iBinFills+=1
                    slopeListForMean=[]                 
                
                plotbyFill.SetMarkerColor(color)
                plotbyFill.SetMarkerSize(0.5)
                plotbyFill.SetLineWidth(1)
                plotbyFill.SetMarkerStyle(2)
                plotbyFill.SetLineColor(33)
                plotbyFill.SetTitle(ylabel+str(fill))
                plotbyFill.GetXaxis().SetTitle(xlabel)
                plotbyFill.GetYaxis().SetTitle(ylabel)
                plotbyFill.GetYaxis().SetRangeUser(ymin, ymax)
    
#                saveintoCanvas_and_toFileWIP(solocan_square,plotbyFill,"AP",
#                                               byFillFile+"/" +str(fill)+"fill_scaled.png","il_sq",year)            
                
                fill=key[0]
                plotbyFill=ROOT.TGraph()
                iBin=0
                
    savedTotLumi=totLumi
    meanSlope_ErrW,err_meanSlope_ErrW = getMean_wErrs(slopeListForMeanAll,minlim=slope_minval,maxlim=slope_maxval)       
    
    #byFillSlopes.Fit("pol1")
    #fbyFills=byFillSlopes.GetFunction("pol1")
    #slopeByFills=fbyFills.GetParameter(1)
    #slopeErrByFills=fbyFills.GetParError(1)
    
    setPlotProperties(byFillSlopesVsIntLumiAvg,"Slopes by Fill","Integrated Luminosity [fb^{-1}]",labelRatio.upper()+"slope [(hz/ub)^{-1}]")
    saveintoCanvas_and_toFileWIP(solocan_square,byFillSlopesVsIntLumiAvg,"AP",
                                   outDir_corr+"/"+labelRatioMod+"_byFillSlopesVsIntLumiAvg.pdf","il_sq",year,energy)
    setPlotProperties(byFillSlopesVsIntLumiAvgNoW,"Slopes by Fill","Integrated Luminosity [fb^{-1}]",labelRatio.upper()+"slope [(hz/ub)^{-1}]")
    saveintoCanvas_and_toFileWIP(solocan_square,byFillSlopesVsIntLumiAvg,"AP",
                                   outDir_corr+"/"+labelRatioMod+"_byFillSlopesVsIntLumiAvgNoW.pdf","il_sq",year,energy)
    
    
    
    totLumi=0.0
    totLumi_scaled=0.0
    
    totLumiMean_lw=0.0
    totLumistdv_lw=0.0
    totLumiMean_plus_lw=0.0
    totLumiMean_low_lw=0.0
    totLumiMean_up_lw=0.0
    totLumiMean_down_lw=0.0
    
    totLumiMean_up=0.0
    totLumiMean_down=0.0
    
    totLumiMean_errw_up=0.0
    totLumiMean_errw_down=0.0
    
    totLumiMean_lw_errw_up=0.0
    totLumiMean_lw_errw_down=0.0
    
    totLumiPerFillCorr=0.0
    totLumiPerFillCorr_plus=0.0
    totLumiPerFillCorr_low=0.0
    totLumiPerFillCorr_up=0.0
    totLumiPerFillCorr_down=0.0
    totLumiPerFillCorr_rs_up=0.0
    totLumiPerFillCorr_rs_down=0.0
    totLumiPerFillCorr_rs=0.0
    totLumiPerFillCorr_plus_rs=0.0
    totLumiPerFillCorr_low_rs=0.0
    
    lumi1=0.0
    lumi2=0.0
    lumi2resnls=0.0
    inls=0
    del nlsdivs[:]
    nlsdivs_res=[]
    iBinNls=0
    cont=0
    fill=keys[0][0]
    
    plotnlsAll=ROOT.TGraphErrors()
    plotnlsAll_res=ROOT.TGraphErrors()
#    plotnlsAll_binned=ROOT.TGraphErrors()
#    plotnlsAll_res_binned=ROOT.TGraphErrors()
#    binnedNls_data=[]
#    
    plotnlsAllTH2D=ROOT.TH2F("TH2DAll","TH2DAll",bins_fit,min(nlssbilAll),max(nlssbilAll),30,ymin,ymax)
    plotnlsAllTH2Dres=ROOT.TH2F("TH2DAllres","TH2DAllres",bins_fit,min(nlssbilAll),max(nlssbilAll),30,ymin,ymax)
    lumiInRange=0.0
    
    
    
    ##Corrections
    for key in keys:
        if key[0] in linFills:
            ##Using mean values for the entire period
            realLumi=ratiosDict[key][2]
            realLumi_scaled=ratiosDict_res[key][2]
            sbil=realLumi/(nBXPerFill[key[0]])
            totLumi+=realLumi
            totLumi_scaled+=realLumi_scaled
            
            if sbil>fit_min and sbil<fit_max:
                lumiInRange+=realLumi
                
            totLumiMean_lw+=(realLumi + sbil*meanSlope_lw*realLumi)
            totLumistdv_lw+=(realLumi + sbil*stdvSlope_lw*realLumi)
            totLumiMean_plus_lw+=(realLumi + sbil*(meanSlope_lw+errSlope_lw)*realLumi)
            totLumiMean_low_lw+=(realLumi + sbil*(meanSlope_lw-errSlope_lw)*realLumi)
            totLumiMean_up_lw+=realLumi*(1 + sbil*meanSlope_lw)
            totLumiMean_down_lw+=realLumi*(1 - sbil*meanSlope_lw)
            
            totLumiMean_errw_up+=realLumi*(1 + sbil*meanSlope_errw)
            totLumiMean_errw_down+=realLumi*(1 - sbil*meanSlope_errw)
            
            totLumiMean_lw_errw_up+=realLumi*(1 + sbil*meanSlope_errw_lw)
            totLumiMean_lw_errw_down+=realLumi*(1 - sbil*meanSlope_errw_lw)
            
            totLumiMean_up+=realLumi*(1 + sbil*meanSlope)
            totLumiMean_down+=realLumi*(1 - sbil*meanSlope)
            
            ##Per Fill correction (variation with slope)            
            byfillSlope=fillLinearityInfo[key[0]][1]
            byfillErrSlope=fillLinearityInfo[key[0]][2]
            byfill_p0=fillLinearityInfo[key[0]][4]
            byfill_mean=fillLinearityInfo[key[0]][6]
            
            totLumiPerFillCorr+=realLumi*(byfill_p0+sbil*byfillSlope)/byfill_mean
            totLumiPerFillCorr_plus += realLumi*(byfill_p0 + sbil*(byfillSlope+byfillErrSlope))/byfill_mean
            totLumiPerFillCorr_low += realLumi*(byfill_p0 + sbil*(byfillSlope-byfillErrSlope))/byfill_mean
            totLumiPerFillCorr_up += realLumi*(1 + sbil*byfillSlope)
            totLumiPerFillCorr_down += realLumi*(1 - sbil*byfillSlope)
            
            ###Per Fill correction (variation with slope) ----- After rescale           
            
            totLumiPerFillCorr_rs+=realLumi_scaled*(byfill_p0+sbil*byfillSlope)
            totLumiPerFillCorr_rs_up+=realLumi_scaled*(1+sbil*byfillSlope)
            totLumiPerFillCorr_rs_down+=realLumi_scaled*(1-sbil*byfillSlope)
            totLumiPerFillCorr_plus_rs += realLumi_scaled*(1 + sbil*(byfillSlope+byfillErrSlope))
            totLumiPerFillCorr_low_rs += realLumi_scaled*(1 + sbil*(byfillSlope-byfillErrSlope))
                        
            
            ##Full period correction filling plot
            inls+=1
            cont+=1      
            lumi1+=ratiosDict[key][1]
            lumi2+=ratiosDict[key][2]
            lumi2resnls+=ratiosDict_res[key][2]
            nlsdivs.append(ratiosDict[key][0])
            nlsdivs_res.append(ratiosDict_res[key][0])
            
            if (inls==nls or cont>=len(keys) or key[0]!=fill) and inls>3:
                div=lumi1/lumi2
                divres=lumi1/lumi2resnls
                sbilnls=lumi2/(nBXPerFill[key[0]]*nls)
                sbilnls_res=lumi2resnls/(nBXPerFill[key[0]]*nls)
                
                if div>0.8 and div<1.2:
                    #err=abs(lumi2-lumi1)/lumi2
                    #errdiv=statistics.stdev(nlsdivs)
                    errdiv=statistics.stdev(nlsdivs)/math.sqrt(len(nlsdivs))
                    errdiv_res=statistics.stdev(nlsdivs_res)/math.sqrt(len(nlsdivs_res))
                                        
                    plotnlsAll.SetPoint(iBinNls,sbilnls,div)
                    plotnlsAll.SetPointError(iBinNls,0.0,errdiv)
                    plotnlsAll_res.SetPoint(iBinNls,sbilnls_res,divres)
                    plotnlsAll_res.SetPointError(iBinNls,0.0,errdiv_res)
                    plotnlsAllTH2D.Fill(sbilnls,div)
                    plotnlsAllTH2Dres.Fill(sbilnls_res,divres)
                    
                    iBinNls+=1
                
                lumi1=0.0
                lumi2=0.0
                lumi2resnls=0.0
                inls=0
                del nlsdivs[:]
                del nlsdivs_res[:]
                
            if key[0]!=fill:
                fill=key[0]
                
    plotnlsAll.SetMarkerColor(color)
    plotnlsAll.SetMarkerSize(0.4)
    plotnlsAll.SetLineWidth(1)
    plotnlsAll.SetMarkerStyle(2)
    plotnlsAll.SetLineColor(33)
    plotnlsAll.SetTitle(ylabel)
    plotnlsAll.GetXaxis().SetTitle(xlabel)
    plotnlsAll.GetYaxis().SetTitle(ylabel)
    plotnlsAll.GetYaxis().SetRangeUser(ymin, ymax)
    plotnlsAll.Fit("pol1")
    plotnlsAll_res.SetMarkerColor(color)
    plotnlsAll_res.SetMarkerSize(0.4)
    plotnlsAll_res.SetLineWidth(1)
    plotnlsAll_res.SetMarkerStyle(2)
    plotnlsAll_res.SetLineColor(33)
    plotnlsAll_res.SetTitle(ylabel)
    plotnlsAll_res.GetXaxis().SetTitle(xlabel)
    plotnlsAll_res.GetYaxis().SetTitle(ylabel)
    plotnlsAll_res.GetYaxis().SetRangeUser(ymin, ymax)
    plotnlsAll_res.Fit("pol1")
    saveintoCanvas_and_toFileWIP(solocan_square,plotnlsAll,"AP",
                                   outDir_corr+"/"+labelRatioMod+"_plotnlsAll.pdf","il_sq",year,energy)
    saveintoCanvas_and_toFileWIP(solocan_square,plotnlsAll_res,"AP",
                                   outDir_corr+"/"+labelRatioMod+"_plotnlsAll_res.pdf","il_sq",year,energy)
    
    plotnlsAllTH2D_Pfx=plotnlsAllTH2D.ProfileX("plotnlsAllTH2D")
    setPlotProperties(plotnlsAllTH2D_Pfx,"ratios avg Vs SBIL","SBIL [hz/\mu b]","ratio avg",ymin,ymax)
    plotnlsAllTH2D_Pfx_res=plotnlsAllTH2Dres.ProfileX("plotnlsAllTH2Dres")
    setPlotProperties(plotnlsAllTH2D_Pfx_res,"ratios avg Vs SBIL","SBIL [hz/\mu b]","ratio avg",ymin,ymax)
    maxUncert_byFill= max(abs(totLumi-totLumiPerFillCorr_up)*100/totLumi,abs(totLumi-totLumiPerFillCorr_down)*100/totLumi)
    porc_lumiInRange=100
    if fit_min!=0.0 and fit_max!=0.0:
        
        f_pol1inRange=ROOT.TF1("fit1","pol1",fit_min,fit_max)
        plotnlsAllTH2D_Pfx.Fit(f_pol1inRange,"R")
        porc_lumiInRange=lumiInRange*100/totLumi
        print("Luminosity in fitting range (%): ",porc_lumiInRange )   
        
        fall=plotnlsAllTH2D_Pfx.GetFunction("fit1")
        slope_plotnlsAllTH2D_Pfx=fall.GetParameter(1)
        slope_plotnlsAllTH2D_Pfx_Err=fall.GetParError(1)
        p0_plotnlsAllTH2D_Pfx=fall.GetParameter(0)
        
        plotnlsAllTH2D_Pfx_res.Fit(f_pol1inRange,"R")
        fall_res=plotnlsAllTH2D_Pfx_res.GetFunction("fit1")
        slope_plotnlsAllTH2D_Pfx_res=fall_res.GetParameter(1)
        slope_plotnlsAllTH2D_Pfx_Err_res=fall_res.GetParError(1)
        p0_plotnlsAllTH2D_Pfx_res=fall_res.GetParameter(0)
        
#        plot_linearity_corr=plotBinnedNptsTgraphFromDict_sbil(ratiosDict_res,nBXPerFill,sbilNpts)
        plot_linearity_corrSaved=plotnlsAllTH2D_Pfx_res
#        plot_linearity_corr.Fit(f_pol1inRange,"R")
#        #plot_linearity_corr.Fit("pol1")
#        fall_res=plot_linearity_corr.GetFunction("fit1")
#        slope_plot_linearity_corr=fall_res.GetParameter(1)
#        slope_Err_plot_linearity_corr=fall_res.GetParError(1)
#        p0_plot_linearity_corr=fall_res.GetParameter(0)
#        plot_linearity_corr.SetFillColor(6);
#        plot_linearity_corr.SetFillStyle(3005)
#        
        slope_plot_linearity_corr=slope_plotnlsAllTH2D_Pfx_res
        slope_Err_plot_linearity_corr=slope_plotnlsAllTH2D_Pfx_Err_res
        p0_plot_linearity_corr=p0_plotnlsAllTH2D_Pfx_res
        
        #setPlotProperties(plot_linearity_corr,"linearity plot","SBIL [hz/\mub]",labelRatio.upper()+" avg ratio",ymin,ymax)
        setPlotProperties(plotnlsAllTH2D_Pfx_res,"linearity plot","SBIL [hz/\mub]",labelRatio.upper()+" avg ratio",ymin,ymax)
        
        
        uncertAll_Fmean,uncertAll_F,uncertAll_up,uncertAll_down,ratioDict_uncertFmean,ratioDict_uncertF,ratioDict_uncert_up,ratioDict_uncertF_down=applyFakeCorrectionToRatioDict(ratiosDict,nBXPerFill,slope_plot_linearity_corr,p0_plot_linearity_corr)
        #uncertAll_res_Fmean,uncertAll_res_F,uncertAll_res_up,uncertAll_res_down,ratioDict_uncertFmeanRes,ratioDict_uncertFRes,ratioDict_uncert_upRes,ratioDict_uncertF_downRes=applyFakeCorrectionToRatioDict(ratiosDict_res,nBXPerFill,slope_plot_linearity_corr,p0_plot_linearity_corr)        
        uncertAll_res_Fmean,uncertAll_res_F,uncertAll_res_up,uncertAll_res_down,ratioDict_uncertFmeanRes,ratioDict_uncertFRes,ratioDict_uncert_upRes,ratioDict_uncertF_downRes=applyFakeCorrectionToRatioDict(ratiosDict_res,nBXPerFill,slope_plotnlsAllTH2D_Pfx_res,p0_plotnlsAllTH2D_Pfx_res)        
        
        
        maxUncert=max(uncertAll_res_down,uncertAll_res_up,uncertAll_res_Fmean)
        
#        plot_linearity_fakeFmean=plotBinnedNptsTgraphFromDict_sbil(ratioDict_uncertFmeanRes,nBXPerFill,sbilNpts)
#        plot_linearity_fakeUp=plotBinnedNptsTgraphFromDict_sbil(ratioDict_uncert_upRes,nBXPerFill,sbilNpts)
#        plot_linearity_fakeDown=plotBinnedNptsTgraphFromDict_sbil(ratioDict_uncertF_downRes,nBXPerFill,sbilNpts)
#        setPlotProperties(plot_linearity_fakeFmean,"linearity plot","SBIL [hz/\mub]",labelRatio.upper()+" avg ratio",ymin,ymax)
#        setPlotProperties(plot_linearity_fakeUp,"linearity plot","SBIL [hz/\mub]",labelRatio.upper()+" avg ratio",ymin,ymax)
#        setPlotProperties(plot_linearity_fakeDown,"linearity plot","SBIL [hz/\mub]",labelRatio.upper()+" avg ratio",ymin,ymax)
#        
##        plot_linearity_corrSaved.SetMarkerColorAlpha(1,0.5)
##        plot_linearity_corrSaved.SetLineColorAlpha(1,0.5)
##        plot_linearity_corrSaved.SetMarkerSize(0.3)
##        plot_linearity_corrSaved.SetMarkerStyle(4)
#        
#        #plot_linearity_fakeFmean.SetMarkerColor(8)
#        plot_linearity_fakeFmean.SetMarkerColorAlpha(8,0.5)
#        plot_linearity_fakeFmean.SetLineColorAlpha(8,0.5)
#        plot_linearity_fakeFmean.SetMarkerSize(0.5)
#        
#        #plot_linearity_fakeUp.SetMarkerColor(2)	
#        plot_linearity_fakeUp.SetLineColorAlpha(2,0.5)
#        plot_linearity_fakeUp.SetMarkerSize(0.5)
#        plot_linearity_fakeUp.SetMarkerColorAlpha(2,0.5)
#        
#        #plot_linearity_fakeDown.SetMarkerColor(4)
#        plot_linearity_fakeDown.SetLineColorAlpha(4,0.5)
#        plot_linearity_fakeDown.SetMarkerSize(0.5)
#        plot_linearity_fakeDown.SetMarkerColorAlpha(4,0.5)
#        
#        legFakeCorr=ROOT.TLegend(.915,.77,.55,.95)
#        legFakeCorr.AddEntry(plot_linearity_corrSaved,"Original ","p")
#        legFakeCorr.AddEntry(plot_linearity_fakeFmean,"Fmean(Rotation)","p")
#        legFakeCorr.AddEntry(plot_linearity_fakeUp,"Up -> L_corr=L*(1+m*sbil)","p")
#        legFakeCorr.AddEntry(plot_linearity_fakeDown,"Down -> L_corr=L*(1-m*sbil)","p")
#        
#        plot_AllFakeCorr=ROOT.TMultiGraph()
#        
#        #plot_summary.Add(lineInSlopeAll,"L 4")
#        plot_AllFakeCorr.Add(plot_linearity_corrSaved,"AP")
#        plot_AllFakeCorr.Add(plot_linearity_fakeFmean,"AP")
#        plot_AllFakeCorr.Add(plot_linearity_fakeUp,"AP")
#        plot_AllFakeCorr.Add(plot_linearity_fakeDown,"AP")
        
        saveintoCanvas_and_toFileWIP(solocan_square,plotnlsAllTH2D_Pfx,"",
                                   outDir_corr+"/"+labelRatioMod+"_plotnlsAllTH2D.pdf","il_sq",year,energy,extraText1="lumiFit(%)="+str(float("{0:.1f}".format(porc_lumiInRange))),
                                   extraText2="linUnc(%)="+str(float("{0:.2f}".format(uncertAll_Fmean))))
        saveintoCanvas_and_toFileWIP(solocan_square,plotnlsAllTH2D_Pfx_res,"",
                                   outDir_corr+"/"+labelRatioMod+"_plotnlsAllTH2D_res.pdf","il_sq",year,energy,extraText1="lumiFit(%)="+str(float("{0:.1f}".format(porc_lumiInRange))),
                                   extraText2="linUnc(%)="+str(float("{0:.2f}".format(maxUncert))))
        
#        saveintoCanvas_and_toFileWIP(solocan_square,plot_linearity_corr,"AP4",
#                                   outDir_corr+"/"+labelRatioMod+"_plot_linearity_corr.pdf","wip_il_sq",year,extraText1="lumiFit(%)="+str(float("{0:.1f}".format(porc_lumiInRange))),
#                                   extraText2="linUnc(%)="+str(float("{0:.2f}".format(maxUncert))))
#        saveintoCanvas_and_toFileWIP(solocan_square,plot_linearity_fakeFmean,"AP",outDir_corr+"/"+labelRatioMod+"_plot_linearity_fakeFmean.pdf","wip_il_sq",year)
#        saveintoCanvas_and_toFileWIP(solocan_square,plot_linearity_fakeUp,"AP",outDir_corr+"/"+labelRatioMod+"_plot_linearity_fakeUp.pdf","wip_il_sq",year)
#        saveintoCanvas_and_toFileWIP(solocan_square,plot_linearity_fakeDown,"AP",outDir_corr+"/"+labelRatioMod+"_plot_linearity_fakeDown.pdf","wip_il_sq",year)
#        plot_AllFakeCorr.SetTitle("plot_AllFakeCorrp_lot; SBIL [(hz/ub)] ; ratio")
#        saveintoCanvas_and_toFileWIP(solocan_square,plot_AllFakeCorr,"A",
#                                   outDir_corr+"/"+labelRatioMod+"_plot_AllFakeCorr.pdf","wip_il_sq",year,
#                                   ymin=0.98,ymax=1.02,drawleg=True,leg=legFakeCorr)
                
        
    else:
        plotnlsAllTH2D_Pfx.Fit("pol1")   
        saveintoCanvas_and_toFileWIP(solocan_square,plotnlsAllTH2D_Pfx,"",
                                   outDir_corr+"/"+labelRatioMod+"_plotnlsAllTH2D.pdf","il_sq",year,energy)
        fall=plotnlsAllTH2D_Pfx.GetFunction("pol1")
        slope_plotnlsAllTH2D_Pfx=fall.GetParameter(1)
        slope_plotnlsAllTH2D_Pfx_Err=fall.GetParError(1)
        p0_plotnlsAllTH2D_Pfx=fall.GetParameter(0)
        
        plotnlsAllTH2D_Pfx_res.Fit("pol1")
        fall_res=plotnlsAllTH2D_Pfx_res.GetFunction("pol1")
        slope_plotnlsAllTH2D_Pfx_res=fall_res.GetParameter(1)
        slope_plotnlsAllTH2D_Pfx_Err_res=fall_res.GetParError(1)
        p0_plotnlsAllTH2D_Pfx_res=fall_res.GetParameter(0)
        
        slope_plot_linearity_corr=slope_plotnlsAllTH2D_Pfx_res
        slope_Err_plot_linearity_corr=slope_plotnlsAllTH2D_Pfx_Err_res
        p0_plot_linearity_corr=p0_plotnlsAllTH2D_Pfx_res
        
        
#        plot_linearity_corr=plotBinnedNptsTgraphFromDict_sbil(ratiosDict_res,nBXPerFill,sbilNpts)
#        plot_linearity_corr.Fit("pol1")
#        fall_res=plot_linearity_corr.GetFunction("pol1")
#        slope_plot_linearity_corr=fall_res.GetParameter(1)
#        slope_Err_plot_linearity_corr=fall_res.GetParError(1)
#        p0_plot_linearity_corr=fall_res.GetParameter(0)
#        plot_linearity_corr.SetFillColor(6);
#        plot_linearity_corr.SetFillStyle(3005)
#        setPlotProperties(plot_linearity_corr,"linearity plot","SBIL [hz/\mub]",labelRatio+" avg ratio",ymin,ymax)
        
        uncertAll_Fmean,uncertAll_F,uncertAll_up,uncertAll_down,ratioDict_uncertFmean,ratioDict_uncertF,ratioDict_uncert_up,ratioDict_uncertF_down=applyFakeCorrectionToRatioDict(ratiosDict,nBXPerFill,slope_plot_linearity_corr,p0_plot_linearity_corr)
        uncertAll_res_Fmean,uncertAll_res_F,uncertAll_res_up,uncertAll_res_down,ratioDict_uncertFmeanRes,ratioDict_uncertFRes,ratioDict_uncert_upRes,ratioDict_uncertFRes_down=applyFakeCorrectionToRatioDict(ratiosDict_res,nBXPerFill,slope_plot_linearity_corr,p0_plot_linearity_corr)        
        
        maxUncert=max(uncertAll_res_down,uncertAll_res_up,uncertAll_res_Fmean)
        
        saveintoCanvas_and_toFileWIP(solocan_square,plotnlsAllTH2D_Pfx,"",
                                   outDir_corr+"/"+labelRatioMod+"_plotnlsAllTH2D.pdf","il_sq",year,energy,extraText1="lumiFit(%)="+str(float("{0:.1f}".format(porc_lumiInRange))),
                                   extraText2="linUnc(%)="+str(float("{0:.2f}".format(uncertAll_Fmean))))
        saveintoCanvas_and_toFileWIP(solocan_square,plotnlsAllTH2D_Pfx_res,"",
                                   outDir_corr+"/"+labelRatioMod+"_plotnlsAllTH2D_res.pdf","il_sq",year,energy,extraText1="lumiFit(%)="+str(float("{0:.1f}".format(porc_lumiInRange))),
                                   extraText2="linUnc(%)="+str(float("{0:.2f}".format(maxUncert))))
        
#        saveintoCanvas_and_toFileWIP(solocan_square,plot_linearity_corr,"AP4",
#                                   outDir_corr+"/"+labelRatioMod+"_plot_linearity_corr.pdf","wip_il_sq",year,extraText1="lumiFit(%)="+str(float("{0:.1f}".format(porc_lumiInRange))),
#                                   extraText2="linUnc(%)="+str(float("{0:.2f}".format(maxUncert)))) 
        
    ##Create Summary Plot ############################################################################################
    lineInSlopeAll=ROOT.TGraphErrors()
    lineInSlopeAll_res=ROOT.TGraphErrors()
    
    lineInSlopeMeanHist=ROOT.TGraphErrors()
    lineInSlopeMeanHist_err=ROOT.TGraphErrors()
    
    lineInSlopeMeanHist_lw=ROOT.TGraphErrors()
    lineInSlopeMeanHist_lw_err=ROOT.TGraphErrors()
    
    lineInSlopeMeanHist_lw_ew_err=ROOT.TGraphErrors()
    
    lineInSlopeMean_errW=ROOT.TGraphErrors()
    
    iBin=0
    lNpts=50
    deltaL=savedTotLumi/lNpts
    for l in range(0,lNpts+1):
        lumil=l*deltaL        
        lineInSlopeAll.SetPoint(iBin,lumil*23.31/1000000000.,slope_plotnlsAllTH2D_Pfx)
        lineInSlopeAll.SetPointError(iBin,0.0,slope_plotnlsAllTH2D_Pfx_Err)
        lineInSlopeAll_res.SetPoint(iBin,lumil*23.31/1000000000.,slope_plot_linearity_corr)
        lineInSlopeAll_res.SetPointError(iBin,0.0,slope_Err_plot_linearity_corr)
       
        lineInSlopeMeanHist.SetPoint(iBin,lumil*23.31/1000000000.,meanSlope)
        lineInSlopeMeanHist.SetPointError(iBin,0.0,stdvSlope)
        lineInSlopeMeanHist_err.SetPoint(iBin,lumil*23.31/1000000000.,meanSlope)
        lineInSlopeMeanHist_err.SetPointError(iBin,0.0,errSlope)
        
        lineInSlopeMeanHist_lw.SetPoint(iBin,lumil*23.31/1000000000.,meanSlope_lw)
        lineInSlopeMeanHist_lw.SetPointError(iBin,0.0,stdvSlope_lw)
        lineInSlopeMeanHist_lw_err.SetPoint(iBin,lumil*23.31/1000000000.,meanSlope_lw)
        lineInSlopeMeanHist_lw_err.SetPointError(iBin,0.0,errSlope_lw)
        
        lineInSlopeMeanHist_lw_ew_err.SetPoint(iBin,lumil*23.31/1000000000.,meanSlope_errw_lw)
        lineInSlopeMeanHist_lw_ew_err.SetPointError(iBin,0.0,errSlope_errw_lw)
        
        lineInSlopeMean_errW.SetPoint(iBin,lumil*23.31/1000000000.,meanSlope_ErrW)
        lineInSlopeMean_errW.SetPointError(iBin,0.0,err_meanSlope_ErrW)
        iBin+=1
    
    lineInSlopeAll.SetFillColor(8)
    lineInSlopeAll.SetLineColor(8)
    lineInSlopeAll.SetFillStyle(3004)
    
    lineInSlopeAll_res.SetFillColor(9)
    lineInSlopeAll_res.SetLineColor(9)
    lineInSlopeAll_res.SetFillStyle(3004)
    lineInSlopeAll_res.SetLineStyle(10)
    
    lineInSlopeMeanHist_err.SetFillColor(8)
    lineInSlopeMeanHist_err.SetLineColor(8)
    lineInSlopeMeanHist_err.SetFillStyle(3003)
    
    lineInSlopeMeanHist_lw.SetFillColor(7)
    lineInSlopeMeanHist_lw.SetLineColor(7)
    lineInSlopeMeanHist_lw.SetFillStyle(3003)
    
    lineInSlopeMean_errW.SetFillColor(6)
    lineInSlopeMean_errW.SetLineColor(6)
    lineInSlopeMean_errW.SetFillStyle(3005)
    
    lineInSlopeMeanHist_lw_err.SetLineStyle(10)
    lineInSlopeMeanHist_lw_err.SetFillColor(3)
    lineInSlopeMeanHist_lw_err.SetLineColor(3)
    lineInSlopeMeanHist_lw_err.SetFillStyle(3004)
    
    lineInSlopeMeanHist_lw_ew_err.SetLineStyle(10)
    lineInSlopeMeanHist_lw_ew_err.SetFillColor(4)
    lineInSlopeMeanHist_lw_ew_err.SetLineColor(4)
    lineInSlopeMeanHist_lw_ew_err.SetFillStyle(3005)
    
    legSummary=ROOT.TLegend(.915,.77,.66,.95)
    plot_summary=ROOT.TMultiGraph("summary","summary")
    
    if setts.flags_summary_byFillSlopesVsIntLumiAvg:
        legSummary.AddEntry(byFillSlopesVsIntLumiAvg,"By fill slopes (Avg)","pe")
        plot_summary.Add(byFillSlopesVsIntLumiAvg,"AP")
    if setts.flags_summary_lineInSlopeMeanHist_lw_err:
        legSummary.AddEntry(lineInSlopeMeanHist_lw_err,"Mean (L weight.) #pm err","lf")
        plot_summary.Add(lineInSlopeMeanHist_lw_err,"L 4")
    if setts.flags_summary_lineInSlopeMean_errW:
        legSummary.AddEntry(lineInSlopeMean_errW,"Mean (err weight.) #pm err","lf")
        plot_summary.Add(lineInSlopeMean_errW,"L 4")
    if setts.flags_summary_lineInSlopeMeanHist_err:
        legSummary.AddEntry(lineInSlopeMeanHist_err,"Mean #pm err","lf")
        plot_summary.Add(lineInSlopeMeanHist_err,"L 4")
    if setts.flags_summary_lineInSlopeAll_res:
        legSummary.AddEntry(lineInSlopeAll_res,"comb. meth. slope #pm err","lf")
        plot_summary.Add(lineInSlopeAll_res,"L 4")
        
    legSummary.AddEntry(lineInSlopeMeanHist_lw_ew_err,"lumi and err. weighted slope #pm err","lf")
    plot_summary.Add(lineInSlopeMeanHist_lw_ew_err,"L 4")
    
    
    
    
    summary_min=slope_minval
    summary_max=slope_maxval
    
    if year==2016:
        summary_min=-0.006
        summary_max=0.006
    
    
    outputdict={}
    outputdict["Suggested uncertainty (from err. and lumi. weighted hist)"]=abs(totLumi-totLumiMean_lw_errw_up)*100/totLumi
    outputdict["meanSlope lw err hist"]=meanSlope_errw_lw
    outputdict["stdvSlope lw err hist"]=stdvSlope_errw_lw
    outputdict["errSlope lw err hist"]=errSlope_errw_lw
    outputdict["meanSlope lw hist"]=meanSlope_lw
    outputdict["stdvSlope lw hist"]=stdvSlope_lw
    outputdict["errSlope lw hist"]=errSlope_lw
    outputdict["meanSlope errw hist"]=meanSlope_errw
    outputdict["stdvSlope errw hist"]=stdvSlope_errw
    outputdict["errSlope errw hist"]=errSlope_errw  
    outputdict["meanSlope hist"]=meanSlope
    outputdict["stdvSlope hist"]=stdvSlope
    outputdict["errSlope hist"]=errSlope
    
    
    #slope histogram
    outputdict["unc_mean_up"]=abs(totLumi-totLumiMean_up)*100/totLumi
    outputdict["unc_mean_down"]=abs(totLumi-totLumiMean_down)*100/totLumi
    
    outputdict["unc_mean_lw"]=abs(totLumi-totLumiMean_lw)*100/totLumi
    outputdict["unc_stdvSlope_lw"]=abs(totLumistdv_lw-totLumi)*100/totLumi
    outputdict["unc_mean_plus_lw"]=abs(totLumi-totLumiMean_plus_lw)*100/totLumi
    outputdict["unc_mean_low_lw"]=abs(totLumi-totLumiMean_low_lw)*100/totLumi
    outputdict["unc_mean_up_lw"]=abs(totLumi-totLumiMean_up_lw)*100/totLumi
    outputdict["unc_mean_down_lw"]=abs(totLumi-totLumiMean_down_lw)*100/totLumi
    
    outputdict["unc_mean_errw_up"]=abs(totLumi-totLumiMean_errw_up)*100/totLumi
    outputdict["unc_mean_errw_down"]=abs(totLumi-totLumiMean_errw_down)*100/totLumi
    #outputdict["unc_mean_low_plus"]=abs(totLumiMean_low-totLumiMean_plus)*100/totLumi
    
    outputdict["unc_mean_lw_errw_up"]=abs(totLumi-totLumiMean_lw_errw_up)*100/totLumi
    outputdict["unc_mean_lw_errw_down"]=abs(totLumi-totLumiMean_lw_errw_down)*100/totLumi
    
    ##fill by fill slopes
    outputdict["unc_bff"]=abs(totLumi-totLumiPerFillCorr)*100/totLumi
    outputdict["unc_bff_plus"]=abs(totLumi-totLumiPerFillCorr_plus)*100/totLumi
    outputdict["unc_bff_low"]=abs(totLumi-totLumiPerFillCorr_low)*100/totLumi
    outputdict["unc_bff_up"]=abs(totLumi-totLumiPerFillCorr_up)*100/totLumi
    outputdict["unc_bff_down"]=abs(totLumi-totLumiPerFillCorr_down)*100/totLumi
    
    ##fill by fill slopes (rs)
    outputdict["unc_bff_rs"]=abs(totLumi_scaled-totLumiPerFillCorr_rs)*100/totLumi_scaled
    outputdict["unc_bff_rs_plus"]=abs(totLumi_scaled-totLumiPerFillCorr_plus_rs)*100/totLumi_scaled
    outputdict["unc_bff_rs_low"]=abs(totLumi_scaled-totLumiPerFillCorr_low_rs)*100/totLumi_scaled
    outputdict["unc_bff_rs_up"]=abs(totLumi_scaled-totLumiPerFillCorr_rs_up)*100/totLumi_scaled
    outputdict["unc_bff_rs_down"]=abs(totLumi_scaled-totLumiPerFillCorr_rs_down)*100/totLumi_scaled
    
    ##all data slope
    outputdict["allSlope"]=slope_plot_linearity_corr
    outputdict["allSlopeErr"]=slope_Err_plot_linearity_corr
    outputdict["unc_allSlope"]=uncertAll_Fmean
    outputdict["unc_allSlope_up"]=uncertAll_up
    outputdict["unc_allSlope_down"]=uncertAll_down
    
    ##all data slope (applied to rescale data)
    outputdict["unc_allSlope_res"]=uncertAll_res_Fmean
    outputdict["unc_allSlope_up_res"]=uncertAll_res_up
    outputdict["unc_allSlope_down_res"]=uncertAll_res_down
    outputdict["byFill slope min"]=min(slopeList)
    outputdict["byFill slope max"]=max(slopeList)
    outputdict["mean_errW"]=meanSlope_ErrW
    outputdict["err_meanSlope_ErrW"]=err_meanSlope_ErrW
    outputdict["savedTotLumi"]=savedTotLumi
    outputdict["summary_min"]=summary_min
    outputdict["summary_max"]=summary_max
    
    ##Saving some functions:
    #outputdict["plot_byFillSlopesVsIntLumiAvg"]=byFillSlopesVsIntLumiAvg
    fout = ROOT.TFile(outDir_corr+"/"+labelRatioMod+"_byFillSlopesVsIntLumiAvg.root", "recreate")
    fout.WriteTObject(byFillSlopesVsIntLumiAvg,"byFillSlopesVsIntLumiAvg")
    
    
    
    plot_summary.SetTitle("summary plot; Integrated Luminosity [fb^{-1}]; "+labelRatio.upper() +" slope [(hz/ub)^{-1}]");
    maxUncert_bff=max(outputdict["unc_bff_rs_up"],outputdict["unc_bff_rs_down"])
    
    saveintoCanvas_and_toFileWIP(solocan_square,plot_summary,"A",
                                   outDir_corr+"/"+labelRatioMod+"_plot_summary.pdf","il_sq",year,energy,
                                   ymin=summary_min,ymax=summary_max,drawleg=True,leg=legSummary,
                                   #0pextraText2="Lumi. uncert (byFill): "+str(float("{0:.2f}".format(maxUncert_bff)))+"%",pos_extraText2=[0.87,0.21],s_extraText2=0.025,
                                   #extraText1="Lumi. uncert (comb.): "+str(float("{0:.2f}".format(maxUncert)))+"%",pos_extraText1=[0.87,0.25],s_extraText1=0.025
                                   )
#    saveintoCanvas_and_toFileWIP(solocan_square,plot_summary_errAvg,"A",
#                                   outDir_corr+"/"+labelRatioMod+"_plot_summary_errAvg.pdf","wip_il_sq",year,
#                                   ymin=summary_min,ymax=summary_max,drawleg=True,leg=legSummary_errAvg,
#                                   #0pextraText2="Lumi. uncert (byFill): "+str(float("{0:.2f}".format(maxUncert_bff)))+"%",pos_extraText2=[0.87,0.21],s_extraText2=0.025,
#                                   #extraText1="Lumi. uncert (comb.): "+str(float("{0:.2f}".format(maxUncert)))+"%",pos_extraText1=[0.87,0.25],s_extraText1=0.025
#                                   )
#    saveintoCanvas_and_toFileWIP(solocan_square,plot_summary_comb,"A",
#                                   outDir_corr+"/"+labelRatioMod+"_plot_summary_comb.pdf","wip_il_sq",year,
#                                   ymin=summary_min,ymax=summary_max,drawleg=True,leg=legSummary_comb,
#                                   #0pextraText2="Lumi. uncert (byFill): "+str(float("{0:.2f}".format(maxUncert_bff)))+"%",pos_extraText2=[0.87,0.21],s_extraText2=0.025,
#                                   #extraText1="Lumi. uncert (comb.): "+str(float("{0:.2f}".format(maxUncert)))+"%",pos_extraText1=[0.87,0.25],s_extraText1=0.025
#                                   )
#    saveintoCanvas_and_toFileWIP(solocan_square,plot_summary_all,"A",
#                                   outDir_corr+"/"+labelRatioMod+"_plot_summary_all.pdf","wip_il_sq",year,
#                                   ymin=summary_min,ymax=summary_max,drawleg=True,leg=legSummary_all,
#                                   #0pextraText2="Lumi. uncert (byFill): "+str(float("{0:.2f}".format(maxUncert_bff)))+"%",pos_extraText2=[0.87,0.21],s_extraText2=0.025,
#                                   #extraText1="Lumi. uncert (comb.): "+str(float("{0:.2f}".format(maxUncert)))+"%",pos_extraText1=[0.87,0.25],s_extraText1=0.025
#                                   )
    
    
    
    ##Summary text file
    summaryFileName=labelRatioMod+"_linearityUncertainty.txt"
    summaryFile=open(outDir_corr+"/" +summaryFileName,'w')
    saveInfoInFile(summaryFile,outputdict)
    summaryFile.close()
    
    
    return outputdict,fillLinearityInfo

def saveInfoInFile(outFile,outputdict,selectedKeys=[]):
    
    lines="********** Summary info ****************** \n"
    if len(selectedKeys)>0:
        keys=selectedKeys
    else:
        keys=list(outputdict)
    keys.sort()
    for key in keys:
        lines+=(key+":"+str(outputdict[key])+ "\n")
    outFile.write(lines)

def plot_linearitySummaryTwoDetects(outputdict1,outputdict2,labelRatio1,labelRatio2,outputName,ymin=0.0,ymax=0.0):
    lineInSlopeAll_res1=ROOT.TGraphErrors()
    lineInSlopeMeanHist_err1=ROOT.TGraphErrors()

    slope_plot_linearity_corr1=outputdict1["allSlope"]
    slope_Err_plot_linearity_corr1=outputdict1["allSlopeErr"]
    meanSlope1=outputdict1["meanSlope"]
    errSlope1=outputdict1["errSlope"]
    
    lineInSlopeAll_res2=ROOT.TGraphErrors()
    lineInSlopeMeanHist_err2=ROOT.TGraphErrors()
    
    slope_plot_linearity_corr2=outputdict2["allSlope"]
    slope_Err_plot_linearity_corr2=outputdict2["allSlopeErr"]
    meanSlope2=outputdict2["meanSlope"]
    errSlope2=outputdict2["errSlope"]   
    
    iBin=0
    lNpts=50
    savedTotLumi=max(outputdict1["savedTotLumi"],outputdict2["savedTotLumi"])
    deltaL=savedTotLumi/lNpts
    
    for l in range(0,lNpts+1):
        lumil=l*deltaL
        lineInSlopeAll_res1.SetPoint(iBin,lumil*23.31/1000000000.,slope_plot_linearity_corr1)
        lineInSlopeAll_res1.SetPointError(iBin,0.0,slope_Err_plot_linearity_corr1)
        lineInSlopeMeanHist_err1.SetPoint(iBin,lumil*23.31/1000000000.,meanSlope1)
        lineInSlopeMeanHist_err1.SetPointError(iBin,0.0,errSlope1)
        
        lineInSlopeAll_res2.SetPoint(iBin,lumil*23.31/1000000000.,slope_plot_linearity_corr2)
        lineInSlopeAll_res2.SetPointError(iBin,0.0,slope_Err_plot_linearity_corr2)
        lineInSlopeMeanHist_err2.SetPoint(iBin,lumil*23.31/1000000000.,meanSlope2)
        lineInSlopeMeanHist_err2.SetPointError(iBin,0.0,errSlope2)
        
        
        iBin+=1
    
    lineInSlopeAll_res1.SetFillColor(ROOT.kBlue)
    lineInSlopeAll_res1.SetLineColor(ROOT.kBlue)
    lineInSlopeAll_res1.SetFillStyle(3004)
    lineInSlopeMeanHist_err1.SetFillColor(ROOT.kViolet)
    lineInSlopeMeanHist_err1.SetLineColor(ROOT.kViolet)
    lineInSlopeMeanHist_err1.SetLineStyle(9)
    lineInSlopeMeanHist_err1.SetFillStyle(3005)
    
    lineInSlopeAll_res2.SetFillColor(ROOT.kRed)
    lineInSlopeAll_res2.SetLineColor(ROOT.kRed)
    lineInSlopeAll_res2.SetFillStyle(3004)
    lineInSlopeMeanHist_err2.SetFillColor(ROOT.kPink)
    lineInSlopeMeanHist_err2.SetLineColor(ROOT.kPink)
    lineInSlopeMeanHist_err2.SetLineStyle(9)
    lineInSlopeMeanHist_err2.SetFillStyle(3005)
    
    labelRatioMod1=labelRatio1.replace('/','-')
    labelRatioMod2=labelRatio2.replace('/','-')
    f1 = ROOT.TFile(outputName+"/"+labelRatioMod1+"_byFillSlopesVsIntLumiAvg.root")
    f2 = ROOT.TFile(outputName+"/"+labelRatioMod2+"_byFillSlopesVsIntLumiAvg.root")
    byFillSlopesVsIntLumiAvg1=f1.Get("byFillSlopesVsIntLumiAvg")
    byFillSlopesVsIntLumiAvg1.SetMarkerColor(9)
    byFillSlopesVsIntLumiAvg1.SetLineColor(9)
    byFillSlopesVsIntLumiAvg2=f2.Get("byFillSlopesVsIntLumiAvg")
    byFillSlopesVsIntLumiAvg2.SetMarkerColor(2)
    byFillSlopesVsIntLumiAvg2.SetLineColor(2)
    
    legSummary=ROOT.TLegend(.915,.77,.55,.95)
    legSummary.AddEntry(byFillSlopesVsIntLumiAvg1,"fill by fill ("+labelRatio1+")","pe")
    legSummary.AddEntry(lineInSlopeAll_res1,"comb. ("+labelRatio1+")","lf")
    legSummary.AddEntry(lineInSlopeMeanHist_err1,"mean ("+labelRatio1+")","lf")
    
    legSummary.AddEntry(byFillSlopesVsIntLumiAvg2,"fill by fill ("+labelRatio2+")","pe")    
    legSummary.AddEntry(lineInSlopeAll_res2,"comb. ("+labelRatio2+")","lf")    
    legSummary.AddEntry(lineInSlopeMeanHist_err2,"mean ("+labelRatio2+")","lf")
    
    plot_summary=ROOT.TMultiGraph()
    plot_summary.Add(byFillSlopesVsIntLumiAvg1,"AP")
    plot_summary.Add(byFillSlopesVsIntLumiAvg2,"AP")
    plot_summary.Add(lineInSlopeAll_res1,"L 4")
    plot_summary.Add(lineInSlopeAll_res2,"L 4")
    plot_summary.Add(lineInSlopeMeanHist_err1,"L 4")
    plot_summary.Add(lineInSlopeMeanHist_err2,"L 4")
    
    plot_summary.SetTitle("summary plot all; Integrated Luminosity [fb^{-1}];  slope [(hz/ub)^{-1}]")    
    
    return plot_summary,legSummary

def getMean_Errs(slopeListForMean):
    N=len(slopeListForMean)
    vals=[]
    errs=[]
    
    for i in range(0,N-1):
        vals.append(slopeListForMean[i][0])
        errs.append(slopeListForMean[i][1])
    
    meanVal=statistics.mean(vals)
    
    systerr=0.0
    for i in range(0,N-1):
        systerr+=(errs[i]/vals[i])**2
    
    systerr = meanVal*math.sqrt(systerr)
    
    staterr=statistics.stdev(vals)/math.sqrt(N)
    
    err=math.sqrt(systerr**2 + staterr**2)
    
    return meanVal,err

def getMean_wErrs(slopeListForMean,minlim=0.0,maxlim=0.0):
    N=len(slopeListForMean)
    Nsel=0
    sumw=0.0
    meanVal=0.0
    vals=[]
    
    uselim=False
    if minlim!=0 and maxlim!=0:
        uselim=True
        
    if uselim==True:        
        for i in range(0,N):
            if slopeListForMean[i][0]>=minlim and slopeListForMean[i][0]<=maxlim:
                wi = (1/(slopeListForMean[i][1]**2))
                sumw += wi
                meanVal+= wi*slopeListForMean[i][0]
                vals.append(slopeListForMean[i][0])
                Nsel+=1
    else:
        for i in range(0,N):
            wi = (1/(slopeListForMean[i][1]**2))
            sumw += wi
            meanVal+= wi*slopeListForMean[i][0]
            vals.append(slopeListForMean[i][0])
            Nsel+=1
    
    
    #err=math.sqrt(1/sumw)
    err=statistics.stdev(vals)/math.sqrt(Nsel)
    
    return meanVal,err

def getMean_wErrsLumi(slopeListForMean,minlim=0.0,maxlim=0.0):
    N=len(slopeListForMean)
    sumEw=0.0
    sumLw=0.0
    meanVal=0.0
    vals=[]
    
    uselim=False
    if minlim!=0 and maxlim!=0:
        uselim=True
        
    if uselim==True:        
        for i in range(0,N):
            if slopeListForMean[i][0]>=minlim and slopeListForMean[i][0]<=maxlim:
                sumEw += (1/(slopeListForMean[i][1]**2))
                sumLw += slopeListForMean[i][2]**2
                vals.append(slopeListForMean[i][0])
        for i in range(0,N):
            if slopeListForMean[i][0]>=minlim and slopeListForMean[i][0]<=maxlim:
                meanVal+= (((1/(slopeListForMean[i][1]**2))/sumEw + (slopeListForMean[i][2]**2)/sumLw)/2)*slopeListForMean[i][0]
    else:
        for i in range(0,N):
            sumEw += (1/(slopeListForMean[i][1]**2))
            sumLw += slopeListForMean[i][2]**2
            vals.append(slopeListForMean[i][0])
        for i in range(0,N):
                meanVal+= (((1/(slopeListForMean[i][1]**2))/sumEw + (slopeListForMean[i][2]**2)/sumLw)/2)*slopeListForMean[i][0]

    
    meanVal=meanVal
    #err=math.sqrt(1/sumw)
    err=statistics.stdev(vals)/math.sqrt(len(vals))
    
    return meanVal,err


def applyFakeCorrectionToRatioDict(ratioDict,nBXPerFill,slope,p0=1):
    keys=list(ratioDict)
    keys.sort()
    totLumiReal=0.0
    totLumiFakeFmean=0.0
    totLumiFakeF=0.0
    totLumiFake_up=0.0
    totLumiFake_down=0.0
    ratios_list=[]
    ratioDict_uncertFmean={}
    ratioDict_uncertF={}
    ratioDict_uncert_up={}
    ratioDict_uncertF_down={}
    
    for key in keys:
        ratios_list.append(ratioDict[key][0])        
    
    mean_ratio=statistics.mean(ratios_list)
    
    for key in keys:
        lumi1=ratioDict[key][1]
        lumi2=ratioDict[key][2]
        totLumiReal+=lumi2
        sbil=lumi2/nBXPerFill[key[0]]
        
        lumi2fakeFmean=lumi2*(p0 + sbil*slope)/mean_ratio
        lumi2fakeF=lumi2*(p0 + sbil*slope)
        lumi2fakeF_up=lumi2*(1 + sbil*slope)
        lumi2fakeF_down=lumi2*(1 - sbil*slope)
        
        
        totLumiFakeFmean+=lumi2fakeFmean
        totLumiFakeF+=lumi2fakeF
        totLumiFake_up+=lumi2fakeF_up
        totLumiFake_down+=lumi2fakeF_down
        
        ratioDict_uncertFmean[key]=[lumi1/lumi2fakeFmean,lumi1,lumi2fakeFmean,lumi1+lumi2fakeFmean]
        ratioDict_uncertF[key]=[lumi1/lumi2fakeF,lumi1,lumi2fakeF,lumi1+lumi2fakeF]
        ratioDict_uncert_up[key]=[lumi1/lumi2fakeF_up,lumi1,lumi2fakeF_up,lumi1+lumi2fakeF_up]
        ratioDict_uncertF_down[key]=[lumi1/lumi2fakeF_down,lumi1,lumi2fakeF_down,lumi1+lumi2fakeF_down]
        
    uncertFmean=abs(totLumiFakeFmean-totLumiReal)*100/totLumiReal
    uncertF=abs(totLumiFakeF-totLumiReal)*100/totLumiReal
    uncert_up=abs(totLumiFake_up-totLumiReal)*100/totLumiReal
    uncertF_down=abs(totLumiFake_down-totLumiReal)*100/totLumiReal
    
    return uncertFmean,uncertF,uncert_up,uncertF_down,ratioDict_uncertFmean,ratioDict_uncertF,ratioDict_uncert_up,ratioDict_uncertF_down

def normalizeDict(ratiosDict,meanVal):
    keys=list(ratiosDict)
    keys.sort()
    for key in keys:
        ratiosDict[key][0] = ratiosDict[key][0]/meanVal
    
    return ratiosDict

def applyFakeCorrectionToRatioDictNls(ratioDict,nBXPerFill,nls,slope,p0=1):
    keys=list(ratioDict)
    keys.sort()
    totLumiReal=0.0
    totLumiFake=0.0
    inls=0
    count=0
    lumi2nls=0.0
    fill=keys[0][0]
    
    for key in keys:
        inls+=1
        count+=1
        lumi2=ratioDict[key][2]
        lumi2nls+=lumi2
        totLumiReal+=lumi2
        
        if inls==nls or count==len(keys) or key[0]!=fill:
            sbilnls=lumi2nls/(nBXPerFill[fill]*inls)        
            lumi2fake=lumi2nls*(p0 + sbilnls*slope)        
            totLumiFake+=lumi2fake
            
            lumi2nls=0.0
        
        if key[0]!=fill:
            fill=key[0]
    
    return abs(totLumiFake-totLumiReal)*100/totLumiReal
    


def plot_ratioInDict_vs_IntLumi(Dict,xlabel,ylabel,title,ymin,ymax):
    f=ROOT.TGraph()
    
    keys=list(Dict)
    keys.sort()
    
    iBin=0
    IntLumi=0.0
    for key in keys:
        IntLumi+=Dict[key][1]*23.31/1000000000.
        f.SetPoint(iBin,IntLumi,Dict[key][0])
        iBin+=1
    
    color=4
    f.SetMarkerColor(color)
    f.SetMarkerSize(0.3)
    f.SetLineWidth(1)
    f.SetMarkerStyle(2)
    f.SetLineColor(33)
    f.SetFillColor(color)
    f.SetTitle(ylabel+" "+title)
    f.GetXaxis().SetTitle(xlabel)
    f.GetYaxis().SetTitle(ylabel)
    f.GetYaxis().SetRangeUser(ymin, ymax)
    f.GetXaxis().SetRangeUser(0.0, IntLumi)
    
    return f

    

#def plot_ratioInDict_vs_VariablePos_errs(Dict,pos_x_inKeys,totLumi,xlabel,ylabel,title,ymin,ymax,xmin,xmax):
#    
#    
#    ex=None
#    ey=[]
#    x=[]
#    y=[]
#    keys=list(Dict)
#    keys.sort()
#    
#    for key in keys:
#        x.append(key[pos_x_inKeys])
#        y.append(Dict[key][0])
#        ey.append(Dict[key][2]/totLumi)
#    
#    n=len(keys)
#    f=ROOT.TGraphErrors(n,x,y,ex,ey)
#    
#    color=4
#    f.SetMarkerColor(color)
#    f.SetMarkerSize(0.3)
#    f.SetLineWidth(1)
#    f.SetMarkerStyle(2)
#    f.SetLineColor(33)
#    f.SetFillColor(color)
#    f.SetTitle(ylabel+" "+title)
#    f.GetXaxis().SetTitle(xlabel)
#    f.GetYaxis().SetTitle(ylabel)
#    f.GetYaxis().SetRangeUser(ymin, ymax)
#    
#    if xmin!=0 and xmax!=0:
#        f.GetXaxis().SetRangeUser(xmin, xmax)
#        
#    return f    

## For comparing an array of Histos using THStack class 
def plot_Hist2D_byruns(overlapKeys,dict1,dict2,xlabel,ylabel,title,ymin,ymax,nxbins,nybins):
    
    x0=overlapKeys[0][1]
    print ("initial Run: ",x0)
    maxval=overlapKeys[len(overlapKeys)-1][1]-x0
    print ("Final Run: ",overlapKeys[len(overlapKeys)-1][1])
    print ("Plot equivalent Final Run: ",maxval)
    f=ROOT.TH2F("ratios_by_runs","",nxbins,0,maxval,nybins,ymin,ymax)
    
    
    
    for key in overlapKeys:
        try:
           f.Fill(key[1]-x0,dict1[key][1]/dict2[key][1])
            
        except:
           pass
       
    #f.GetYaxis().SetRangeUser(ymin, ymax)
#    f.SetMarkerColor(4)
#    f.SetMarkerSize(0.5)
#    f.SetMarkerStyle(20)
#    f.SetLineColor(4)
#    f.SetFillColor(4)
    f.SetTitle(ylabel+" "+title)
    f.GetXaxis().SetTitle(xlabel)
    f.GetYaxis().SetTitle(ylabel)
    
    return f    




def plot_ratio_TH2D(ratios,xlabel,ylabel,title,ymin,ymax,nxbins,nybins):
    maxval = ratios[len(ratios)-1][0]
    minval = ratios[0][0]
    
    f=ROOT.TH2F(title+"TH2D",title,nxbins,minval,maxval,nybins,ymin,ymax)  
    
    for iBin in range(0,len(ratios)):
        try:
            f.Fill(ratios[iBin][0],ratios[iBin][1])
            
        except:
           pass
       
    #color=4
   # xmax=len(ratios)-1
#    f.SetMarkerColor(color)
#    f.SetMarkerSize(0.1)
#    f.SetMarkerStyle(20)
#    f.SetLineColor(color)
#    f.SetFillColor(color)
#    f.SetTitle(ylabel+" "+title)
#    f.GetXaxis().SetTitle(xlabel)
#    f.GetYaxis().SetTitle(ylabel)
#    f.GetYaxis().SetRangeUser(ymin, ymax)
   # f.GetXaxis().SetRangeUser(0, xmax)
    
    return f

def plot_ratio_TH2D_fromDict(Dict,pos_varKey,xlabel,ylabel,title,ymin,ymax,nxbins,nybins):
    keys=list(Dict)
    keys.sort()
    maxval = keys[len(keys)-1][pos_varKey]
    minval = keys[0][pos_varKey]
    
    f=ROOT.TH2F(title+"TH2D"+str(pos_varKey),title,nxbins,minval,maxval,nybins,ymin,ymax)  
    
    for key in keys:
        f.Fill(key[pos_varKey],Dict[key][0])
       
    #color=4
   # xmax=len(ratios)-1
#    f.SetMarkerColor(color)
#    f.SetMarkerSize(0.1)
#    f.SetMarkerStyle(20)
#    f.SetLineColor(color)
#    f.SetFillColor(color)
#    f.SetTitle(ylabel+" "+title)
#    f.GetXaxis().SetTitle(xlabel)
#    f.GetYaxis().SetTitle(ylabel)
    f.GetYaxis().SetRangeUser(ymin, ymax)
   # f.GetXaxis().SetRangeUser(0, xmax)
    
    return f

def plot_ratio_TH2D_fromDict_lumiW(Dict,pos_varKey,pos_varW,totW,xlabel,ylabel,title,ymin,ymax,nxbins,nybins):
    keys=list(Dict)
    keys.sort()
    maxval = keys[len(keys)-1][pos_varKey]
    minval = keys[0][pos_varKey]
    
    f=ROOT.TH2F(title+"TH2D"+str(pos_varKey),title,nxbins,minval,maxval,nybins,ymin,ymax)  
    
    for key in keys:
        f.Fill(key[pos_varKey],Dict[key][0],Dict[key][pos_varW]/totW)
       
    #color=4
   # xmax=len(ratios)-1
#    f.SetMarkerColor(color)
#    f.SetMarkerSize(0.1)
#    f.SetMarkerStyle(20)
#    f.SetLineColor(color)
#    f.SetFillColor(color)
#    f.SetTitle(ylabel+" "+title)
#    f.GetXaxis().SetTitle(xlabel)
#    f.GetYaxis().SetTitle(ylabel)
    f.GetYaxis().SetRangeUser(ymin, ymax)
   # f.GetXaxis().SetRangeUser(0, xmax)
    
    return f

def plot_ratio_TH2D_fromDictInverted(Dict,pos_varKey,xlabel,ylabel,title,ymin,ymax,nxbins,nybins,xcovertion):
    keys=list(Dict)
    keys.sort()
    
    templist=[]
    for key in keys:
        templist.append(Dict[key][pos_varKey]*xcovertion)
    minval=min(templist)
    maxval=max(templist)
        
    
    f=ROOT.TH2F(title+"TH2D"+str(pos_varKey),title,nxbins,minval,maxval,nybins,ymin,ymax)  
    
    for key in keys:
        f.Fill(Dict[key][pos_varKey]*xcovertion,Dict[key][0])
       
    #color=4
   # xmax=len(ratios)-1
#    f.SetMarkerColor(color)
    f.SetMarkerSize(0.05)
#    f.SetMarkerStyle(20)
    f.SetLineColor(46)
#    f.SetFillColor(color)
#    f.SetTitle(ylabel+" "+title)
#    f.GetXaxis().SetTitle(xlabel)
#    f.GetYaxis().SetTitle(ylabel)
#    f.GetYaxis().SetRangeUser(ymin, ymax)
   # f.GetXaxis().SetRangeUser(0, xmax)
    
    return f

def plot_ratio_TH2D_fromDictInvertedSBIL(Dict,nBXPerFill,pos_varKey,xlabel,ylabel,title,ymin,ymax,nxbins,nybins,xcovertion):
    keys=list(Dict)
    keys.sort()
    
    templist=[]
    for key in keys:
        templist.append(Dict[key][pos_varKey]*xcovertion/nBXPerFill[key[0]])
    minval=min(templist)
    maxval=max(templist)
        
    
    f=ROOT.TH2F(title+"TH2D"+str(pos_varKey),title,nxbins,minval,maxval,nybins,ymin,ymax)  
    
    for key in keys:
        f.Fill(Dict[key][pos_varKey]*xcovertion/nBXPerFill[key[0]],Dict[key][0])
       
    #color=4
   # xmax=len(ratios)-1
#    f.SetMarkerColor(color)
    f.SetMarkerSize(0.05)
#    f.SetMarkerStyle(20)
    f.SetLineColor(46)
#    f.SetFillColor(color)
#    f.SetTitle(ylabel+" "+title)
#    f.GetXaxis().SetTitle(xlabel)
#    f.GetYaxis().SetTitle(ylabel)
#    f.GetYaxis().SetRangeUser(ymin, ymax)
   # f.GetXaxis().SetRangeUser(0, xmax)
    
    return f

def plot_ratio_TH2D_fromDictInvertedW(Dict,pos_varKey,xlabel,ylabel,title,ymin,ymax,nxbins,nybins,xcovertion):
    keys=list(Dict)
    keys.sort()
    
    templist=[]
    for key in keys:
        templist.append(Dict[key][pos_varKey]*xcovertion)
    minval=min(templist)
    maxval=max(templist)
        
    
    f=ROOT.TH2F(title+"TH2D"+str(pos_varKey),title,nxbins,minval,maxval,nybins,ymin,ymax)  
    
    for key in keys:
        f.Fill(Dict[key][pos_varKey]*xcovertion,Dict[key][0],Dict[key][pos_varKey])
       
    #color=4
   # xmax=len(ratios)-1
#    f.SetMarkerColor(color)
    f.SetMarkerSize(0.05)
#    f.SetMarkerStyle(20)
    f.SetLineColor(46)
#    f.SetFillColor(color)
#    f.SetTitle(ylabel+" "+title)
#    f.GetXaxis().SetTitle(xlabel)
#    f.GetYaxis().SetTitle(ylabel)
#    f.GetYaxis().SetRangeUser(ymin, ymax)
   # f.GetXaxis().SetRangeUser(0, xmax)
    
    return f

def plot_ratio_TH2D_w(ratios,xlabel,ylabel,title,ymin,ymax,nxbins,nybins):
    maxval = ratios[len(ratios)-1][0]
    minval = ratios[0][0]
    
    f=ROOT.TH2F(title+"TH2D",title,nxbins,minval,maxval,nybins,ymin,ymax)  
    
    for iBin in range(0,len(ratios)):
        try:
            f.Fill(ratios[iBin][0],ratios[iBin][1],ratios[iBin][2])
            
        except:
           pass
       
    #color=4
   # xmax=len(ratios)-1
#    f.SetMarkerColor(color)
#    f.SetMarkerSize(0.1)
#    f.SetMarkerStyle(20)
#    f.SetLineColor(color)
#    f.SetFillColor(color)
#    f.SetTitle(ylabel+" "+title)
#    f.GetXaxis().SetTitle(xlabel)
#    f.GetYaxis().SetTitle(ylabel)
#    f.GetYaxis().SetRangeUser(ymin, ymax)
   # f.GetXaxis().SetRangeUser(0, xmax)
    
    return f        

def plot_ratioByRuns_TH2D_noempty(ratios_runs,xlabel,ylabel,title,ymin,ymax,nxbins,nybins):
    maxval = len(ratios_runs)-1
    minval = ratios_runs[0][0]
    
    f=ROOT.TH2F("ratios_by_runs","",nxbins,0,maxval,nybins,ymin,ymax)
    runid=0
    current_run=minval
    for iBin in range(0,len(ratios_runs)):
        try:
            if ratios_runs[iBin][0]!=current_run:
                runid+=1
                current_run=ratios_runs[iBin][0]
            f.Fill(float(runid),ratios_runs[iBin][1])
            
        except:
           pass
       
   # color=4
   # xmax=len(ratios)-1
#    f.SetMarkerColor(color)
#    f.SetMarkerSize(0.1)
#    f.SetMarkerStyle(20)
#    f.SetLineColor(color)
#    f.SetFillColor(color)
#    f.SetTitle(ylabel+" "+title)
#    f.GetXaxis().SetTitle(xlabel)
#    f.GetYaxis().SetTitle(ylabel)
#    f.GetYaxis().SetRangeUser(ymin, ymax)
   # f.GetXaxis().SetRangeUser(0, xmax)
    
    return f    

def saveintoCanvas_and_toFile(canvas,plot,plot_opt,outputName):
    canvas.cd()
    canvas.Clear()
    ROOT.gPad.SetRightMargin(0.12)
    if plot_opt=="":
        plot.Draw()
    else:
        plot.Draw(plot_opt)
    
    xthick=plot.GetXaxis().GetTickLength()
    ratioCan=float(canvas.GetWh())/float(canvas.GetWw())
    plot.GetYaxis().SetTickLength(xthick*ratioCan);
    
    #Modification de the info box position    
    ROOT.gPad.Modified() 
    ROOT.gPad.Update()
    st = ROOT.gPad.GetPrimitive("stats") 
    if (st):
        #st.SetX1NDC(st.GetX1NDC() + 0.01) 
        #st.SetX2NDC(st.GetX2NDC() + 0.01)
        st.SetY1NDC(st.GetY1NDC() - 0.02)
        st.SetY2NDC(st.GetY2NDC() - 0.02)
        ROOT.gPad.Modified(); 
        ROOT.gPad.Update()
    canvas.Update()
    canvas.SaveAs(outputName)
    
def saveintoCanvas_and_toFileWIP(canvas,plot,plot_opt,outputName,opt,yearLabel=0,energy=0,
                                 extraText1="",pos_extraText1=[0.87,0.25],s_extraText1=0.0375,extraText2="",pos_extraText2=[0.32,0.05],s_extraText2=0.0375,
                                 ymin=0.0,ymax=0.0,drawleg=False,leg=ROOT.TLegend()):
    
    canvas.cd()
    canvas.Clear()
    ROOT.gPad.SetRightMargin(0.12)
    
    align,x,y,textsize,thetext=setTextLabelParm(opt)
    
    align_y,x_y,y_y,textsize_y,thetext_y=setTextLabelParmYear(yearLabel,energy,opt)
    
    if plot_opt=="":
        plot.Draw()
    else:
        plot.Draw(plot_opt)
    if drawleg==True:
        leg.Draw()
        
    if extraText1!="":
        extra_text = ROOT.TLatex()
        extra_text.SetNDC()
        extra_text.SetTextFont(62)
        extra_text.SetTextSize(s_extraText1)
        extra_text.SetTextAlign(align[0])
        extraText1 = '#bf{'+ extraText1 +'}'
        extra_text.DrawLatex(pos_extraText1[0], pos_extraText1[1],extraText1)
        
    if extraText2!="":
        extra_text2 = ROOT.TLatex()
        extra_text2.SetNDC()
        extra_text2.SetTextFont(62)
        extra_text2.SetTextSize(s_extraText2)
        extra_text2.SetTextAlign(align[0])
        extraText2 = '#bf{'+ extraText2 +'}'
        extra_text2.DrawLatex(pos_extraText2[0], pos_extraText2[1],extraText2)       
        
    text = ROOT.TLatex()
    text.SetNDC()
    text.SetTextFont(62)
    text.SetTextSize(textsize)
    text.SetTextAlign(align[0])
    
    texty = ROOT.TLatex()
    texty.SetNDC()
    texty.SetTextFont(62)
    texty.SetTextSize(textsize_y)
    texty.SetTextAlign(align_y[0])
    
    yaxis=plot.GetYaxis()
    xaxis=plot.GetXaxis()
    
    xthick=xaxis.GetTickLength()
    ratioCan=float(canvas.GetWh())/float(canvas.GetWw())
    yaxis.SetTickLength(xthick*ratioCan);
    
#    #Modification de the info box position    
#    ROOT.gPad.Modified() 
#    ROOT.gPad.Update()
#    st = ROOT.gPad.GetPrimitive("stats") 
#    if (st):
#        #st.SetX1NDC(st.GetX1NDC() + 0.01) 
#        #st.SetX2NDC(st.GetX2NDC() + 0.01)
#        st.SetY1NDC(st.GetY1NDC() - 0.02)
#        st.SetY2NDC(st.GetY2NDC() - 0.02)
#        ROOT.gPad.Modified(); 
#        ROOT.gPad.Update()
        
    
    text.DrawLatex(x, y, thetext)
    texty.DrawLatex(x_y, y_y, thetext_y)
    canvasTypeInfo=opt.split("_")
    canvasType=canvasTypeInfo[len(canvasTypeInfo)-1]
    
    if canvasType=="nsq":
        yaxis.SetTitleOffset(0.5)
        xaxis.SetTitleOffset(.9)
        canvas.SetLeftMargin(0.08)
        canvas.SetRightMargin(0.05)
        canvas.SetBottomMargin(0.15)
    
    if canvasType=="sq":
        canvas.SetLeftMargin(0.15)
        yaxis.SetTitleOffset(1.4)
        canvas.SetLeftMargin(0.2)
        canvas.SetRightMargin(0.08)
        canvas.SetBottomMargin(0.15)
    
    if ymin!=0 and ymax!=0:
        ROOT.gPad.Modified()
        plot.GetYaxis().SetRangeUser(ymin,ymax)
        
    
    canvas.Update()
    canvas.SaveAs(outputName)
    
def saveintoCanvas_and_toFileWIP_lines(canvas,plot,lines,plot_opt,outputName,opt,yearLabel=0,energy=0):
    canvas.cd()
    canvas.Clear()
    ROOT.gPad.SetRightMargin(0.12)
    
    align,x,y,textsize,thetext=setTextLabelParm(opt)
    align_y,x_y,y_y,textsize_y,thetext_y=setTextLabelParmYear(yearLabel,energy,opt)
    
    if plot_opt=="":
        plot.Draw()
    else:
        plot.Draw(plot_opt)
        
    for line in lines:
        line.Draw()
        
    text = ROOT.TLatex()
    text.SetNDC()
    text.SetTextFont(62)
    text.SetTextSize(textsize)
    text.SetTextAlign(align[0])
    
    texty = ROOT.TLatex()
    texty.SetNDC()
    texty.SetTextFont(62)
    texty.SetTextSize(textsize_y)
    texty.SetTextAlign(align_y[0])
    
    yaxis=plot.GetYaxis()
    xaxis=plot.GetXaxis()
    
    xthick=xaxis.GetTickLength()
    ratioCan=float(canvas.GetWh())/float(canvas.GetWw())
    yaxis.SetTickLength(xthick*ratioCan);
    
#    #Modification de the info box position    
#    ROOT.gPad.Modified() 
#    ROOT.gPad.Update()
#    st = ROOT.gPad.GetPrimitive("stats") 
#    if (st):
#        #st.SetX1NDC(st.GetX1NDC() + 0.01) 
#        #st.SetX2NDC(st.GetX2NDC() + 0.01)
#        st.SetY1NDC(st.GetY1NDC() - 0.02)
#        st.SetY2NDC(st.GetY2NDC() - 0.02)
#        ROOT.gPad.Modified(); 
#        ROOT.gPad.Update()
        
    
    text.DrawLatex(x, y, thetext)
    texty.DrawLatex(x_y, y_y, thetext_y)
    canvasTypeInfo=opt.split("_")
    canvasType=canvasTypeInfo[len(canvasTypeInfo)-1]
    
    if canvasType=="nsq":
        yaxis.SetTitleOffset(0.5)
        xaxis.SetTitleOffset(.9)
        canvas.SetLeftMargin(0.08)
        canvas.SetRightMargin(0.05)
        canvas.SetBottomMargin(0.15)
    
    if canvasType=="sq":
        canvas.SetLeftMargin(0.15)
        yaxis.SetTitleOffset(1.4)
        canvas.SetLeftMargin(0.2)
        canvas.SetRightMargin(0.08)
        canvas.SetBottomMargin(0.15)
        
    canvas.Update()
    canvas.SaveAs(outputName)
    


#def saveintoCanvas_and_toFileWIPExtraAxis(canvas,plot,plot_opt,outputName,opt):
#    canvas.cd()
#    canvas.Clear()
#    ROOT.gPad.SetRightMargin(0.12)
#    
#    align,x,y,textsize,thetext=setTextLabelParm(opt)
#    
#    if plot_opt=="":
#        plot.Draw()
#    else:
#        plot.Draw(plot_opt)
#        
#    axisExtra = ROOT.TGaxis(ROOT.gPad.GetUxmin(),ROOT.gPad.GetUymax(),ROOT.gPad.GetUxmax(),ROOT.gPad.GetUymax(),low,high,510,"+L")
#    axisExtra.Draw()
#        
#    text = ROOT.TLatex()
#    text.SetNDC()
#    text.SetTextFont(62)
#    text.SetTextSize(textsize)
#    text.SetTextAlign(align[0])
#    
#    yaxis=plot.GetYaxis()
#    xaxis=plot.GetXaxis()
#    
#    xthick=xaxis.GetTickLength()
#    ratioCan=float(canvas.GetWh())/float(canvas.GetWw())
#    yaxis.SetTickLength(xthick*ratioCan);
#    
##    #Modification de the info box position    
##    ROOT.gPad.Modified() 
##    ROOT.gPad.Update()
##    st = ROOT.gPad.GetPrimitive("stats") 
##    if (st):
##        #st.SetX1NDC(st.GetX1NDC() + 0.01) 
##        #st.SetX2NDC(st.GetX2NDC() + 0.01)
##        st.SetY1NDC(st.GetY1NDC() - 0.02)
##        st.SetY2NDC(st.GetY2NDC() - 0.02)
##        ROOT.gPad.Modified(); 
##        ROOT.gPad.Update()
#        
#    
#    text.DrawLatex(x, y, thetext)
#    canvasTypeInfo=opt.split("_")
#    canvasType=canvasTypeInfo[len(canvasTypeInfo)-1]
#    
#    if canvasType=="nsq":
#        yaxis.SetTitleOffset(0.5)
#        xaxis.SetTitleOffset(.9)
#        canvas.SetLeftMargin(0.08)
#        canvas.SetRightMargin(0.05)
#        canvas.SetBottomMargin(0.15)
#    
#    if canvasType=="sq":
#        canvas.SetLeftMargin(0.15)
#        yaxis.SetTitleOffset(1.4)
#        canvas.SetLeftMargin(0.2)
#        canvas.SetRightMargin(0.08)
#        canvas.SetBottomMargin(0.15)
#        
#    canvas.Update()
#    canvas.SaveAs(outputName)
    
    
    

def saveintoCanvas_and_toFileWIP_withLeg(canvas,plot,leg,plot_opt,outputName,opt,yearLabel=0,energy=0):
    canvas.cd()
    canvas.Clear()
    
    align,x,y,textsize,thetext=setTextLabelParm(opt)
    align_y,x_y,y_y,textsize_y,thetext_y=setTextLabelParmYear(yearLabel,energy,opt)
    
    if plot_opt=="":
        plot.Draw()
    else:
        plot.Draw(plot_opt)
    leg.Draw()
    
    text = ROOT.TLatex()
    text.SetNDC()
    text.SetTextFont(62)
    text.SetTextSize(textsize)
    text.SetTextAlign(align[0])
    
    texty = ROOT.TLatex()
    texty.SetNDC()
    texty.SetTextFont(62)
    texty.SetTextSize(textsize_y)
    texty.SetTextAlign(align_y[0])
    
    xthick=plot.GetXaxis().GetTickLength()
    ratioCan=float(canvas.GetWh())/float(canvas.GetWw())
    yaxis=plot.GetYaxis()
    yaxis.SetTickLength(xthick*ratioCan)
#    yaxis.SetLabelSize(labelsize)
#       
    xaxis=plot.GetXaxis()
#    xaxis.SetLabelSize(labelsize)
#    
    text.DrawLatex(x, y, thetext)
    texty.DrawLatex(x_y, y_y, thetext_y)
    
    canvasTypeInfo=opt.split("_")
    canvasType=canvasTypeInfo[len(canvasTypeInfo)-1]
    
    if canvasType=="nsq":
        yaxis.SetTitleOffset(0.5)
        xaxis.SetTitleOffset(.9)
        canvas.SetLeftMargin(0.08)
        canvas.SetRightMargin(0.05)
        canvas.SetBottomMargin(0.15)
    if canvasType=="sq":
        canvas.SetLeftMargin(0.18)
        xaxis.SetTitleOffset(0.8)
        yaxis.SetTitleOffset(1.2)
        
    canvas.Update()
    canvas.SaveAs(outputName)
    
def setTextLabelParm(opt,year=0):
    textsize=0.0375
    align = (33, 13)
    y=0.0
    x=0.0
    ##square canvas histos,etc
    ##wip top left
#    if opt=="wip_tl_sq":
#        textsize=0.0375
#        y=0.92
#        x=0.15
#        thetext = '#bf{#scale[0.75]{#it{Work in Progress}}}'
#            
    ##wip inside left
    if opt=="il_sq":
        if setts.plot_mode == "Work in progress":
            textsize=0.05
            y=0.92
            x=0.49
            thetext = '#splitline{CMS}{#bf{#scale[0.75]{#it{'+ setts.plot_mode +'}}}}'
        elif setts.plot_mode == "Preliminary":
            textsize=0.05
            y=0.92
            x=0.40
            thetext = '#splitline{CMS}{#bf{#scale[0.75]{#it{'+ setts.plot_mode +'}}}}'
                
        
    
    ##square canvas histos,etc    
    elif opt=="il_nsq":
        if setts.plot_mode == "Work in progress":
            textsize=0.06
            y=0.91
            x=0.19
            thetext = '#splitline{CMS}{#bf{#scale[0.75]{#it{'+ setts.plot_mode +'}}}}'
        elif setts.plot_mode == "Preliminary":
            textsize=0.06
            y=0.91
            x=0.16
            thetext = '#splitline{CMS}{#bf{#scale[0.75]{#it{'+ setts.plot_mode +'}}}}'
        
    
    ##square canvas histos,etc    
#    elif opt=="wip_il_nsq":
#        textsize=0.06
#        y=0.91
#        x=0.2
#        thetext = '#splitline{CMS}{#bf{#scale[0.75]{#it{Work in Progress}}}}'
#    
#    elif opt=="wip_tl_nsq":
#        textsize=0.06
#        y=0.93
#        x=0.2
#        thetext = '#splitline{CMS}{#bf{#scale[0.75]{#it{Work in Progress}}}}'
#        
#    ##square canvas histos,etc    
#    elif opt=="wip_il_pf":
#        textsize=0.06
#        y=0.88
#        x=0.21
#        thetext = '#splitline{CMS}{#bf{#scale[0.75]{#it{Work in Progress}}}}'
#    
#    elif opt=="wip_tl_pf":
#        textsize=0.06
#        y=0.92
#        x=0.25
#        thetext = '#splitline{CMS}{#bf{#scale[0.75]{#it{Work in Progress}}}}'
#    
    elif opt=="":
        textsize=0.06
        y=0.92
        x=0.25
        thetext = ''
    else:
        print (opt+" Option not found!!!!")
        exit()
    
    return align,x,y,textsize,thetext

def setTextLabelParmYear(year=0,energy=0,opt=""):
    textsize=0.0375
    align = (33, 13)
    y=0.0
    x=0.0
    energy_label=setts.energyLabel_list[energy]
    ##square canvas histos,etc
    ##wip top left
    thetext=""
    if year!=0:        
        if opt=="":
            textsize=0.045
            y=0.96
            x=0.5
            thetext = '#scale[0.8]{#it{' + energy_label +  ' (' + str(year) +')}}'
            
        elif opt=="il_nsq":
            textsize=0.04
            y=0.99
            x=0.95
            thetext = '#it{' + energy_label +  ' (' + str(year) +')}'    
        elif opt=="tl_nsq":
            textsize=0.04
            y=0.99
            x=0.95
            thetext = '#it{' + energy_label +  ' (' + str(year) +')}'    
        elif opt=="il_sq":
            textsize=0.04
            y=0.99
            x=0.925
            thetext = '#it{' + energy_label +  ' (' + str(year) +')}'
            
        elif opt=="pre_il_nsq":
            textsize=0.04
            y=0.99
            x=0.95
            thetext = '#it{' + energy_label +  ' (' + str(year) +')}'    
        elif opt=="pre_tl_nsq":
            textsize=0.04
            y=0.99
            x=0.95
            thetext = '#it{' + energy_label +  ' (' + str(year) +')}'
        elif opt=="pre_il_sq":
            textsize=0.04
            y=0.99
            x=0.925
            thetext = '#it{' + energy_label +  ' (' + str(year) +')}'
            
    return align,x,y,textsize,thetext

def saveintoCanvas_and_toFile_withLines(canvas,plot,lines,important_labels,plot_opt,outputName):
    canvas.cd()
    canvas.Clear()
    
    if plot_opt=="":
        plot.Draw()
    else:
        plot.Draw(plot_opt)
    
    ##Fix y axe distortion
    xthick=plot.GetXaxis().GetTickLength()
    ratioCan=float(canvas.GetWh())/float(canvas.GetWw())
    plot.GetYaxis().SetTickLength(xthick*ratioCan)
        
    if len(important_labels)!=0:
        nl=0    
        legl=ROOT.TLegend (.95,.8,.85,.9)
        for l in lines:
            l.SetLineColor(nl+7)
            legl.AddEntry(l,str(important_labels[nl]),"l") 
            l.Draw()
            nl+=1
        legl.Draw()
    canvas.Update()
    canvas.SaveAs(outputName)
    
def saveintoCanvas_and_toFile_withLeg(canvas,plot,leg,plot_opt,outputName):
    canvas.cd()
    canvas.Clear()
    
    if plot_opt=="":
        plot.Draw()
    else:
        plot.Draw(plot_opt)
    leg.Draw()

    
    xthick=plot.GetXaxis().GetTickLength()
    ratioCan=float(canvas.GetWh())/float(canvas.GetWw())
    plot.GetYaxis().SetTickLength(xthick*ratioCan);
    
    canvas.Update()
    canvas.SaveAs(outputName)
    
def saveintoCanvas_and_toFile_withLinesExtraAxis(canvas,plot,lines,important_labels,axis,plot_opt,outputName):
    canvas.cd()
    canvas.Clear()
    
    if plot_opt=="":
        plot.Draw()
    else:
        plot.Draw(plot_opt)
        
    if len(important_labels)!=0:
        nl=0    
        legl=ROOT.TLegend (.95,.8,.85,.9)
        for l in lines:
            l.SetLineColor(nl+7)
            legl.AddEntry(l,str(important_labels[nl]),"l") 
            l.Draw()
            nl+=1
        legl.Draw()
    axis.Draw()
    canvas.Update()
    canvas.SaveAs(outputName)

def getFillsTimeAxis(labelFillinNls):
    h=ROOT.TH1F()
    for i in range(0,len(labelFillinNls)-1):
        h.Fill(0,labelFillinNls[i][0])
        
    f=ROOT.TF1("f",h.Fit("pol2"),0,len(labelFillinNls)-1)
       
    axis= ROOT.TGaxis(0,len(labelFillinNls)-1,labelFillinNls[0][0],labelFillinNls[len(labelFillinNls)-1][0],"f",510,"G")
    
    return axis    
        
   
def setPlotProperties(plot,title,xlabel,ylabel,ymin=0.0,ymax=0.0):
    plot.SetTitle(title)
    plot.GetXaxis().SetTitle(xlabel)
    plot.GetYaxis().SetTitle(ylabel)
    if ymin!=0.0 or ymax!=0.0:
        plot.GetYaxis().SetRangeUser(ymin, ymax)
    

def histo_ratioFromArray(values,title,xlabel,ylabel):
    
    hist=ROOT.TH1F()
    hist.SetTitle(title)
    hist.GetXaxis().SetTitle(xlabel)
    hist.GetYaxis().SetTitle(ylabel)
    for v in values:
        hist.Fill(v)
        
    return hist

def draw3plotsSameCanvasAndSave(canvas,plot1,plot2,plot3,xmin,xmax,ymin,ymax,plot1_opt,plot2_opt,plot3_opt,labelratios,outputName):
    canvas.cd()
    canvas.Clear()
    
    if ymin!="" and ymax!="":
        plot1.GetYaxis().SetRangeUser(ymin, ymax)
    if xmin!="" and xmax!="":
        plot1.GetXaxis().SetRangeUser(xmin, xmax)
    
    
    plot1.Draw(plot1_opt)
    plot2.Draw("same"+plot2_opt)
    plot3.Draw("same"+plot3_opt)
    
    leg = ROOT.TLegend (.9,.7,.75,.9)
    leg.SetFillColor(0)
    leg.AddEntry(plot1,labelratios[0],"l")       
    leg.AddEntry(plot2,labelratios[1],"l")
    leg.AddEntry(plot2,labelratios[2],"l")    
    
    leg.Draw()
    canvas.Update()
    canvas.SaveAs(outputName) 


def setRESRStyle():
  resrStyle =  ROOT.TStyle("resrStyle","Style for RESR")

#   #for the canvas:
  resrStyle.SetCanvasBorderMode(0)
  resrStyle.SetCanvasColor(ROOT.kWhite)
#  resrStyle.SetCanvasDefH(600) #Height of canvas
#  resrStyle.SetCanvasDefW(600) #Width of canvas
#  resrStyle.SetCanvasDefX(0)   #POsition on screen
#  resrStyle.SetCanvasDefY(0)


  resrStyle.SetPadBorderMode(0)
  #resrStyle.SetPadBorderSize(Width_t size = 1)
  resrStyle.SetPadColor(ROOT.kWhite)
  resrStyle.SetPadGridX(False)
  resrStyle.SetPadGridY(False)
  resrStyle.SetGridColor(0)
  resrStyle.SetGridStyle(3)
  resrStyle.SetGridWidth(1)

#For the frame:
  resrStyle.SetFrameBorderMode(0)
  resrStyle.SetFrameBorderSize(1)
  resrStyle.SetFrameFillColor(0)
  resrStyle.SetFrameFillStyle(0)
  resrStyle.SetFrameLineColor(1)
  resrStyle.SetFrameLineStyle(1)
  resrStyle.SetFrameLineWidth(1)
  
#For the histo:
  #resrStyle.SetHistFillColor(1)
  #resrStyle.SetHistFillStyle(0)
  resrStyle.SetHistLineColor(1)
  resrStyle.SetHistLineStyle(0)
  resrStyle.SetHistLineWidth(1)
  #resrStyle.SetLegoInnerR(Float_t rad = 0.5)
  #resrStyle.SetNumberContours(Int_t number = 20)

  resrStyle.SetEndErrorSize(2)
  #resrStyle.SetErrorMarker(20)
  #resrStyle.SetErrorX(0.)
  
  resrStyle.SetMarkerStyle(20)
  
#For the fit/function:
  resrStyle.SetOptFit(1)
  resrStyle.SetFitFormat("5.4g")
  resrStyle.SetFuncColor(2)
  resrStyle.SetFuncStyle(1)
  resrStyle.SetFuncWidth(2)

#For the date:
  resrStyle.SetOptDate(0)
  # resrStyle.SetDateX(Float_t x = 0.01)
  # resrStyle.SetDateY(Float_t y = 0.01)

# For the statistics box:
  resrStyle.SetOptFile(0)
  resrStyle.SetOptStat("mr") # To display the mean and RMS:   SetOptStat("mr")
  resrStyle.SetStatColor(ROOT.kWhite)
  resrStyle.SetStatFont(42)
#  resrStyle.SetStatFontSize(0.085)
  resrStyle.SetStatTextColor(1)
  resrStyle.SetStatFormat("5.4g")
  resrStyle.SetStatBorderSize(1)
  #resrStyle.SetStatH(0.1)
  #resrStyle.SetStatW(0.15)
  # resrStyle.SetStatStyle(Style_t style = 1001)
  resrStyle.SetStatX(0.92)
  resrStyle.SetStatY(0.95)

# Margins:
  resrStyle.SetPadTopMargin(0.05)
  resrStyle.SetPadBottomMargin(0.13)
  resrStyle.SetPadLeftMargin(0.16)
  resrStyle.SetPadRightMargin(0.02)

# For the Global title:

  resrStyle.SetOptTitle(0)
  resrStyle.SetTitleFont(42)
  resrStyle.SetTitleColor(1)
  resrStyle.SetTitleTextColor(1)
  resrStyle.SetTitleFillColor(10)
  resrStyle.SetTitleFontSize(0.05)
  # resrStyle.SetTitleH(0) # Set the height of the title box
  # resrStyle.SetTitleW(0) # Set the width of the title box
  # resrStyle.SetTitleX(0) # Set the position of the title box
  # resrStyle.SetTitleY(0.985) # Set the position of the title box
  # resrStyle.SetTitleStyle(Style_t style = 1001)
  # resrStyle.SetTitleBorderSize(2)

# For the axis titles:

  resrStyle.SetTitleColor(1, "XYZ")
  resrStyle.SetTitleFont(42, "XYZ")
  resrStyle.SetTitleSize(0.07, "XYZ")
  # resrStyle.SetTitleXSize(Float_t size = 0.02) # Another way to set the size?
  # resrStyle.SetTitleYSize(Float_t size = 0.02)
#  resrStyle.SetTitleXOffset(0.9)
#  resrStyle.SetTitleYOffset(1.25)
  # resrStyle.SetTitleOffset(1.1, "Y") # Another way to set the Offset

# For the axis labels:

  resrStyle.SetLabelColor(1, "XYZ")
  resrStyle.SetLabelFont(42, "XYZ")
#  resrStyle.SetLabelOffset(0.007, "XYZ")
  resrStyle.SetLabelSize(0.05, "XYZ")

# For the axis:

  resrStyle.SetAxisColor(1, "XYZ")
  resrStyle.SetStripDecimals(True)
  resrStyle.SetTickLength(0.03, "XYZ")
  resrStyle.SetNdivisions(510, "XYZ")
  resrStyle.SetPadTickX(1)  # To get tick marks on the opposite side of the frame
  resrStyle.SetPadTickY(1)

# Change for log plots:
  resrStyle.SetOptLogx(0)
  resrStyle.SetOptLogy(0)
  resrStyle.SetOptLogz(0)

# Postscript options:
  #resrStyle.SetPaperSize(20.,20.)
  # resrStyle.SetLineScalePS(Float_t scale = 3)
  # resrStyle.SetLineStyleString(Int_t i, const char* text)
  # resrStyle.SetHeaderPS(const char* header)
  # resrStyle.SetTitlePS(const char* pstitle)

  # resrStyle.SetBarOffset(Float_t baroff = 0.5)
  # resrStyle.SetBarWidth(Float_t barwidth = 0.5)
  # resrStyle.SetPaintTextFormat(const char* format = "g")
  # resrStyle.SetPalette(Int_t ncolors = 0, Int_t* colors = 0)
  # resrStyle.SetTimeOffset(Double_t toffset)
  # resrStyle.SetHistMinimumZero(kTRUE)

  resrStyle.SetHatchesLineWidth(5)
  resrStyle.SetHatchesSpacing(0.05)

  resrStyle.cd()


######################################################
##############   Fitting   #############
####################################################
def addFitWithRange(funct,fitFunct,legend,colors):
    if len(legend)!=0:
        funct.Fit(fitFunct,"R+")
    else:
        funct.Fit(fitFunct,"R")
        
    





######################################################
##############   uncertainties   #############
####################################################
def getArrayMeanAndSigma(array):
    
    sum=0.0
    for val in array:
        sum+=val
    mean=sum/len(array)
    sigma=0.0
    for val in array:
        sigma=abs(val-mean)
    err=sigma/math.sqrt(len(array)-1)
    
    return mean,sigma,err

#def getArraySigmaRef(array):
#    sigma=0.0
#    for val in array:
#        sigma=abs(val-array[0])
#    
#    if len(array)>2:
#        err=sigma/math.sqrt(len(array)-2)
#    else:
#        err=sigma
#        
#    return mean,sigma,err


def getIntegralUncert(overlapKeys,dicts):
    
    integ=[]
    integNls=0
    
    for dict in dicts:
        integ.append(0.0)
    
    for key in overlapKeys:
            nd=0
            for dict in dicts:
                integ[nd]=integ[nd]+dict[key][1]
                nd+=1
            integNls+=1
    
    #meanInt,sigmaInt,errInt=getArrayMeanAndSigma(integ)
    meanInt=statistics.mean(integ)
    sigmaInt=statistics.stdev(integ)
    errInt=sigmaInt/math.sqrt(len(integ)-1)
    relErrMean=errInt*100/meanInt
    relErrRef=errInt*100/integ[0]
    print ("Integral values: ",integ)
    print ("mean value: ",meanInt)
    print ("sigma: ",sigmaInt)
    print ("err: ",errInt)
    print ("rel. to mean err (in %): ", relErrMean)
    print ("rel. to reference detctor (in %): ", relErrRef)
    
    
    return integ,meanInt,sigmaInt,errInt,relErrMean,relErrRef
def getIntegralUncert2dicts(overlapKeys,dict1,dict2):
    
    integ1=0.0
    integ2=0.0
    integNls=0
    
    
    for key in overlapKeys:
        integ1+=dict1[key][1]
        integ2+=dict2[key][1]
        
        integNls+=1
    integ=[]
    integ.append(integ1)
    integ.append(integ2)
    #meanInt,sigmaInt,errInt=getArrayMeanAndSigma(integ)
    meanInt=statistics.mean(integ)
    sigmaInt=statistics.stdev(integ)
    errInt=sigmaInt/math.sqrt(len(integ)-1)
    relErrMean=errInt*100/meanInt
    relErrRef=errInt*100/integ[0]
    print ("Integral values: ",integ)
    print ("mean value: ",meanInt)
    print ("sigma: ",sigmaInt)
    print ("err: ",errInt)
    print ("rel. to mean err (in %): ", relErrMean)
    print ("rel. to reference detctor (in %): ", relErrRef)
    
    
    return integ,meanInt,sigmaInt,errInt,relErrMean,relErrRef


def getMaxRatioDiff(dict1,dict2,dict3,keys,Nls):
    maxDiff=0.0
    minDiff=10000000.0
    nls=0
    sum1=0.0
    sum2=0.0
    sum3=0.0
    sumd=0.0
    sumdW=0.0
    checkSum=0.0
    totLumi=0.0
    n=0
    nd=0

    
    
    for key in keys:
        try:
            if dict1[key][1]>0 and dict2[key][1]>0 and dict3[key][1]>0:
                totLumi+=dict3[key][1]
        except:
            pass  
    
    hdiff=ROOT.TH1F("his1","",100,0,2)
    hdiff.SetTitle("diff histogram")
    
    hdiffW=ROOT.TH1F("his1w","",100,0,2)
    hdiffW.SetTitle("diff Lumi. weighted histogram")
    
    
    for key in keys:
        n+=1
        if dict1[key][1]>0 and dict2[key][1]>0 and dict3[key][1]>0:
            sum1+=dict1[key][1]
            sum2+=dict2[key][1]
            sum3+=dict3[key][1]
            nls+=1
            
        if nls==Nls or n==len(keys):
            ratio1=sum1/sum3
            ratio2=sum2/sum3            
            
            d=abs(ratio1-ratio2)
                        
            nd+=1
            sumd+=d
            w=sum3/totLumi
            sumdW+=(d*w)
            
            checkSum+=w
            
            hdiff.Fill(d)
            hdiffW.Fill(d*w)
            
            if d>maxDiff:
                maxDiff=d
            if d<minDiff:
                minDiff=d
            sum1=0.0
            sum2=0.0
            sum3=0.0
            nls=0
    
    if abs(checkSum-1)>0.01:
        print ("check sum(w)=1 failed!!!!")
    
    meanDiff=sumd/nd
    meanWDiff=sumdW/nd
    print ("minDiff: ",minDiff)
    print ("maxDiff: ",maxDiff)
    print ("meanDiff,(%): ",meanDiff,", (",meanDiff*100,"%)")
    print ("meanWDiff,(%): ",meanWDiff,", (",meanWDiff*100,"%)")
    
    return maxDiff,minDiff,meanDiff,meanWDiff,hdiff,hdiffW

        
def getMaxDiff(dicts,keys):
    minDiff=1000000000.0
    maxDiff=0.0
    
    for key in keys:
        for i in range(0,len(dicts)-2):
            for j in range(i+1,len(dicts)-1):
                diff=abs(dicts[i][key][1]-dicts[j][key][1])
                if diff>maxDiff:
                    maxDiff=diff
                if diff<minDiff:
                    minDiff=diff
    
    return maxDiff,minDiff

def getMaxDiff3dicts(dict1,dict2,dict3,keys,Nls):
    diff=[]
    nls=0
    intLumi=0.0
    sum1=0.0
    sum2=0.0
    sum3=0.0
    n=0
    for key in keys:
        n+=1
        if dict1[key][1]>0 and dict2[key][1]>0 and dict3[key][1]>0:
            sum1+=dict1[key][1]
            sum2+=dict2[key][1]
            sum3+=dict3[key][1]
            intLumi+=sum3
            nls+=1
        
        if nls==Nls or n==len(keys):
            diff.append(abs(sum1-sum3))
            diff.append(abs(sum2-sum3))
            diff.append(abs(sum1-sum2))
            sum1=0.0
            sum2=0.0
            sum3=0.0
            nls=0
            
    minDiff=min(diff)
    maxDiff=max(diff)
    
    print (minDiff*100/intLumi,maxDiff*100/intLumi)
    return minDiff,maxDiff

def saveListInFile(mylist,filename):
    with open(filename, 'w') as f:
        for item in mylist:
            f.write("%s\n" % str(item))
            
def get1colFileInList(filename,listtype):
    file=open(filename)
    mylist=[]
    if listtype=="int":
        for line in file.readlines():
            mylist.append(int(line))
    elif listtype=="str":
        for line in file.readlines():
            mylist.append(line)
    elif listtype=="float":
        for line in file.readlines():
            mylist.append(float(line))
    return mylist    
            
    
        

#def getIntegralUncertRef(overlapKeys,dicts):
#    
#    integ=[]
#    integNls=0
#    
#    for dict in dicts:
#        integ.append(0.0)
#    
#    for key in overlapKeys:
#            nd=0
#            for dict in dicts:
#                try:
#                    integ[nd]=integ[nd]+dict[key][1]
#                    nd+=1
#                except:
#                    integNls-=1
#                    break
#            integNls+=1
#    
#    meanInt,sigmaInt,errInt=getArraySigmaRef(integ)
#    print "Integral values: ",integ
#    print "mean value: ",meanInt
#    print "sigma: ",sigmaInt
#    print "err: ",errInt
#    print "rel. to mean err (in %): ", errInt/meanInt
#    print "rel. to reference detctor (in %): ", errInt/integ[0]
#    
#    return integ,meanInt,sigmaInt,errInt,relErrMean,relErrRef    
    
    

    


########################################
##############    EXTRA    #############
########################################


## Analise single fill, return TGraph object    
def singleFillAnalysis(dict1,dict2,fill_number,labelRatio,overlapKeys):
    singleFillTag=fill_number
    SingleFillVsTime=ROOT.TGraph()
    #SingleFillVsTime_ls=ROOT.TGraph()
    SingleFillVsTime.SetTitle(labelRatio+" Fill:"+str(singleFillTag)+";Time (s);Ratio")
    SingleFillVsTime.SetMarkerStyle(20)
    SingleFillVsTime.SetMarkerSize(0.5)
    iBin=0
    iBinSingFill=0
    
    for key in overlapKeys:
        num=dict1[key][1]
        den=dict2[key][1]
        try:
            if overlapKeys[iBin][0]==singleFillTag:
                if iBinSingFill==0:
                    time0SingFill=dict1[key][0]
                SingleFillVsTime.SetPoint(iBinSingFill,dict1[key][0]-time0SingFill,num/den)
                iBinSingFill+=1
        
       # ratioVsTime.SetPoint(iBin,dict1[key][0]-time0,num/den)
            iBin+=1
        except:
           pass
    return SingleFillVsTime
    

## Returns run id for a given fill    
def id_from_fill(fill,trackfillRun,runs):
    run=list(trackfillRun.keys())[list(trackfillRun.values()).index(fill)]
    idn=runs.index(run)
    
    return idn

def findTimeFromFill(fill,dict1,dict2):
    dict={}
    time=0
    
    if len(dict1)>len(dict2):
        dict=dict1
    else:
        dict=dict2
    keys=list(dict)
    keys.sort()
    for key in keys:
        if fill in key:
            time=dict[key][0]
            break
        
    return time
    
def fill_line(idn,ymin,ymax):
    l = ROOT.TLine(idn,ymin,idn,ymax)
    l.SetLineStyle(9)
    
    return l

def getFillLines(ymin,ymax,dict,idPos):
    lines=[]
    ntot=0
    keys=list(dict)
    keys.sort()
    
    fill=keys[0][0]
    
    for key in keys:
        ntot+=1
        if fill!=key[0] or ntot==len(keys):
            idn=key[idPos]
            l = ROOT.TLine(idn,ymin,idn,ymax)
            l.SetLineStyle(2)
            l.SetLineColorAlpha(ROOT.kRed, 0.35)
            fill=key[0]
            lines.append(l)  
    
    return lines

def getFillLinesIntLumi(ymin,ymax,dict,convertion):
    lines=[]
    ntot=0
    keys=list(dict)
    keys.sort()
    
    fill=keys[0][0]
    intLumi=0
    
    for key in keys:
        ntot+=1
        intLumi+=dict[key][1]
        if fill!=key[0] or ntot==len(keys):
            idn=intLumi*convertion
            l = ROOT.TLine(idn,ymin,idn,ymax)
            l.SetLineStyle(2)
            l.SetLineColorAlpha(ROOT.kRed, 0.35)
            fill=key[0]
            lines.append(l)  
    
    return lines




#def modify_paletteOptions(showOpt="default",dx1,dx2,dy1,dy2):
#    ROOT.gPad.Update()
#    st = ROOT.gPad.GetPrimitive("stats") 
#    if (st):
#        st.SetX1NDC(st.GetX1NDC() + dx1) 
#        st.SetX2NDC(st.GetX2NDC() + dx2)
#        st.SetY1NDC(st.GetY1NDC() + dy1)
#        st.SetY2NDC(st.GetY2NDC() + dy2)
#        ROOT.gPad.Modified();
#    if showOpt!="default":
#        ROOT.gStyle.SetOptStat(showOpt)
#    ROOT.gPad.Update()

########################################
##############    Using matplotlib    #############
########################################

def plot_ratiosWithLumiColors_noempty(ratios,w,xlabel,ylabel,title,ymin,ymax):
    
    x=[]
    y=[]
    color=[]
    
    for i in range(0,len(ratios)-1):
        x.append(i)
        y.append(ratios[i])
        color.append(w[i])
    fig,ax1=plt.subplots()
    plot=ax1.scatter(x,y,c=color,alpha=0.5,cmap='viridis')
    ax1.set_xlabel(xlabel)
    ax1.set_ylabel(ylabel)
    ax1.set_ylim(ymin=ymin,ymax=ymax)
    ax1.set_xlim(xmin=0,xmax=len(ratios)-1)
    
    
    ax1.set_title(title)
    plt.colorbar(plot, ax=ax1)
    
    return fig

def save_pyFigToFile(fig,outputName,opt="",year=0):
        fig.savefig(outputName)

def plotmpl_ratioInDict_vs_VariablePos_errs(Dict,pos_x_inKeys,pos_err_inKeys,totLumi,xlabel,ylabel,title,ymin,ymax,xmin,xmax):

    ey=[]
    x=[]
    y=[]
    keys=list(Dict)
    keys.sort()
    
    for key in keys:
        x.append(key[pos_x_inKeys])
        y.append(Dict[key][0])
        ey.append(Dict[key][pos_err_inKeys])
    
    f=plt.figure()
    plt.errorbar(x, y, yerr=ey,capsize=0,elinewidth=0.5,markeredgewidth=0.5,ecolor='red',linewidth=0.5,alpha=1,linestyle="--")
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    
    plt.ylim(ymin,ymax)
    if xmin!=0 and xmax!=0:
        plt.xlim(xmin,xmax)
    
    
    return f

#def plotmpl_ratioInDict_vs_VariablePos_pointSize(Dict,pos_x_inKeys,totLumi,xlabel,ylabel,title,ymin,ymax,xmin,xmax):
#
#    ey=[]
#    x=[]
#    y=[]
#    colors=[]
#    keys=list(Dict)
#    keys.sort()
#    
#    for key in keys:
#        x.append(key[pos_x_inKeys])
#        y.append(Dict[key][0])
#        ey.append(1000*Dict[key][2]/totLumi)
#        colors.append(Dict[key][2]/totLumi)
#    
#    f, ax = plt.subplots()
#    
#    cs=ax.scatter(x, y, s=ey,c=ey)
#    ax.plot(x,y,linewidth=0.5,alpha=0.8,linestyle="--")
#    #plt.colormaps()
#    cbar = f.colorbar(cs)
#    ax.title(title)
#    ax.xlabel(xlabel)
#    ax.ylabel(ylabel)
#    
#    ax.ylim(ymin,ymax)
#    if xmin!=0 and xmax!=0:
#        ax.xlim(xmin,xmax)
#    
#    
#    return f

def plotmpl_ratioInDict_vs_VariablePos_pointSize(Dict,pos_x_inKeys,totLumi,xlabel,ylabel,title,ymin,ymax,xmin,xmax,opt="",yearLabel=0,energy=0):
    ey=[]
    x=[]
    y=[]
    colors=[]
    keys=list(Dict)
    keys.sort()
    
    for key in keys:
        x.append(key[pos_x_inKeys])
        y.append(Dict[key][0])
        ey.append(1000*Dict[key][2]/totLumi)
        colors.append(100*Dict[key][2]/totLumi)
    
    f=plt.figure()
    sc=plt.scatter(x, y, s=ey,c=colors,vmin=0, vmax=max(colors))
    plt.plot(x,y,linewidth=0.5,alpha=0.8,linestyle="--")
    #plt.colormaps()
    cbar=plt.colorbar(sc)
    cbar.set_label('% of the total integrated luminosity')
    
    #plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    
    plt.ylim(ymin,ymax)
    if xmin!=0 and xmax!=0:
        plt.xlim(xmin,xmax)
        
    tcms = "CMS"
    tworkinP=setts.plot_mode
    
    plt.text(0, 0.99*ymax, tcms, ha='left',fontsize=16,fontweight='bold')
    plt.text(0, 0.984*ymax, tworkinP, ha='left',fontsize=14,fontstyle='italic',fontweight='light')
    if yearLabel!=0:
        tyearEnergy=setts.energyLabel_list[energy]+" ("+str(yearLabel)+")"
        plt.text(0.68*max(x), 1.004*ymax, tyearEnergy, ha='left',fontsize=14,fontweight='light')
    
    
    return f

def plotmpl_SlopeVsLumi_pointSize(Dict,ymin,ymax,labelRatio,opt="",yearLabel=0,energy=0):
    col=[]
    x=[]
    y=[]
    ey=[]
    colors=[]
    keys=list(Dict)
    keys.sort()
    
    totLumi=0.0
    
    for key in keys:
        totLumi+=Dict[key][0]
    
    for key in keys:
        x.append(key)
        y.append(Dict[key][1])
        ey.append(Dict[key][2])
        col.append(1000*Dict[key][0]/totLumi)
        colors.append(100*Dict[key][0]/totLumi)
    
    f=plt.figure()
    plt.subplots_adjust(left=0.2, right=0.99, top=0.94, bottom=0.13)
    plt.errorbar(x, y, yerr=ey,fmt='none',elinewidth=0.1)
    sc=plt.scatter(x, y, s=col,c=colors,vmin=0, vmax=max(colors))
    ax= f.add_subplot(111)
    
    #plt.plot(x,y,linewidth=0.5,alpha=0.8,linestyle="--")
    #plt.colormaps()
    cbar=plt.colorbar(sc)
    cbar.set_label('% of the total integrated luminosity')
    
    #plt.title(title)
    plt.xlabel("Fill")
    plt.ylabel(labelRatio.upper() + " slope $[(hz/ub)^{-1}]$")
    
    if ymin!=-100 and ymax!=-100:
        plt.ylim(ymin,ymax)
#    if xmin!=0 and xmax!=0:
#        plt.xlim(xmin,xmax)
#        
    tcms = "CMS"
    tworkinP=setts.plot_mode
    
    plt.text(0.01, 0.95, tcms, ha='left',fontsize=16,fontweight='bold',transform=ax.transAxes)
    plt.text(0.01, 0.90, tworkinP, ha='left',fontsize=14,fontstyle='italic',fontweight='light',transform=ax.transAxes)
    if yearLabel!=0:
        tyearEnergy=setts.energyLabel_list[energy] +" ("+str(yearLabel)+")"
        plt.text(0.68, 1.02, tyearEnergy, ha='left',fontsize=14,fontweight='light',transform=ax.transAxes)
    
    
    return f


    
    

def plotmpl_ratioInDict_vs_VariablePos_pointSizeFillsInfo(Dict,meanVal,stdVal,pos_x_inKeys,totLumi,xlabel,ylabel,title,ymin,ymax,xmin,xmax,ratioSensivty,lumiSensivty,filePath,year=0,energy=0):

    ey=[]
    x=[]
    y=[]
    bad_fills=[]
    bad_fillsPos=[]
    colors=[]
    keys=list(Dict)
    keys.sort()
    plt.rcParams.update({'font.size': 14})
    sigma_rel=ratioSensivty
    #1.3
    #FillShowRange=2
    
    tcms = "CMS"
    tworkinP=setts.plot_mode
    
    
    for key in keys:
        x.append(key[pos_x_inKeys])
        y.append(Dict[key][0])
        ey.append(1000*Dict[key][2]/totLumi)
        colors.append(100*Dict[key][2]/totLumi)
    #stdv=statistics.stdev(y)
    #meanratio=statistics.mean(y)
    stdv=stdVal
    meanratio=meanVal
    
    limratioUP=meanratio+stdv*sigma_rel
    limratioDOWN=meanratio-stdv*sigma_rel
    print (limratioUP,limratioDOWN,meanratio,stdv,sigma_rel)
    #print("limratio",limratioDOWN,limratioUP)
    
    #0.1
    lumi_porcent=max(colors)*lumiSensivty
    #print ("lumiPorcent",lumi_porcent)
    for key in keys:
        if (Dict[key][0]>limratioUP or Dict[key][0]<limratioDOWN)  and (100*Dict[key][2]/totLumi)>lumi_porcent:
            if key[0] not in bad_fills:
                bad_fills.append(key[0])
                bad_fillsPos.append((key[pos_x_inKeys],Dict[key][0]))
    
    print ("Fills to analize",bad_fills)
    ##write file:
    fileout=open(filePath+"/fills_to_analyse.txt","w")
    fileout.write(str(bad_fills))
    fileout.close()
    
    f=plt.figure()
    sc=plt.scatter(x, y, s=ey,c=colors,vmin=0, vmax=max(colors))
    plt.plot(x,y,linewidth=0.5,alpha=0.8,linestyle="--")
    plt.axhline(meanratio, color='black', lw=0.8,alpha=0.7,linestyle="--")
    plt.axhline(limratioUP, color='red', lw=0.8,alpha=0.7,linestyle="--")
    plt.axhline(limratioDOWN, color='red', lw=0.8,alpha=0.7,linestyle="--")
    for i, txt in enumerate(bad_fills):
        plt.annotate(txt, bad_fillsPos[i],xytext=(-25, 25),
        textcoords='offset points', ha='right', va='top',
        bbox=dict(boxstyle='round,pad=0.3', fc='yellow', alpha=0.5),
        arrowprops=dict(arrowstyle = '->', connectionstyle='arc3,rad=0'))
    #plt.colormaps()
    cbar=plt.colorbar(sc)
    cbar.set_label('% of the total integrated luminosity')
    
    #plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    ax= f.add_subplot(111)
    
    plt.ylim(ymin,ymax)
    if xmin!=0 and xmax!=0:
        plt.xlim(xmin,xmax)
    
    plt.text(0.01, 0.95, tcms, ha='left',fontsize=16,fontweight='bold',transform=ax.transAxes)
    plt.text(0.01, 0.90, tworkinP, ha='left',fontsize=14,fontstyle='italic',fontweight='light',transform=ax.transAxes)
    if year!=0:
        tyearEnergy=setts.energyLabel_list[energy] +" ("+str(year)+")"
        plt.text(0.68, 1.02, tyearEnergy, ha='left',fontsize=14,fontweight='light',transform=ax.transAxes)
    
    return f


def plotmpl_ratioInDict_vs_VariablePos_pointSizeRunsInfo(Dict,meanVal,stdVal,pos_x_inKeys,totLumi,xlabel,ylabel,title,ymin,ymax,xmin,xmax,ratioSensivty,lumiSensivty,filePath,year=0,energy=0,ptsLabels=True,reducePtsLabels=False,fileID=""):

    ey=[]
    x=[]
    y=[]
    bad_runs=[]
    bad_runsPos=[]
    bad_pts_info={}
    colors=[]
    keys=list(Dict)
    keys.sort()
    plt.rcParams.update({'font.size': 14})
    sigma_rel=ratioSensivty
    #1.3
    #FillShowRange=2
    tcms = "CMS"
    tworkinP=setts.plot_mode
    
    
    for key in keys:
        x.append(key[pos_x_inKeys])
        y.append(Dict[key][0])
        ey.append(1000*Dict[key][2]/totLumi)
        colors.append(100*Dict[key][2]/totLumi)
     #Why this doesn't work for 2018???????????   
#    stdv=statistics.stdev(y)
#    meanratio=statistics.mean(y)
    stdv=stdVal
    meanratio=meanVal
    
    limratioUP=meanratio+stdv*sigma_rel
    limratioDOWN=meanratio-stdv*sigma_rel
    #print("limratio",limratioDOWN,limratioUP)
    
    #0.1
    lumi_porcent=max(colors)*lumiSensivty
    #print ("lumiPorcent",lumi_porcent)
    for key in keys:
        if (Dict[key][0]>limratioUP or Dict[key][0]<limratioDOWN)  and (100*Dict[key][2]/totLumi)>lumi_porcent:
            if key[1] not in bad_runs:
                bad_runs.append(key[1])
                bad_runsPos.append((key[pos_x_inKeys],Dict[key][0]))
                bad_pts_info[key[0],key[1]]=0
            else:
                nBadPtsInRun=bad_pts_info[key[0],key[1]]
                nBadPtsInRun+=1
                bad_pts_info[key[0],key[1]]=nBadPtsInRun
        
    
    print ("Runs to analize",bad_runs)
    ##write file:
    fileout=open(filePath+"/runs_to_analyse"+fileID+str(lumiSensivty)+"LumiSesit.txt","w")
    fileout.write(str(bad_runs))
    fileout.close()
    
    if reducePtsLabels:
        minNlsToPLotLabel=5
        for key_bad in list(bad_pts_info):
            if bad_pts_info[key_bad]<minNlsToPLotLabel:
                idr=bad_runs.index(key_bad[1])
                del bad_runsPos[idr]
                del bad_runs[idr]
        
    f=plt.figure()
    sc=plt.scatter(x, y, s=ey,c=colors,vmin=0, vmax=max(colors))
    plt.plot(x,y,linewidth=0.0,alpha=0.8,linestyle="--")
    
    if ptsLabels:
        plt.axhline(meanratio, color='black', lw=0.8,alpha=0.7,linestyle="--")
        plt.axhline(limratioUP, color='red', lw=0.8,alpha=0.7,linestyle="--")
        plt.axhline(limratioDOWN, color='red', lw=0.8,alpha=0.7,linestyle="--")
    
        for i, txt in enumerate(bad_runs):
            plt.annotate(txt, bad_runsPos[i],xytext=(-25, 25),
            textcoords='offset points', ha='right', va='top',
            bbox=dict(boxstyle='round,pad=0.3', fc='yellow', alpha=0.5),
            arrowprops=dict(arrowstyle = '->', connectionstyle='arc3,rad=0'))
    #plt.colormaps()
    cbar=plt.colorbar(sc)
    cbar.set_label('% of the total integrated luminosity')
    
    #plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    ax= f.add_subplot(111)
    
    plt.ylim(ymin,ymax)
    if xmin!=0 and xmax!=0:
        plt.xlim(xmin,xmax)
        
    plt.text(0.01, 0.95, tcms, ha='left',fontsize=16,fontweight='bold',transform=ax.transAxes)
    plt.text(0.01, 0.90, tworkinP, ha='left',fontsize=14,fontstyle='italic',fontweight='light',transform=ax.transAxes)
    if year!=0:
        tyearEnergy=setts.energyLabel_list[energy] +" ("+str(year)+")"
        plt.text(0.68, 1.02, tyearEnergy, ha='left',fontsize=14,fontweight='light',transform=ax.transAxes)
    
    
    return f

#def plotmpl_ratioInDict_vs_VariablePos_pointSize_date(Dict,pos_x_inKeys,totLumi,xlabel,ylabel,title,ymin,ymax,xmin,xmax):
#
#    ey=[]
#    x=[]
#    y=[]
#    colors=[]
#    keys=list(Dict)
#    keys.sort()
#    
#    for key in keys:
#        x.append(key[pos_x_inKeys])
#        y.append(Dict[key][0])
#        ey.append(1000*Dict[key][2]/totLumi)
#        colors.append(100*Dict[key][2]/totLumi)
#    
#    
#    
#    dates=[dt.datetime.fromtimestamp(ts) for ts in x]
#    xfmt = md.DateFormatter('%Y-%m-%d')
#    
#    
#    f=plt.figure()
#    sc=plt.scatter(dates, y, s=ey,c=ey,vmin=0, vmax=max(colors))
#    ax=plt.gca()
#    ax.xaxis.set_major_formatter(xfmt)
#    
#    plt.plot(dates,y,linewidth=0.5,alpha=0.8,linestyle="--")
#    #plt.colormaps()
#    cbar=plt.colorbar(sc)
#    cbar.set_label('% of the total integrated luminosity')
#    
#    plt.title(title)
#    plt.xlabel(xlabel)
#    plt.ylabel(ylabel)
#    
#    plt.ylim(ymin,ymax)
#    if xmin!=0 and xmax!=0:
#        plt.xlim(xmin,xmax)
#    
#    
#    return f

def getLegend(elements,header="legend",pos=[.87,.6,.6,.88],fillColor=0,textFont=50,textSize=.04,borderSize=0):
    leg = ROOT.TLegend (pos[0],pos[1],pos[2],pos[3])
    leg.SetFillColor(fillColor)
    leg.SetTextFont(textFont)
    leg.SetTextSize(textSize)
    leg.SetBorderSize(borderSize)
    
    
    return leg
    

def plotmpl_ratioInDict_vs_VariablePos_LineColor(Dict,pos_x_inKeys,totLumi,xlabel,ylabel,title,ymin,ymax,xmin,xmax):

    ey=[]
    x=[]
    y=[]
    colors=[]
    keys=list(Dict)
    keys.sort()
    
    for key in keys:
        x.append(key[pos_x_inKeys])
        y.append(Dict[key][0])
        ey.append(1000*Dict[key][2]/totLumi)
        colors.append(100*Dict[key][2]/totLumi)
    
    f=plt.figure()
    sc=plt.scatter(x, y,s=0.8,c=ey,vmin=0, vmax=max(colors))
    #plt.colormaps()
    cbar=plt.colorbar(sc)
    cbar.set_label('% of the total integrated luminosity')
    
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    
    plt.ylim(ymin,ymax)
    if xmin!=0 and xmax!=0:
        plt.xlim(xmin,xmax)
    
    
    return f

###Tests

def runTests(dict1,dict2,overlapKeys,labelRatio,detector1label,detector2label,colors,hist_nbins,hist_xmin,hist_xmax,options,year=0,energy=0):
    idL1=1
    nbins_list=setts.nbins_list
    nls_list=setts.nls_list

    
    nlsDicts=[]
    plots_hist_byLsW_testNls=[]
    plots_hist_byLsW_testBins=[]
    plots_binsTest_hist= ROOT.THStack("hsall_bins_test_histos","")
    plots_binsTest_hist.SetTitle(";Detector ratios;Integrated Luminosity [fb^{-1}]") 
    plots_nlsTest_hist= ROOT.THStack("hsall_bins_test_histos","")
    plots_nlsTest_hist.SetTitle(";"+labelRatio+" ratio;Integrated Luminosity [fb^{-1}]") 
    
    plot_sigmaVsNbin=ROOT.TGraph()
    plot_sigmaVsNls=ROOT.TGraph()
    plot_sigmaVsStdErr=ROOT.TGraph()
    plot_sigmaVsRelStdErr=ROOT.TGraph()
    
    leg_binsTest_hist = ROOT.TLegend (.87,.6,.6,.88)
    leg_binsTest_hist.SetFillColor(0)
    leg_binsTest_hist.SetTextFont(50)
    leg_binsTest_hist.SetTextSize(.04)
    leg_binsTest_hist.SetBorderSize(0)
    
    leg_nlsTest_hist = ROOT.TLegend (.87,.6,.6,.88)
    
    if year==2015:
        leg_nlsTest_hist = ROOT.TLegend (.815,.65,.55,.945)
    
    leg_nlsTest_hist.SetFillColor(0)
    leg_nlsTest_hist.SetTextFont(50)
    leg_nlsTest_hist.SetTextSize(.03)
    leg_nlsTest_hist.SetBorderSize(0)
    leg_nlsTest_hist.SetHeader("Interval (stdv)[std err %]")
    
    
    
    
    
    pt=0
    for nLS in nls_list:
        nlsDicttemp,totLumi1,totLumi2=calc_ratio2detectNlsDict(dict1,dict2,overlapKeys,nLS)
        nlsDicts.append(nlsDicttemp)
        
        tempPlot=histoW_fromDictColor(nlsDicttemp,0,idL1,labelRatio,hist_nbins,hist_xmin,hist_xmax,colors)
        setPlotProperties(tempPlot,"",labelRatio.upper()+" ratio per "+str(nLS)+" LS","Luminosity [fb^{-1}]")
        plots_hist_byLsW_testNls.append(tempPlot)
        
        stdv=tempPlot.GetStdDev()
        stdvErr=tempPlot.GetStdDevError()
        
        #tempPlot.SetFillColor(colors[pt])
        try:
            tempPlot.SetLineColor(colors[pt])
            if pt!=(len(colors)-1):
                leg_nlsTest_hist.AddEntry(tempPlot,str(nLS)+" ls ("+str(float("{0:.4f}".format(stdv)))+")["+str(float("{0:.1f}".format(stdvErr*100/stdv)))+"%]","l")
            else:
                leg_nlsTest_hist.AddEntry(tempPlot,"> "+ str(nLS)+" ls","l")
        except:
            pass

        
        plots_nlsTest_hist.Add(tempPlot,"HIST")
        
        plot_sigmaVsNls.SetPoint(pt,nLS,stdv)
        plot_sigmaVsStdErr.SetPoint(pt,nLS,stdvErr)       
        plot_sigmaVsRelStdErr.SetPoint(pt,nLS,stdvErr/stdv)                               
        
        pt+=1
    
    pt=0
    nlsPos=0
    for nb in nbins_list:
        tempPlot=histoW_fromDictColor(nlsDicts[nlsPos],0,idL1,labelRatio,nb,hist_xmin,hist_xmax,colors)
        setPlotProperties(tempPlot,"",labelRatio.upper()+" ratio per "+str(nb)+" LS","Luminosity [fb^{-1}]")
        plots_hist_byLsW_testBins.append(tempPlot)
        
        #tempPlot.SetFillColor(colors[pt])
        try:
            tempPlot.SetLineColor(colors[pt])
            if pt!=(len(colors)-1):
                leg_binsTest_hist.AddEntry(tempPlot,str(nb)+" bins","l")
            else:
                leg_binsTest_hist.AddEntry(tempPlot,"> "+ str(nb)+" bins","l")
        except:
            pass

        
        plots_binsTest_hist.Add(tempPlot,"HIST")
        
        plot_sigmaVsNbin.SetPoint(pt,nb,tempPlot.GetStdDev())  
        
        pt+=1
    
    if options.lumibin:
        minratio=0.15
        maxratio=1.85
        lumibins_list=[50,100,150,300,400]
        lumibinOff=150
        plots_hist_byLsW_testLumBins=[]
        plots_hist_byLsW_testLumRBins=[]
        plots_RbinsTest_hist= ROOT.THStack("plots_RbinsTest_hist","")
        plots_RbinsTest_hist.SetTitle(";"+labelRatio.upper()+ " ratio;Integrated Luminosity [fb^{-1}]") 
        plots_LbinsTest_hist= ROOT.THStack("plots_LbinsTest_hist","")
        plots_LbinsTest_hist.SetTitle(";"+labelRatio.upper()+ " ratio;Integrated Luminosity [fb^{-1}]") 
        
        plot_sigmaVsRbin=ROOT.TGraph()
        plot_sigmaVsLbin=ROOT.TGraph()
        
        leg_lbinTest_hist = ROOT.TLegend (.87,.6,.6,.88)
        leg_lbinTest_hist.SetFillColor(0)
        leg_lbinTest_hist.SetTextFont(50)
        leg_lbinTest_hist.SetTextSize(.04)
        leg_lbinTest_hist.SetBorderSize(0)
        leg_lbinTest_hist.SetHeader('Binning(stdv)')

        leg_rbinTest_hist = ROOT.TLegend (.87,.6,.6,.88)
        leg_rbinTest_hist.SetFillColor(0)
        leg_rbinTest_hist.SetTextFont(50)
        leg_rbinTest_hist.SetTextSize(.04)
        leg_rbinTest_hist.SetBorderSize(0)
        
        ratiosDict=calc_ratioDict2detect(dict1,dict2,overlapKeys,minratio,maxratio)
        nLumRatioBins=100
        pt=0
        for lumiBin in lumibins_list:
            lumEqDict=equalLumiAnalysis(ratiosDict,lumiBin,totLumi2)
            tempPlot=histo_fromDict(lumEqDict,0,labelRatio,nLumRatioBins,hist_xmin,hist_xmax)    
            try:
                tempPlot.SetLineColor(colors[pt])
                if pt!=(len(colors)-1):
                    leg_lbinTest_hist.AddEntry(tempPlot,str(lumiBin)+" lbin ("+str(float("{0:.4f}".format(stdv)))+")","l")
                else:
                    leg_lbinTest_hist.AddEntry(tempPlot,"> "+ str(lumiBin)+" lbin","l")
            except:
                pass
            
            
            plots_LbinsTest_hist.Add(tempPlot,"HIST")
            stdv=tempPlot.GetStdDev()
            
            plot_sigmaVsLbin.SetPoint(pt,lumiBin,stdv)  
            
            if lumiBin==lumibinOff:
                pt2=0
                for lumiBinR in lumibins_list:
                    templotR=histo_fromDict(lumEqDict,0,labelRatio,lumiBinR,hist_xmin,hist_xmax)
                    try:
                        templotR.SetLineColor(colors[pt2])
                        if pt2!=(len(colors)-1):
                            leg_rbinTest_hist.AddEntry(templotR,str(lumiBinR)+" rbin ("+str(float("{0:.4f}".format(stdv)))+")","l")
                        else:
                            leg_rbinTest_hist.AddEntry(templotR,"> "+ str(lumiBinR)+" rbin","l")
                    except:
                        pass
                    
                    plots_RbinsTest_hist.Add(templotR,"HIST")
                    stdv=templotR.GetStdDev()
                    
                    pt2+=1
                    
            
            pt+=1
            
    
    
    solocan=ROOT.TCanvas("solocan","solocan",900,300)
    solocan_square=ROOT.TCanvas("sqcan","sqcan",600,600)
    
    #plots_nlsTest_hist.GetYaxis().SetRangeUser(hist_xmin,hist_xmax)
    saveintoCanvas_and_toFileWIP(solocan_square,plots_nlsTest_hist,"nostack",
                                   options.outDir+ "/tests/" +detector1label+"_"+detector2label+"plots_nlsTest_hist.pdf","il_sq",year,energy,
                                   drawleg=True,leg=leg_nlsTest_hist)
    #plots_binsTest_hist.GetYaxis().SetRangeUser(hist_xmin,hist_xmax)
    saveintoCanvas_and_toFileWIP_withLeg(solocan_square,plots_binsTest_hist,leg_binsTest_hist,"nostack",
                               options.outDir+ "/tests/" +detector1label+"_"+detector2label+"plots_binsTest_hist.pdf","il_sq") 
    
    if options.lumibin:
        saveintoCanvas_and_toFileWIP_withLeg(solocan_square,plots_LbinsTest_hist,leg_lbinTest_hist,"nostack",
                               options.outDir+ "/tests/" +detector1label+"_"+detector2label+"plots_LbinsTest_hist.pdf","il_sq") 
        saveintoCanvas_and_toFileWIP_withLeg(solocan_square,plots_RbinsTest_hist,leg_rbinTest_hist,"nostack",
                               options.outDir+ "/tests/" +detector1label+"_"+detector2label+"plots_RbinsTest_hist.pdf","il_sq") 
   
    plot_sigmaVsNls.SetLineColor(colors[0])
    plot_sigmaVsNls.SetMarkerColor(colors[1])
    setPlotProperties(plot_sigmaVsNls,"","integrated lumisections","\sigma")
    saveintoCanvas_and_toFileWIP(solocan,plot_sigmaVsNls,"ALP",
                               options.outDir+ "/tests/" +detector1label+"_"+detector2label+"_plot_sigmaVsNls.pdf","il_nsq")
    plot_sigmaVsStdErr.SetLineColor(colors[0])
    plot_sigmaVsStdErr.SetMarkerColor(colors[1])
    setPlotProperties(plot_sigmaVsStdErr,"","integrated lumisections","\sigma error")
    saveintoCanvas_and_toFileWIP(solocan,plot_sigmaVsStdErr,"ALP",
                               options.outDir+ "/tests/" +detector1label+"_"+detector2label+"_plot_sigmaVsStdErr.pdf","il_nsq")
    plot_sigmaVsRelStdErr.SetLineColor(colors[0])
    plot_sigmaVsRelStdErr.SetMarkerColor(colors[1])
    setPlotProperties(plot_sigmaVsRelStdErr,"","integrated lumisections","rel. \sigma err")
    saveintoCanvas_and_toFileWIP(solocan,plot_sigmaVsRelStdErr,"ALP",
                               options.outDir+ "/tests/" +detector1label+"_"+detector2label+"_plot_sigmaVsRelStdErr.pdf","il_nsq")
    
    plot_sigmaVsNbin.SetLineColor(colors[0])
    plot_sigmaVsNbin.GetYaxis().SetRangeUser(0.003, 0.007)
    plot_sigmaVsNbin.SetMarkerColor(colors[1])
    #plot_sigmaVsNbin.SetMarkerSize(0.05)
    setPlotProperties(plot_sigmaVsNbin,"","bining","\sigma")
    saveintoCanvas_and_toFileWIP(solocan,plot_sigmaVsNbin,"ALP",
                               options.outDir+ "/tests/" +detector1label+"_"+detector2label+"_plot_sigmaVsNbin.pdf","il_nsq")



def detcNameModifier(detector1label,detector2label):
    if detector1label=="physics" and detector2label=="physics_compare":
        print("official luminosity analysis")
        detector1label="best"
        detector2label="second"        
    elif detector2label=="physics" and detector1label=="physics_compare":
        print("official luminosity analysis")
        detector2label="best"
        detector1label="second"
    
    for known_d in setts.known_luminometers:
        if known_d in detector1label:
            detector1label=known_d
        if known_d in detector2label:
            detector2label=known_d
        
    return detector1label,detector2label

def detcNameModifierMulti(detectorlabelList):
    output=[]
    for det in detectorlabelList:
       if det=="physics":
            print("official luminosity analysis")
            det="best"
       elif det=="physics_compare":
             det="second"
       elif "plt" in det:
             det="plt"
       output.append(det) 
             
def fit(funct):
    pol1_range = ROOT.TF1("pol1range","pol1",3.0,13.9)
    pol1 = ROOT.TF1("pol1","pol1",0.0,13.9)
    pol4 = ROOT.TF1("pol4","pol4",0.0,13.9)
    pol4range = ROOT.TF1("pol4range","pol4",1.5,13.9)
    pol1_range2 = ROOT.TF1("pol1range2","pol1",0.0,1.5)
    
    pol1_range.SetLineColor(9)    
    pol1.SetLineColor(8)
    pol4.SetLineColor(11)
    pol4range.SetLineColor(12)
    pol1_range2.SetLineColor(13)
    
    plot_ratioVsLumiAll_TH2D_Pfx.Fit(pol1,"R")
    plot_ratioVsLumiAll_TH2D_Pfx.Fit(pol1_range,"R+")
    plot_ratioVsLumiAll_TH2D_Pfx.Fit(pol4,"R+")
    plot_ratioVsLumiAll_TH2D_Pfx.Fit(pol4range,"R+")
    plot_ratioVsLumiAll_TH2D_Pfx.Fit(pol1_range2,"R+")
    
def saveInfoToFile(detector1label,detector2label,options):
    extraFileName=detector1label+"-"+detector2label
    filePath=options.outDir+"/"
    fileout=open(filePath+"!!outputinfo"+extraFileName+".txt","w")
    
    fileout.write("******* Detectors in analysis: "+ detector1label + " and " + detector2label,"******* \n")
        
    
    
    
    
    
    
    
    