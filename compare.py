#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 29 12:52:07 2018

@author: sosarica
!!!!!!!!!!!!!!!!!!!!!!!!!!!Luminosity most be in 1/ub
"""

import ROOT
import optparse
import os
import math     
import warnings

#import shutil
#from rootpy.plotting import Hist, HistStack, Legend, Canvas
#import rootpy.plotting.root2matplotlib as rplt

#By me
import lumi_compare as lumi
import settings as setts

ROOT.gROOT.SetBatch(ROOT.kTRUE)

#my style
lumi.setRESRStyle()
colors=setts.colors

#CMS style
#tdrstyle.setTDRStyle()

important_fills=[]
excludedFills=[]

#################### Load settings from settings.py (change parameters and add information there) ############################
detectors_excludedFills=setts.detectors_excludedFills
detectors_excludedFills_for_linearity=setts.detectors_excludedFills_for_linearity
detectors_with_exclusions=list(detectors_excludedFills)

xLS=setts.xLS  

ymin=setts.ymin
ymax=setts.ymax
minratio=setts.minratio
maxratio=setts.maxratio
nls=setts.nls
hist_xmin=setts.hist_xmin
hist_xmax=setts.hist_xmax
hist_nbins=setts.hist_nbins
nxbins=setts.nxbins
nybins=setts.nybins
nLumBins=setts.nLumBins
nLumRatioBins=setts.nLumRatioBins
lumiPjxBins=setts.lumiPjxBins

time_xmin=setts.time_xmin
time_xmax=setts.time_xmax
energy=setts.default_energy

idTime=setts.idTime
idRun=setts.idRun
idFill=setts.idFill
idN=setts.idN
idL1=setts.idL1
idL2=setts.idL2
idErrinEqLumDIct=setts.idErrinEqLumDIct

ratioSensivty=setts.ratioSensivty
lumiSensivty=setts.lumiSensivty
flag_useSBILrange=setts.flag_useSBILrange
sbil_range=setts.sbil_range
sbil_min=setts.sbil_min
sbil_max=setts.sbil_max


p = optparse.OptionParser()
usage = "usage: %prog [options] arg1 arg2 ..."
p.add_option("-o", "--outdir", type="string",help="Path to output Dir", dest="outDir", default=".")
p.add_option("-i", "--indir", type="string",help="Path to input Dir", dest="inDir", default=".")
p.add_option("-n", "--nbxfile", type="string",help="Path to NBXperFILL input file", dest="inNBX", default="")
p.add_option("-f", "--plotformat", type="string",help="output format for plots", dest="plotformat", default="")

p.add_option("-p", "--physics",action="store_true",help="takes physics normtag as reference in time", dest="physics", default=False)
p.add_option("-a", "--all",action="store_true",help="all data vs. selected analysis", dest="all", default=False)
p.add_option("-x", "--exclusion",action="store_true",help="Exclude fills for detectors as especified in code", dest="exclusion", default=False)
p.add_option("-z", "--lumibin",action="store_true",help="activate lumibin analysis", dest="lumibin", default=False)
p.add_option("-y", "--useyearopts",action="store_true",help="activate the automatic setting for specifics years", dest="useYearOpts", default=False)
p.add_option("-t", "--test",action="store_true",help="some binning test are performed", dest="test", default=False)
p.add_option("-l", "--corr",action="store_true",help="linearity analysis", dest="corr", default=False)
p.add_option("-b", "--begining", type="int",help="initial Run or Fill", dest="ini", default=1)
p.add_option("-e", "--end", type="int",help="end Run or Fill", dest="end", default=1)


(options, args) = p.parse_args()

### Initial checkings
n_files=len(args)
plotformat=options.plotformat
if plotformat=="":
    plotformat=setts.plotformat


inidigits=int(math.log10(options.ini))+1
enddigits=int(math.log10(options.end))+1

iniFill=-1
endFill=-1
if iniFill!=-1 and endFill!=-1:
    time_xmin=0
    time_xmax=0


#Fills exclusion
if options.exclusion:
    print ("Excluding fills: ")
    for det in detectors_with_exclusions:    
        if (det in args[0]) or (det in args[1]):
            try:
                excludedFills.extend(detectors_excludedFills[det])
            except:
                print("No excluded fills defined for: ",det)
            if options.corr:
                try:
                    excludedFills.extend(detectors_excludedFills_for_linearity[det])
                except:
                    print("No excluded fills for linearity defined for: ",det)
    print (excludedFills)

if n_files == 2:
    
    print ("******** 2 detector comprison choosed!! ******")
    if options.physics and options.exclusion:
        print ("Not posible to use exclusion and physics at the same time")
        exit()
    
    detector1label=args[0]
    detector2label=args[1]
    
    filename1=detector1label + ".csv"
    filename2=detector2label + ".csv"
    file_path1=options.inDir +"/"+ filename1
    file_path2=options.inDir +"/"+ filename2
    print ("input file 1: " + file_path1)
    print ("input file 2: " + file_path2)
    
    #open files
    try:
        file1=open(file_path1)
        file2=open(file_path2)
    except:
        print ("input files cant't be opened!!")
        quit()
        
    ## Getting detectors names from file names
    print("detector label 1: " + detector1label)
    print("detector label 2: " + detector2label)
    
    ##Create ouput files
    options.outDir=options.outDir+"/" + detector1label + "-" + detector2label
    try:
        os.makedirs(options.outDir)
    except:
        pass
    
    
    detector1label,detector2label=lumi.detcNameModifier(detector1label,detector2label)
    
        
    labelRatio=detector1label+"/"+detector2label
    
    #info
    print ("******* Detectors in analysis: ",detector1label ," and ", detector2label,"*******") 
    
    ### To do: reduce this!!
    if options.physics:
        if labelRatio!="best/second":
            warnings.warn("input files ("+ file_path1+","+ file_path2 + " not reconized as physics/physics_compare. Physics most be in the numerator!")
        dict1,labels=lumi.myDictFromCSVFileReadAll(file1)
        dict2,labels=lumi.myDictFromCSVFileReadAll(file2)
    else:
        dict1all,dict1=lumi.myDictFromCSVFileAndExclud(file1,excludedFills)
        dict2all,dict2=lumi.myDictFromCSVFileAndExclud(file2,excludedFills)
    
    if iniFill!=-1 and endFill!=-1:
        dict1=lumi.myDictFromCSVFileFillRangAndExcl(file1,excludedFills,iniFill,endFill)
        dict2=lumi.myDictFromCSVFileFillRangAndExcl(file2,excludedFills,iniFill,endFill)
    
    #Select only the common lumi sections for both detectors
    overlapKeys=list(set(dict1).intersection(dict2))
    
    #Sorting to evoid disorder in plots
    overlapKeys.sort()
    
    
    if len(excludedFills)!=0:
        overlapKeysAll=list(set(dict1all).intersection(dict2all))
        overlapKeysAll.sort()
  
    #Identify year and energy from file using the ranges stablished in settings.py
    year,energy=lumi.identifyYear_and_Energy(overlapKeys)
    
    try:
        nls=setts.nls_year[year,energy]
    except:
        nls=setts.nls

    print("\n -------------------- Year in analysis: "+str(year)+"("+ str(energy)+"TeV) --------------------")
    
    file1.close()
    file2.close()
    
    
    if options.physics:
        lumi.physicsFileAnalisysCompare(dict1,dict2,labels,options.outDir,year,energy)
    
    #info
    print ("Lenghts file1,file2,overlapKeys: ",len(dict1),len(dict2),len(overlapKeys))
    
    histo_ratio=lumi.histo_ratio_2detectors(overlapKeys,dict1,dict2,labelRatio,hist_nbins,hist_xmin,hist_xmax)
    
    ratiosDict=lumi.calc_ratioDict2detect(dict1,dict2,overlapKeys,minratio,maxratio)
    nlsDict,totLumi1,totLumi2=lumi.calc_ratio2detectNlsDict(dict1,dict2,overlapKeys,nls)
    runDict=lumi.calc_ratio2detectByRunDict(dict1,dict2,overlapKeys)
    fillDict=lumi.calc_ratio2detectByFillDict(dict1,dict2,overlapKeys)
    
    if len(excludedFills)!=0:
        nlsDictAll,totLumi1All,totLumi2All=lumi.calc_ratio2detectNlsDict(dict1all,dict2all,overlapKeysAll,nls)
        plot_histAll_byLsW=lumi.histoW_fromDictColor(nlsDictAll,0,idL1,labelRatio,hist_nbins,hist_xmin,hist_xmax)
    
    if options.lumibin:
        outDir_lumibin=options.outDir+"/lumibin"        
        
        try:
            os.mkdir(outDir_lumibin)
        except:
            pass       
    
        totLum12infb=(totLumi1)*xLS*(10**(-9))/nLumBins
        print("LumiBin ON",nLumBins,str(totLum12infb)+" [1/fb]")
        lumEqDict=lumi.equalLumiAnalysis(ratiosDict,nLumBins,totLumi1)
        
        plot_byLumBin_noempty=lumi.plot_ratio_noempty_fromDict(lumEqDict,"nLumBin("+str(float("{0:.3f}".format(totLum12infb)))+" fb^{-1})",labelRatio.upper()," Int. Lumi Ratio by lumiBins(det2) of: "+str(float("{0:.3f}".format(totLum12infb)))+" [1/fb]",ymin,ymax)
        plot_byLumBin_vsTime=lumi.plot_ratioInDict_vs_VariablePos(lumEqDict,idTime,"time","ratio",labelRatio+" Int. Lumi Ratio by int. lum of: "+str(float("{0:.3f}".format(totLum12infb)))+" [1/fb]",ymin,ymax,time_xmin,time_xmax)
        
        plot_hist_byLumBin=lumi.histo_fromDict(lumEqDict,0,labelRatio,nLumRatioBins,hist_xmin,hist_xmax)
        lumi.setPlotProperties(plot_hist_byLumBin,"",labelRatio.upper()+" ("+str(float("{0:.3f}".format(totLum12infb)))+" fb^{-1})","Number of bins")         
        plot_hist_byLumBin.SetLineColor(colors[0])
    
    plot_byNls_noempty=lumi.plot_ratio_noempty_fromDict(nlsDict,"n"+str(nls)+"ls",labelRatio," Int. Lumi Ratio by "+str(nls)+" LS",ymin,ymax)
    lines_fills_nls=lumi.getFillLines(ymin,ymax,nlsDict,3)
    lines_fills_integratedLumi=lumi.getFillLinesIntLumi(ymin,ymax,nlsDict,xLS*(10**(-9)))
    plot_byRun_noempty=lumi.plot_ratio_noempty_fromDict(runDict,"nRuns",labelRatio," Int. Lumi Ratio by non-empty Run number",ymin,ymax)
    
    plot_byNls_vsTime=lumi.plot_ratioInDict_vs_VariablePos(nlsDict,idTime,"time","ratio",labelRatio+" Int. Lumi Ratio by "+str(nls)+" LS vs Time",ymin,ymax,time_xmin,time_xmax)
    plot_byRun_vsTime=lumi.plot_ratioInDict_vs_VariablePos(runDict,idTime,"time","ratio",labelRatio+" Int. Lumi Ratio by Run",ymin,ymax,time_xmin,time_xmax)
    plot_byNls_vsFill=lumi.plot_ratioInDict_vs_VariablePos(nlsDict,idFill,"Fill","ratio per "+str(nls)+" LS",labelRatio+" Int. Lumi Ratio by Run",ymin,ymax,0,0)
    plot_byFill_vsFill=lumi.plot_ratioInDict_vs_VariablePos(fillDict,idFill,"Fill","ratio per fill",labelRatio+" Int. Lumi Ratio by Fill",ymin,ymax,0,0)
    
    plot_vsFill=lumi.plot_ratioInDict_vs_VariablePos(ratiosDict,idFill,"Fill","ratio",labelRatio+" ratio vs fill",ymin,ymax,0,0)
    
    plot_hist_byLs=lumi.histo_fromDict(nlsDict,0,labelRatio,hist_nbins,hist_xmin,hist_xmax)
    plot_hist_byLs.SetTitle(labelRatio+" histogram of ratios per "+str(nls) +"ls ")
    lumi.setPlotProperties(plot_hist_byLs,"",labelRatio.upper()+" ratio per "+str(nls)+" LS","N")
    plot_hist_byLs.SetFillColor(setts.getColorForDetectorPair(labelRatio))
    plot_hist_byLs.SetLineColor(setts.getColorForDetectorPair(labelRatio))
    
    plot_hist_byLsW=lumi.histoW_fromDictColor(nlsDict,0,idL1,labelRatio,hist_nbins,hist_xmin,hist_xmax)
    lumi.setPlotProperties(plot_hist_byLsW,"",labelRatio.upper()+" ratio per "+str(nls)+" LS","Integrated Luminosity [fb^{-1}]")
    plot_hist_byLsW.SetFillColor(setts.getColorForDetectorPair(labelRatio))
    meanVal=plot_hist_byLsW.GetMean()
    stdVal=plot_hist_byLsW.GetStdDev()
    errVal=plot_hist_byLsW.GetMeanError()
    
    lumi.setPlotProperties(histo_ratio,"",labelRatio.upper()+" ratio","N")
    histo_ratio.SetLineColor(setts.getColorForDetectorPair(labelRatio))
    histo_ratio.SetFillColor(setts.getColorForDetectorPair(labelRatio))

    plot_ratioByRuns_TH2D=lumi.plot_ratio_TH2D_fromDict(runDict,idRun,"Runs","ratios avg per Run ",labelRatio+" ratios avg per Run",ymin,ymax,nxbins,nybins)
    plot_ratioByRuns_TH2D_Pfx=plot_ratioByRuns_TH2D.ProfileX("plot_ratioByRuns_TH2D_Pfx")
    lumi.setPlotProperties(plot_ratioByRuns_TH2D_Pfx,"Instantaneus Luminosity ratio per Run for "+ labelRatio,"Runs","Ratios",ymin,ymax)
    
    plot_ratioVsInst_all=lumi.plot_ratioInDict_vs_VariablePosDict(ratiosDict,idL2,"Lumi. [hz/\mub]",labelRatio + " ratio",labelRatio,ymin,ymax,time_xmin,time_xmax)
    
    ## Renormalize data to 1 using computed mean value
    ratiosDict_norm=lumi.normalizeDict(ratiosDict,meanVal)
    nlsDict_norm=lumi.normalizeDict(nlsDict,meanVal)
    plot_hist_byLsW_norm=lumi.histoW_fromDictColor(nlsDict_norm,0,idL1,labelRatio,hist_nbins,hist_xmin,hist_xmax)
    lumi.setPlotProperties(plot_hist_byLsW_norm,"renormalized ratio histogram",labelRatio.upper()+" ratio per "+str(nls)+" LS","Luminosity [fb^{-1}]")
    plot_hist_byLsW_norm.SetFillColor(setts.getColorForDetectorPair(labelRatio))
    
    if options.corr:
        ## sbil range definition:
        if flag_useSBILrange:
            
            SBIL_infoList=list(sbil_range)
            sbil_tag=(year,labelRatio)
            if sbil_tag in SBIL_infoList:
                sbil_min=sbil_range[sbil_tag][0]
                sbil_max=sbil_range[sbil_tag][1]
                print ("You have selected taking into account only the sbil range: ",sbil_min,sbil_max)
            else:
                try:
                    sbil_min=sbil_range[year][0]
                    sbil_max=sbil_range[year][1]
                    print ("You have selected taking into account only the sbil range: ",sbil_min,sbil_max)
                except:
                    print("No sbil range info for the year: ",year,'.  You can add it at the beginning of compare.py (Settings for linearity)')
                    print("Runing without sbil range!!")
        
        outDir_corr=options.outDir+"/linearity"
        if options.inNBX!="":
            nBXPerFill=lumi.readNBXperFill(options.inNBX)
        else:
            if year!=0:
                options.inNBX="NBX_files/NBX_"+str(year)+"_perFill.csv"
                nBXPerFill=lumi.readNBXperFill(options.inNBX)
            else:
                print ("Please specify NBX_file for you data to use SBIL related analysis")
                quit()
                
        try:
            os.mkdir(outDir_corr)
        except:
            pass 
        
        plot_sbil_all=lumi.plot_sbill_all(ratiosDict,nBXPerFill,idL2,"Lumi. [hz/ub] per BX","ratios avg",labelRatio+" ratios Vs Lumi.",ymin,ymax)
        plot_sbil_nls=lumi.plot_sbill_nls(nlsDict,nBXPerFill,idL2,nls,"Lumi. [hz/ub] per BX","ratios "+str(nls)+" avg",labelRatio+" ratios avg Vs Lumi. every",ymin,ymax)
        
        if year==2016:            
            ##Restrict fitting range 2016
            fit_min=1.7
            fit_max=5.2
            if labelRatio=="hfoc/dt":
                fit_min=2.0
                fit_max=6.0 
        elif year==2015 and energy==5:
            ##Restrict fitting range 2016
            fit_min=0.0
            fit_max=0.0
        elif year==2015:
            ##Restrict fitting range 2016
            fit_min=1.4
            fit_max=2.35
        else:
            fit_min=0.0
            fit_max=0.0       
        
        perFillLinInfo,fillLinearityInfoDict=lumi.perFillLineatiyAnalysis(ratiosDict_norm,nlsDict_norm,nBXPerFill,ymin,ymax,labelRatio,outDir_corr,fit_min,fit_max,year,sbil_min,sbil_max,energy)
        
    plot_byNls_vsIntegratedLumi=lumi.plot_vsIntegratedLumi_fromDict(nlsDict,"Integrated Luminosity [fb^{-1}]",labelRatio+" ratio in "+str(nls)+"LS"," Int. Lumi Ratio by "+str(nls)+" LS vs Integrated Luminosity",ymin,ymax)            

        
    
    ###Save PLots!
    solocan=ROOT.TCanvas("solocan","solocan",900,300)
    solocan_square=ROOT.TCanvas("sqcan","sqcan",600,600)
    ROOT.gStyle.SetOptStat(1100)
    
    lumi.saveintoCanvas_and_toFileWIP(solocan,plot_byNls_noempty,"",
                                   options.outDir+ "/" + detector1label + "-" + detector2label +"_plot_byNls_noempty"+plotformat,"il_nsq",year,energy)    
    lumi.saveintoCanvas_and_toFileWIP_lines(solocan,plot_byNls_noempty,lines_fills_nls,"",
                                   options.outDir+ "/" + detector1label + "-" + detector2label +"_plot_byNls_noempty_fillLines"+plotformat,"il_nsq",year,energy)    
    lumi.saveintoCanvas_and_toFileWIP(solocan,plot_byRun_noempty,"",
                                   options.outDir+ "/" + detector1label + "-" + detector2label +"_plot_byRun_noempty"+plotformat,"il_nsq",year,energy)
    lumi.saveintoCanvas_and_toFileWIP(solocan,plot_byNls_vsTime,"",
                                   options.outDir+ "/" + detector1label + "-" + detector2label +"_plot_byNls_vsTime"+plotformat,"il_nsq",year,energy)
    lumi.saveintoCanvas_and_toFileWIP(solocan,plot_byNls_vsFill,"AP",
                                   options.outDir+ "/" + detector1label + "-" + detector2label +"_plot_byNls_vsFill"+plotformat,"il_nsq",year,energy)
    lumi.saveintoCanvas_and_toFileWIP(solocan,plot_vsFill,"AP",
                                   options.outDir+ "/" + detector1label + "-" + detector2label +"_plot_vsFill"+plotformat,"il_nsq",year,energy)
    lumi.saveintoCanvas_and_toFileWIP(solocan,plot_byFill_vsFill,"AP",
                               options.outDir+ "/" + detector1label + "-" + detector2label +"_plot_byFill_vsFill"+plotformat,"il_nsq",year,energy)
    lumi.saveintoCanvas_and_toFileWIP(solocan,plot_byRun_vsTime,"",
                                   options.outDir+ "/" + detector1label + "-" + detector2label +"_plot_byRun_vsTime"+plotformat,"il_nsq",year,energy)
    
    
    if options.lumibin:
        lumi.saveintoCanvas_and_toFileWIP(solocan,plot_byLumBin_noempty,"",
                                   outDir_lumibin+ "/" + detector1label + "-" + detector2label +"_plot_byLumBin_noempty"+plotformat,"il_nsq",year,energy)
        lumi.saveintoCanvas_and_toFileWIP(solocan,plot_byLumBin_vsTime,"",
                                   outDir_lumibin+ "/" + detector1label + "-" + detector2label +"_plot_byLumBin_vsTime"+plotformat,"il_nsq",year,energy)
        lumi.saveintoCanvas_and_toFileWIP(solocan_square,plot_hist_byLumBin,"HIST",
                                   outDir_lumibin+ "/" + detector1label + "-" + detector2label +"_plot_hist_byLumBin"+plotformat,"il_sq",year,energy)

        
    ROOT.gStyle.SetOptStat(0)
    lumi.saveintoCanvas_and_toFileWIP(solocan,plot_ratioByRuns_TH2D_Pfx,"",
                                   options.outDir+ "/" + detector1label + "-" + detector2label +"_plot_ratioByRuns_TH2D_Pfx"+plotformat,"")
    lumi.saveintoCanvas_and_toFileWIP(solocan_square,plot_hist_byLsW,"HIST",
                                   options.outDir+ "/" + detector1label + "-" + detector2label +"_plot_hist_byLsWstat"+plotformat,"il_sq",year,energy,
                                   extraText1='#splitline{Mean = '+str(float("{0:.4f}".format(meanVal)))+'}{#splitline{#sigma = '+str(float("{0:.4f}".format(stdVal)))+'}{}}',
                                   pos_extraText1=[0.88,0.92],s_extraText1=0.035)
    lumi.saveintoCanvas_and_toFileWIP(solocan_square,plot_hist_byLsW_norm,"HIST",
                                   options.outDir+ "/" + detector1label + "-" + detector2label +"_plot_hist_byLsWstat_norm"+plotformat,"il_sq",year,energy,
                                   extraText1='#sigma = '+str(float("{0:.4f}".format(stdVal))),
                                   pos_extraText1=[0.88,0.92],s_extraText1=0.035)
    lumi.saveintoCanvas_and_toFileWIP(solocan_square,histo_ratio,"HIST",
                                   options.outDir+ "/" + detector1label + "-" + detector2label +"_plot_hist_stat"+plotformat,"il_sq",year,energy,
                                   extraText1='#splitline{Mean = '+str(float("{0:.4f}".format(histo_ratio.GetMean())))+'}{#splitline{#sigma = '+str(float("{0:.4f}".format(histo_ratio.GetStdDev())))+'}{}}',
                                   pos_extraText1=[0.88,0.92],s_extraText1=0.035)
    lumi.saveintoCanvas_and_toFileWIP(solocan_square,plot_hist_byLs,"HIST",
                                   options.outDir+ "/" + detector1label + "-" + detector2label +"_plot_hist_byLsStat"+plotformat,"il_sq",year,energy,
                                   extraText1='#splitline{Mean = '+str(float("{0:.4f}".format(plot_hist_byLs.GetMean())))+'}{#splitline{#sigma = '+str(float("{0:.4f}".format(plot_hist_byLs.GetStdDev())))+'}{}}',
                                   pos_extraText1=[0.88,0.92],s_extraText1=0.035)
    

    if options.corr: 
        lumi.saveintoCanvas_and_toFileWIP(solocan_square,plot_sbil_all,"AP",
                                   outDir_corr+ "/" + detector1label + "-" + detector2label +"_plot_sbil_all"+plotformat,"il_sq",year,energy)
        lumi.saveintoCanvas_and_toFileWIP(solocan_square,plot_sbil_nls,"AP",
                                   outDir_corr+ "/" + detector1label + "-" + detector2label +"_plot_sbil_nls"+plotformat,"il_sq",year,energy)
    lumi.saveintoCanvas_and_toFileWIP(solocan,plot_byNls_vsIntegratedLumi,"AL",
                                   options.outDir+ "/" + detector1label + "-" + detector2label +"_plot_byNls_vsIntegratedLumi"+plotformat,"il_nsq",year,energy)
    lumi.saveintoCanvas_and_toFileWIP_lines(solocan,plot_byNls_vsIntegratedLumi,lines_fills_integratedLumi,"AL",
                                   options.outDir+ "/" + detector1label + "-" + detector2label +"_plot_byNls_vsIntegratedLumi_fillLines"+plotformat,"il_nsq",year,energy)    
    lumi.saveintoCanvas_and_toFileWIP(solocan_square,plot_hist_byLsW,"HIST",
                                   options.outDir+ "/" + detector1label + "-" + detector2label +"_plot_hist_byLsW"+plotformat,"il_sq",year,energy)

    
    if len(excludedFills)!=0:
        plot_hist_allAndExcl,leg_allAndExcl=lumi.plot_compare_All_with_exclude(plot_hist_byLsW,plot_histAll_byLsW,labelRatio,"",labelRatio.upper()+" ratio per "+str(nls)+" LS","Luminosity [fb^{-1}]",colors)
        lumi.saveintoCanvas_and_toFileWIP_withLeg(solocan_square,plot_hist_allAndExcl,leg_allAndExcl,"nostack",
                                   options.outDir+ "/" +detector1label+"_"+detector2label+"_plots_hist_allAndExcl"+plotformat,"il_sq",year,energy)
    
    if options.test:
        outDir_tests=options.outDir+"/tests"
        try:
            os.mkdir(outDir_tests)
        except:
            pass    
        lumi.runTests(dict1,dict2,overlapKeys,labelRatio,detector1label,detector2label,colors,hist_nbins,hist_xmin,hist_xmax,options,year,energy)
    
        
            
        
            
    print ("\n ************************** Info **********************")
    #print ("rms=",plot_hist_byLsW.GetRMS())
    print ("sigma=",plot_hist_byLsW.GetStdDev())
    print ("Excluded fills: ", excludedFills)
    print ("Integrated Luminosity diference |L1-L2|/L2 (%): ",abs(totLumi2-totLumi1)*100/totLumi2)
    if options.lumibin:
        print ("sigma (lumiBin)=",plot_hist_byLumBin.GetStdDev())
        #print ("rms (lumiBin)=",plot_hist_byLumBin.GetRMS())
    
    
#    if options.corr:
#        print ("-------------- After linearity correction (General way) ----------------- ")
#        print ("sigma after correction =",plot_hist_byLsWcorr.GetStdDev())
#        print ("Integrated Luminosity diference |L2-L2corr|/L2 (%): ",abs(corr_totLumi2-totLumi2)*100/totLumi2)         
#        print ("\n ********************* PerFill linearity results (histogram results) *********************************\n")
#        print ("Mean slope: ",perFillLinInfo["meanSlope"])
#        print ("stdv slope: ", perFillLinInfo["stdvSlope"])
#        print ("uncert(%) using mean slope:",perFillLinInfo["unc_mean"])
#        print ("uncert(%) using STDVslope:",perFillLinInfo["unc_stdvSlope"])   
#        print ("uncert(%) using mean slope + err:",perFillLinInfo["unc_mean_plus"])        
#        print ("uncert(%) using mean slope - err:",perFillLinInfo["unc_mean_low"])  
#        print ("uncert(%) using mean slope +- err:",perFillLinInfo["unc_mean_low_plus"])  
#        print ("\n ********************* PerFill linearity results (fitting results) *********************************\n")
#        print ("uncert(%) using per fill slope:",perFillLinInfo["unc_mean_bff"])  
#        print ("uncert(%) using per fill slope + err:",perFillLinInfo["unc_mean_bff_plus"])  
#        print ("uncert(%) using per fill slope - err:",perFillLinInfo["unc_mean_bff_low"],"\n")
##        print ("\n ********************* PerFill linearity results (fitting results) (After rescale) *********************************\n")
##        print ("uncert(%) using per fill slope (rs):",perFillLinInfo["unc_mean_bff_rs"])  
##        print ("uncert(%) using per fill slope + err (rs):",perFillLinInfo["unc_mean_bff_plus_rs"])  
##        print ("uncert(%) using per fill slope - err (rs):",perFillLinInfo["unc_mean_bff_low_rs"],"\n")
#        print ("\n ********************* Full period linearity results (fitting results) *********************************\n")
#        print ("Fitted slope:",perFillLinInfo["allSlope"])  
#        print ("uncert(%) using Fitted slope::",perFillLinInfo["unc_allSlope"])
    
    
    

    ##Matplotlib plots
    print ("-------------- Bad fills and runs info ----------------- ")
    plotmpl_byRun_noemptyPointSLumi=lumi.plotmpl_ratioInDict_vs_VariablePos_pointSize(runDict,idN,totLumi2,"nRuns","ratio",labelRatio+" ratios (TotLumi. %  in point size and colormap)",ymin,ymax,0,0,"",year,energy)
    
    plotmpl_byRun_noemptyPointSLumiFillsInfo=lumi.plotmpl_ratioInDict_vs_VariablePos_pointSizeFillsInfo(runDict,meanVal,stdVal,idN,totLumi2,"nRuns",labelRatio+" ratio(in "+ "1 run" +")",labelRatio+" ratios (TotLumi. %  in point size and colormap)",
                                                                                                        ymin,ymax,0,0,ratioSensivty,lumiSensivty,options.outDir,year=year,energy=energy)
    plotmpl_byRun_noemptyPointSLumiRunsInfo=lumi.plotmpl_ratioInDict_vs_VariablePos_pointSizeRunsInfo(runDict,meanVal,stdVal,idRun,totLumi2,"Runs",labelRatio+" ratio(in "+ "1 run" +")",labelRatio+" ratios (TotLumi. %  in point size and colormap)",
                                                                                                      ymin,ymax,0,0,ratioSensivty,lumiSensivty,options.outDir,year=year,energy=energy)
    plotmpl_byNls_noemptyPointSLumiRunsInfo=lumi.plotmpl_ratioInDict_vs_VariablePos_pointSizeRunsInfo(nlsDict,meanVal,stdVal,idRun,totLumi2,"Runs",labelRatio+" ratio(in "+ str(nls)+"LS" +")",labelRatio+" ratios (TotLumi. %  in point size and colormap)",
                                                                                                      ymin,ymax,0,0,ratioSensivty,lumiSensivty,options.outDir,year=year,energy=energy,ptsLabels=False,reducePtsLabels=True,fileID="byNls")
#    plotmpl_byRun_noemptyLineColorLumi=lumi.plotmpl_ratioInDict_vs_VariablePos_LineColor(runDict,idN,totLumi2,
#                                                                                         "nRuns","ratio",labelRatio+" ratios (TotLumi. %  in point size and colormap)",
#                                                                                         ymin,ymax,0,0)
#    plotmpl_byRun_vsTimePointSLumi=lumi.plotmpl_ratioInDict_vs_VariablePos_pointSize(runDict,idTime,totLumi2,
#                                                                                        "timestamp(s)","ratio",labelRatio+" ratios (TotLumi. %  in point size and colormap)",
#                                                                                        ymin,ymax,time_xmin,time_xmax,"",year)
    if options.lumibin:
        plotmpl_byLumBin_noemptyErrs=lumi.plotmpl_ratioInDict_vs_VariablePos_errs(lumEqDict,idTime,idErrinEqLumDIct,totLumi2,"timestamp(s)","ratio",labelRatio+" Ratio by integrated lum bins of: "+str(float("{0:.3f}".format(totLum12infb)))+" [1/fb]",
                                                                              ymin,ymax,time_xmin,time_xmax)
  

    lumi.save_pyFigToFile(plotmpl_byRun_noemptyPointSLumi,
                          options.outDir+ "/" + detector1label + "-" + detector2label +"_plotmpl_byRun_noemptyPointSLumi"+plotformat)
    lumi.save_pyFigToFile(plotmpl_byRun_noemptyPointSLumiFillsInfo,
                          options.outDir+ "/" + detector1label + "-" + detector2label +"_plotmpl_byRun_noemptyPointSLumiFillsInfo"+plotformat)
    lumi.save_pyFigToFile(plotmpl_byRun_noemptyPointSLumiRunsInfo,
                          options.outDir+ "/" + detector1label + "-" + detector2label +"_plotmpl_byRun_noemptyPointSLumiRunsInfo"+plotformat)
    lumi.save_pyFigToFile(plotmpl_byNls_noemptyPointSLumiRunsInfo,
                          options.outDir+ "/" + detector1label + "-" + detector2label +"_plotmpl_byNls_noemptyPointSLumiRunsInfo"+plotformat)
    
#    lumi.save_pyFigToFile(plotmpl_byRun_noemptyLineColorLumi,
#                          options.outDir+ "/" + detector1label + "-" + detector2label +"_plotmpl_byRun_noemptyLineColorLumi"+plotformat)
#    lumi.save_pyFigToFile(plotmpl_byRun_vsTimePointSLumi,
#                          options.outDir+ "/" + detector1label + "-" + detector2label +"_plotmpl_byRun_vsTimePointSLumi"+plotformat)
    if options.lumibin:
        lumi.save_pyFigToFile(plotmpl_byLumBin_noemptyErrs,
                          options.outDir+ "/" + detector1label + "-" + detector2label +"_plotmpl_byLumBin_noemptyErrs"+plotformat)
    if options.corr:
        plotmpl_byFill_LinSlopesPointSLumi=lumi.plotmpl_SlopeVsLumi_pointSize(fillLinearityInfoDict,setts.slope_plotting_range[year,energy][0],setts.slope_plotting_range[year,energy][1],labelRatio,"",year,energy)
        lumi.save_pyFigToFile(plotmpl_byFill_LinSlopesPointSLumi,
                          outDir_corr+ "/" + detector1label + "-" + detector2label +"_plotmpl_byFill_LinSlopesPointSLumi"+plotformat)
        
        
        
        
    #lumi.saveInfoToFile(detector1label,detector2label,options)
        

if n_files == 3 and options.physics==False and options.all==False: 
    
    print ("******** 3 detector comparison choosed!! ******")
    allcan=ROOT.TCanvas("c", "c", 900, 300)
    solocan_square=ROOT.TCanvas("sqcan2detcs","sqcan2detcs",600,600)
    print ("Detectors: ",args)
        
    detector1label=args[0]
    detector2label=args[1]
    detector3label=args[2]
    
    filename1=detector1label + ".csv"
    filename2=detector2label + ".csv"
    filename3=detector3label + ".csv"
            
    file_path1=options.inDir +"/"+ filename1
    file_path2=options.inDir +"/"+ filename2
    file_path3=options.inDir +"/"+ filename3
    
    print ("input file 1: " + file_path1)
    print ("input file 2: " + file_path2)
    print ("input file 3: " + file_path3)
    
    #open files
    try:
        file1=open(file_path1)
        file2=open(file_path2)
        file3=open(file_path3)
    except:
        print ("input files cant't be opened!!")
        quit()
    
    ##Create ouput files
    options.outDir=options.outDir+"/" + detector1label + "-" + detector2label+ "-" + detector3label
    
    try:
        os.makedirs(options.outDir)
    except:
        pass
    
    detector1label,detector2label=lumi.detcNameModifier(detector1label,detector2label)
    detector2label,detector3label=lumi.detcNameModifier(detector2label,detector3label)
    
    labelratios=[]
    
    labelRatio13=detector1label+"/"+detector3label
    labelRatio23=detector2label+"/"+detector3label
    labelRatio12=detector1label+"/"+detector2label
    
    labelratios.append(labelRatio13)
    labelratios.append(labelRatio23)
    labelratios.append(labelRatio12)
    
    #Create dicts from csv file
    dict1=lumi.myDictFromCSVFileFillRangAndExcl(file1,excludedFills,iniFill,endFill)
    dict2=lumi.myDictFromCSVFileFillRangAndExcl(file2,excludedFills,iniFill,endFill)
    dict3=lumi.myDictFromCSVFileFillRangAndExcl(file3,excludedFills,iniFill,endFill)
    
    file1.close()
    file2.close()
    file3.close()
    
    overlapKeys13=list(set(dict1).intersection(dict3))
    overlapKeys23=list(set(dict2).intersection(dict3))
    overlapKeys=list(set(overlapKeys13).intersection(overlapKeys23))
    overlapKeys.sort()    
    
    #year=lumi.identifyYear(overlapKeys)
    year,energy=lumi.identifyYear_and_Energy(overlapKeys)
    try:
        nls=setts.nls_year[year,energy]
    except:
        print("** Warning **: using default number of ls for integration interval ("+ str(setts.nls) +")")
        nls=setts.nls

    print("\n -------------------- Year in analysis: ",year,energy," TeV --------------------")
    if year!=0 and options.useYearOpts:
        ymin,ymax,minratio,maxratio,nls,hist_xmin,hist_xmax,hist_nbins,nxbins,nybins,nLumBins,nLumRatioBins,lumiPjxBins=lumi.setYearlyOpts(year)
    elif year!=0 and options.useYearOpts==False:
        print ("Automatic setting for specifics years not activated. Add -y to your command if you want to use this option \n")
    print ("\n")
    
    ratiosDict13,ratiosDict23,ratiosDict12=lumi.calc_ratioDict3detect(dict1,dict2,dict3,overlapKeys,minratio,maxratio)
    nlsDict13,nlsDict23,nlsDict12,totLumi1,totLumi2,totLumi3=lumi.calc_ratio3detectNlsDict(dict1,dict2,dict3,overlapKeys,nls)
    runDict13,runDict23,runDict12=lumi.calc_ratio3detectByRunDict(dict1,dict2,dict3,overlapKeys)
    
    if options.corr:
        lin_ratiosDict13=lumi.calc_ratioDict2detect(dict1,dict3,overlapKeys,minratio,maxratio)
        lin_nlsDict13,lin_totLumi113,lin_totLumi213=lumi.calc_ratio2detectNlsDict(dict1,dict2,overlapKeys,nls)
        lin_ratiosDict23=lumi.calc_ratioDict2detect(dict2,dict3,overlapKeys,minratio,maxratio)
        lin_nlsDict23,lin_totLumi123,lin_totLumi223=lumi.calc_ratio2detectNlsDict(dict2,dict3,overlapKeys,nls)
    
    
    plots_byNls_noempty=[]
    plots_byNls_noemptyExcl=[]
    plots_byNls_noempty2=[]
    plots_byNls_vsTime=[]
    plots_byRun_noempty=[]
    plots_byNls_vsIntLumi=[]
    plots_byNls_vsIntLumiExcl=[]
    plots_byRun_vsIntLumi=[]
    
    
    plots_byNls_vsTime.append(lumi.plot_ratioInDict_vs_VariablePos(nlsDict13,idTime,"time","ratio",labelRatio13+" Int. Lumi Ratio by "+str(nls),ymin,ymax,time_xmin,time_xmax))
    plots_byNls_vsTime.append(lumi.plot_ratioInDict_vs_VariablePos(nlsDict23,idTime,"time","ratio",labelRatio23+" Int. Lumi Ratio by "+str(nls),ymin,ymax,time_xmin,time_xmax))
    plots_byNls_vsTime.append(lumi.plot_ratioInDict_vs_VariablePos(nlsDict12,idTime,"time","ratio",labelRatio12+" Int. Lumi Ratio by "+str(nls),ymin,ymax,time_xmin,time_xmax))
    
    plots_byNls_noempty.append(lumi.plot_ratioInDict_vs_VariablePos(nlsDict13,idN,"nNls","ratio",labelRatio13+" Int. Lumi Ratio by "+str(nls),ymin,ymax,time_xmin,time_xmax))
    plots_byNls_noempty.append(lumi.plot_ratioInDict_vs_VariablePos(nlsDict23,idN,"nNls","ratio",labelRatio23+" Int. Lumi Ratio by "+str(nls),ymin,ymax,time_xmin,time_xmax))
    plots_byNls_noempty.append(lumi.plot_ratioInDict_vs_VariablePos(nlsDict12,idN,"nNls","ratio",labelRatio12+" Int. Lumi Ratio by "+str(nls),ymin,ymax,time_xmin,time_xmax))
    
    plots_byNls_noempty2.append(lumi.plot_ratioInDict_vs_VariablePos(nlsDict13,idN,"nNls","ratio",labelRatio13+" Int. Lumi Ratio by "+str(nls),ymin,ymax,time_xmin,time_xmax))
    plots_byNls_noempty2.append(lumi.plot_ratioInDict_vs_VariablePos(nlsDict23,idN,"nNls","ratio",labelRatio23+" Int. Lumi Ratio by "+str(nls),ymin,ymax,time_xmin,time_xmax))
    
    plots_byRun_noempty.append(lumi.plot_ratioInDict_vs_VariablePos(runDict13,idN,"nRun","ratio",labelRatio13+" Int. Lumi Ratio by Run",ymin,ymax,0,0))
    plots_byRun_noempty.append(lumi.plot_ratioInDict_vs_VariablePos(runDict23,idN,"nRun","ratio",labelRatio23+" Int. Lumi Ratio by Run",ymin,ymax,0,0))
    plots_byRun_noempty.append(lumi.plot_ratioInDict_vs_VariablePos(runDict12,idN,"nRun","ratio",labelRatio12+" Int. Lumi Ratio by RUn",ymin,ymax,0,0))
    
    plots_byNls_vsIntLumi.append(lumi.plot_vsIntegratedLumi_fromDictLdict(nlsDict13,nlsDict13,"Integrated Luminosity [fb^{-1}]","ratio","",ymin,ymax))
    plots_byNls_vsIntLumi.append(lumi.plot_vsIntegratedLumi_fromDictLdict(nlsDict23,nlsDict13,"Integrated Luminosity [fb^{-1}]","ratio","",ymin,ymax))
    plots_byNls_vsIntLumi.append(lumi.plot_vsIntegratedLumi_fromDictLdict(nlsDict12,nlsDict13,"Integrated Luminosity [fb^{-1}]","ratio","",ymin,ymax))
    
    plots_byNls_vsIntLumiExcl.append((lumi.plot_vsIntegratedLumi_fromDictLdictExcl(nlsDict13,"Integrated Luminosity [fb^{-1}]","ratio","",ymin,ymax,labelRatio13)))
    plots_byNls_vsIntLumiExcl.append((lumi.plot_vsIntegratedLumi_fromDictLdictExcl(nlsDict23,"Integrated Luminosity [fb^{-1}]","ratio","",ymin,ymax,labelRatio23)))
    plots_byNls_vsIntLumiExcl.append((lumi.plot_vsIntegratedLumi_fromDictLdictExcl(nlsDict12,"Integrated Luminosity [fb^{-1}]","ratio","",ymin,ymax,labelRatio12)))
    
    hsNlsTime,legNlsTime=lumi.plot_compare_multiple_detectorsColorList(plots_byNls_vsTime,labelratios,"","Timestamp"," Int. Lumi Ratio by "+str(nls)+" LS")
    hsNlsNoempty,legNlsNoempty=lumi.plot_compare_multiple_detectorsColorList(plots_byNls_noempty,labelratios,"","n"+str(nls)+" LS"," Int. Lumi Ratio by "+str(nls)+" LS")
    hsNlsNoempty2,legNlsNoempty2=lumi.plot_compare_multiple_detectorsColorList(plots_byNls_noempty2,labelratios,"","n"+str(nls)+" LS"," Int. Lumi Ratio by "+str(nls)+" LS")
    hsRunNoempty,legRunNoempty=lumi.plot_compare_multiple_detectorsColorList(plots_byRun_noempty,labelratios,"","nRun"," Int. Lumi per Run Ratio")
    hsNlsVsIntLumi,legNlsVsIntLumi=lumi.plot_compare_multiple_detectorsColorList(plots_byNls_vsIntLumi,labelratios,"","Integrated Lumi. [fb^{-1}]"," Int. Lumi Ratio by "+str(nls)+" LS")
    hsNlsVsIntLumiExcl,legNlsVsIntLumiExcl=lumi.plot_compare_multiple_detectorsColorList(plots_byNls_vsIntLumiExcl,labelratios,"","Integrated Lumi. [fb^{-1}]"," Int. Lumi Ratio by "+str(nls)+" LS",plotOpt="AP")
    
    if options.corr:
        outDir_corr=options.outDir+"/linearity"
        if options.inNBX!="":
            nBXPerFill=lumi.readNBXperFill(options.inNBX)
        else:
            if year!=0:
                options.inNBX="NBX_files/NBX_"+str(year)+"_perFill.csv"
                nBXPerFill=lumi.readNBXperFill(options.inNBX)
            else:
                print ("Please specify NBX_file for you data to use SBIL related analysis")
                quit()
            
        try:
            os.mkdir(outDir_corr)
        except:
            pass
        
        if year==2016:            
            ##Restrict fitting range 2016
            fit_min=1.7
            fit_max=5.2
        elif year==2015:
            ##Restrict fitting range 2016
            fit_min=1.4
            fit_max=2.35
        else:
            fit_min=0.0
            fit_max=0.0  
            
        perFillLinInfo13,fillLinearityInfoDict13=lumi.perFillLineatiyAnalysis(lin_ratiosDict13,lin_nlsDict13,nBXPerFill,ymin,ymax,labelRatio13,outDir_corr,fit_min,fit_max,year)
        perFillLinInfo23,fillLinearityInfoDict23=lumi.perFillLineatiyAnalysis(lin_ratiosDict23,lin_nlsDict23,nBXPerFill,ymin,ymax,labelRatio23,outDir_corr,fit_min,fit_max,year)
        plot_linearityTwoDects,leg_linearityTwoDects=lumi.plot_linearitySummaryTwoDetects(perFillLinInfo13,perFillLinInfo23,labelRatio13,labelRatio23,outDir_corr)
    
    hsNlsTime.GetYaxis().SetRangeUser(ymin,ymax)
    hsNlsNoempty.GetYaxis().SetRangeUser(ymin,ymax)
    hsNlsNoempty2.GetYaxis().SetRangeUser(ymin,ymax)
    hsRunNoempty.GetYaxis().SetRangeUser(ymin,ymax)
    hsNlsVsIntLumi.GetYaxis().SetRangeUser(ymin,ymax)      
    hsNlsVsIntLumiExcl.GetYaxis().SetRangeUser(ymin,ymax)       
   
    lumi.saveintoCanvas_and_toFileWIP_withLeg(allcan,hsNlsTime,legNlsTime,"APL",
                                   options.outDir+ "/" +detector1label+"_"+detector2label+"_" +detector3label+"_plots_byNls_vsTime"+plotformat,"il_nsq",year,energy)
    lumi.saveintoCanvas_and_toFileWIP_withLeg(allcan,hsNlsNoempty,legNlsNoempty,"APL",
                                   options.outDir+ "/" +detector1label+"_"+detector2label+"_" +detector3label+"_plots_byNls_noempty"+plotformat,"il_nsq",year,energy)
    lumi.saveintoCanvas_and_toFileWIP_withLeg(allcan,hsNlsNoempty2,legNlsNoempty2,"APL",
                                   options.outDir+ "/" +detector1label+"_"+detector2label+"_" +detector3label+"_plots_byNls_noempty2"+plotformat,"il_nsq",year,energy)
    lumi.saveintoCanvas_and_toFileWIP_withLeg(allcan,hsRunNoempty,legRunNoempty,"APL",
                                   options.outDir+ "/" +detector1label+"_"+detector2label+"_" +detector3label+"_plots_byRun_noempty"+plotformat,"il_nsq",year,energy)
    lumi.saveintoCanvas_and_toFileWIP_withLeg(allcan,hsNlsVsIntLumi,legNlsVsIntLumi,"APL",
                                   options.outDir+ "/" +detector1label+"_"+detector2label+"_" +detector3label+"_plots_byNls_vsIntLumi"+plotformat,"il_nsq",year,energy)
    lumi.saveintoCanvas_and_toFileWIP_withLeg(allcan,hsNlsVsIntLumiExcl,legNlsVsIntLumiExcl,"AP",
                                   options.outDir+ "/" +detector1label+"_"+detector2label+"_" +detector3label+"_plots_byNls_vsIntLumiExcl"+plotformat,"il_nsq",year,energy)
    
    if options.corr:
        if year==2016:
            summary_min=-0.006
            summary_max=0.006
        else:
            summary_min=min(perFillLinInfo13["summary_min"],perFillLinInfo23["summary_min"])
            summary_max=max(perFillLinInfo13["summary_max"],perFillLinInfo23["summary_max"])
        lumi.saveintoCanvas_and_toFileWIP(solocan_square,plot_linearityTwoDects,"A",
                                   outDir_corr+"/"+detector1label+"_"+detector2label+"_" +detector3label+"_plot_summary"+plotformat,"il_sq",year,energy,
                                   ymin=summary_min,ymax=summary_max,drawleg=True,leg=leg_linearityTwoDects)

if n_files == 3 and options.physics==False and options.all:
    print ("******** 3 detector comparison including all data choosed!! ******")
    allcan=ROOT.TCanvas("c", "c", 900, 300)
    solocan_square=ROOT.TCanvas("sqcan2detcs","sqcan2detcs",600,600)
    print ("Detectors: ",args)
        
    detector1label=args[0]
    detector2label=args[1]
    detector3label=args[2]
    
    filename1=detector1label + ".csv"
    filename2=detector2label + ".csv"
    filename3=detector3label + ".csv"
    
    filename1all=detector1label + "_all.csv"
    filename2all=detector2label + "_all.csv"
    filename3all=detector3label + "_all.csv"
        
    file_path1=options.inDir +"/"+ filename1
    file_path2=options.inDir +"/"+ filename2
    file_path3=options.inDir +"/"+ filename3
    file_path1all=options.inDir +"/"+ filename1all
    file_path2all=options.inDir +"/"+ filename2all
    file_path3all=options.inDir +"/"+ filename3all
    
    
    print ("input file 1: " + file_path1)
    print ("input file 2: " + file_path2)
    print ("input file 3: " + file_path3)
    print ("input file 1_all: " + file_path1all)
    print ("input file 2_all: " + file_path2all)
    print ("input file 3_all: " + file_path3all)
    
    
    #open files
    try:
        file1=open(file_path1)
        file2=open(file_path2)
        file3=open(file_path3)
    except:
        print ("input files cant't be opened!!")
        quit()
    try:
        file1all=open(file_path1all)
        file2all=open(file_path2all)
        file3all=open(file_path3all)
    except:
        print ("some of the *_all.csv files cant't be opened!!")
        quit()
        
    ##Create ouput files
    options.outDir=options.outDir+"/" + detector1label + "-" + detector2label+ "-" + detector3label
    try:
        os.makedirs(options.outDir)
    except:
        pass
    
    labelratios=[]
    
    labelRatio13=detector1label+"/"+detector3label
    labelRatio23=detector2label+"/"+detector3label
    labelRatio12=detector1label+"/"+detector2label
    
    labelratios.append(labelRatio13)
    labelratios.append(labelRatio23)
    labelratios.append(labelRatio12)
    
    dict1=lumi.myDictFromCSVFileFillRangAndExcl(file1,excludedFills,iniFill,endFill)
    dict2=lumi.myDictFromCSVFileFillRangAndExcl(file2,excludedFills,iniFill,endFill)
    dict3=lumi.myDictFromCSVFileFillRangAndExcl(file3,excludedFills,iniFill,endFill)
    
    dict1all=lumi.myDictFromCSVFileFillRangAndExcl(file1all,excludedFills,iniFill,endFill)
    dict2all=lumi.myDictFromCSVFileFillRangAndExcl(file2all,excludedFills,iniFill,endFill)
    dict3all=lumi.myDictFromCSVFileFillRangAndExcl(file3all,excludedFills,iniFill,endFill)                    
    
    file1.close()
    file2.close()
    file3.close()
    file1all.close()
    file2all.close()
    file3all.close()
    
    overlapKeys13=list(set(dict1).intersection(dict3))
    overlapKeys23=list(set(dict2).intersection(dict3))
    overlapKeys=list(set(overlapKeys13).intersection(overlapKeys23))
    overlapKeys.sort()  
    
    if len(overlapKeys)==0:
        print ("Problem reading the files! Keys lenght = 0!!")
        exit()
    
    year,energy=lumi.identifyYear_and_Energy(overlapKeys)
    print("\n -------------------- Year in analysis: ",year,energy," TeV --------------------")
    try:
        nls=setts.nls_year[year,energy]
    except:
        print("** Warning **: using default number of ls for integration interval ("+ str(setts.nls) +")")
        nls=setts.nls
    keys1all=list(dict1all)
    keys2all=list(dict2all)
    keys3all=list(dict3all)
    
    
    #nls=5
    
    
    allkeys=[]
    largerDictID=0
    if (keys1all>=keys2all and keys1all>=keys3all):
        allkeys=keys1all
        biggerDict=dict1all
        largerDictID=1
        print("Biggest detector dataset: " + detector1label)
    elif (keys2all>=keys1all and keys2all>=keys3all):
        allkeys=keys2all
        biggerDict=dict2all
        largerDictID=2
        print("Biggest detector dataset: " + detector2label)
    elif (keys3all>=keys1all and keys3all>=keys2all):
        allkeys=keys3all
        biggerDict=dict3all
        largerDictID=3
        print("Biggest detector dataset: " + detector3label)
    if len(allkeys)==0:
        print ("problem finding the detector with more data!")
        exit()
    allkeys.sort()
    
    nlsDict12,nlsDict12all,totLumi1,totLumi2,totLumi1all,totLumi2all,totLumi3all=lumi.calc_ratio2detectNlsDictWithAllData(dict1,dict2,dict1all,dict2all,allkeys,biggerDict,nls)
    nlsDict23,nlsDict23all,totLumi2,totLumi3,totLumi2all,totLumi3all,totLumi3all=lumi.calc_ratio2detectNlsDictWithAllData(dict2,dict3,dict2all,dict3all,allkeys,biggerDict,nls)
    nlsDict13,nlsDict13all,totLumi1,totLumi3,totLumi1all,totLumi3all,totLumi3all=lumi.calc_ratio2detectNlsDictWithAllData(dict1,dict3,dict1all,dict3all,allkeys,biggerDict,nls) 

    ratioDict12,ratioDict12all,totLumi1,totLumi2,totLumi1all,totLumi2all,totLumi3all=lumi.calc_ratio2detectDictWithAllData(dict1,dict2,dict1all,dict2all,allkeys,biggerDict)
    ratioDict23,ratioDict23all,totLumi2,totLumi3,totLumi2all,totLumi3all,totLumi3all=lumi.calc_ratio2detectDictWithAllData(dict2,dict3,dict2all,dict3all,allkeys,biggerDict)
    ratioDict13,ratioDict13all,totLumi1,totLumi3,totLumi1all,totLumi3all,totLumi3all=lumi.calc_ratio2detectDictWithAllData(dict1,dict3,dict1all,dict3all,allkeys,biggerDict)                                                                                        
    
    plots_byNls_vsIntLumi=[]
    plots_byNls_vsIntLumiAll=[]
    plots_byNls_vsIntLumiExcl=[]
    plots_byNls_vsIntLumiExclOnly2=[]
    
    plots_ratio_vsIntLumi=[]
    plots_ratio_vsIntLumiAll=[]
    plots_ratio_vsIntLumiExcl=[]
    
    ## Select bigger detc1/detc2 dict
    lnls12=len(list(nlsDict12all))
    lnls23=len(list(nlsDict23all))
    lnls13=len(list(nlsDict13all))
    
    if lnls12>=lnls23 and lnls12>=lnls13:
        biggerNlsDict=nlsDict12all
        print("Biggest nlsRatioDict dataset: 12")
    elif lnls23>=lnls12 and lnls23>=lnls13:
        biggerNlsDict=nlsDict23all
        print("Biggest nlsRatioDict dataset: 23")
    elif lnls13>=lnls12 and lnls13>=lnls23:
        biggerNlsDict=nlsDict13all
        print("Biggest nlsRatioDict dataset: 13")
    else:
        print ("problem finding the dictionarie with more data!")
        exit()
        
       
    
    plots_byNls_vsIntLumi.append(lumi.plot_vsIntegratedLumi_fromDictLdict(nlsDict13,biggerNlsDict,"Integrated Luminosity [fb^{-1}]","ratio","",ymin,ymax))
    plots_byNls_vsIntLumi.append(lumi.plot_vsIntegratedLumi_fromDictLdict(nlsDict23,biggerNlsDict,"Integrated Luminosity [fb^{-1}]","ratio","",ymin,ymax))
    plots_byNls_vsIntLumi.append(lumi.plot_vsIntegratedLumi_fromDictLdict(nlsDict12,biggerNlsDict,"Integrated Luminosity [fb^{-1}]","ratio","",ymin,ymax))
    
    plots_byNls_vsIntLumiAll.append(lumi.plot_vsIntegratedLumi_fromDictLdict(nlsDict13all,biggerNlsDict,"Integrated Luminosity [fb^{-1}]","ratio","",ymin,ymax))
    plots_byNls_vsIntLumiAll.append(lumi.plot_vsIntegratedLumi_fromDictLdict(nlsDict23all,biggerNlsDict,"Integrated Luminosity [fb^{-1}]","ratio","",ymin,ymax))
    plots_byNls_vsIntLumiAll.append(lumi.plot_vsIntegratedLumi_fromDictLdict(nlsDict12all,biggerNlsDict,"Integrated Luminosity [fb^{-1}]","ratio","",ymin,ymax))
    
    plots_byNls_vsIntLumiExcl.append(lumi.plotExcl_vsIntegratedLumi_fromDictLdict(nlsDict13,nlsDict13all,biggerNlsDict,"Integrated Luminosity [fb^{-1}]","ratio","",ymin,ymax))
    plots_byNls_vsIntLumiExcl.append(lumi.plotExcl_vsIntegratedLumi_fromDictLdict(nlsDict23,nlsDict23all,biggerNlsDict,"Integrated Luminosity [fb^{-1}]","ratio","",ymin,ymax))
    plots_byNls_vsIntLumiExcl.append(lumi.plotExcl_vsIntegratedLumi_fromDictLdict(nlsDict12,nlsDict12all,biggerNlsDict,"Integrated Luminosity [fb^{-1}]","ratio","",ymin,ymax))
    
    
#    print ("step 1")
#    plots_ratio_vsIntLumi.append(lumi.plot_vsIntegratedLumi_fromDictLdict(ratioDict13,ratioDict13all,"Integrated Luminosity [fb^{-1}]","ratio","",ymin,ymax))
#    plots_ratio_vsIntLumi.append(lumi.plot_vsIntegratedLumi_fromDictLdict(ratioDict23,ratioDict13all,"Integrated Luminosity [fb^{-1}]","ratio","",ymin,ymax))
#    plots_ratio_vsIntLumi.append(lumi.plot_vsIntegratedLumi_fromDictLdict(ratioDict12,ratioDict13all,"Integrated Luminosity [fb^{-1}]","ratio","",ymin,ymax))
#    
#    print ("step 2")
#    plots_ratio_vsIntLumiAll.append(lumi.plot_vsIntegratedLumi_fromDictLdict(ratioDict13all,ratioDict13all,"Integrated Luminosity [fb^{-1}]","ratio","",ymin,ymax))
#    plots_ratio_vsIntLumiAll.append(lumi.plot_vsIntegratedLumi_fromDictLdict(ratioDict23all,ratioDict13all,"Integrated Luminosity [fb^{-1}]","ratio","",ymin,ymax))
#    plots_ratio_vsIntLumiAll.append(lumi.plot_vsIntegratedLumi_fromDictLdict(ratioDict12all,ratioDict13all,"Integrated Luminosity [fb^{-1}]","ratio","",ymin,ymax))
#    
#    print ("step 3")
#    plots_ratio_vsIntLumiExcl.append(lumi.plotExcl_vsIntegratedLumi_fromDictLdict(ratioDict13,ratioDict13all,ratioDict13all,"Integrated Luminosity [fb^{-1}]","ratio","",ymin,ymax))
#    plots_ratio_vsIntLumiExcl.append(lumi.plotExcl_vsIntegratedLumi_fromDictLdict(ratioDict23,ratioDict23all,ratioDict13all,"Integrated Luminosity [fb^{-1}]","ratio","",ymin,ymax))
#    plots_ratio_vsIntLumiExcl.append(lumi.plotExcl_vsIntegratedLumi_fromDictLdict(ratioDict12,ratioDict12all,ratioDict13all,"Integrated Luminosity [fb^{-1}]","ratio","",ymin,ymax))

#    hsNlsVsIntLumiAllExcl,legNlsVsIntLumiAllExcl=lumi.plot_compareInclExcl_multiple_detectorsColorList(plots_byNls_vsIntLumi,plots_byNls_vsIntLumiExcl,labelratios,"","Integrated Lumi. [fb^{-1}]"," Int. Lumi Ratio by "+str(nls)+" LS")
#    hsNlsVsIntLumiAllExcl.GetYaxis().SetRangeUser(ymin,ymax)
#    lumi.saveintoCanvas_and_toFileWIP_withLeg(allcan,hsNlsVsIntLumiAllExcl,legNlsVsIntLumiAllExcl,"APL",
#                                   options.outDir+ "/" +detector1label+"_"+detector2label+"_" +detector3label+"_plotsAll_byNls_vsIntLumiAllExcl"+plotformat,"il_nsq",year,energy)
    print ("plotting")
    hsNlsVsIntLumi,legNlsVsIntLumi=lumi.plot_compare_multiple_detectorsColorList(plots_byNls_vsIntLumi,labelratios,"","Integrated Lumi. [fb^{-1}]"," Int. Lumi Ratio by "+str(nls)+" LS",plotOpt="P",markerS=0.3,legOpt="l")
    hsNlsVsIntLumi.GetYaxis().SetRangeUser(ymin,ymax) 
    
    lumi.saveintoCanvas_and_toFileWIP_withLeg(allcan,hsNlsVsIntLumi,legNlsVsIntLumi,"AP",
                                   options.outDir+ "/" +detector1label+"_"+detector2label+"_" +detector3label+"_plots_byNls_vsIntLumi_only_good"+plotformat,"il_nsq",year,energy)
    
    stylePlotId=0
    hsNlsVsIntLumiAllExcl_redExcl,legNlsVsIntLumiAllExcl_redExcl=lumi.plot_compareInclExcl_multiple_detectorsColorList(plots_byNls_vsIntLumi,plots_byNls_vsIntLumiExcl,labelratios,"","Integrated Lumi. [fb^{-1}]"," Int. Lumi Ratio by "+str(nls)+" LS",plotStyle=stylePlotId)
    hsNlsVsIntLumiAllExcl_redExcl.GetYaxis().SetRangeUser(ymin,ymax)
    
    lumi.saveintoCanvas_and_toFileWIP_withLeg(allcan,hsNlsVsIntLumiAllExcl_redExcl,legNlsVsIntLumiAllExcl_redExcl,"APL",
                                   options.outDir+ "/" +detector1label+"_"+detector2label+"_" +detector3label+"_plotsAll_byNls_vsIntLumiAllExcl_"+str(stylePlotId)+plotformat,"il_nsq",year,energy)
    
    stylePlotId=3
    hsNlsVsIntLumiAllExcl_lowTone,legNlsVsIntLumiAllExcl_lowTone=lumi.plot_compareInclExcl_multiple_detectorsColorList(plots_byNls_vsIntLumi,plots_byNls_vsIntLumiExcl,labelratios,"","Integrated Lumi. [fb^{-1}]"," Int. Lumi Ratio by "+str(nls)+" LS",plotStyle=stylePlotId)
    hsNlsVsIntLumiAllExcl_lowTone.GetYaxis().SetRangeUser(ymin,ymax)
    
    lumi.saveintoCanvas_and_toFileWIP_withLeg(allcan,hsNlsVsIntLumiAllExcl_lowTone,legNlsVsIntLumiAllExcl_lowTone,"APL",
                                   options.outDir+ "/" +detector1label+"_"+detector2label+"_" +detector3label+"_plotsAll_byNls_vsIntLumiAllExcl_"+str(stylePlotId)+plotformat,"il_nsq",year,energy)
   
    
#    stylePlotId=1
#    hsratioVsIntLumiAllExcl_lowTone,legratioVsIntLumiAllExcl_lowTone=lumi.plot_compareInclExcl_multiple_detectorsColorList(plots_ratio_vsIntLumi,plots_ratio_vsIntLumiExcl,labelratios,"","Integrated Lumi. [fb^{-1}]"," Int. Lumi Ratio by "+str(nls)+" LS",plotStyle=stylePlotId)
#    hsratioVsIntLumiAllExcl_lowTone.GetYaxis().SetRangeUser(ymin,ymax)      
#    lumi.saveintoCanvas_and_toFileWIP_withLeg(allcan,hsratioVsIntLumiAllExcl_lowTone,legratioVsIntLumiAllExcl_lowTone,"APL",
#                                   options.outDir+ "/" +detector1label+"_"+detector2label+"_" +detector3label+"_plotsAll_ratio_vsIntLumiAllExcl_"+str(stylePlotId)+plotformat,"il_nsq",year,energy)
#    
    
    
    
    
    