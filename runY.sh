#!/bin/bash
options_ini=$2
options=$(echo $options_ini | sed "s/,/ /g")

python compare.py $options -i $1/goldenJson/csvFiles -o $1/goldenJson/Plots $3 $4 $5
