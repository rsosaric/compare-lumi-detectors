#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  1 15:54:14 2019

@author: rsosaric
"""

import optparse
import json


p = optparse.OptionParser()
usage = "usage: %prog [options] arg1 arg2 ..."
p.add_option("-o", "--outfile", type="string",help="output file name", dest="outFile", default="excluded.json")
(options, args) = p.parse_args()

normtagfilename=args[0]
runsList=[]
for run in args[1].split(","):
    runsList.append(int(run))
    
    
print(normtagfilename,runsList)

print ("reading "+ normtagfilename)
ntfile=open(normtagfilename)
file_json=json.load(ntfile)
filde_json_excl=[]

for line in file_json:
    run=line[1].keys()[0]
    if (int(run) in runsList):
        print ("run "+ str(run) + " found and excluded!")
    else:
        filde_json_excl.append(line)

outputFile=open(options.outFile,"w")
outputFile.write("[\n")

for line in filde_json_excl:
    outputFile.write("[\""+str(line[0])+"\",{\""+str(line[1].keys()[0])+"\":"+str(line[1][line[1].keys()[0]])+"}]")
    if line != filde_json_excl[-1]:
        outputFile.write(",\n")
outputFile.write("\n]\n")
outputFile.close()