#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 19 16:05:01 2018

@author: sosarica
"""

import ROOT
import optparse
#from rootpy.plotting import Hist, HistStack, Legend, Canvas
#import rootpy.plotting.root2matplotlib as rplt

#By me
import lumi_compare as lumi
#import math

ROOT.gROOT.SetBatch(ROOT.kTRUE)

excludedFills=[]
nbins=40
min_val=0.5
max_val=1.5
deltaD=0.08


p = optparse.OptionParser()
p.add_option("-o", "--outdir", type="string",help="Path to output Dir", dest="outDir", default=".")

(options, args) = p.parse_args()

vdmFileLabel=args[0]

try:
    filein=open(vdmFileLabel)
except:
    print("input file error")
    quit()
    
timelist,separationlist,lslist=lumi.headOnSelctionFromCSVvdmFile(filein,deltaD)

lumi.saveListInFile(timelist,options.outDir+ "/" +"timestampsHO.csv")

plot_separationVsTime=ROOT.TGraph()

iBin=0
for item in separationlist:
    plot_separationVsTime.SetPoint(iBin,item[0],item[1])
    iBin+=1

can=ROOT.TCanvas("solocan","solocan",900,300)

lumi.saveintoCanvas_and_toFile(can,plot_separationVsTime,"ALP",
                                   options.outDir+ "/" + "_plot_separationVsTime.pdf")