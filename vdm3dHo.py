#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 19 11:49:59 2018

@author: sosarica
"""

import ROOT
import optparse
#from rootpy.plotting import Hist, HistStack, Legend, Canvas
#import rootpy.plotting.root2matplotlib as rplt

#By me
import lumi_compare as lumi
#import math
colors=[46, 8, 9, 42, 38, 30, 2, 3, 4, 5, 6, 7, 8]

ROOT.gROOT.SetBatch(ROOT.kTRUE)
lumi.setRESRStyle()

excludedFills=[]
nbins=40
min_val=0.5
max_val=1.5
timestampSelName="timestampsHO.csv"

p = optparse.OptionParser()
usage = "usage: %prog [options] arg1 arg2 ..."
p.add_option("-o", "--outdir", type="string",help="Path to output Dir", dest="outDir", default=".")
p.add_option("-i", "--indir", type="string",help="Path to input Dir", dest="inDir", default=".")

(options, args) = p.parse_args()

detector1label=args[0]
detector2label=args[1]
detector3label=args[2]
    
filename1=detector1label + ".csv"
filename2=detector2label + ".csv"
filename3=detector3label + ".csv"

file_path1=options.inDir +"/"+ filename1
file_path2=options.inDir +"/"+ filename2
file_path3=options.inDir +"/"+ filename3
vdmHOTimeStampLabel=options.inDir +"/"+timestampSelName


print ("input file 1: " + file_path1)
print ("input file 2: " + file_path2)
print ("input file 3: " + file_path3)
  
#open files
try:
    file1=open(file_path1)
    file2=open(file_path2)
    file3=open(file_path3)
except:
    print ("input files cant't be opened!!")
    quit()

timestampList=lumi.get1colFileInList(vdmHOTimeStampLabel,"int")
    
labelRatio13=detector1label+"/"+detector3label
labelRatio23=detector2label+"/"+detector3label
labelRatio12=detector1label+"/"+detector2label

labeldetc=[]
labeldetc.append(detector1label)
labeldetc.append(detector2label)
labeldetc.append(detector3label)

labelratios=[]
labelratios.append(labelRatio13)
labelratios.append(labelRatio23)
labelratios.append(labelRatio12)

    
#info

#Create dicts from csv file
dict1=lumi.myDictFromVdmCSVFile(file1)
dict2=lumi.myDictFromVdmCSVFile(file2)
dict3=lumi.myDictFromVdmCSVFile(file3)

file1.close()
file2.close()
file3.close()


#Select only the common lumi sections for both detectors
overlapKeys13=list(set(dict1).intersection(dict3))
overlapKeys23=list(set(dict2).intersection(dict3))
overlapKeys=list(set(overlapKeys13).intersection(overlapKeys23))


#Sorting to evoid disorder in plots
overlapKeys.sort()


plotVsTime1=ROOT.TGraph()
plotVsTime2=ROOT.TGraph()
plotVsTime3=ROOT.TGraph()
plotsVsTime=[]

ratio_plotVsTime13=ROOT.TGraph()
ratio_plotVsTime23=ROOT.TGraph()
ratio_plotVsTime12=ROOT.TGraph()
ratio_plotsVsTime=[]

ratioHist13=ROOT.TH1F(labelRatio13+"ratioHist",";ratio "+labelRatio13+";",nbins,min_val,max_val)
ratioHist23=ROOT.TH1F(labelRatio23+"ratioHist",";ratio "+labelRatio23+";",nbins,min_val,max_val)
ratioHist12=ROOT.TH1F(labelRatio12+"ratioHist",";ratio "+labelRatio12+";",nbins,min_val,max_val)
ratioHists=[]

ho_ratioHist13=ROOT.TH1F(labelRatio13+"ho_ratioHist",";ratio "+labelRatio13+";",nbins,min_val,max_val)
ho_ratioHist23=ROOT.TH1F(labelRatio23+"ho_ratioHist",";ratio "+labelRatio23+";",nbins,min_val,max_val)
ho_ratioHist12=ROOT.TH1F(labelRatio12+"ho_ratioHist",";ratio "+labelRatio12+";",nbins,min_val,max_val)
ho_ratioHists=[]

ho_ratio_plotVsTime13=ROOT.TGraph()
ho_ratio_plotVsTime23=ROOT.TGraph()
ho_ratio_plotVsTime12=ROOT.TGraph()
ho_ratio_plotsVsTime=[]

ho_plotVsTime1=ROOT.TGraph()
ho_plotVsTime2=ROOT.TGraph()
ho_plotVsTime3=ROOT.TGraph()
ho_plotsVsTime=[]


##Filling plots with data
iBin=0
iBinx=0
iBinho=0
for key in overlapKeys:
    plotVsTime1.SetPoint(iBinx,dict1[key][0],dict1[key][1])
    plotVsTime2.SetPoint(iBinx,dict2[key][0],dict2[key][1])
    plotVsTime3.SetPoint(iBinx,dict3[key][0],dict3[key][1])
    iBinx+=1
    try:
        if dict1[key][4]=='STABLE BEAMS':
            div13=dict1[key][1]/dict3[key][1]
            div23=dict2[key][1]/dict3[key][1]
            div12=dict1[key][1]/dict2[key][1]
            
            ratioHist13.Fill(div13)
            ratioHist23.Fill(div23)
            ratioHist12.Fill(div12)
            
            ratio_plotVsTime13.SetPoint(iBin,dict1[key][0],div13)
            ratio_plotVsTime23.SetPoint(iBin,dict2[key][0],div23)
            ratio_plotVsTime12.SetPoint(iBin,dict3[key][0],div12)
            
            if dict1[key][0] in timestampList:
                ho_ratioHist13.Fill(div13)
                ho_ratioHist23.Fill(div23)
                ho_ratioHist12.Fill(div12)
                
                ho_plotVsTime1.SetPoint(iBinho,dict1[key][0],dict1[key][1])
                ho_plotVsTime2.SetPoint(iBinho,dict2[key][0],dict2[key][1])
                ho_plotVsTime3.SetPoint(iBinho,dict3[key][0],dict3[key][1])
                
                ho_ratio_plotVsTime13.SetPoint(iBinho,dict1[key][0],div13)
                ho_ratio_plotVsTime23.SetPoint(iBinho,dict2[key][0],div23)
                ho_ratio_plotVsTime12.SetPoint(iBinho,dict3[key][0],div12)
                
                iBinho+=1
            
            iBin+=1
    except:
        continue

##Filling multiplots
plotsVsTime.append(plotVsTime1)
plotsVsTime.append(plotVsTime2)
plotsVsTime.append(plotVsTime3)
ratio_plotsVsTime.append(ratio_plotVsTime13)
ratio_plotsVsTime.append(ratio_plotVsTime23)
ratio_plotsVsTime.append(ratio_plotVsTime12)
ho_ratio_plotsVsTime.append(ho_ratio_plotVsTime13)
ho_ratio_plotsVsTime.append(ho_ratio_plotVsTime23)
ho_ratio_plotsVsTime.append(ho_ratio_plotVsTime12)

ratioHists.append(ratioHist13)
ratioHists.append(ratioHist23)
ratioHists.append(ratioHist12)
ho_ratioHists.append(ho_ratioHist13)
ho_ratioHists.append(ho_ratioHist23)
ho_ratioHists.append(ho_ratioHist12)
ho_plotsVsTime.append(ho_plotVsTime1)
ho_plotsVsTime.append(ho_plotVsTime2)
ho_plotsVsTime.append(ho_plotVsTime3)


##Saving plots
solocan=ROOT.TCanvas("solocan","solocan",900,300)
solocan_square=ROOT.TCanvas("sqcan","sqcan",600,600)

hsVsTime,legVsTime=lumi.plot_compare_multiple_detectorsOptColorList(plotsVsTime,labeldetc,"Instantaneous luminosity vs Time","timestamp","Instantaneous luminosity","AL",colors)
hsVsTime.GetYaxis().SetRangeUser(0., 4.)
hs_ratioVsTime,leg_ratioVsTime=lumi.plot_compare_multiple_detectorsOptColorList(ratio_plotsVsTime,labelratios,"Instantaneous luminosity ratio vs Time","timestamp","Inst. lum. detector ratio","AP",colors)
hs_ratioVsTime.GetYaxis().SetRangeUser(0., 1.5)
hs_ho_ratioVsTime,leg_ho_ratioVsTime=lumi.plot_compare_multiple_detectorsOptColorList(ho_ratio_plotsVsTime,labelratios,"Instantaneous luminosity ratio vs Time (head-on collisions only) ","timestamp","Inst. lum. detector ratio","AP",colors)
hs_ho_ratioVsTime.GetYaxis().SetRangeUser(0.8, 1.2)

hs_ho_VsTime,ho_legVsTime=lumi.plot_compare_multiple_detectorsOpts(ho_plotsVsTime,"Instantaneous luminosity detector ratio (head-on collisions only)",labeldetc,"timestamp","Instantaneous luminosity detector ratio","APL")
hs_ho_VsTime.GetYaxis().SetRangeUser(0., 4.)


hsRatioHist,legRatioHist=lumi.histo_ratio_multiple_detectors(ratioHists,labelratios)
hs_ho_RatioHist,ho_legRatioHist=lumi.histo_ratio_multiple_detectors(ho_ratioHists,labelratios)
#hsRatioHist.GetXaxis().SetRangeUser(min_val, max_val)


lumi.saveintoCanvas_and_toFileWIP_withLeg(solocan,hsVsTime,legVsTime,"AL",
                                   options.outDir+ "/" +detector1label+"_"+detector2label+"_" +detector3label+"_plotsVsTime.pdf","wip_il_nsq")
lumi.saveintoCanvas_and_toFileWIP_withLeg(solocan,hs_ratioVsTime,leg_ratioVsTime,"AL",
                                   options.outDir+ "/" +detector1label+"_"+detector2label+"_" +detector3label+"_plots_ratioVsTime.pdf","wip_il_nsq")
lumi.saveintoCanvas_and_toFileWIP_withLeg(solocan,hs_ho_ratioVsTime,leg_ho_ratioVsTime,"AL",
                                   options.outDir+ "/" +detector1label+"_"+detector2label+"_" +detector3label+"_ho_plots_ratioVsTime.pdf","wip_il_nsq")
lumi.saveintoCanvas_and_toFileWIP_withLeg(solocan,hs_ho_VsTime,ho_legVsTime,"APL",
                                   options.outDir+ "/" +detector1label+"_"+detector2label+"_" +detector3label+"_ho_plotsVsTime.pdf","wip_il_nsq")

lumi.saveintoCanvas_and_toFileWIP_withLeg(solocan_square,hsRatioHist,legRatioHist,"nostack",
                                   options.outDir+ "/" +detector1label+"_"+detector2label+"_" +detector3label+"_plotsHistos.pdf","wip_il_sq")
lumi.saveintoCanvas_and_toFileWIP_withLeg(solocan_square,hs_ho_RatioHist,ho_legRatioHist,"nostack",
                                   options.outDir+ "/" +detector1label+"_"+detector2label+"_" +detector3label+"_ho_plotsHistos.pdf","wip_il_sq")

lumi.saveintoCanvas_and_toFileWIP_withLeg(solocan,hsVsTime,legVsTime,"AL",
                                   options.outDir+ "/" +detector1label+"_"+detector2label+"_" +detector3label+"_plotsVsTime.png","wip_il_nsq")
lumi.saveintoCanvas_and_toFileWIP_withLeg(solocan,hs_ratioVsTime,leg_ratioVsTime,"AL",
                                   options.outDir+ "/" +detector1label+"_"+detector2label+"_" +detector3label+"_plots_ratioVsTime.png","wip_il_nsq")
lumi.saveintoCanvas_and_toFileWIP_withLeg(solocan,hs_ho_ratioVsTime,leg_ho_ratioVsTime,"AL",
                                   options.outDir+ "/" +detector1label+"_"+detector2label+"_" +detector3label+"_ho_plots_ratioVsTime.png","wip_il_nsq")
lumi.saveintoCanvas_and_toFileWIP_withLeg(solocan,hs_ho_VsTime,ho_legVsTime,"APL",
                                   options.outDir+ "/" +detector1label+"_"+detector2label+"_" +detector3label+"_ho_plotsVsTime.png","wip_il_nsq")

lumi.saveintoCanvas_and_toFileWIP_withLeg(solocan_square,hsRatioHist,legRatioHist,"nostack",
                                   options.outDir+ "/" +detector1label+"_"+detector2label+"_" +detector3label+"_plotsHistos.png","wip_il_sq")
lumi.saveintoCanvas_and_toFileWIP_withLeg(solocan_square,hs_ho_RatioHist,ho_legRatioHist,"nostack",
                                   options.outDir+ "/" +detector1label+"_"+detector2label+"_" +detector3label+"_ho_plotsHistos.png","wip_il_sq")


