#!/bin/bash
echo "running hfoc pcc"
python compare.py -c -i 2016/goldenJson/csvFiles/ -o 2016/goldenJson/Plots/ hfoc pcc

echo "running plt pcc"
python compare.py -c -i 2016/goldenJson/csvFiles/ -o 2016/goldenJson/Plots/ plt pcc

echo "running dt pcc"
python compare.py -c -i 2016/goldenJson/csvFiles/ -o 2016/goldenJson/Plots/ dt pcc

echo "running ram pcc"
python compare.py -c -i 2016/goldenJson/csvFiles/ -o 2016/goldenJson/Plots/ ram pcc

echo "running dt hfoc pcc"
python compare.py -c -i 2016/goldenJson/csvFiles/ -o 2016/goldenJson/Plots/ dt hfoc pcc

echo "running plt hfoc pcc"
python compare.py -c -i 2016/goldenJson/csvFiles/ -o 2016/goldenJson/Plots/ plt hfoc pcc

echo "running ram hfoc pcc"
python compare.py -c -i 2016/goldenJson/csvFiles/ -o 2016/goldenJson/Plots/ ram hfoc pcc

echo "running dt ram pcc"
python compare.py -c -i 2016/goldenJson/csvFiles/ -o 2016/goldenJson/Plots/ dt ram pcc
