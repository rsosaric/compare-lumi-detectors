# -*- coding: utf-8 -*-
"""
Created on Wed May 30 12:21:00 2018

@author: Rafael E. Sosa Ricardo, CMS DESY rafael.sosa.ricardo@desy.de
"""


#import sys
import ROOT
import optparse
import lumi_compare as lumi

#from rootpy.plotting import Hist, HistStack, Legend, Canvas
#import rootpy.plotting.root2matplotlib as rplt

#By me

ROOT.gROOT.SetBatch(ROOT.kTRUE)

##Some important definitions
excludedFills=[5038,5043,5045,5048,5052,5056,4979,5151,5331,5433]

lumi.setRESRStyle()

p = optparse.OptionParser()
usage = "usage: %prog [options] arg1 arg2"
p.add_option("-o", "--outdir", type="string",help="Path to output Dir", dest="outDir", default=".")
#example of use: python certifiedPlotsCompare.py -o 2018/goldenJson/Plots 2018/goldenJson/csvFiles/physics.csv 2018/goldenJson/csvFiles/physics_compare.csv

(options, args) = p.parse_args()

n_files=len(args)

if n_files==2:
    file_path1=args[0]
    file_path2=args[1]
    print ("input file: " + file_path1)
    print ("input file: " + file_path2)
    try:
        file1=open(file_path1)
        file2=open(file_path2)
    except:
        print ("input file cant't be opened!!")
        quit()
    
    
    dictphys,labels=lumi.myDictFromCSVFileReadAll(file1)
    dictcompare,labels=lumi.myDictFromCSVFileReadAll(file2)
            
    overlapKeys=list(set(dictphys).intersection(dictcompare))
    overlapKeys.sort()
    year,energy=lumi.identifyYear_and_Energy(overlapKeys)
    print("\n -------------------- Year in analysis: "+str(year)+"("+ str(energy)+"TeV) --------------------")
    
    #lumi.physicsFileAnalisys(dictphys,labels,excludedFills,options.outDir)
    lumi.physicsFileAnalisysCompare(dictphys,dictcompare,labels,options.outDir,year,energy)
    
    
    
    
else:
    print ("Only 1 file entry allowed, you have entered "+str(n_files)+" files")