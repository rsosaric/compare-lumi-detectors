#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 24 12:17:50 2018

@author: Rafael E. Sosa Ricardo, CMS DESY rafael.sosa.ricardo@desy.de
"""

"""
- The first detector will be taken if need as the reference detector.
- In case of normtag_physics use, it has to be put in the last entry position!!

"""

#import sys
import ROOT
import optparse
#from rootpy.plotting import Hist, HistStack, Legend, Canvas
#import rootpy.plotting.root2matplotlib as rplt

#By me
import lumi_compare as lumi

nls=15
excludedFills=[5038,5043,5045,5048,5052,5056,4979,5151,5331,5433]

p = optparse.OptionParser()
usage = "usage: %prog [options] arg1 arg2 ..."
p.add_option("-o", "--outdir", type="string",help="Path to output Dir", dest="outDir", default=".")
p.add_option("-i", "--indir", type="string",help="Path to input Dir", dest="inDir", default=".")
p.add_option("-p", "--physics",action="store_true",help="takes physics normtag as reference in time", dest="physics", default=False)

(options, args) = p.parse_args()

if options.physics==False and len(args)>3:
    print ("Max number of detectors allowed is 3!!")
    quit()

n_files=0
file=[]
detector=[]
physicsDict={}
for det in args:
    try:
        if options.physics and det==args[len(args)-1]:
            phy_file=open(options.inDir +"/"+ det + ".csv")
            physicsDict=lumi.myDictFromCSVFileReadAllNoLabels(phy_file)
        else:
            file.append(open(options.inDir +"/"+ det + ".csv"))
            detector.append(det)
    except:
        print (options.inDir +"/"+ det + ".csv --->>> File not found!! ")
        continue    
    n_files+=1
    print ("input file: " + det)
    
print (len(detector),"detectors founds and opened--->>> ",detector)
if len(detector)<=1:
    print ("Number of detectors not sufficient!! 2 needed at least!")
    quit()

## getting the dicts
dicts=[]

for f in file:
    dicts.append(lumi.myDictFromCSVFileReadAllNoLabels(f))
    

##Setting common ls

keysAll=list(set(dicts[0]).intersection(dicts[1]))
try:
    keys1=list(set(dicts[0]).intersection(dicts[2]))
    keysAll=list(set(keysAll).intersection(keys1))
except:
    pass

if options.physics:
    phyKeys=list(physicsDict)
    phyKeys.sort()
    keysAll=list(set(keysAll).intersection(phyKeys))

keysAll.sort()
#print (len(keysAll))
#keysAll=lumi.excludeFills(keysAll,excludedFills)
#print (len(keysAll))

###Checking allkeys
#for key in keysAll:
#    for d in dicts:
#        temp=d[key]

"""Starting Uncertainty Analysis
"""
#defining output file name
fileName=""
for det in detector:
    fileName+=(det+"_")
fileName=fileName+"Uncetinties.txt"

outputFile=open(options.outDir+ "/" +fileName,'w')

## Integral method:
print ("\n ********** Starting Integral Uncertainty analysis: ******************")

integ,meanInt,sigmaInt,errInt,relErrMean,relErrRef=lumi.getIntegralUncert(keysAll,dicts)
#integ,meanInt,sigmaInt,errInt,relErrMean,relErrRef=lumi.getIntegralUncert2dicts(keysAll,dicts[0],dicts[1])
    
## Max separation method:
print ("\n ********** Starting Max separation Uncertainty analysis: ******************")

solocan_square=ROOT.TCanvas()

if len(dicts)==3:
    #using ratios diff:
    maxDiff,minDiff,meanDiff,meanWDiff,plot_Hdiff,plot_HdiffW=lumi.getMaxRatioDiff(dicts[1],dicts[2],dicts[0],keysAll,nls)
    
    lumi.saveintoCanvas_and_toFile(solocan_square,plot_Hdiff,"",
                                   options.outDir+ "/" + "_histo_ratioDiff.pdf")
    lumi.saveintoCanvas_and_toFile(solocan_square,plot_HdiffW,"",
                                   options.outDir+ "/" + "_histo_ratioDiffW.pdf")
    outputFile.write("********** Starting Max separation Uncertainty analysis: ****************** \n"
                 + "minDiff: "+ str(minDiff) + "\n"
                 + "maxDiff: " + str(maxDiff) +", ("+ str(maxDiff*100) + "%)" +"\n"
                 + "meanWDiff,(%): " + str(meanWDiff) +", ("+ str(meanWDiff*100) + "%)" + "\n \n \n")
else:
    print ("Analysis of the ratios max diff not possible for less than 3 detectors!")

#using true values diff:
print ("\n")
#NmaxDiff,NminDiff=lumi.getMaxDiff(dicts[1],dicts[2],dicts[0],keysAll)
if len(dicts)==3:
    NmaxDiff,NminDiff=lumi.getMaxDiff3dicts(dicts[1],dicts[2],dicts[0],keysAll,nls)
else:
    print ("Analysis of true values max diff not possible for less than 3 detectors!")
#print (NmaxDiff,NminDiff)





outputFile.write("********** Integral Uncertainty analysis: ****************** \n"
                 + "Integral values: "+ str(integ) + "\n"
                 + "mean value: " + str(meanInt) + "\n"
                 + "sigma: "+ str(sigmaInt) + "\n"
                 + "err: "+str(errInt) + "\n"
                 + "rel. to mean err (in %): " + str(relErrMean) + "\n"
                 + "rel. to reference detctor (in %): " + str(relErrRef) + "\n \n \n")

outputFile.close()