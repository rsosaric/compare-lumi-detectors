#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 18 16:01:28 2018

@author: sosarica
"""

import ROOT
import optparse
#from rootpy.plotting import Hist, HistStack, Legend, Canvas
#import rootpy.plotting.root2matplotlib as rplt

#By me
import lumi_compare as lumi
import math

ROOT.gROOT.SetBatch(ROOT.kTRUE)

excludedFills=[]
nbins=40
min_val=0.5
max_val=1.5

p = optparse.OptionParser()
usage = "usage: %prog [options] arg1 arg2 ..."
p.add_option("-o", "--outdir", type="string",help="Path to output Dir", dest="outDir", default=".")
p.add_option("-i", "--indir", type="string",help="Path to input Dir", dest="inDir", default=".")

(options, args) = p.parse_args()

detector1label=args[0]
detector2label=args[1]
    
filename1=detector1label + ".csv"
filename2=detector2label + ".csv"
file_path1=options.inDir +"/"+ filename1
file_path2=options.inDir +"/"+ filename2
print ("input file 1: " + file_path1)
print ("input file 2: " + file_path2)
    
#open files
try:
    file1=open(file_path1)
    file2=open(file_path2)
except:
    print ("input files cant't be opened!!")
    quit()
        
labelRatio=detector1label+"/"+detector2label
    
#info
print ("******* Detectors in analysis: ",detector1label ," and ", detector2label,"*******") 

#Create dicts from csv file
dict1=lumi.myDictFromVdmCSVFile(file1)
dict2=lumi.myDictFromVdmCSVFile(file2)

file1.close()
file2.close()

#Select only the common lumi sections for both detectors
overlapKeys=list(set(dict1).intersection(dict2))


#Sorting to evoid disorder in plots
overlapKeys.sort()

ratioHist=ROOT.TH1F(labelRatio,";ratio "+labelRatio+";",nbins,min_val,max_val)
ratioplotnoempty=ROOT.TGraph()
plotVsTime1=ROOT.TGraph()
plotVsTime2=ROOT.TGraph()
plotsVsTime = ROOT.TMultiGraph()

iBin=0
iBinx=0
for key in overlapKeys:
    plotVsTime1.SetPoint(iBinx,dict1[key][0],dict1[key][1])
    plotVsTime2.SetPoint(iBinx,dict2[key][0],dict2[key][1])
    iBinx+=1
    try:
        if dict1[key][4]=='STABLE BEAMS':
            div=dict1[key][1]/dict2[key][1]
            ratioHist.Fill(div)
            ratioplotnoempty.SetPoint(iBin,iBin,div)
            iBin+=1
    except:
        continue


ratioplotnoempty.GetYaxis().SetRangeUser(min_val, max_val)
ratioplotnoempty.SetLineColor(4)
plotVsTime1.GetYaxis().SetRangeUser(0., 4.)
plotVsTime1.SetLineColor(4)
plotVsTime2.GetYaxis().SetRangeUser(0., 4.)
plotVsTime2.SetLineColor(3)


##Multigraph
legVsTime = ROOT.TLegend (.9,.7,.75,.9)
legVsTime.SetFillColor(0)
legVsTime.AddEntry(plotVsTime1,detector1label,"l")       
legVsTime.AddEntry(plotVsTime2,detector2label,"l")
plotsVsTime.Add(plotVsTime1,"L")
plotsVsTime.Add(plotVsTime2,"L")
plotsVsTime.GetYaxis().SetRangeUser(0., 4.)

solocan=ROOT.TCanvas("solocan","solocan",900,300)
solocan_square=ROOT.TCanvas()

lumi.saveintoCanvas_and_toFile(solocan_square,ratioHist,"HIST",
                                   options.outDir+ "/" + detector1label + "-" + detector2label +"_ratioHist.pdf")
lumi.saveintoCanvas_and_toFile(solocan,ratioplotnoempty,"AL",
                                   options.outDir+ "/" + detector1label + "-" + detector2label +"_ratioplotVsTime.pdf")
lumi.saveintoCanvas_and_toFile(solocan,plotVsTime1,"AL",
                                   options.outDir+ "/" + detector1label + "-" + detector2label +"_plotVsTime1.pdf")
lumi.saveintoCanvas_and_toFile(solocan,plotVsTime2,"AL",
                                   options.outDir+ "/" + detector1label + "-" + detector2label +"_plotVsTime2.pdf")
lumi.saveintoCanvas_and_toFile_withLeg(solocan,plotsVsTime,legVsTime,"AL",
                                   options.outDir+ "/" +detector1label+"_"+detector2label+"_plotsVsTime.pdf")